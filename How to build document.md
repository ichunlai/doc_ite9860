# 1. How to build document?

> This document is written in [Markdown](https://www.markdownguide.org/getting-started) language. It is recommended to use [Typora](https://typora.io/) to read this file.

All the documents is written in reStructuredText language. So if you want to generate the pdf/word format documents, you need to [Installing Sphinx](http://www.sphinx-doc.org/en/stable/install.html).

The following steps describe how to install the Sphinx.

## 1.1. Install Python

Download the installer file from https://www.python.org/downloads/.

After installation, you better add the Python executable directories to the environment variable **PATH**.

Now run the Command Prompt. After command prompt window appear, type `python` and Enter. If the Python installation was successful, the installed Python version is printed, and you are greeted by the
prompt `>>>`. Type Ctrl+Z and Enter to quit.

## 1.2 Installing Sphinx with pip

type this line in the command prompt:

```
C:\> pip install sphinx
```

After installation, type `sphinx-build -h` on the command prompt. If everything worked fine, you will get a Sphinx version number and a list of options for this command.

Sphinx comes with a script called `sphinx-quickstart` that sets up a source directory and creates a default `conf.py` with the most useful configuration values from a few questions it asks you.

```
sphinx-quickstart <project_name>
```

If you want to write the document in Traditional Chinese or Simplified Chinese,  When you see the following prompt:

```
For a list of supported codes, see
http://sphinx-doc.org/config.html#confval-language.
> Project language [en]:
```

Please choose `zh_TW`  or `zh_CN` 。

The execution of this script create the files `Makefile` (for Unix-like OS) and `make.bat` (for Windows OS) which make building the documentation easy. With them you only need to run:

```
make html
```

To generate a working html version of your reStructuredText source.

## 1.3 Using Markdown in a Sphinx project

It is possible to use Markdown and reStructuredText in the same Sphinx project.

To do this, you need to install `recommonmark`:

```
pip install recommonmark
```

Then edit your `conf.py` and add:

```python
# for Sphinx-1.4 or newer
extensions = ['recommonmark']
```

If you want to use Markdown files with extensions other than `.md`, adjust the [`source_suffix`](https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-source_suffix) variable. The following example configures Sphinx to parse all files with the extensions `.md` and `.txt` as Markdown:

```python
source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'markdown',
    '.md': 'markdown',
}
```

## 1.4 Install RTD theme

It is recommended to use the RTD theme for html output. 

```undefined
pip install sphinx_rtd_theme
```

 To use the theme in your Sphinx project, you will need to add the following to your `conf.py` file:

```
import sphinx_rtd_theme

extensions = [
    ...
    "sphinx_rtd_theme",
]

html_theme = "sphinx_rtd_theme"
```

For more information read the full documentation on [installing the theme](https://sphinx-rtd-theme.readthedocs.io/en/latest/installing.html)

## 1.5 Install tikz extension

1. Install the Tikz Sphinx extension via [PyPl](https://pypi.org/project/sphinxcontrib-tikz/).

   ```
   pip install sphinxcontrib-tikz
   ```

2.  add the following to your `conf.py` file:

   ```
   extensions = [
       ...
       "sphinxcontrib.tikz"
   ]
   ```

   

For more information, see https://github.com/sphinx-contrib/tikz.

## 1.6 Rendering PDF from reStructuredText

Sphinx is using builders to produce output. The builder’s "name" for PDF is either latex (best output) or rst2pdf.

PDF output with rst2pdf is by far not as good as when using LaTeX. So we choose to use latex.

### Installing LaTeX on MS Windows

Download and run the [MiKTeX](http://www.miktex.org/download) installer to setup a basic TeX/LaTeX system on your computer. You can read the section [Installing MiKTeX](http://docs.miktex.org/2.9/manual/installing.html) in the MiKTeX manual, if you want to learn all the details.

The recommended download is the MiKTeX Basic Installer and implies that the installer will install missing packages on-the-fly, by fetching them online.

Alternatively, you may choose to install a full-fledge version of MiKTeX (MiKTeX Net Installer under "Other Downloads" on the MiKTeX's website). But keep in mind that this results in a *lot larger* footprint.

At step 3, the installer will ask you whether missing packages should be installed on-the-fly. We **highly** recommend you to let MiKTeX installing those missing packages without asking you.

When you have installed MiKTeX, it is recommended that you run the update wizard in order to get the latest updates.

For Windows users, please add  the MiKTeX executable directories (ex.`C:\Program Files\MiKTeX 2.9\miktex\bin\x64`) to  the PATH environment variable.

### Add Chinese Support

Edit the `conf.py` and append the following command at the end of the file:

```python
latex_elements = {
    # The paper size ('letterpaper' or 'a4paper').
    'papersize': 'a4paper',

    # The font size ('10pt', '11pt' or '12pt').
    # 'pointsize': '10pt',

    'classoptions': ',english',
    'inputenc': '',
    'utf8extra': '',

    # Additional stuff for the LaTeX preamble.
    'preamble': '''
\\usepackage{xeCJK}
\setCJKmainfont[BoldFont=Microsoft JhengHei Bold, ItalicFont=Microsoft JhengHei]{Microsoft JhengHei}    % 設定中日韓字型
\setCJKmonofont[Scale=0.8]{Courier New} % 設定中日韓等寬字型
\XeTeXlinebreaklocale "zh"              % 設定斷行演算法為中文
\XeTeXlinebreakskip = 0pt plus 1pt      % 設定中文字距與英文字距
''',

    # Latex figure (float) alignment
    # 'figure_align': 'htbp',
}

```

finally, remember to save the `conf.py` file in UTF-8 format.

Besides, all the `.rst` files must be in UTF-8 format.

### Output Latex file

```
make latex
```

### Output PDF file

```
make latexpdf
```



教學請見 https://sublime-and-sphinx-guide.readthedocs.io/en/latest/tables.html

https://rest-sphinx-memo.readthedocs.io/en/latest/ReST.html

https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/IncludingFiles.html