.. _nandflash_support_list:

NAND Flash 支援列表
===============================

NAND flash 為 IT986x 的外接儲存設備。由於現今規格不一，使用前必須查看 AC/DC CHARACTERISTICS 的電氣特性是否相符，以及確認 NAND CMD 是否與 Mask ROM 和 NOR driver 的定義一致。

使用限制
----------------------------------------------------------------

- 使用 AXISPI protocol，NAND clock frequency 需要支援至 60Mhz 以上。
- 一般 SPI0/SPI1 做為 I/O 橋接介面，其頻率需支援至 20MHZ 以上。

支援列表
----------------------------------------------------------------

GIGADEVICE
^^^^^^^^^^^^^

- GD_5F1GQ4UB(停產)
- GD_5F2GQ4UB(停產)
- GD_5F1GQ4UC(停產)
- GD_5F2GQ4UC(停產)
- GD_5F4GQ4UC(停產)
- GD_5F1GQ4UE
- GD_5F2GQ4UE
- GD_5F1GQ4UF
- GD_5F2GQ4UF
- GD_5F4GQ4UF

MXIC
^^^^^^^^^^^^^

- MX35LF1GE4AB
- MX35LF2GE4AB

PARAGON
^^^^^^^^^^^^^

- PN26G01AWSIUG

XTX
^^^^^^^^^^^^^

- XT26G01A
- XT26G02A
- XT26G04A

WINBOND
^^^^^^^^^^^^^

- W25N01GV

ATO
^^^^^^^^^^^^^

- ATO25D1GA

TOSHIBA
^^^^^^^^^^^^^

- TC58CVG2S0HRAIG

Dosilicon
^^^^^^^^^^^^^

- DS35Q1GA


.. raw:: latex

    \newpage
