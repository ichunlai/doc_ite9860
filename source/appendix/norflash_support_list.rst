.. _norflash_support_list:

NOR Flash 支援列表
===============================

NOR flash 為 IT986x 的外接儲存設備。由於現今規格不一，使用前必須查看 AC/DC CHARACTERISTICS 的電氣特性是否相符，以及確認 NOR CMD 是否與 Mask ROM 和 NOR driver 的定義一致。

使用限制
----------------------------------------------------------------

- 使用 AXISPI protocol，NOR clock frequency 需要支援至 60Mhz 以上。
- EX4B CMD 需要是 Mask ROM 所定義的 0xE9
- 只能 support Exit 4-byte command 為 0xE9 的 NOR。

支援列表
----------------------------------------------------------------

我們在 AXISPI_80Mhz 環境下，整理出一份 serial mode 和 QUAD mode 驗證皆為正確的 NOR Support List，這些 NOR 可以支援 IT986x 定義的相關功能。

Macronix
^^^^^^^^^^^^^

- MX_25L25645G

Boya
^^^^^^^^^^^^^

- BY_25Q64AS
- BY_25Q128AS

Zbit
^^^^^^^^^^^^^

- ZB_25VQ64
- ZB_25VQ128

Dosilicon
^^^^^^^^^^^^^

- DS_25Q64M
- DS_25Q4AM

Winbond
^^^^^^^^^^^^^

- WIN_W25Q32JV
- WIN_W25Q64JV
- WIN_W25Q128JV

GigaDevice
^^^^^^^^^^^^^

- GD_GD25Q128C
- GD_GD25Q256D

FORESEE
^^^^^^^^^^^^^

- FS_25Q128F2TFI


.. raw:: latex

    \newpage
