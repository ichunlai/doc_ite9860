.. _usb_spi_tool:

usb_spi_tool - 讀寫暫存器/讀寫記憶體/燒錄 Flash
===================================================

usb_spi_tool 是個在 Windows 下運行的多功能應用程式，可透過 USB to SPI 轉換板來完成以下工作：

- 讀寫晶片上暫存器的數值
- 讀寫晶片上記憶體的內容
- 燒錄 NOR flash
- 燒錄 NAND flash


在使用usb_spi_tool之前，您必須確保：

- USB to SPI 轉換板已正確安裝。

  - 若您尚未將USB to SPI轉換板與PC連接，請參考 :ref:`usb2spi_connect2pc` 。
  - 若您尚未將USB to SPI轉換板與EVB連接，請參考 :ref:`usb2spi_connect2board` 。
  - 若您尚未安裝USB to SPI轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

- EVB已經切換為Co-operative Mode。

  .. note::

     若您無法確定EVB是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

.. _usb_spi_tool_read_reg:

如何讀寫晶片上暫存器的數值
-------------------------------

請先確保：

- USB to SPI 轉換板已正確安裝。

  - 若您尚未將USB to SPI轉換板與PC連接，請參考 :ref:`usb2spi_connect2pc` 。
  - 若您尚未將USB to SPI轉換板與EVB連接，請參考 :ref:`usb2spi_connect2board` 。
  - 若您尚未安裝USB to SPI轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

在確認完硬體連接一切正常之後，就可以開始執行 usb_spi_tool 以讀寫晶片上暫存器的數值。方法分為兩種：

1. Direct address mode

   在 Direct address mode 下，必須輸入要讀寫的暫存器的完整位址。

2. Indirect address mode

   在 Indrect address mode 下，必須針對要讀寫的暫存器選擇正確的暫存器所屬裝置類型，在位址欄位內只要填入 offset 即可，無須填入暫存器的完整位址。

底下分別針對如何使用 Direct address mode 和 Indirect address mode 讀寫暫存器進行詳細說明，並以讀取 chip id (暫存器位址 0xD8000004)為例。

Direct address mode
^^^^^^^^^^^^^^^^^^^^^^^^^

使用 Direct address mode 來讀寫暫存器的詳細步驟如下：

1. 執行 ``<sdk_root>/tool/bin/usb_spi_tool.exe`` 。
2. 取消勾選 :menuselection:`Register --> Indirect address` 。
3. 直接在 :menuselection:`Register --> Address` 欄位輸入要讀取的完整暫存器位址。接著點擊 "Read" 按鈕，即可在 "Data" 欄位看到讀回來的數值。

   下圖展示透過 ``usb_spi_tool.exe`` 讀取 IT986x 暫存器地址 0xD8000004 的數值。

   .. image:: /_static/usb2spi_read_write_reg.png

4. 要對暫存器寫值，則在 :menuselection:`Register --> Address` 欄位輸入要寫入數值的完整暫存器位址。在 :menuselection:`Register --> Data` 欄位輸入要寫入的值。接著點擊 "Write" 按鈕。若成功寫入是不會有任何訊息的，請試著再次讀回該暫存器的值來確認是否成功寫入。

Indirect address mode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

使用 Indirect address mode 來讀寫暫存器的詳細步驟如下：

1. 執行 ``<sdk_root>/tool/bin/usb_spi_tool.exe`` 。
2. 勾選 :menuselection:`Register --> Indirect address` ，並選擇暫存器所屬裝置類型。

   譬如我們要讀取的 chip id 暫存器屬於 General Register 裝置類型，則應選擇 GeneralReg 類型。選擇裝置類型後，會跳出提示 General Register 裝置類型的暫存器基底位址為 0xD8000000。

   .. image:: /_static/usb2spi_select_dev_type.png

3. 在 :menuselection:`Register --> Address` 欄位填入偏移地址。

   譬如我們要讀取的 chip id 暫存器完整位址為 0xD8000004，因為我們已經在前一步選擇期裝置類型為 General Register (基底地址為 0xD8000000)，因此我們只需要在 :menuselection:`Register --> Address` 欄位填入 4 即可。

   .. image:: /_static/usb2spi_read_write_reg_indirect.png

.. _how_to_load_init_script:

如何手動載入initial script
--------------------------------------

若想要讀寫記憶體，或進行記憶體讀寫測試等需要存取記憶體的動作，都必須先讓晶片內的韌體正確啟動，或先透過 usb_spi_tool 軟體手動載入 initial script 以正確初始化記憶體所需的相關 clock。

.. note::

   Initial script 記載一堆暫存器位址和暫存器數值的配對，其主要作用是用來正確初始化晶片所需的 clock。

.. caution::

   請勿在晶片內韌體已啟動的狀態下手動載入 initial script。這可能會產生任何異常行為。

要透過usb_spi_tool手動載入initial script，請先確保：

- USB to SPI 轉換板已正確安裝。

  - 若您尚未將USB to SPI轉換板與PC連接，請參考 :ref:`usb2spi_connect2pc` 。
  - 若您尚未將USB to SPI轉換板與EVB連接，請參考 :ref:`usb2spi_connect2board` 。
  - 若您尚未安裝USB to SPI轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

- EVB已經切換為Co-operative Mode。

  .. note::

     若您無法確定EVB是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

在確認完硬體連接一切正常之後，就可以開始執行 usb_spi_tool 以讀寫晶片上記憶體的內容。步驟如下：

1. 執行 ``<sdk_root>/tool/bin/usb_spi_tool.exe``。
2. 切換到 Host 標籤頁。
3. 點擊 :menuselection:`Script --> Load Script` 按鈕。將彈出檔案開啟對話框。

   .. image:: /_static/usb2spi_load_script.png

4. 選擇您想要載入的 initial script (通常放置在 ``<sdk_root>/sdk/target/debug`` 路徑下) 並點擊 "Open"。

   .. image:: /_static/usb2spi_load_script_dialog.png

   若成功或出現 ``Load Script Finished`` 訊息。

如何進行晶片內建記憶體自我存取測試
------------------------------------

IT986x晶片內建對記憶體存取的自我測試功能。可透過usb_spi_tool利用此功能測試晶片內的CPU以及週邊模組對記憶體的存取是否正常。

要進行此測試，請先確保：

- USB to SPI 轉換板已正確安裝。

  - 若您尚未將USB to SPI轉換板與PC連接，請參考 :ref:`usb2spi_connect2pc` 。
  - 若您尚未將USB to SPI轉換板與EVB連接，請參考 :ref:`usb2spi_connect2board` 。
  - 若您尚未安裝USB to SPI轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

- 晶片內的韌體未啟動，並且已載入initial script。

  .. note::

      若您不知道如何載入 initial script，請參考 :ref:`how_to_load_init_script` 。

在確認完硬體連接一切正常之後，就可以開始執行 usb_spi_tool 以讀寫晶片上記憶體的內容。步驟如下：

1. 執行 ``<sdk_root>/tool/bin/usb_spi_tool.exe`` 。
2. 根據晶片內建記憶體容量在 :menuselection:`Host --> Memory test --> End` 欄位內填入適當的數值。底下列出不同記憶體大小應填入的數值。

   .. flat-table::
      :header-rows: 1

      * - 記憶體大小
        - 應填入的數值
      * - 16MB
        - 7FF
      * - 32MB
        - FFF
      * - 64MB
        - 1FFF
      * - 128MB
        - 3FFF

   其公式為：

   .. math::

       \mathtt{應填入的數值} = \frac{\mathtt{記憶體大小}}{\mathtt{0x2000}} - 1

3. 點擊 "Memory Test" 按鈕開始執行測試。

   .. image:: /_static/usb2spi_mem_test.png

   若測試通過將彈出 ``Memory test pass`` 訊息。若測試失敗將彈出 ``Memory test fail`` 訊息。

在運行時期存取GPIO狀態
-----------------------------

usb_spi_tool 提供從 PC 存取 GPIO 狀態的功能。可執行的功能如下：

1. 切換某 GPIO pin 的複用模式。
2. 強迫某 GPIO pin 將複用模式切換為 GPIO output 模式，並輸出高電位。
3. 強迫某 GPIO pin 將複用模式切換為 GPIO output 模式，並輸出低電位。
4. 強迫某 GPIO pin 將複用模式切換為 GPIO input 模式，並獲取該 GPIO pin 的高低電位狀態。

底下將針對如何透過 usb_spi_tool 執行這些功能進行講解。要執行這些功能之前，請先確保：

- USB to SPI 轉換板已正確安裝。

  - 若您尚未將USB to SPI轉換板與PC連接，請參考 :ref:`usb2spi_connect2pc` 。
  - 若您尚未將USB to SPI轉換板與EVB連接，請參考 :ref:`usb2spi_connect2board` 。
  - 若您尚未安裝USB to SPI轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

在確認完硬體連接一切正常之後，就可以開始執行上述四項功能。底下分別針對各個功能做講解。

切換某 GPIO pin 的複用模式
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

步驟如下：

1. 執行 ``<sdk_root>/tool/bin/usb_spi_tool.exe`` 。
2. 在 :menuselection:`Host --> GPIO --> GPIO` 欄位內填入欲操作的 GPIO pin 的編號。
3. 在 :menuselection:`HOST --> GPIO --> MODE` 欄位內填入欲切換的複用模式編號。

   .. image:: /_static/usb2spi_switch_gpio.png


.. raw:: latex

    \newpage

強迫某 GPIO pin 將複用模式切換為 GPIO output 模式，並輸出高電位
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

步驟如下：

1. 執行 ``<sdk_root>/tool/bin/usb_spi_tool.exe`` 。
2. 在 :menuselection:`Host --> GPIO --> GPIO` 欄位內填入欲操作的 GPIO pin 的編號。
3. 點擊 :menuselection:`HOST --> GPIO --> Output 1` 按鈕。

   .. image:: /_static/usb2spi_switch_gpio_output_high.png

.. raw:: latex

    \newpage

強迫某 GPIO pin 將複用模式切換為 GPIO output 模式，並輸出低電位
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

步驟如下：

1. 執行 ``<sdk_root>/tool/bin/usb_spi_tool.exe`` 。
2. 在 :menuselection:`Host --> GPIO --> GPIO` 欄位內填入欲操作的 GPIO pin 的編號。
3. 點擊 :menuselection:`HOST --> GPIO --> Output 0` 按鈕。

   .. image:: /_static/usb2spi_switch_gpio_output_low.png

.. raw:: latex

    \newpage

強迫某 GPIO pin 將複用模式切換為 GPIO input 模式，並獲取該 GPIO pin 的高低電位狀態
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

步驟如下：

1. 執行 ``<sdk_root>/tool/bin/usb_spi_tool.exe`` 。
2. 在 :menuselection:`Host --> GPIO --> GPIO` 欄位內填入欲操作的 GPIO pin 的編號。
3. 點擊 :menuselection:`HOST --> GPIO --> Input Read` 按鈕。

   .. image:: /_static/usb2spi_switch_gpio_input.png

在運行時期控制LCD輸出畫面
-------------------------------

usb_spi_tool 提供了可以在 LCD controller 正確啟動的狀態下，動態改變 LCD 輸出畫面的功能。此外，也提供用戶可在運行時期將 LCD 輸出畫面擷取為圖片的功能。這些功能主要是讓用戶方便驗證其 LCD controller 是否設定正確，以及無須撰寫程式便可完整截圖。

要執行這些功能之前，請先確保：

- USB to SPI 轉換板已正確安裝。

  - 若您尚未將USB to SPI轉換板與PC連接，請參考 :ref:`usb2spi_connect2pc` 。
  - 若您尚未將USB to SPI轉換板與EVB連接，請參考 :ref:`usb2spi_connect2board` 。
  - 若您尚未安裝USB to SPI轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

在確認完硬體連接一切正常之後，我們將針對如何驗證所選的 lcd script 的正確性以及如何截取LCD輸出畫面進行描述。

如何驗證所選的lcd script的正確性
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

這裡我們將使用 IT9866 EVB 和 TL068HWXH08 面板為例。首先，我們要準備好晶片對應的 initial script 以及面板對應的 lcd script。

- IT9866 EVB 對應的 initial script 為 ``IT9860_360Mhz_DDR2_360Mhz.txt`` ，位於 ``<sdk_root>/sdk/target/debug`` 目錄下。
- TL068HWXH08 面板所對應的LCD script是 ``IT9860_MIPI_TL068HWXH08_EK79030_480x1280_4LANE_byteclk53.txt`` ，位於 ``<sdk_root>/sdk/target/lcd`` 目錄下。

  .. note::

     此處的 LCD script 並未啟用背光，因此還得根據您的電路設計加入啟用背光的設定。若您的電路是採用 PWM 點亮背光的話，請在使用 LCD tool 產生 LCD script 時，加入適當的 GPIO 設定，以使 PWM 使用的 pin 保持在 high 的狀態。

1. 點擊 "Host" 以切換到 Host 標籤頁。
2. 參考 :ref:`how_to_load_init_script` 載入 initial script 和 LCD script。務必先載入 initial script 再載入 LCD script。
3. 點擊 "LCD" 以切換到 LCD 標籤頁。
4. 選擇 :menuselection:`Function --> Load Image` 選項，在 :menuselection:`Function --> File` 欄位指定要載入的圖片的檔案路徑。

   .. image:: /_static/usb2spi_lcd_load_img.png

   .. note::

      載入圖片時會顯示 Width / Height / Pitch (圖片解析度需與 LCD Panel 的解析度一致)。需根據 LCD 設定正確選擇 "LCD Data Format" 選項。

5.	點擊 "Exec" 按鍵，將圖片載入至晶片內部 LCD frame buffer 對應的記憶體。
6.	圖片載入完成後，將彈出 "Load Script Finished" 訊息視窗。
7.	完成後若一切設定正常，應可在平台的 LCD 螢幕上顯示出剛載入的圖片。

如何截取 LCD 輸出畫面
^^^^^^^^^^^^^^^^^^^^^^

要獲得 LCD 輸出畫面的截圖，得先知道 LCD 的五個數值，分別是：

1. LCD 的寬度：可從暫存器 0xD0000008 的 0 ~ 11 位元獲得。
2. LCD 的高度：可從暫存器 0xD0000008 的 16 ~ 27 位元獲得。
3. LCD 的 pitch：可從暫存器 0xD000000C 獲得。
4. LCD frame buffer 的記憶體位址：可從暫存器 0xD0000010、0xD0000014、0xD0000018 獲得。這三個暫存器位址分別對應 frame buffer A 的位址、frame buffer B 的位址、frame buffer C 的位址。
5. LCD frame buffer 的 color format。可從暫存器 0xD0000004 的第 12 ~ 14 位元獲得。其值與 color format 的對應關係如下：

   .. flat-table::
      :header-rows: 1

      * - 0xD0000004 [14:12]
        - Color Format
      * - 0
        - RGB565
      * - 1
        - ARGB1555
      * - 2
        - ARGB4444
      * - 3
        - ARGB8888
      * - Others
        - YUV

請先啟動系統，並在可以從 LCD 看到畫面的狀況下，按照 :ref:`usb_spi_tool_read_reg` 描述的方法取得這四個數值。接著請按這以下步驟操作：

1. 執行 ``<sdk_root>/tool/bin/usb_spi_tool.exe`` 。
2. 點擊 :menuselection:`LCD --> Dump Memroy` 。
3. 在 :menuselection:`LCD --> File` 欄位填入要 dump 的檔案路徑，副檔名記得設定為 ``.bmp``。
4. 在 :menuselection:`LCD --> Setting --> Width` 欄位填入 LCD 的寬度。
5. 在 :menuselection:`LCD --> Setting --> Height` 欄位填入 LCD 的高度。
6. 在 :menuselection:`LCD --> Setting --> Pitch` 欄位填入 LCD 的 pitch。
7. 在 :menuselection:`LCD --> Setting --> Base` 欄位填入 frame buffer 的位址(若不知道要填哪個 frame buffer 的位址，可以直接填 frame buffer A 的位址)。
8. :menuselection:`LCD --> LCD Data Format` 欄位目前只能選擇 RGB565。因此若您從暫存器獲得的 LCD frame buffer 的 color format 不是 RGB565，轉存的圖片檔案內容可能會有問題。

   .. image:: /_static/usb2spi_lcd_dump_img.png

9. 點擊 "Exec" 按鈕。

   .. image:: /_static/usb2spi_lcd_dumping_img.png

   若執行完成會顯示 ``Dump Image Finished`` 訊息。

.. _usb_spi_tool_rw_memory:

讀寫晶片上記憶體的內容
---------------------------

請先確保：

- USB to SPI 轉換板已正確安裝。

  - 若您尚未將USB to SPI轉換板與PC連接，請參考 :ref:`usb2spi_connect2pc` 。
  - 若您尚未將USB to SPI轉換板與EVB連接，請參考 :ref:`usb2spi_connect2board` 。
  - 若您尚未安裝USB to SPI轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

- 晶片內的韌體未啟動，並且已載入initial script。

  .. note::

      若您不知道如何載入 initial script，請參考 :ref:`how_to_load_init_script` 。

在確認完硬體連接一切正常之後，就可以開始執行 usb_spi_tool 以讀寫晶片上記憶體的內容。步驟如下：

1. 執行 ``<sdk_root>/tool/bin/usb_spi_tool.exe`` 。
2. 若您只需要讀寫大小為 32 位元以內的範圍：

   - 直接在 :menuselection:`Register --> Address` 欄位輸入要讀取的記憶體位址。接著點擊 "Read" 按鈕，即可在 "Data" 欄位看到讀回來的 32 位元數值。
   - 要對記憶體寫入 32 位元數值，請在 :menuselection:`Register --> Address` 欄位輸入要寫入值的記憶體位址。在 :menuselection:`Register --> Data` 欄位輸入要寫入的值。接著點擊 "Write" 按鈕。若成功寫入是不會有任何訊息的，請試著再次讀回該記憶體的值來確認是否成功寫入。

     .. image:: /_static/usb2spi_read_write_mem.png

3. 若您需要讀寫大小超過 32 位元的範圍：

   - 若您要讀取大小超過32位元範圍的記憶體內容。

      1. 點擊 "Memory" 切換到 Memory 標籤頁。
      2. 在 :menuselection:`Dump Memory --> Address` 欄位輸入要讀取的記憶體起始位址。
      3. 在 :menuselection:`Dump Memory --> Size` 欄位輸入要讀取的大小(以 byte 為單位)。
      4. 在 :menuselection:`Dump Memory --> File` 欄位輸入讀取出來的內容要放到哪個檔案的檔案路徑。
      5. 點擊 "Exec"。若成功會出現 ``Dump Memory Finished`` 訊息。

         .. image:: /_static/usb2spi_memory_dump.png

      6. 您可以使用任何 Hex Editor，開啟該檔案以查看記憶體內容。

         .. image:: /_static/usb2spi_use_hex_tool_to_view_memory_data.png

   - 若您要將大小超過 32 位元的資料寫入記憶體。

      1. 點擊 "Memory" 切換到 Memory 分頁。
      2. 在 :menuselection:`Dump Memory --> File` 欄位輸入包含要寫入記憶體的資料的檔案的檔案路徑。
      3. 在 :menuselection:`Dump Memory --> Address` 欄位輸入要寫入的記憶體起始位址。
      4. 點擊 "Exec" 。若成功會出現 ``Load Memory Finished`` 訊息。

         .. image:: /_static/usb2spi_memroy_write.png

.. _write_image_to_nandflash:

燒錄映像檔至NAND flash
---------------------------------

請先確保：

- USB to SPI 轉換板已正確安裝。

  - 若您尚未將USB to SPI轉換板與PC連接，請參考 :ref:`usb2spi_connect2pc` 。
  - 若您尚未將USB to SPI轉換板與EVB連接，請參考 :ref:`usb2spi_connect2board` 。
  - 若您尚未安裝USB to SPI轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

- EVB 已經切換為 Co-operative Mode。

  .. note::

     若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

在確認完硬體連接一切正常之後，就可以開始執行燒錄軟體。步驟如下：

1. 執行 ``<sdk_root>/tool/bin/usb_spi_tool.exe``。

   .. image:: /_static/usb2spi_software_init.png

   .. note::

      若 USB to SPI 轉換板與 EVB 或 PC 之間的連接不正確，會出現以下讀取不到 chip id 的畫面：

      .. image:: /_static/usb2spi_connect_fail.png

2. 切換到 SPI Writer 標籤頁。

   .. image:: /_static/usb2spi_spi_writer_tag.png

3. 將 "SPI MODE" 方框內的選項從 "NOR" 切換到 "NAND"。
4. 點擊 "GET ID" 按鍵，確認可以正常讀取和識別 NAND flash 資訊。

   .. image:: /_static/usb2spi_spiwriter_nandflash.png

5. 選擇欲燒錄韌體的bootloader檔案檔 - ``kproc.sys`` (通常位於 ``<sdk_root>/openrtos/<project_name>/project/bootloader`` )。選定後，點擊 "Burn" 開始燒錄。
6. 燒錄完成後會顯示 ``Spi burn Success!!`` 訊息。

   .. image:: /_static/usb2spi_write_norflash_success.png

7. 關閉電源。
8. 將實際要燒錄的韌體的 PKG 檔放到 USB 隨身碟內，插入裝置上的 USB 槽。開機等待更新完成。

.. _dump_norflash:

將NOR flash的內容傾印出來
------------------------------

請先確保：

- USB to SPI 轉換板已正確安裝。

  - 若您尚未將USB to SPI轉換板與PC連接，請參考 :ref:`usb2spi_connect2pc` 。
  - 若您尚未將USB to SPI轉換板與EVB連接，請參考 :ref:`usb2spi_connect2board` 。
  - 若您尚未安裝USB to SPI轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

- EVB 已經切換為 Co-operative Mode。

  .. note::

     若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

在確認完硬體連接一切正常之後，就可以開始執行 usb_spi_tool 以將 NOR flash 的內容傾印出來。步驟如下：

1. 執行 ``<sdk_root>/tool/bin/usb_spi_tool.exe``。

   .. image:: /_static/usb2spi_software_init.png

   .. note::

      若 USB to SPI 轉換板與 EVB 或 PC 之間的連接不正確，會出現以下讀取不到 chip id 的畫面：

      .. image:: /_static/usb2spi_connect_fail.png

2. 切換到 SPI Writer 標籤頁。

   .. image:: /_static/usb2spi_spi_writer_tag.png

3. 點擊 "GET ID" 按鍵，確認可以正常讀取和識別 NOR flash 資訊。

   .. image:: /_static/usb2spi_get_nor_info.png

4. 選擇欲存放傾印內容的檔案路徑。

5. 在 "Dump Address" 欄位和 "Dump Size" 欄位填入要讀取的 NOR flash 的起始位址和大小。選定後，點擊 "Dump" 開始傾印內容。

   .. image:: /_static/usb2spi_spiwriter_dump_norflash.png

6. 完成後會顯示 ``Dump Finished`` 訊息。

   .. image:: /_static/usb2spi_spiwriter_dump_norflash_success.png

.. _dump_nandflash:

將 NAND flash 的內容傾印出來
----------------------------------


- USB to SPI 轉換板已正確安裝。

  - 若您尚未將USB to SPI轉換板與PC連接，請參考 :ref:`usb2spi_connect2pc` 。
  - 若您尚未將USB to SPI轉換板與EVB連接，請參考 :ref:`usb2spi_connect2board` 。
  - 若您尚未安裝USB to SPI轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

- EVB 已經切換為 Co-operative Mode。

  .. note::

     若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

在確認完硬體連接一切正常之後，就可以開始執行 usb_spi_tool 以將 NAND flash 的內容傾印出來。步驟如下：

1. 執行 ``<sdk_root>/tool/bin/usb_spi_tool.exe``。

   .. image:: /_static/usb2spi_software_init.png

   .. note::

      若 USB to SPI 轉換板與 EVB 或 PC 之間的連接不正確，會出現以下讀取不到 chip id 的畫面：

      .. image:: /_static/usb2spi_connect_fail.png

2. 切換到 SPI Writer 標籤頁。

   .. image:: /_static/usb2spi_spi_writer_tag.png

3. 將 "SPI MODE" 方框內的選項從 "NOR" 切換到 "NAND"。
4. 點擊 "GET ID" 按鍵，確認可以正常讀取和識別 NAND flash 資訊。

   .. image:: /_static/usb2spi_spiwriter_nandflash.png

5. 選擇欲存放傾印內容的檔案路徑。
6. 在 "Dump Address" 欄位和 "Dump Size" 欄位填入要讀取的 NAND flash 的起始位址和大小。選定後，點擊 "Dump" 開始傾印內容。

   .. image:: /_static/usb2spi_spiwriter_dump_nandflash.png

7. 完成後會顯示 ``Dump Finished`` 訊息。

   .. image:: /_static/usb2spi_spiwriter_dump_norflash_success.png
