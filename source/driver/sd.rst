.. _sd:

SD/SDIO
===========

ITE 平台提供兩組 SD 控制器，兩組皆可支援 SD 卡、eMMC 卡及 SDIO wifi 模組。視產品應用兩組 SD 控制器可能共用部份 pin 腳，也可能 pin 腳完全獨立。

.. note::

    SD 模組僅提供 library 不提供原始程式碼。

SD/eMMC 卡皆透過檔案系統來存取，不開放 API直接控制。

目前 SDIO 介面支援 wifi 應用，SDIO 介面所提供的 API 請參考 ``<sdk_root>/sdk/include/ite/ite_sdio.h`` 。

軟體配置說明
---------------------------------------

SD/eMMC 卡
^^^^^^^^^^^^^^^^^^^^^^^^^

- 根據系統應用，可個別開啟 SD0/SD1 或是兩者皆開啟。
- 若 SD0 所有 GPIO pin 腳完全沒有和其它裝置共用，請勾選 :menuselection:`SD0 --> SD0 Device --> SD0 no pin share` 選項； SD1 同理。
- 若 SD0 沒有 card detect pin 腳，請勾選 :menuselection:`SD0 --> SD0 Device --> SD0 as static device` 選項； SD1 同理。
- SD0 card 1-bit mode 為測試用，一般不需要勾選；SD1 同理。
- SD New Driver 預設為勾選。

  .. image:: /_static/qconf_storage_sd.png
 

根據平台的設計，需填妥下列的 GPIO 設定：

- 若所使用平台沒提供 power enable gpio，請將 "SD0 Power Enable Pin" 設為-1；write protect同理。
- SD I/O pin依順序為：CLK, CMD, D0, D1, D2, D3, D4, D5, D6, D7
- 若使用 eMMC 且提供 8-bit 模式，則 D4 ~ D7 需填妥對應的 GPIO 設定，否則皆設為 -1。

  .. image:: /_static/qconf_gpio_sd.png
 
 
SDIO 配置 
^^^^^^^^^^^^^^^^^^^^^^^^^

- 支援 SDIO 功能需要額外勾選 SDIO Enable，若 SDIO 裝置連接在SD0控制器，則需對應勾選 "SDIO 0 as static device" 選項；若 SDIO 裝置連接在 SD1 控制器，則需對應勾選 "SDIO 1 as static device" 選項。
- 初期若線路不佳可取消勾選 "Enable SDIO 4-bit mode" 來實驗，此時將以 1-bit mode 運作。
- 若使用 ESP32 模組，需先確認模組是否支援 4-bit mode，若沒有請取消勾選 "Enable SDIO 4-bit mode" 選項。

.. image:: /_static/qconf_peripheral_sd.png
 
Timing 設定
^^^^^^^^^^^^^^^^^^^^^^^^^

若一塊新設計的板子懷疑線路不佳，可以先執行測試程式所提供的 scan window 功能，只有 SD 卡有提供此項功能，SDIO 沒有。執行前需先確認相關的 GPIO 是否有根據板子的設計設定對，並在 Kconfig 勾選 "Scan window for SD"。

.. image:: /_static/qconf_sdtest_scan_window_for_sd.png
 
執行最後會列出如下的表格，建議將結果提供給 RD 分析。

.. image:: /_static/console_sdtest_scan_window_for_sd.png
 
Timing 相關的設定可以參考 ``<sdk_root>/sdk/driver/ith/ith_card.c`` 中的 ``ithSdDelay()`` 。


.. raw:: latex

    \newpage
