.. _canbus:

CanBus Controller
============================

功能描述
--------------------

IT986x 系列包含兩個 CAN 模組。這兩個 CAN 模組均符合 ISO 11898-1:2015 (CAN-specification 2.0B)。本文件將以 can0 與 can1 表示這兩個模組。

控制器區域網路 (Controller Area Network)，允許網路上的多個微控制器或設備直接互相通訊，且網路上不需要主機(Host)控制通訊。Canbus 具有高擴充性、高可靠度且低成本等特性，同一個線上的裝置必需有相同的 baud rate 才可通訊，其基礎網路架構圖如下圖。

.. image:: /_static/diagram_canbus.png

Canbus 訊號介紹
^^^^^^^^^^^^^^^^^^^^

CANBUS在傳輸顯性（0）訊號時，會將 CANH 端抬向 5V 高電位，將 CANL 拉向 0V 低電位。當傳輸隱性（1） 訊號時，並不會驅動 CANH 或者 CANL 端，如下圖。

.. image:: /_static/diagram_canbus_signal.png

Canbus frame 介紹
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Canbus 通訊以一個 frame 為一個單位，而 CAN 2.0B 有 4 種 frame 類型。

.. flat-table::
  :header-rows: 1

  * - 類型
    - 用途
  * - 資料 frame
    - 傳輸的節點資料的 frame
  * - 遠端 frame (RTR)
    - 請求傳輸特定識別碼的 frame
  * - 錯誤 frame
    - 檢測到錯誤的節點傳送的 frame
  * - 過載f frame
    - 資料框或遠端 frame 之間插入延遲的 frame

其中資料 frame (data frame)為使用者最需要自行設定的類型，它有兩種資訊結構：

.. flat-table::
  :header-rows: 1

  * - 類型 (IDE)
    - 支援 ID bits
  * - 基本 frame 格式 (STD frame)
    - 11
  * - 擴充 frame 格式 (EXT frame)
    - 29

.. image:: /_static/diagram_canbus_frame_format.png

基本 frame 格式說明 (CAN 2.0 standard frame)：

  .. flat-table::
    :header-rows: 1

    * - 欄位
      - 位元數
      - 說明
    * - SOF
      - 1
      - 表示 frame 的傳輸起點
    * - BASE ID
      - 11
      - 辨識碼(ID)，也帶有傳輸優先級涵義
    * - RTR
      - 1
      - 遠端請求 frame 設定，資料框設（0），遠端請求 frame 設（1）
    * - IDE
      - 1
      - 基本 frame 設(0)
    * - R0
      - 1
      - 預留位
    * - DLC
      - 4
      - 資料的位元組數
    * - Data
      - 0 - 64
      - 待傳輸資料（長度由資料長度碼 DLC 指定）
    * - CRC
      - 16
      - CRC 錯誤檢查
    * - ACK
      - 2
      - 發信端傳送（1）但是任何接收端可以設（0）
    * - EOF
      - 7
      - 表示 frame 結束必為 (1)


擴充 frame 格式 (CAN 2.0 extended frame)：


  .. flat-table::
    :header-rows: 1

    * - 欄位
      - 位元數
      - 說明
    * - SOF
      - 1
      - 表示 frame 的傳輸起點
    * - BASE ID
      - 11
      - 辨識碼(ID)，也帶有傳輸優先級涵義
    * - SRR
      - 1
      - 替代遠端請求，必為(1)
    * - IDE
      - 1
      - 擴充 frame 設(1)
    * - Extended ID
      - 18
      - 第二部分辨識碼(ID)，同 BASE ID 帶有傳輸優先級涵義
    * - RTR
      - 1
      - 遠端請求 frame 設定，資料框設（0），遠端請求 frame 設（1）
    * - R0
      - 1
      - 預留位
    * - DLC
      - 4
      - 資料的位元組數
    * - Data
      - 0 - 64
      - 待傳輸資料（長度由資料長度碼 DLC 指定）
    * - CRC
      - 16
      - CRC 錯誤檢查
    * - ACK
      - 2
      - 發信端傳送（1）但是任何接收端可以設（0）
    * - EOF
      - 7
      - 表示 frame 結束必為（1）

DLC(Data Length Code)轉換表：


  .. flat-table::
    :header-rows: 1

    * - DLC
      - bytes
    * - 0x0000
      - 0
    * - 0x0001
      - 1
    * - 0x0010
      - 2
    * - 0x0011
      - 3
    * - 0x0100
      - 4
    * - 0x0101
      - 5
    * - 0x0110
      - 6
    * - 0x0111
      - 7
    * - 0x1000
      - 8


Canbus Message buffers介紹:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Message buffers 架構圖如下:

.. image:: /_static/diagram_canbus_msg_buffer.png

IT986x 支援16個RB，資料格式如下圖。

Standard frame:

.. table::

  +--------------+---------------------------------------------------------------+
  | Address      |                       Bit Position                            |
  +--------------+-------+-------+-------+-------+-------+-------+-------+-------+
  |              | 7     | 6     | 5     | 4     | 3     | 2     | 1     | 0     |
  +==============+=======+=======+=======+=======+=======+=======+=======+=======+
  | RBUF         |                       ID(7:0)                                 |
  +--------------+---------------------------------------+-----------------------+
  | RBUF+1       |                   \-                  |        ID(10:8)       |
  +--------------+---------------------------------------+-----------------------+
  | RBUF+2       |                        \-                                     |
  +--------------+-------+-------------------------------+-----------------------+
  | RBUF+3       | ESI   |                         \-                            |
  +--------------+-------+-------+-------+-------+-------------------------------+
  | RBUF+4       | IDE=0 | RTR   | EDL   | BRS   |           DLC(3:0)            |
  +--------------+-------+-------+-------+-------+-------------------------------+
  | RBUF+5       |         KOER          | TX    |           \-                  |
  +--------------+-----------------------+-------+-------------------------------+
  | RBUF+6       |                       CYCLE_TIMIE(7:0)                        |
  +--------------+---------------------------------------------------------------+
  | RBUF+7       |                      CYCLE_TIMIE(15:8)                        |
  +--------------+---------------------------------------------------------------+
  | RBUF+8       |                          d1(7:0)                              |
  +--------------+---------------------------------------------------------------+
  | RBUF+9       |                          d2(7:0)                              |
  +--------------+---------------------------------------------------------------+
  | ...          |                          ...                                  |
  +--------------+---------------------------------------------------------------+
  | RBUF+71      |                          d64(7:0)                             |
  +--------------+---------------------------------------------------------------+
  | RBUF+72      |                          RTS(7:0)                             |
  +--------------+---------------------------------------------------------------+
  | ...          |                          ...                                  |
  +--------------+---------------------------------------------------------------+
  | RBUF+79      |                          RTS(63:56)                           |
  +--------------+---------------------------------------------------------------+

Extended frame:


.. table::

  +--------------+---------------------------------------------------------------+
  | Address      |                       Bit Position                            |
  +--------------+-------+-------+-------+-------+-------+-------+-------+-------+
  |              | 7     | 6     | 5     | 4     | 3     | 2     | 1     | 0     |
  +==============+=======+=======+=======+=======+=======+=======+=======+=======+
  | RBUF         |                       ID(7:0)                                 |
  +--------------+---------------------------------------------------------------+
  | RBUF+1       |                       ID(15:8)                                |
  +--------------+---------------------------------------------------------------+
  | RBUF+2       |                       ID(23:16)                               |
  +--------------+-------+---------------+---------------------------------------+
  | RBUF+3       | ESI   |     \-        |         ID(28:24)                     |
  +--------------+-------+-------+-------+-------+-------------------------------+
  | RBUF+4       | IDE=1 | RTR   | EDL   | BRS   |           DLC(3:0)            |
  +--------------+-------+-------+-------+-------+-------------------------------+
  | RBUF+5       |         KOER          | TX    |           \-                  |
  +--------------+-----------------------+-------+-------------------------------+
  | RBUF+6       |                       CYCLE_TIMIE(7:0)                        |
  +--------------+---------------------------------------------------------------+
  | RBUF+7       |                      CYCLE_TIMIE(15:8)                        |
  +--------------+---------------------------------------------------------------+
  | RBUF+8       |                          d1(7:0)                              |
  +--------------+---------------------------------------------------------------+
  | RBUF+9       |                          d2(7:0)                              |
  +--------------+---------------------------------------------------------------+
  | ...          |                          ...                                  |
  +--------------+---------------------------------------------------------------+
  | RBUF+71      |                          d64(7:0)                             |
  +--------------+---------------------------------------------------------------+
  | RBUF+72      |                          RTS(7:0)                             |
  +--------------+---------------------------------------------------------------+
  | ...          |                          ...                                  |
  +--------------+---------------------------------------------------------------+
  | RBUF+79      |                          RTS(63:56)                           |
  +--------------+---------------------------------------------------------------+


IT986x 支援兩種TB: PTB 與STB 其中PTB的優先權高於STB，STB 可以一次傳送多個框架，傳送順序為先進先出，資料格式如下圖。

Standard frame:


.. table::

  +--------------+---------------------------------------------------------------+
  | Address      |                       Bit Position                            |
  +--------------+-------+-------+-------+-------+-------+-------+-------+-------+
  |              | 7     | 6     | 5     | 4     | 3     | 2     | 1     | 0     |
  +==============+=======+=======+=======+=======+=======+=======+=======+=======+
  | TBUF         |                       ID(7:0)                                 |
  +--------------+---------------------------------------+-----------------------+
  | TBUF+1       |                   \-                  |        ID(10:8)       |
  +--------------+---------------------------------------+-----------------------+
  | TBUF+2       |                        \-                                     |
  +--------------+-------+-------------------------------+-----------------------+
  | TBUF+3       | TTSEN |                         \-                            |
  +--------------+-------+-------+-------+-------+-------------------------------+
  | TBUF+4       | IDE=0 | RTR   | EDL   | BRS   |           DLC(3:0)            |
  +--------------+-------+-------+-------+-------+-------------------------------+
  | TBUF+8       |                          d1(7:0)                              |
  +--------------+---------------------------------------------------------------+
  | TBUF+9       |                          d2(7:0)                              |
  +--------------+---------------------------------------------------------------+
  | ...          |                          ...                                  |
  +--------------+---------------------------------------------------------------+
  | TBUF+71      |                          d64(7:0)                             |
  +--------------+---------------------------------------------------------------+


Extended frame:

.. table::

  +--------------+---------------------------------------------------------------+
  | Address      |                       Bit Position                            |
  +--------------+-------+-------+-------+-------+-------+-------+-------+-------+
  |              | 7     | 6     | 5     | 4     | 3     | 2     | 1     | 0     |
  +==============+=======+=======+=======+=======+=======+=======+=======+=======+
  | TBUF         |                       ID(7:0)                                 |
  +--------------+---------------------------------------------------------------+
  | TBUF+1       |                       ID(15:8)                                |
  +--------------+---------------------------------------------------------------+
  | TBUF+2       |                       ID(23:16)                               |
  +--------------+-------+---------------+---------------------------------------+
  | TBUF+3       | TTSEN |     \-        |         ID(28:24)                     |
  +--------------+-------+-------+-------+-------+-------------------------------+
  | TBUF+4       | IDE=1 | RTR   | EDL   | BRS   |           DLC(3:0)            |
  +--------------+-------+-------+-------+-------+-------------------------------+
  | TBUF+8       |                          d1(7:0)                              |
  +--------------+---------------------------------------------------------------+
  | TBUF+9       |                          d2(7:0)                              |
  +--------------+---------------------------------------------------------------+
  | ...          |                          ...                                  |
  +--------------+---------------------------------------------------------------+
  | TBUF+71      |                          d64(7:0)                             |
  +--------------+---------------------------------------------------------------+


Canbus Acceptance filtering 介紹:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Canbus 在傳送資料的過程是類似廣播的方式進行，所以在裝置數量眾多的時候，單一裝置會收到所有裝置所傳出的各種資料，但其實只要接收與它有相關的資料即可，所以在接收端就需要 ID filter 來過濾資料，好處是可以降低裝置處理資料的負擔，下圖為 filter 作用的示意圖。

.. image:: /_static/diagram_canbus_filter.png

.. flat-table::
  :header-rows: 1

  * - 控制欄位
    - 說明
  * - AE_x
    - Filter enable，AE_x 設為1，此組 filter enable 才會開啟，下面的欄位才有作用，IT986x 系列共有 16 組，只要有其中一組接受，則資料就會被收下。
  * - | ACODE_x
      | AMASK_x
    - | ACODE & AMASK搭配使用，ACODE 可以指定限定的 ID，只有 ID 與 ACODE 相同才會被 HW 收下來，AMASK 可以達到遮罩作用，舉例說明:
      |
      | Example1:
      |     Acode = 0x000000011
      |     Amask = 0x000000000
      | 此組即代表 ID 0x11 可以被接受
      |
      | Example2:
      |     Acode = 0x000000011
      |     Amask = 0x00000000F
      | 此組即代表 ID 0x10 ~ 0x1F 皆可被接受
      |
      | Example3:
      |     Acode = 0x000000000
      |     Amask = 0x1FFFFFFF
      | 此組即代表全部ID都接受
  * - AIDEE
    - | AIDEE
      | 0 - accept both standard or extended frame
      | 1 - defined by AIDE
  * - AIDE
    - | AIDE
      | 0 - accept only standard frames
      | 1 - only extended frames

Canbus Debug 模式介紹：
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

IT986x supports two Loop Back Modes: internal (LBMI) and external (LBME). Both modes result in reception of the own t ransmitted frame which can be useful for self tests.

In LBMI is disconnected from the CAN bus and the txd output is set to recessive. The output data stream is internally fedback to the input.

In LBME stays connected to the transceiver and a transmitted frame will be visible on the bus. Therefore in LBME with SACK=0 there are two possible results upon a frame transmission:


.. flat-table::

  * - Success
    - Another node receives the frame too and generates an ACK. This will result in a successful transmission and reception.
  * - Fail
    - No other node is connected to the bus and this results in an ACK error.

.. image:: /_static/diagram_canbus_lbmi_lbme.png

相關的 KConfig 設定
----------------------------------------------------------------

- :menuselection:`Peripheral --> Canbus Enable`

  決定是否要啟用 Canbus 裝置，若要啟用 Canbus0 ~ 1 之間任一裝置都必須啟用此選項，否則將無法使用相關 API。



相關的程式碼檔案
----------------------------

Canbus 以 library 方式提供。

.. flat-table::
  :header-rows: 1

  * - 路徑
    - 描述
  * - ``<sdk_root>/sdk/include/can_bus/it9860/can_api.h``
    - 供上層 AP 呼叫之函式列於此處。
  * - ``<sdk_root>/sdk/include/can_bus/it9860/can_hw.h``
    - 底層控制涵式。
  * - ``<sdk_root>/sdk/include/can_bus/it9860/can_reg.h``
    - 硬體暫存器定義。


相關的 API 描述
---------------------------------------

ithCANSetGPIO
^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithCANSetGPIO(uint32_t Instance, uint32_t rxpin, uint32_t txpin)

**參數**

``uint32_t instance``

  Canbus device (0) or Canbus device 1 (1)


``uint32_t rxpin``

  Set RX pin by target board

``uint32_t txpin``

  Set TX pin by target board


**描述**

設定 CAN Controller 所使用的 GPIO_RX 與 GPIO_TX 的 PIN Number，將 GPIO pin mux 切換至 Canbus function，訊號才能正常進出。

ithCANOpen
^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithCANOpen(CAN_HANDLE* base, void* rx_callback, void* tx_callback)

**參數**

``CAN_HANDLE* Base``

  .. list-table::
    :header-rows: 1
    :class: longtable

    * - CAN_HANDLE struct
      - Option
    * - Instance
      - | 0 (Canbus device 0)
        | 1 (Canbus device 1)
    * - ADDR
      - | CAN0_BASE_ADDRESS
        | CAN1_BASE_ADDRESS
    * - BaudRate
      - | CAN_USER_DEFINED
        | CAN_20K
        | CAN_50K
        | CAN_100K
        | CAN_125K_500K
        | CAN_125K_833K
        | CAN_250K_1M
        | CAN_250K_1M5
        | CAN_250K_2M
        | CAN_250K_3M
        | CAN_250K_4M
        | CAN_500K_1M
        | CAN_500K_2M
        | CAN_500K_3M
        | CAN_500K_4M
        | CAN_500K_5M
        | CAN_1000K_4M
    * - SourceClock
      - | CAN_SRCCLK_40M
        | CAN_SRCCLK_60M
        | CAN_SRCCLK_80M
    * - ProtocolType
      - protocol_CAN_2_0B
    * - ExternalLoopBackMode
      - | 0 - off
        | 1 - on
        | IT9860 supports two Loop Back Modes: internal and external. Both modes result in reception of the own transmitted frame which can be useful for self-tests. In external mode, IT9860 stays connected to the transceiver and a transmitted frame will be visible on the bus.
    * - InternalLoopBackMode
      - | 0 - off
        | 1 - on
        | In internal mode, IT9860 is disconnected from the CAN bus and the tx output is set to recessive. The output data stream is internally feedback to the input.
    * - ListenOnlyMode
      - | 0 - off
        | 1 - on
        | LOM provides the ability of monitoring the CAN bus without any influence to the bus.
    * - Tptr
      - Filter table pointer.
    * - SlowBitRate
      - If BaudRate is CAN_USER_DEFINED, user can set bit rate on here.
    * - Interrupt table
      - Enable interrupt or not.
    * - InterruptHD
      - Interrupt callback function.

  Source Clock 根據 Project 選擇的 RAM script 來設定，對應列表如下:

  .. flat-table::
    :header-rows: 1

    * - Script
      - Source CLK
    * - IT9860_264Mhz_DDR2_264Mhz
      - 60Mhz
    * - IT9860_324Mhz_DDR2_324Mhz
      - 60Mhz
    * - IT9860_360Mhz_DDR2_360Mhz
      - 40Mhz
    * - IT9860_396Mhz_DDR2_396Mhz
      - 60Mhz
    * - IT9860_720Mhz_DDR2_396Mhz
      - 80Mhz
    * - IT9860_780Mhz_DDR2_396Mhz
      - 60Mhz
    * - IT9860_396Mhz_DDR3_792Mhz
      - 60Mhz
    * - IT9860_780Mhz_DDR3_792Mhz
      - 60Mhz
    * - IT9860_792Mhz_DDR2_396Mhz
      - No support Can
    * - IT9860_792Mhz_DDR3_792Mhz
      - No support Can


``void　*rx_callback``

  When controller receive a frame, this function will be called.

``void　*tx_callback``

  When controller send frame finished, this function will be called.

**描述**

  根據使用者傳入的參數，初始化 Canbus Controller，CAN0 與 CAN1 完全獨立，Source Clock & Baud Rate 最為重要必需設定正確才會正常工作。

 
ithCANRead
^^^^^^^^^^^^^^^^^^^

.. c:function:: int ithCANRead(CAN_HANDLE* base, CAN_RXOBJ* info)

**參數**

``CAN_HANDLE* Base``

  詳見 ithCANOpen。

``CAN_RXOBJ* info``

  .. flat-table::
    :header-rows: 1

    * - CAN_RXOBJ struct
      - Option
    * - Identifier
      - ID
    * - Control
      - | - DLC (Data len code)
        | - RTR (Remote Transmission Request)
        | - IDE (0 - STD format, 1 - EXT format)
    * - RXData[DLC_MAX]
      - receive buffers, size = 8
    * - RXRTS[2]
      - timestamp

**描述**

此函式會將一個 frame 的資料從硬體的 SRAM 中讀出透過 info 回傳，並告知硬體該記憶體區塊已被讀取，硬體才會再次寫入新收到的 frame。

 
ithCANWrite
^^^^^^^^^^^^^^^^^^^

.. c:function:: int ithCANWrite(CAN_HANDLE *base, CAN_TXOBJ *info, uint8_t *dataptr)

**參數**

``CAN_HANDLE* Base``

  詳見ithCANOpen。

``CAN_TXOBJ* info``

  .. flat-table::
    :header-rows: 1

    * - CAN_TXOBJ struct
      - Option
    * - Identifier
      - ID
    * - Control
      - | - DLC (Data len code)
        | - RTR (Remote Transmission Request)
        | - IDE (0 - STD format, 1 - EXT format)
    * - SingleShot
      - | 0 - off
        | 1 - on
    * - TTSENSEL
      - | 0 - off (disable enable CiA 603 time stamping)
        | 1 - on (enable CiA 603 time stamping)

``uint8_t *dataptr``

  data array pointer.

**描述**

根據使用者設定的格式，傳送一個 frame 至網路，長度由 DLC 決定，data array 資料內容需大於等於 DLC 所定義大小。

ithCANFIFOUpdate
^^^^^^^^^^^^^^^^^^^

.. c:function:: int ithCANFIFOUpdate(CAN_HANDLE *base, CAN_TXOBJ *info, uint8_t *dataptr)

**參數**

``CAN_HANDLE* Base``

  詳見 ithCANOpen。

``CAN_TXOBJ* info``

  詳見 ithCANWrite。

``uint8_t *dataptr``

  data array pointer.

**回傳值**

0 代表 update success，1 代表 fifo full。


**描述**

提供使用者將資料寫入STB buffers。


ithCANFIFOWrite
^^^^^^^^^^^^^^^^^^^

.. c:function:: int ithCANFIFOWrite(CAN_HANDLE *base, CAN_TXOBJ *info)


``CAN_HANDLE* Base``

  詳見 ithCANOpen。

``CAN_TXOBJ* info``

  詳見 ithCANWrite。


**描述**

提供使用者將STB buffers 中的資料發送至網路。



ithCANClose
^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithCANClose(CAN_HANDLE *base)

**參數**

``CAN_HANDLE* Base``

  詳見 ithCANOpen。

**描述**

  關閉 Controller，欲使用需先再次 Open。

 
ithCANDlcToBytes
^^^^^^^^^^^^^^^^^^^

.. c:function:: uint32_t ithCANDlcToBytes(CAN_DLC dlc)

**參數**

CAN_DLC dlc

  Canbus 2.0B 定義傳送的資料數目表示法

**描述**

DLC 轉換 Bytes 涵式，方便使用者得到正確資料 Bytes 數目。

 
**回傳值**

DLC 轉換成 Bytes 數目

ithCANGetTTS
^^^^^^^^^^^^^^^^^^^

.. c:function:: int ithCANGetTTS(CAN_HANDLE *base)

**參數**

``CAN_HANDLE* Base``

  詳見 ithCANOpen。

**描述**

提供使用者確認上一個 frame 傳送完成時間。

**回傳值**

上一個傳送 frame 完成之時間戳。




ithCANReset
^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithCANReset(CAN_HANDLE *base)

**參數**

``CAN_HANDLE* Base``

  詳見 ithCANOpen。

**描述**

Controller sw reset。


ithCANGetErrorState
^^^^^^^^^^^^^^^^^^^

.. c:function:: CAN_ERROR_STATE ithCANGetErrorState(CAN_HANDLE *base)

**參數**

``CAN_HANDLE* Base``

  詳見 ithCANOpen。


**描述**

取得Controller 錯誤狀態。



**回傳值**

- 0x00: ERROR ACTIVE
- 0x01: ERROR PASSIVE
- 0x02: BUS OFF



ithCANGetREC
^^^^^^^^^^^^^^^^^^^

.. c:function:: uint32_t ithCANGetREC(CAN_HANDLE *base)

**參數**

``CAN_HANDLE* Base``

  詳見 ithCANOpen。

**描述**

提供使用者取得接收錯誤影格計數。



**回傳值**

0 ~ 255

ithCANGetTEC
^^^^^^^^^^^^^^^^^^^

.. c:function:: uint32_t ithCANGetTEC(CAN_HANDLE *base)


**參數**

``CAN_HANDLE* Base``

  詳見 ithCANOpen。

**描述**

提供使用者取得傳送錯誤影格計數。



**回傳值**

0 ~ 255


ithCANGetKOER
^^^^^^^^^^^^^^^^^^^

.. c:function:: CAN_ERROR_TYPE ithCANGetKOER(CAN_HANDLE *base)

**參數**

``CAN_HANDLE* Base``

  詳見 ithCANOpen。


**描述**

提供使用者取得最新的錯誤類型。





**回傳值**

- 0x0: CAN_NO_ERROR
- 0x1: CAN_BIT_ERROR
- 0x2: CAN_FORM_ERROR
- 0x3: CAN_STUFF_ERROR
- 0x4: CAN_ACKNOWLEDGEMENT_ERROR
- 0x5: CAN_CRC_ERROR
- 0x6: CAN_OTHER_ERROR


完整範例展示
--------------------------------------------

硬體部分
^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. 根據下圖將 DB_CANBUS 子版連接至 Standard EVB

  .. image:: /_static/e_circuit_canbus.png

  .. flat-table::
    :header-rows: 1

    * - DB_CANBUS
      - IT9866 Standard EVB
    * - TXD
      - GPIO 50
    * - RXD
      - GPIO 51
    * - Enable
      - GPIO 52
    * - 5V
      - EVB 5V
    * - 3.3V
      - EVB 3.3V
    * - GND
      - EVB GND

2. 連接完成參考圖

  .. image:: /_static/e_circuit_canbus_connect.png

3. DB_CANBUS 子版 CAN H & CAN L 連結到網路端

  .. attention::

    網路要有終端電阻120ohm

  .. image:: /_static/diagram_canbus_connect.png


軟體部分
^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Open ``<sdk_root>/project/test_canbus/test_canbus9860.c``
2. 設定 baud rate (注意事項：需與網路其他設備相同)
3. 控制define 選項，選擇想測試的功能。

   - RX_DEBUG_PRINTF

     顯示CAN收到的資訊

     .. note::

        ithPrintf 耗時所以建議DEBUG時才打開

   - TX_PTB_TEST

     利用PTB發送資料測試

   - TX_STB_TSET

     利用STB發送資料測試

   .. code-block:: c
      :linenos:
      :emphasize-lines: 7-9

      #include <stdio.h>
      #include <malloc.h>
      #include "ite/itp.h"    //for all ith driver
      #include "ite/ith.h"
      #include "can_bus/it9860/can_api.h"

      #define  RX_DEBUG_PRINTF 0
      #define  TX_PTB_TEST     1
      #define  TX_STB_TSET     0

4. 請執行 ``<sdk_root>/build/openrtos/test_canbus.cmd`` 。

5. 點擊 |build_icon| 按鈕建置專案。

  .. |build_icon| image:: /_static/qconf_build_icon.png
    :alt: build icon
    :width: 14
    :height: 14

  .. |run_icon| image:: /_static/qconf_run_icon.png
    :alt: run icon
    :width: 14
    :height: 14

驗證結果
^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. 建置完成後請再確認下列都已完成

  - USB to SPI 轉換板已正確安裝。

    - 若您尚未將 USB to SPI 轉換板與 PC 連接，請參考 :ref:`usb2spi_connect2pc` 。
    - 若您尚未將 USB to SPI 轉換板與 EVB 連接，請參考 :ref:`usb2spi_connect2board` 。
    - 若您尚未安裝 USB to SPI 轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

  - EVB 已經切換為 Co-operative Mode。

    .. note::

       若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

2. 重置 EVB 的電源。
3. 執行 Tera Term 或任何 Serial Port 軟體。
4. 點擊 |run_icon| 按鈕即可透過 USB to SPI 轉換板啟動系統。


5. Serial Port 軟體顯示以下訊息，即可以使用其他設備送出訊號給 IT986x。

   .. code-block:: shell

      booting time: 0ms
      CLK: cpu=396000000 hz,mem=396000000 hz,bus=975000000 hz, risc:195000000 hz
      test 9860 canbus 2.0B!
      [MSG]CAN [S]Prescaler = 2, Bit_Time = 40, Seg1 = 30, Seg2 = 7, SJW = 7


6. 若顯示 Invalid Chip 表示 Chip 型號有誤，請更換正確型號。

   .. code-block:: shell

      booting time: 0ms
      CLK: cpu=396000000 hz,mem=396000000 hz,bus=975000000 hz, risc:195000000 hz
      test 9860 canbus 2.0B!
      [MSG]CAN [S]Prescaler = 2, Bit_Time = 40, Seg1 = 30, Seg2 = 7, SJW = 7
      [MSG]Invalid CHIP!!!





.. raw:: latex

    \newpage
