.. _pwm:

PWM
===

功能描述
--------------------

PWM 是一種利用數位訊號模擬類比訊號的方式。通常我們可以用來調整燈光的亮度、馬達的轉速、RGB LED 的配色、螢幕亮度控制、喇叭的大小聲/聲音頻率等…。

IT986X 提供 PWM1 ~ PWM8 供使用者使用，PWM1 ~ 8 使用與內部 TIMER1 ~ 8 編號相對，例如：TIMER8 已先被 OS 使用則不能再選擇相對的 PWM8。以下為 986X 一般 TIMER 使用的配置，如需使用 PWM 必須先注意 TIMER 是否已經先被使用，避免使用相同的編號。

.. flat-table::

    * - timer 1
      - free
    * - timer 2
      - free
    * - timer 3
      - backlight/sdk/driver/itp/itp_backlight.c
    * - timer 4
      - RTC
    * - timer 5
      - DEBUG
    * - timer 6
      - 遠端UART喚醒
    * - timer 7
      - free
    * - timer 8
      - OS


相關的 KConfig 設定
----------------------------------------------------------------

- :menuselection:`Screen --> Backlight# Enable`

    決定是否要啟用 Backlight。勾選此選項後，系統才會在 ``itpInit()`` 函式內透過 ``itpRegisterDevice()`` 函式註冊對應 Backlight 裝置並進行初始化動作。

- :menuselection:`GPIO --> Backlight PWM Pin`

    指定該 Backlight 要使用哪個 GPIO pin 作為 PWM pin。

- :menuselection:`GPIO --> Backlight PWM Number`

    指定該 Backlight 要使用哪組 PWM。


相關的程式碼檔案
----------------------------


.. flat-table::
   :header-rows: 1

   * - 路徑
     - 描述
   * - ``<sdk_root>/sdk/driver/ith/ith_pwm.c``
     - 最上層函式。


相關的 API 描述
---------------------------------------

ithPwmInit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithPwmInit(ITHPwm pwm, unsigned int freq, unsigned int duty)

**參數**

``ITHPwm pwm``

    裝置標識符。與 PWM 裝置相關的裝置標識符有 ``ITH_PWM1`` ~ ``ITH_PWM8`` 。

``unsigned int freq``

    頻率。

``unsigned int duty``

    空佔比。值的範圍是 0 ~ 100。

**描述**

初始化PWM裝置。
 
ithPwmReset
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithPwmReset(ITHPwm pwm, unsigned int pin, unsigned int gpio_mode)

**參數**

``ITHPwm pwm``

    裝置標識符。與 PWM 裝置相關的裝置標識符有 ``ITH_PWM1`` ~ ``ITH_PWM8`` 。

``unsigned int pin``

    GPIO 編號。

``unsigned int gpio_mode``

    GPIO MODE：0 ~ 4。此參數是為了相容性，在 IT986x 平台會忽略此參數。

**描述**

重置PWM裝置。

ithPwmSetDutyCycle
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithPwmSetDutyCycle(ITHPwm pwm, unsigned int duty)

**參數**

``ITHPwm pwm``

    裝置標識符。與 PWM 裝置相關的裝置標識符有 ``ITH_PWM1`` ~ ``ITH_PWM8`` 。

``unsigned int duty``

    空佔比，其值範圍為 0 ~ 100。

**描述**

設置 PWM 空佔比。
 
ithPwmEnable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithPwmEnable(ITHPwm pwm, unsigned int pin, unsigned int gpio_mode)

**參數**

``ITHPwm pwm``

    裝置標識符。與 PWM 裝置相關的裝置標識符有 ``ITH_PWM1`` ~ ``ITH_PWM8`` 。

``unsigned int pin``

    GPIO 編號

``unsigned int gpio_mode``

    GPIO MODE：0 ~ 4。此參數是為了相容性，在 IT986x 平台會忽略此參數。

**描述**

啟用裝置。
 
ithPwmDisable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithPwmDisable(ITHPwm pwm, unsigned int pin)

**參數**

``ITHPwm pwm``

    裝置標識符。與 PWM 裝置相關的裝置標識符有 ``ITH_PWM1`` ~ ``ITH_PWM8`` 。

``unsigned int pin``

    GPIO 編號

**描述**

停用裝置。

.. raw:: latex

    \newpage