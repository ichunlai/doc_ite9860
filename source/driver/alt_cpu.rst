.. _alt_cpu:

Alternative Processor
===========================

功能描述
----------------------

Alternative Processor，簡稱 ALT CPU，ALT CPU 為 32-bit RISC 架構的 CPU，提供開發人員開發軟體 IO 協定、規律性程序等需要即時性處理的應用，以降低主 CPU 使用率，提高負載平衡。

.. image:: /_static/diagram_alt_cpu.png

使用範圍:

- 用於處理簡單低速的 IO 協定，如：I2C、RSL、UART 等。
- 波形產生器，如：PWM，Heartbeat 等。
- 客製化協定。
- 作為 MCU 處理規律性程序等，如：即時監控和回應某些事件。

使用限制:

- 無 OS。
- Big Endian。
- 16KB SRAM；可讀寫 DRAM 記憶體空間，但是效能差。
- 不能使用動態記憶體相關 API，如 : ``malloc()`` 函式。

ALT CPU 目錄架構
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: /_static/file_struct_alt_cpu.png

ALT CPU 的控制需要主 CPU 的配合，因此其完整的程式碼包含了主 CPU 端(ARM)和 ALT CPU 端(RISC)的部分。底下針對 RISC 部分、ARM 部分、ARM/RISC 共同部分有哪些相關的程式碼做個概述。

- RISC 部分

  此部分的程式碼運行在 ALT CPU 上，其原始碼包含在以下兩個目錄：

  - ``<sdk_root>/project/alt_cpu`` ：提供 ALT CPU 專用的 GPIO 控制、tick timer 相關工具函式。
  - ``<sdk_root>/sdk/alt_cpu/xxx`` ：ALT CPU 各種應用的實作。

  運行在 ALT CPU 上的程式碼會被預先建置以便用於隨後的 ARM 相關專案。

- ARM 部分

  此部分的程式碼運行在主 CPU 上，其原始碼包含在以下兩個目錄：

  - ``<sdk_root>/sdk/driver/itp/itp_alt_cpu.c`` ：上層 ALT CPU 驅動函式實作。提供 POSIX 型式的操作函式供 AP 層載入 ALT CPU 內的韌體、重置 ALT CPU 等操作。
  - ``<sdk_root>/sdk/driver/alt_cpu/xxx`` ：底層 ALT CPU 驅動函式實作，未來有可能變動，因此請勿直接呼叫此部分的函式。

- ARM/RISC 共同部分

  此部分包含 ARM 和 RISC 都會用到的原始程式碼，主要集中在 ``<sdk_root>/sdk/include/alt_cpu/xxx`` 目錄，其中包含：

  - ARM/RISC 互相通訊時所使用的訊息結構的宣告。
  - 命令 ID 的定義。
  - 共享記憶體位址偏移量。

ARM/RISC 之間的通訊主要是透過共享記憶體機制並使用多個未用到的 IO 暫存器所實作的 read/write queue 來交換命令和資料。

新增裝置模組的方法
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

透過為 ALT CPU 撰寫韌體，可將 ALT CPU 作為一個實際是由軟體模擬的裝置模組使用。底下描述 ITE 為了將 ALT CPU 作為一個 pattern generator 裝置，做了哪些修改：

- 在 ``<sdk_root>/sdk/alt_cpu`` 下加入 alt_cpu 韌體實作程式。
- 將 ARM/RISC 共享的 include 檔案放置到 ``<sdk_root>/include/alt_cpu`` 。
- 將 ARM 部分的裝置驅動程式放置於 ``<sdk_root>/driver/alt_cpu`` 。
- 在 ``<sdk_root>/project/test_pattern_gen`` 下新增測試範例。
- 將新的驅動程式加入至位於 ``<sdk_root>/sdk/driver/itp_alt_cpu.c`` 的上層 alt cpu 裝置驅動程式中。如下方程式碼粗體字部分：

  .. code-block:: c

    ...
    #ifdef CFG_PATTERN_GEN
    extern ITPDevice itpDevicePatternGen;
    #endif
    ...
    static ALT_CPU_ENGINE gptAltCpuEngineArray[] =
    {
       ...

    #ifdef CFG_PATTERN_GEN
        { ALT_CPU_PATTERN_GEN, &itpDevicePatternGen },
    #endif
       ...
    }
    ...

- 在 ``<sdk_root>/sdk/def.cmake`` 加入編譯選項。

  .. code-block:: shell

    ...
    if (DEFINED CFG_ALT_CPU_ENABLE)
        set(CFG_BUILD_RISC 1)
        set(CFG_BUILD_ALT_CPU 1)
        add_definitions(
            -DCFG_RISC_ENABLE
            -DCFG_ALT_CPU_ENABLE
        )
        ITE_ADD_DEFINITIONS_IF_DEFINED(CFG_RSL_MASTER)
        ITE_ADD_DEFINITIONS_IF_DEFINED(CFG_RSL_SLAVE)
        ITE_ADD_DEFINITIONS_IF_DEFINED(CFG_PATTERN_GEN)
        ITE_ADD_DEFINITIONS_IF_DEFINED(CFG_OLED_CTRL)
        ...

- 在 ``<sdk_root>/sdk/build.cmake`` 加入函式庫連結選項。

  .. code-block:: shell

    ...
        # move alt cpu libraray here to solve linking order issue
        # ALT CPU
        if (DEFINED CFG_ALT_CPU_ENABLE)
            target_link_libraries(${CMAKE_PROJECT_NAME}
                risc
            )

            ITE_LINK_LIBRARY_IF_DEFINED(CFG_RSL_MASTER rslMaster)
            ITE_LINK_LIBRARY_IF_DEFINED(CFG_RSL_SLAVE rslSlave)
            ITE_LINK_LIBRARY_IF_DEFINED(CFG_SW_PWM swPwm)
            ITE_LINK_LIBRARY_IF_DEFINED(CFG_PATTERN_GEN patternGen)
            ITE_LINK_LIBRARY_IF_DEFINED(CFG_SW_UART swUart)
            ITE_LINK_LIBRARY_IF_DEFINED(CFG_SW_SERIAL_PORT swSerialPort)
            ITE_LINK_LIBRARY_IF_DEFINED(CFG_OLED_CTRL oledCtrl)
    ...

- 在 ``<sdk_root>/sdk/Kconfig`` 中為通用裝置添加 Kconfig 選項。 對於自定義設備，建議僅通過專案預設的 Kconfig 設置啟用裝置。

  .. code-block:: shell

    ...
    config PATTERN_GEN
        bool "Use ALT CPU to generate certain data pattern"
        default n
        depends on ALT_CPU_ENABLE
        help
            Use ALT CPU to generate certain data pattern.
        help_cht
            使用 ALT CPU 模擬自訂數據波形模式
        help_chs
            使用 ALT CPU 仿真自定义数据波形模式
    ...

- 在 ``<sdk_root>/project/test_pattern_gen`` 下加入 Kconfig 設定。

  .. code-block:: shell

    ...
    config ALT_CPU_ENABLE
        def_bool y

    config PATTERN_GEN
        def_bool y
    ...

- 在 ``<sdk_root>/build/openrtos`` 下加入 build 和 build_all script，即檔案 ``<sdk_root>/build/openrtos/test_pattern_gen.cmd`` 和 ``<sdk_root>/build/openrtos/test_pattern_gen_all.cmd`` 。
- 在 build_all script (即 ``<sdk_root>/build/openrtos/test_pattern_gen_all.cmd``)內加入alt cpu 預建置選項。

  .. code-block:: shell

    @echo off

    set CFG_PROJECT=%~n0

    set ALT_CPU=1
    call test_pattern_gen.cmd

    @if not defined NO_PAUSE pause

如何除錯
^^^^^^^^^^^^^^^^^^^

- RISC 上的韌體可以使用 RISC 未使用的 IO 暫存器儲存訊息，並透過 ARM 測試程式碼或 usb_to_spi 工具讀取這些暫存器以檢查狀態。需要提醒韌體開發人員的另一件事是，底下列出的這些未使用的暫存器可能會被實際專案中的音頻處理器使用。

  .. flat-table::
    :header-rows: 1

    * - Address
      - Type
      - Field
      - Description
    * - 0xD800 0200
      - R/W
      - USER1
      - User Defined Register
    * - 0xD800 0204
      - R/W
      - USER2
      - User Defined Register
    * - 0xD800 0208
      - R/W
      - USER3
      - User Defined Register
    * - ...
      - ...
      - ...
      - ...
    * - 0xD800 027C
      - R/W
      - USER160
      - User Defined Register

- 使用未使用的共享記憶體區塊寫入除錯訊息，並透過 ARM 測試程式碼顯示該訊息。 usb_to_spi 工具無法直接存取 SRAM，因此，ARM 測試程式碼是存取除錯訊息的唯一方法。

  譬如：

  - 在 RISC 端的程式碼，譬如 ``<sdk_root>/sdk/alt_cpu/patternGen/main.c`` 內加入：

    .. code-block:: c

        ...
        int main(int argc, char **argv)
        {
            //Set GPIO and Clock Setting
            int inputCmd = 0;

            //Start Timer
            startTimer(0);

            {
                uint32_t* pDebugAddr = (uint32_t*)(15 * 1024);
                *pDebugAddr = ENDIAN_SWAP32(0x12345678);
            }
        ...

- 在 ARM 端的測試專案程式碼，譬如 ``<sdk_root>/project/test_pattern_gen/test_pattern_gen.c`` 內加入：

  .. code-block:: c

    ...
    void* TestFunc(void* arg)
    {
        #ifdef CFG_DBG_TRACE
        uiTraceStart();
        #endif

        itpInit();
        //_SimpleTest();
        _MultiPleDeviceTest();

        {
            uint32_t* pDebugAddress =  (uint32_t*)(iteRiscGetTargetMemAddress(ALT_CPU_IMAGE_MEM_TARGET) + 15 * 1024);
            printf("debug msg: 0x%X\n”, *pDebugAddress");
        }
        ...
    }

重新建置專案並運行，可在主 CPU 輸出的 debug 訊息中看到從 RISC 送過來的訊息 0x12345678。

.. image:: /_static/teraterm_alt_cpu_debug_msg.png

應用範例
^^^^^^^^^^^^^^^^^

ITE 目前已為 ALT CPU 提供一些韌體以完成以下應用：

- PWM :

  通常用於透過不同的佔空比調節來控制提供給電氣設備的功率。例如：LCD背光控制、LED亮度控制。

  .. image:: /_static/wave_pwm.png

- Pattern Generator :

  用於產生特定的 waveform pattern。

  .. image:: /_static/wave_custom.png

相關的 KConfig 設定
---------------------------------------

- :menuselection:`Peripheral --> ALT CPU Enable`

  決定是否要啟用 ALT CPU 模擬功能，若要啟用 ALT CPU 相關功能都必須啟用此選項。

- :menuselection:`Peripheral --> ALT CPU Enable --> Use ALT CPU to generate certain data pattern`

  若欲使用 ALT CPU 模擬自訂數據波形模式，則啟用此選項。

相關的程式碼檔案
----------------------------------------------------------------

.. flat-table::
  :header-rows: 1

  * - 路徑
    - 描述
  * - ``sdk/driver/alt_cpu/xxx``
    - 參考 API 原型。


相關的 API 描述
---------------------------------------

本節描述用來操作 ALT CPU 模擬介面的相關 API。依據 POSIX 規範實作的 API，用戶可以透過 ``open()``/``read()``/``write()``/``ioctl()`` 等函式對 Wiegand 裝置進行如讀寫檔案般的操作。

ioctl
^^^^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr)

  控制裝置。

**參數**

``int file``

  裝置標識符。其值為 ``ITP_DEVICE_ALT_CPU`` 。

``unsigned long request``

  request 參數用來選擇要對裝置運行的操作功能，並將取決於所指定的裝置。

``void *ptr``

  ptr 參數表示對此特定裝置執行請求的功能所需的額外訊息。ptr 的型別取決於特定的控制請求，但它可以是整數，也可以是指向裝置特定資料結構的指標。

**描述**

``ioct()`` 函數用於在 ALT CPU 模擬介面上執行各種控制功能。每種模擬介面都有各自定義的命令，其中比較重要的命令，如下所述。

- ITP_IOCTL_ALT_CPU_SWITCH_EN

  用來使 ALT CPU 切換成某個模擬介面。如要模擬 pattern generator，則參數為 ``ALT_CPU_PATTERN_GEN`` 。

- ITP_IOCTL_INIT

  用來初始化 ALT CPU 模擬介面。

範例程式
---------------

.. code-block:: c

    // pattern generator
    #include "ite/itp.h"        // for ITP_DEVICE_WIEGAND0 & ITP_DEVICE_WIEGAND1
    #include "alt_cpu/alt_cpu_device.h"
    #include "alt_cpu/patternGen/patternGen.h"

    PATTERN_GEN_WRITE_CMD_DATA gtPatternData[] =
    {
        {PATTERN_GEN0, 6, INIFINITE_SEND_COUNT, {{0, 2000}, {1, 600}, {0, 200}, {1, 1000},
        {0, 500}, {1, 10000}}}
    };

    int main(void)
    {
        int altCpuEngineType = ALT_CPU_PATTERN_GEN;
        PATTERN_GEN_INIT_DATA tInitData = { 0 };

        // Load Pattern Generator Engine on ALT CPU
        printf("Load Pattern Generator Engine\n”);
        ioctl(ITP_DEVICE_ALT_CPU, ITP_IOCTL_ALT_CPU_SWITCH_ENG, &altCpuEngineType);

        ioctl(ITP_DEVICE_ALT_CPU, ITP_IOCTL_INIT, NULL);

        printf("Init Pattern Generator Parameters\n”);
        tInitData.patternGenId = PATTERN_GEN0;
        tInitData.cpuClock = ithGetRiscCpuClock();
        tInitData.patternGenGpio = 86;
        tInitData.defaultGpioValue = GPIO_PULL_HIGH;
        tInitData.timeUnitInUs = 1;
        ioctl(ITP_DEVICE_ALT_CPU, ITP_IOCTL_INIT_PATTERN_GEN_PARAM, &tInitData);

        printf("Start Pattern Generator Write\n”);
        ioctl(ITP_DEVICE_ALT_CPU, ITP_IOCTL_PATTERN_GEN_WRITE_DATA, &gtPatternData[0]);
    }


.. raw:: latex

    \newpage
