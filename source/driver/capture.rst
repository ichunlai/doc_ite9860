.. _capture:

Capture
==========

功能描述
------------------------

IT986x 提供一個 capture 模組，該模組具有數位影像接收功能，其規格如下表：

.. flat-table::
  :stub-columns: 1

  * - 最大解析度
    - 1920 x 1080
  * - 最大 Pixel CLK  
    - 148.5 Mhz
  * - 輸入介面  
    - | BT.601 / BT.656 / BT.1120
      | 8 / 16 / 24-bit
  * - 輸入顏色格式    
    - RGB444 / YUV444 / YUV422
  * - 輸出格式  
    - NV12
  * - 其他功能  
    - | ROI
      | horizontal downscaling
      | RGB to YUV
      | dithering

該 capture 模組硬體內部架構如下：

.. image:: /_static/diagram_capture.png
 
其中：

- VDU：負責解析輸入的 BT.601/BT.656/BT.1120 訊號。
- Dither：對輸入畫面進行 dithering 處理。
- ROI (Region of Interest)：裁剪出用戶設定感興趣的區域。
- RGB to YUV：將裁剪後的畫面進行 color space 的轉換。
- Horizontal Scaling：進行水平縮放。

縮放後的畫面，可選擇直接傳遞給 ISP 模組做進一步的影像處理，或再透過 YUV422 to YUV420 子模組進一步轉換後儲存到記憶體空間，然後再看看要不要做進一步處理。

Capture 模組的操作模式分成兩種：

1. Memory Mode：

  Capture 將接收到的數位影像資料經過簡易的操作後直接寫入記憶體，若要更進階的影像處理，再由 ISP 模組從記憶體讀取資料再做進一步處理。這方法的優點是支援 interlanced 的輸入，在不需要進階影像處理的狀況下可空出 ISP。缺點是需要額外的記憶體空間。

2. Onfly Mode：

  Capture 在接收到數位影像資料一部份時，就將此部分的資料直接傳送給 ISP 模組做更進階的影像處理。這方法的優點是節省記憶體使用量。缺點是不支援 interlanced 的輸入，以及占用 ISP 模組處理時間。
 
.. image:: /_static/diagram_memory_mode_vs_onfly_mode.png 

相關的 KConfig 設定
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- :menuselection:`Peripheral --> Capture initial`

  Capture 初始化設定檔，其內容由使用者根據電路線路配置圖設定提供，以用來設定 capture 模組初始狀態。設定檔路徑位於 ``<sdk_root>/target/defcap`` 下。

  .. note:: 
      
      此檔案可以由 capture 工具產生，無需手動編輯。Capture 工具路徑位於 ``<sdk_root>/tool/bin/capture.exe`` 。

- :menuselection:`Peripheral --> Sensor Enable`

  決定 Sensor 裝置是否啟用，啟用後才可以選擇前端裝置。

- :menuselection:`Peripheral --> Sensor Enable --> Sensor`

  選擇 Sensor 裝置驅動程式碼。

- :menuselection:`Peripheral --> Sensor Enable --> Flick`

  選擇 Sensor 裝置電源交流電的頻率，部分裝置會自動偵測。

- :menuselection:`Peripheral --> Sensor Enable --> IIC_PORT_SEL`

  選擇 Sensor 裝置使用到的 I2C 編號。

- :menuselection:`Peripheral --> Sensor Enable --> RESETPIN Enable`

  選擇是否啟用重置功能，啟用後才可以在 GPIO 選項內選擇對應的 GPIO Pin。

- :menuselection:`Peripheral --> Sensor Enable --> POWERDOWNPIN Enable`

  選擇是否啟用電源控制功能，啟用後才可以在 GPIO 選項內選擇對應的 GPIO Pin。

- :menuselection:`Peripheral --> Sensor Enable --> LEDPIN Enable`

  選擇是否啟用 LED 控制功能，啟用後才可以在 GPIO 選項內選擇對應的 GPIO Pin。

相關的程式碼檔案
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. flat-table::
  :header-rows: 1

  * - 路徑
    - 描述
  * - ``sdk/driver/capture/it9860/mmp_capture.c``
    - 供上層 AP 呼叫之函式。
  * - ``sdk/driver/capture/it9860/device_modules.c``
    - 供上層 AP 呼叫之感測器控制函式。
  * - | ``sdk/driver/capture/it9860/cap_util.c``
      | ``sdk/driver/capture/it9860/capture.c``
      | ``sdk/driver/capture/it9860/capture_hw.c``
    - 底層硬體控制函式。
    
Capture 工具介紹
------------------------

Capture 工具提供使用者能夠以 GUI 介面快速產生 capture 初始化設定檔。該工具的位於 ``<sdk_root>/tool/bin/capture.exe`` 。其操作選單說明如下：


.. flat-table::
  :header-rows: 1

  * - 選單
    - 描述
  * - Chip
    - 選用 ITE 系列型號。Ex. IT986x 即選擇 it9860。
  * - Reset
    - 回復初始設定。
  * - Save
    - 儲存當前設定，轉換成 initialize file。
  * - Load
    - 讀取已存在的 initialize file ( ``.txt`` )
  * - Capture
    - 選擇使用單路 CAPTURE。
  * - About
    - 顯示軟體版本。
    
.. image:: /_static/capture_tool.png

至於底下的細部選項說明如下：

.. flat-table::
  :stub-columns: 1
  :widths: 1 8

  * - ITU-R
    - Separator sync (BT601、BT709) or Embedded sync (BT656 、BT1120)(依照感測器送出訊號格式決定)
  * - inputData
    - 輸入訊號 n pins (依照感測器送出訊號格式決定)
  * - GPIO
    - 依照電路圖接線決定
  * - DataEnable
    - | separator sync 選擇對應 GPIO
      | embedded sync 無須選擇 GPIO，僅勾選即可
  * - Hsync
    - | separator sync 依照感測器需求設定
      | embedded sync 無須選擇 GPIO，僅勾選即可
  * - Vsync 
    - | separator sync 選擇對應 GPIO
      | embedded sync 無須選擇 GPIO，僅勾選即可
  * - Pixelclk  
    - 感測器送出的 clk，選擇對應GPIO
  * - ColorFormat   
    - 依照感測器送出的顏色格式
  * - ColorOrder    
    - 依照感測器送出的顏色排列順序
  * - CheckDE   
    - :rspan:`2` Capture 硬體自動偵測穩定才輸出資料，建議開啟
  * - CheckVSNC    
  * - CheckHSNC    
  * - Interlaced
    - | 依照感測器輸出 Interlaced or progressive   
      | 若兩者都有，請在程式碼中做自動偵測的動作
  * - Clkinvert    
    - :rspan:`1` 如果 pixel clk pin，data pin 都設對，Capture 依然 unstable，可以調整Clkinvert 、Clkdelay 來使 capture 達到 stable
  * - Clkdelay 
  * - AutoDelay    
    - 開啟硬體自動偵測 delay 功能，it9860 系列建議打開

 
Capture 模組相關 API 描述
-----------------------------------------------

ithCapInitialize
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: MMP_RESULT ithCapInitialize(void)

**描述**

此函式會將 capture 模組初始化，每次開機只需要呼叫一次即可。

 
ithCapConnect
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: MMP_RESULT ithCapConnect(CAPTURE_HANDLE *cap_handle, CAPTURE_SETTING info)

**參數**

``CAPTURE_HANDLE *ptDev``

  Capture handle。Capture 相關的設定都暫存於此。

``CAPTURE_SETTING info``

  Caputre 模組初始化設定項目。其包含以下欄位：

  .. flat-table::
    :header-rows: 1

    * - 項目 
      - 描述    
    * - inputsource 
      - | ``MMP_CAP_DEV_SENSOR`` ，數位感測器選擇此類。 
        | ``MMP_CAP_DEV_ANALOG_DECODER`` ，類比轉數位感測器選擇此類。
    * - OnflyMode_en 
      - | TURE，開啟 capture & ISP Onfly mode    
        | FALSE，Capture 資料寫入記憶體 (IT986X 系列預設使用此模式)
    * - Interrupt_en 
      - 開啟 interrupt 功能    
    * - Max_Width 
      - 前端輸入源最大寬度
    * - Max_Height 
      - 前端輸入源最大高度

**描述**

``ithCapConnect()`` 根據 info 設定 capture handle，同時將 capture 初始化設定檔設定導入，與 ``ithCapDisConnect()`` 需成對出現。
 
ithCapDisConnect
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: MMP_RESULT ithCapDisConnect(CAPTURE_HANDLE *ptDev)

**參數**

``CAPTURE_HANDLE *ptDev``

  Capture handle，capture 相關的設定都暫存於此。

**描述**

結束使用時呼叫 ``ithCapDisConnect()`` ，以重置 capture handle 並釋放記憶體。
 
ithCapTerminate
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: MMP_RESULT ithCapTerminate( void)

**描述**

停止所有硬體動作，且讓硬體初始化。
 
**回傳值**

回傳 0 表示成功，1 表示失敗。

ithCapIsFire
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: MMP_BOOL ithCapIsFire(CAPTURE_HANDLE *ptDev)

**參數**

``CAPTURE_HANDLE *ptDev``

  Capture handle，capture相關的設定都暫存於此。

**描述**

提供使用者得知目前 capture 模組的狀態。
 
**回傳值**

1 表示 fired (running)，0 表示 unfired (stop)。

ithCapGetEngineErrorStatus
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: MMP_UINT32 ithCapGetEngineErrorStatus(CAPTURE_HANDLE *ptDev, MMP_CAP_LANE_STATUS lanenum)

**參數**

``CAPTURE_HANDLE *ptDev`` 

  Capture handle，capture相關的設定都暫存於此。

``MMP_CAP_LANE_STATUS lanenum``

  指定讀取的位置(指第幾組狀態暫存器)。

**回傳值**
    
回傳一個 32 位元的數值以表示目前狀態，其中每個位元的涵義如下表：

.. flat-table::
  :header-rows: 1

  * - Bits 
    - Description   
  * - 0  
    - | 1: Hsync Stable
      | 0: Hsync Unstable  
  * - 1  
    - | 1: Vsync Stable
      | 0: Vsync Unstable
  * - 2  
    - | 1: DE Stable(X)
      | 0: DE Unstable(X)
  * - 3  
    - | 1: DE Stable(Y)
      | 0: DE Unstable(Y)
  * - 11:8  
    - | Error code 
      | [1]: Hsync loss
      | [2]: Vsync loss
      | [3]: DE loss
      | [4]: frame end error
      | [5]: capture overflow
      | [7]: frame rate change
      | [8]: time out
  * - 31  
    - | 0: no interrupt 
      | 1: interrupt 

**描述**

讀取 capture 模組目前是否有錯誤狀態。
 
ithCapParameterSetting
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: MMP_RESULT ithCapParameterSetting(CAPTURE_HANDLE *ptDev)

**參數**

``CAPTURE_HANDLE *ptDev``

  Capture handle，capture 相關的設定都暫存於此。

**描述**

將 capture handle 暫存的設定寫入 HW 暫存器。
 
**回傳值**

回傳 0 表示成功，1 表示失敗。

ithCapFire
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithCapFire(CAPTURE_HANDLE *ptDev, MMP_BOOL enable)

**參數**

``CAPTURE_HANDLE *ptDev``

  Capture handle，capture 相關的設定都暫存於此。

``MMP_BOOL enable``

 0 表示 stop；1 表示 start。

**描述**

要求 capture 模組開始擷取或停止擷取輸入的數位影像訊號。
 
ithCapRegisterIRQ
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithCapRegisterIRQ(ITHIntrHandler caphandler, CAPTURE_HANDLE *ptDev)

**參數**

``ITHIntrHandler caphandler``

  傳入中斷處理函式指標。

``CAPTURE_HANDLE *ptDev``

  Capture handle，capture 相關的設定都暫存於此。

**描述**

設定 ISR 函式，以處理中斷。
 


ithCapDisableIRQ
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithCapDisableIRQ(void)

**描述**

禁用 capture IRQ，系統將不會收到中斷通知。
 


ithCapClearInterrupt
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: MMP_UINT16 ithCapClearInterrupt(CAPTURE_HANDLE *ptDev, MMP_BOOL get_err)

**參數**

``CAPTURE_HANDLE *ptDev``

  Capture handle，capture相關的設定都暫存於此。

``MMP_BOOL get_err``

  - 1 表示當下有錯誤中斷，清除中斷旗標與重置錯誤。
  - 0 表示一般正常中斷，單純清除中斷旗標。

**描述**

清除 capture 中斷。
 


ithCapReturnWrBufIndex
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: MMP_UINT16 ithCapReturnWrBufIndex(CAPTURE_HANDLE *ptDev)

**參數**

``CAPTURE_HANDLE *ptDev``

  Capture handle，capture 相關的設定都暫存於此。

**描述**

讀取 capture 模組目前正在寫入的記憶體區塊編號(0 ~ 2)，預設使用 3 塊記憶體。
 


ithCapGetInputFrameRate
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: MMP_CAP_FRAMERATE ithCapGetInputFrameRate(CAPTURE_HANDLE *ptDev)

**參數**

``CAPTURE_HANDLE *ptDev``

  Capture handle，capture 相關的設定都暫存於此。


**描述**

回傳 capture 模組偵測到的每秒幀數(FPS)，此函式要等待 engine 訊號穩定後才可以得到正確值。
 
**回傳值**

``MMP_CAP_FRAMERATE`` ，每秒幀數(fps)



ithCapGetInputSrcInfo
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: MMP_CAP_INPUT_INFO ithCapGetInputSrcInfo(CAPTURE_HANDLE *ptDev)

**參數**

``CAPTURE_HANDLE *ptDev``

  Capture handle，capture 相關的設定都暫存於此。

**描述**

根據 capture 模組偵測到的資訊，回傳目前輸入的數位影像訊號制式，此函式要等待輸入訊號穩定後才可以得到正確值。

 
ithCapPowerUp
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithCapPowerUp(void)

**描述**

開啟 capture 模組電源。
 


ithCapPowerDown
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithCapPowerDown(void)

**描述**

關閉 capture 模組電源。
 


ithCapGetDetectedWidth
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: MMP_UINT32 ithCapGetDetectedWidth(CAPTURE_HANDLE *ptDev)

**參數**

``CAPTURE_HANDLE *ptDev``

  Capture handle，capture 相關的設定都暫存於此。

**描述**

獲取輸入 capture 模組的數位影像的寬度。

**回傳值**

回傳 capture 模組偵測到的寬度。

 


ithCapGetDetectedHeight
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: MMP_UINT32 ithCapGetDetectedHeight(CAPTURE_HANDLE *ptDev)

**參數**

``CAPTURE_HANDLE *ptDev``

  Capture handle，capture 相關的設定都暫存於此。

**描述**

獲取輸入 capture 模組的數位影像的高度。

**回傳值**

回傳 capture 模組偵測到的高度。

 

ithCapGetDetectedInterleave
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: MMP_UINT32 ithCapGetDetectedInterleave(CAPTURE_HANDLE *ptDev)

**參數**

``CAPTURE_HANDLE *ptDev``

  Capture handle，capture相關的設定都暫存於此。

**描述**

回傳 capture 模組偵測到的訊號源傳送格式。

**回傳值**

0 表示訊號源為循序式掃瞄，1 表示訊號源為交錯式掃瞄。


 
ithCapSetInterleave 
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithCapSetInterleave(   CAPTURE_HANDLE *ptDev, MMP_UINT32 interleave)

**參數**

``CAPTURE_HANDLE *ptDev``

  Capture handle，capture 相關的設定都暫存於此。


``interleave``

  0 表示循序式掃瞄，1 表示交錯式掃瞄。

**描述**

強制設定 capture 模組為循序式掃瞄或交錯式掃瞄。
 


感測器控制相關 API 描述
--------------------------

ithCapDeviceAllDeviceTriState
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithCapDeviceAllDeviceTriState(void)

**描述**

停止感測器傳送訊號。
 


ithCapDeviceInitialize
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: MMP_RESULT ithCapDeviceInitialize(void)

**描述**

開啟電源，初始化感測器。
 


ithCapDeviceTerminate
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithCapDeviceTerminate(void)

**描述**

結束感測器，關閉電源。
 


ithCapDeviceIsSignalStable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: MMP_BOOL ithCapDeviceIsSignalStable(void)

**描述**

回傳感測器訊號是否穩定。

**回傳值**

0 表示無訊號，1 表示訊號穩定。

 
ithCapGetDeviceInfo
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithCapGetDeviceInfo(CAPTURE_HANDLE *ptDev)

**參數**

``CAPTURE_HANDLE *ptDev``

  Capture handle，capture相關的設定都暫存於此。


**描述**

參照感測器驅動回傳的值，將對應的參數設定至 capture handle。
 
ithCapDeviceGetProperty
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: MMP_UINT16 ithCapDeviceGetProperty(DEVICESPROPERTYS option)

**參數**

``DEVICESPROPERTYS option``

  感測器屬性類別。

**描述**

回傳對應的感測器屬性。

**回傳值**

回傳對應的感測器屬性。
 


ithCapDeviceLEDON
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithCapDeviceLEDON(MMP_BOOL enable)

**參數**

``MMP_BOOL enable``

  0 表示關閉燈源，1 表示開啟燈源。

**描述**

控制感測器燈源。
 


ithCapDeviceCHSwitch
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithCapDeviceCHSwitch(MMP_UINT16 ch)

**參數**

``MMP_UINT16 ch``

  設定通道編號。

**描述**

切換通道，僅供支援多通道感測器使用。
 

完整範例展示
-------------------------

硬體部分
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

ITE提供兩種子板，供範例測試

.. flat-table::
  :header-rows: 1

  * - 感測器種類  
    - 子板    
  * - 數位感測器  
    - VGA/720p Camera Module  
  * - 類比解碼晶片  
    - AHD Decoder Module
  
使用 VGA/720p Camera Module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

根據下圖將 VGA/720p Camera Module 連接至 Standard EVB。

.. image:: /_static/e_circuit_capture_vga.png

.. image:: /_static/e_circuit_capture_vga_sensor.png
  
使用 AHD Decoder Module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

根據下圖將AHD Decoder Module連接至Standard EVB

.. image:: /_static/e_circuit_capture_ahd.png

.. image:: /_static/e_circuit_capture_ahd_sensor.png

.. note::

  此測試還需要 MIPI Panel，若您尚未測試過 MIPI Panel 請先參考 :ref:`lcd` 章節。

Kconfig設定
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

請執行 ``<sdk_root>/build/openrtos/test_cap.cmd`` 。接著請根據所使用的 Camera 模組採用不同的 Kconfig 設定。

使用 VGA/720p Camera Module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

請根據以下步驟採用不同的Kconfig設定：

1. :menuselection:`Peripheral --> Capture initial --> DEMO_9860_DEFAULT.txt` 。
2. :menuselection:`Peripheral --> Sensor Enable --> Sensor --> NT99141.c` 。
3. :menuselection:`Peripheral --> Sensor Enable --> Flick --> 50HZ` 。
4. :menuselection:`Peripheral --> I2C0 Enable` 。
5. :menuselection:`Peripheral --> Sensor Enable --> IIC_PORT_SEL --> IIC0` 。
6. :menuselection:`Peripheral --> Sensor Enable --> RESETPIN Enable` 。
7. :menuselection:`Peripheral --> Sensor Enable --> POWERDOWNPIN Enable` 。
8. :menuselection:`GPIO --> IIC0 CLK GPIO Pin --> 41` 。
9. :menuselection:`GPIO --> IIC0 DATA GPIO Pin --> 40` 。
10. :menuselection:`GPIO --> SENSOR RESET GPIO Pin --> 43` 。
11. :menuselection:`GPIO --> SENSOR POWER DOWN GPIO Pin --> 42` 。

使用AHD Decoder Module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. :menuselection:`Peripheral --> Capture initial --> DEMO_9860_RN6752.txt` 。
2. :menuselection:`Peripheral --> Sensor Enable --> Sensor --> RN6752.c` 。
3. :menuselection:`Peripheral --> Sensor Enable --> Flick --> Don’t care` 。
4. :menuselection:`Peripheral --> I2C0 Enable` 。
5. :menuselection:`Peripheral --> Sensor Enable --> IIC_PORT_SEL --> IIC0` 。
6. :menuselection:`Peripheral --> Sensor Enable --> RESETPIN Enable` 。
7. :menuselection:`Peripheral -->Sensor Enable --> POWERDOWNPIN Enable` 。
8. :menuselection:`GPIO --> IIC0 CLK GPIO Pin --> 41` 。
9. :menuselection:`GPIO --> IIC0 DATA GPIO Pin --> 40` 。
10. :menuselection:`GPIO --> SENSOR RESET GPIO Pin --> 43` 。
11. :menuselection:`GPIO --> SENSOR POWER DOWN GPIO Pin --> 42` 。

相關的程式碼檔案
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. flat-table::
  :header-rows: 1

  * - 路徑  
    - 描述    
  * - ``<sdk_root>/project/test_cap/test_modules.c``  
    - test_cap 主程式，其呼叫 ``capdev_play()`` 函式控制 capture 擷取畫面，並透過 ISP 將畫面縮放至特定大小，再經 2D 引擎將畫面與 UI 合成並顯示至 LCD。   
  * - ``<sdk_root>/share/ffmpeg/castor3player.c``  
    - | 控制 capture 流程的播放器。
      | 其中包含與開始流程有關的API如下：
      | ``mtal_pb_init()``
      | ``mtal_pb_select_file()``
      | ``mtal_pb_play()``
      |
      | 與結束流程有關的API如下：
      | ``mtal_pb_stop()``
      | ``mtal_pb_exit()``


castor3player.c 使用說明 (Capture 播放使用)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

底下針對 ``castor3player.c`` 檔案內所使用的 API 做個簡略的描述，詳細 API 用法請看 :ref:`video_play_api` 。

mtal_pb_init
""""""""""""""""""

呼叫 ``mtal_pb_init`` 函式時需傳入一個 callback 函式的指標。該 callback 函式的定義如下：

.. code-block:: c

  typedef void (*cb_handler_t)(PLAYER_EVENT event_id, void *arg);

其中 ``event_id`` 可以是以下數值之一：

.. code-block:: c

    typedef enum
    {
        PLAYER_EVENT_EOF,
        PLAYER_EVENT_OPEN_FILE_FAIL,
        PLAYER_EVENT_UNSUPPORT_FILE,
        PLAYER_EVENT_CAPTURE_DEV
    } PLAYER_EVENT;

不過在 capture 的應用中，只會回傳 ``PLAYER_EVENT_CAPTURE_DEV`` 以表示 capture 發生錯誤，需重新啟動 player。

mtal_pb_select_file 
""""""""""""""""""""""""

呼叫 ``mtal_pb_select_file`` 函式時需傳入一個 ``MTAL_SPEC`` 結構型別的參數。該結構的定義如下：

.. code-block:: c

    typedef struct
    {
        int vol_level;
        int prev_file_pos;
        int camera_in;
        char srcname[MTAL_MAX_CSTRING_LEN]; 
    } MTAL_SPEC;

在 capture 的應用中，其中每個欄位應填入的值如下：

.. flat-table::
  :header-rows: 1  

  * - 欄位名稱
    - 說明  
  * - camera_in
    - 必須選擇 CAPTURE_IN
  * - srcname
    - 可以切換不同通道
  * - vol_level
    - Don’t care
  * - prev_file_pos
    - Don’t care

底下是本函式的簡易操作範例：

.. code-block:: c

  char filepath[5] = {'C', 'H', '0'};
  mtal_pb_init(EventHandler);
  mtal_spec.camera_in = CAPTURE_IN; // select capture device player
  strcpy(mtal_spec.srcname, filepath); // select input channel 0
  mtal_pb_select_file(&mtal_spec);
  mtal_pb_play();

mtal_pb_play    
""""""""""""""""""""""""

開始擷取影像。

mtal_pb_stop    
""""""""""""""""""""""""

停止擷取影像。

mtal_pb_exit  
""""""""""""""""""""""""

關閉播放器，釋放記憶體。

  .. |run_icon| image:: /_static/qconf_run_icon.png
    :alt: run icon
    :width: 14
    :height: 14
    

  .. |build_icon| image:: /_static/qconf_build_icon.png
    :alt: build icon
    :width: 14
    :height: 14

驗證結果
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. 點擊 |build_icon| 按鈕建置專案。確保：

  - USB to SPI 轉換板已正確安裝。
  
    - 若您尚未將 USB to SPI 轉換板與 PC 連接，請參考 :ref:`usb2spi_connect2pc` 。
    - 若您尚未將 USB to SPI 轉換板與 EVB 連接，請參考 :ref:`usb2spi_connect2board` 。
    - 若您尚未安裝 USB to SPI 轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。
  
  - EVB 已經切換為 Co-operative Mode。
  
    .. note:: 
  
       若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

2. 執行 Tera Term 或任何 Serial Port 軟體。
3. 點擊 |run_icon| 按鈕即可透過 USB to SPI 轉換板啟動系統。
 
.. image:: /_static/teraterm_capture_example_log.png

.. image:: /_static/e_circuit_capture_example.png
 
新增感測器
^^^^^^^^^^^^^^^^^^^^^^^^

使用者若想要新增自定義感測器可以實作在 ``<sdk_root>/driver/sensor/userdefine.c`` 檔案中。

.. attention::

  注意 ``<sdk_root>/include/capture/video_device_table.h`` 檔案中的 ``VIDEO_TABLE[]`` 陣列是否已包含對應的解析度設定。

  .. code-block:: c

    static CAP_NORMAL_TIMINFO_TABLE VIDEO_TABLE [] = {
        ...                                                                                                                                       
    };


.. raw:: latex

    \newpage
