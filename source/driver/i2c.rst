﻿.. _i2c:

I2C
=======

功能描述
--------------------

IT986x 系列支援 4 組 I2C 模組，皆可做為 I2C Master/Slave 使用。每組 I2C 模組均支援 standard (100 kbit/s) 和 fast modes (400 kbits/s)，以及 7 bit addressing mode。

相關的 KConfig 設定
----------------------------------------------------------------

- :menuselection:`Peripheral --> I2C# Enable`

  決定是否要啟用 I2C# 裝置。勾選此選項後，系統才會在 ``itpInit()`` 函式內透過 ``itpRegisterDevice()`` 函式註冊對應 I2C# 裝置並進行初始化動作。

- :menuselection:`Peripheral --> I2C# Enable --> I2C# Clock Rate`

  決定 I2C# 裝置的 clock rate。預設為 400kHz。

- :menuselection:`GPIO --> IIC# CLK GPIO Pin`

  指定該 I2C 模組要使用哪個 GPIO pin 作為 Clock pin。

- :menuselection:`GPIO --> IIC# DATA GPIO Pin`

  指定該 I2C 模組要使用哪個 GPIO pin 作為 Data pin。


相關的程式碼檔案
----------------------------


.. flat-table::
   :header-rows: 1

   * - 路徑
     - 描述
   * - ``<sdk_root>/sdk/driver/itp/itp_i2c.c``
     - 最上層函式，主要提供 POSIX 規範實作的 API，供上層 AP 透過 ``open()``、``close()``、``ioctl()`` 等函式對裝置進行操作。
   * - | ``<sdk_root>/sdk/driver/iic/*``
       | ``<sdk_root>/sdk/include/iic/*``
     - 底層函式，未來可能會變動。


相關的 API 描述
---------------------------------------

本節描述用來操作 I2C 裝置的相關 API。I2C driver 提供依據 POSIX 規範實作的 API，用戶可以透過 ``open()``/``read()``/``write()``/``ioctl()`` 等函式對 I2C 裝置進行如讀寫檔案般的操作。

itpRegisterDevice
^^^^^^^^^^^^^^^^^

.. c:function:: void itpRegisterDevice(ITPDeviceType type, const ITPDevice *device)

  註冊裝置。

**參數**

``ITPDeviceType type``

  裝置類型。完整裝置類型列表請見 ``<sdk_root>/sdk/include/ite/itp.h`` 。與 I2C 裝置相關的裝置類型有 ``ITP_DEVICE_I2C0`` ~ ``ITP_DEVICE_I2C3`` 。

``const ITPDevice *device``

  裝置標識符。與 I2C 裝置相關的裝置標識符有 ``itpDeviceI2c0`` ~ ``itpDeviceI2c3`` 。

**描述**

此函式可用來將 I2C# 裝置註冊到核心裝置列表中，使上層 AP 可以透過 ``ioctl``/``read``/``write`` 等函式來操作 I2C 的功能。本函式會在系統初始化期間被 ``itpInit()`` 函式呼叫 (見 ``<sdk_root>/sdk/driver/itp_init_openrtos.c`` )，因此若在您的專案中有呼叫過 ``itpInit()`` 函式，將無須再執行此註冊函式。

.. note::

  建議呼叫 I2C API 時盡量使用 ``ioctl``/``read``/``write`` 的方式，不要直接呼叫 I2C 底層驅動。

底下展示如何註冊 I2C2 裝置到系統核心。

.. code-block:: c

    itpRegisterDevice(ITP_DEVICE_I2C0, &itpDeviceI2c2);


ioctl
^^^^^^^^^^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr)

  控制裝置。

**參數**

``int file``

  裝置標識符。其值為 ``ITPDeviceType`` 列表中的值。與 I2C 裝置相關的裝置標識符有 ``ITP_DEVICE_I2C0`` ~ ``ITP_DEVICE_I2C3`` 。

``unsigned long request``

  request 參數用來選擇要對裝置運行的操作功能，並將取決於所指定的裝置。

``void *ptr``

  ptr 參數表示對此特定裝置執行請求的功能所需的額外訊息。ptr 的型別取決於特定的控制請求，但它可以是整數，也可以是指向裝置特定資料結構的指標。

**描述**

``ioct()`` 函數用於在 I2C 裝置上執行各種控制功能。request 參數和可選的第三個參數（具有不同的型別）將傳遞給與 file 關聯的 I2C 裝置的相應部分並由其解釋。

- ITP_IOCTL_INIT

  用來初始化 I2C 裝置。必須透過 ptr 參數傳入欲將該組 I2C 設定為 Master 或是 Slave mode。

  底下展示如何透過預設設定以 Master mode 初始化 I2C2 裝置並以 Slave mode 初始化 I2C3 裝置。

  .. code-block:: c

    IIC_MODE iic_port2_mode = MASTER_MODE;
    ioctl(ITP_DEVICE_I2C2, ITP_IOCTL_INIT, (void *)iic_port2_mode);
    IIC_MODE iic_port3_mode = IIC_SLAVE_MODE;
    ioctl(ITP_DEVICE_I2C3, ITP_IOCTL_INIT, (void*)iic_port3_mode);

- ITP_IOCTL_RESET

  同 ITP_IOCTL_INIT。只是 ITP_IOCTL_RESET 會先 Terminate I2C 裝置在對其重新初始化。因此其 ptr 參數的型別和用途與 ITP_IOCTL_INIT 的 ptr 參數的型別和用途完全相同。

  底下展示如何透過預設設定 Master mode 和使用 ``ioctl()`` 函式來重置 I2C0 裝置。

  .. code-block:: c

    IIC_MODE iic_port2_mode = MASTER_MODE;
    ioctl(ITP_DEVICE_I2C2, ITP_IOCTL_RESET, (void *)iic_port2_mode);

open
^^^^^^^^^^^^^^^^^

.. c:function:: int open(const char * name, int value)

  開啟裝置。

**參數**

``const char name``

  欲開啟裝置名稱。EX: ``ITP_DEVICE_I2C2`` 。

``int value``

  待定義。

**描述**

使用者可透過此函式開啟 I2C 埠。

底下展示如何使用函式。

.. code-block:: c

  int gMasterDev = 0;
  gMasterDev = open(":i2c2", 0);

read
^^^^^^^^^^^^^^^^^

.. c:function:: int read(int __fd, void *__buf, size_t __nbyte)

  從 Slave 端讀取資料。

**參數**

``int __fd``

  欲執行的 I2C 埠。EX: ``ITP_DEVICE_I2C2`` 。

``void *__buf``

  讀取資料所使用結構的位置。EX:

  .. code-block:: c

    typedef struct
    {
        uint8_t     slaveAddress; //slave address
        uint8_t*    cmdBuffer; 	  //send command buffer
        uint32_t    cmdBufferSize; //send command size
        uint8_t*    dataBuffer;   //receive data buffer
        uint32_t    dataBufferSize; //receive data size
    } ITPI2cInfo;

``size_t __nbyte``

  待定義。

**描述**

  使用者可透過此函式從特定 I2C 埠中讀回資料。

底下展示如何使用函式。

.. code-block:: c

    ITPI2cInfo evt;
    evt.slaveAddress   = 0x77;
    evt.cmdBuffer      = 0;
    evt.cmdBufferSize  = 0;
    evt.dataBuffer     = recvBuffer;
    evt.dataBufferSize = 256;
    read(gMasterDev, &evt, 1);


write
^^^^^^^^^^^^^^^^^

.. c:function:: int write(int __fd, const void *__buf, size_t __nbyte)

  從 Master 端發送資料。

**參數**

``int __fd``

  欲執行的 I2C 埠。EX: ``ITP_DEVICE_I2C2`` 。

``void *__buf``

  發送資料所使用結構的位置。EX:

  .. code-block:: c

    typedef struct
    {
        uint8_t     slaveAddress; //slave address
        uint8_t*    cmdBuffer; 	  //send command buffer
        uint32_t    cmdBufferSize; //send command size
        uint8_t*    dataBuffer;   //receive data buffer
        uint32_t    dataBufferSize; //receive data size
    } ITPI2cInfo;

``size_t __nbyte``

    待定義。

**描述**

使用者可透過此函式從特定I2C埠中送出資料。

底下展示如何使用函式。

.. code-block:: c

    ITPI2cInfo evt;
    IicMasterWriteBuffer[0] = 0xAD;
    IicMasterWriteBuffer[1] = 0xB8;
    IicMasterWriteBuffer[2] = 0xB7;
    IicMasterWriteBuffer[3] = 0xB6;
    IicMasterWriteBuffer[4] = 0xB5;

    evt.slaveAddress   = 0x77;
    evt.cmdBuffer      = IicMasterWriteBuffer;
    evt.cmdBufferSize  = 5;
    evt.dataBuffer     = 0;
    evt.dataBufferSize = 0;
    write(gMasterDev, &evt, 1);


完整範例展示
---------------------------------------

在 ``<sdk_root>/project/test_iic`` 目錄下附有一個 I2C 範例程式。該程式展示了一個 I2C2 和 I2C3 互相讀寫功能。預設將 I2C2 設為 Master mode，I2C3 設為 Slave mode，以一個迴圈的方式反覆從 I2C2 送出 5 個byte的指令到 Slave address 為 0x77 的 I2C3 裝置，並讀回 256 個 byte 的回傳資料。

I2C 的溝通主要是透過兩根 pin 腳讀寫資料，分別為 CLOCK 和 DATA pin，所以板端須將 I2C2 的 GPIO_SDA 和 I2C3 的 GPIO_SDA 對接，以及將 I2C2 的 GPIO_SCL 和 I2C3 的 GPIO_SCL 對接。

I2C clock rate 預設為 400kHz，需視對接裝置可支援的頻率做調整。

硬體連接
^^^^^^^^^^^^^^^^^^

將 CLK GPIO61 和 GPIO41 對接，DATA GPIO62 和 GPIO42 對接。所使用 GPIO 皆須 PULL HIGH。

.. image:: /_static/e_circuit_i2c_left.png

.. image:: /_static/e_circuit_i2c_right.png

KConfig 設定
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

請執行 ``<sdk_root>/build/openrtos/test_iic.cmd`` ，並按照以下步驟設定：

- 勾選 :menuselection:`Peripheral --> I2C2 Enable` 。
- 勾選 :menuselection:`Peripheral --> I2C3 Enable` 。

  .. image:: /_static/qconf_check_i2c_enable.png

接下來設定 IIC2 和 IIC3 使用到的 pin。

- 將 GPIO --> IIC2 CLK Pin 設為 61。
- 將 GPIO --> IIC2 DATA Pin 設為 62。
- 將 GPIO --> IIC3 CLK Pin 設為 41。
- 將 GPIO --> IIC3 DATA Pin 設為 42。

  .. image:: /_static/qconf_gpio_i2c.png

驗證結果
^^^^^^^^^^^^

  .. |build_icon| image:: /_static/qconf_build_icon.png
    :alt: build icon
    :width: 14
    :height: 14

  .. |run_icon| image:: /_static/qconf_run_icon.png
    :alt: run icon
    :width: 14
    :height: 14

1. 點擊 |build_icon| 按鈕建置專案。確保：

  - USB to SPI 轉換板已正確安裝。

    - 若您尚未將 USB to SPI 轉換板與 PC 連接，請參考 :ref:`usb2spi_connect2pc` 。
    - 若您尚未將 USB to SPI 轉換板與 EVB 連接，請參考 :ref:`usb2spi_connect2board` 。
    - 若您尚未安裝 USB to SPI 轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

  - EVB 已經切換為 Co-operative Mode。

    .. note::

       若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

2. 重置 EVB 的電源。
3. 等程式建置完成，點擊 |run_icon| 按鈕即可透過 USB to SPI 轉換板啟動系統。
4. 執行 Tera Term，點擊 Tera Term 主選單的 :menuselection:`Setup --> Serial Port` 。Port 的部分請選擇 USB to TTL 轉換板對應的 com port。Speed 的部分請選擇 115200。
5. 測試執行結果如下，發送端送出 5 個 byte 資料，同時收到 256 個 byte 資料做比對是否成功，且反覆執行該流程。

  .. image:: /_static/console_i2c_test_result.png

.. raw:: latex

    \newpage
