.. _wifi:

Wifi
================================================================

功能描述
-------------------------------------

IT986x 支援 SDIO 介面的數種 WiFi 模組，預設通用模組為瑞昱的 RTL 8189FTV，支援頻段 2.4GHz，授權方式為 Non-GPL。每種 WiFi 模組均支援兩種模式：

- STA 模式：將裝置設定為客戶端(Client)，選擇存在的無線熱點(Access Point)並連線。

  .. note::

    某些應用功能需要熱點有網際網路能力。

- HostAP 模式：將裝置設定為無線熱點(Access Point)，可被其它客戶端設備連接

  兩種模式可以透過 API 互相切換，但無法同時存在。

.. _wifi_kconfig:

相關的 KConfig 設定
----------------------------------------------------------------

- :menuselection:`Network --> Enable WiFi module`

  決定是否要啟用 WiFi 裝置，若決定啟用則會出現子選項。

- :menuselection:`WiFi Interface --> SDIO WiFi device`

  此為啟用 WiFi 後的子選項，先選擇介面、再選擇對應介面的模組(IT986x 系列不提供 USB 介面的 WiFi)。注意!! "SDIO WiFi device" 選項的出現，除了 "WiFi Interface" 外，還必需勾選某些前置選項，通常是在 "Peripheral" 的介面選項，下面將會說明。

- :menuselection:`Storage --> SD1 Device --> Peripheral --> SDIO Enable --> SDIO 1 as static device`

  這是 RTL 8189FTV 的前置選項，由於該模組是 SDIO 介面，必須將 SDIO 設備打開才得以掛載驅動程式，否則 "SDIO WiFi device" 的選項為空。

- :menuselection:`Network --> Enable WiFi SDIO Power OnOff User Defined`

  IT986x 有規定 SDIO 介面 WiFi 的 Power Pin，開啟則可以自訂給電時機、不開啟則為開機立刻給電。

- :menuselection:`GPIO --> SD1 WiFi Card Power Enable --> SD1 WiFi Power Pin`

  填入該 WiFi 模組的電源 GPIO 編號。

- :menuselection:`GPIO --> SD1 WiFi Card Power Enable --> SD1 WiFi Detect Pin Active High`

  模組 on board 預設為高電平，沒特殊需求的話就打勾即可。

- :menuselection:`GPIO --> SD1 I/O Pin`

  填入該 WiFi 模組的 GPIO 編號，依序為 CLK, CMD, SD_D0, SD_D1, SD_D2, SD_D3。

相關的程式碼檔案
---------------------------

.. flat-table::
  :header-rows: 1
  
  * - 路徑
    - 描述
  * - ``sdk/driver/itp/itp_wifi.c``
    - 中介函式，進行 LWIP 網路介面的設置、綁訂操作，並供上層 AP 如 WifiMgr 透過 ``ioctl()`` 對裝置進行操作(客戶端開發人員一般不直接使用)。
  * - ``sdk/driver/non_gpl_wifi``
    - RTL 8189FTV 驅動程式
  * - ``sdk/share/lwip``
    - TCP/IP Stack
  * - ``sdk/share/Wifi_Mgr``
    - WiFi 操作管理程式與 API，實作各種 WiFi 功能與運作。開發應用程式如有需要操作 WiFi，不需要直接呼叫 ioctl，在 WifiMgr 皆已包裝成 API，使用這裡所提供的 API 即可，以上定義在 ``WifiMgr.h`` 。
 
相關的 API 描述
---------------------------

本節描述用來操作 WiFi 裝置的常用 API。WifiMgr 有各種 WiFi 基本控制的 API 供開發人員操作，例如 ``WifiMgr_Init()``/``WifiMgr_Sta_Connect()``/``WifiMgr_Get_Scan_Info()`` 等函式對 WiFi 裝置進行如設定模式、連線、掃描等等操作，以下將介紹其中幾種常用 API。

WifiMgr_Init
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int WifiMgr_Init(WIFIMGR_MODE_E init_mode, int mp_mode, WIFI_MGR_SETTING wifiSetting)

  初始化WiFi裝置。

**參數**

``WIFIMGR_MODE_E Init_Mode``

  初始化 WiFi 模式。STA 模式為 0、HostAP 模式為 1。

``Int Mp_Mode``

  開發者模式。關閉為 0、開啟為 1，一般直接設為 0，不需要進入該模式。

``WIFI_MGR_SETTING WifiSetting``

  應用層 WiFi 數據結構，存在 INI 的資料會將對應的資訊如 SSID、PW 寫入該結構中。

**描述**

此函式為管理程式 WifiMgr 與 WiFi 運作模式的初始化，執行此函式後即可使用 WifiMgr 提供的 API 操作裝置。

**使用範例**

.. code-block:: c

    static WIFI_MGR_SETTING gWifiSetting;

    void NetworkWifiInit(void)
    {
        int ret;
        ret = WifiMgr_Init(WIFIMGR_MODE_CLIENT, 0, gWifiSetting);
    } 

WifiMgr_Get_Scan_AP_Info
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int WifiMgr_Get_Scan_AP_Info(WIFI_MGR_SCANAP_LIST* pList)

  掃描並取得SSID列表。

**參數**

``WIFI_MGR_SCANAP_LIST  *pList``

  熱點資訊結構。掃描後會將目前獲取到的資訊存在該結構中，最多儲存64個熱點資訊。

**描述**

使用該 API 進行掃描，並且將資訊存起來，回傳值為目前取得熱點個數。

資訊包括 SSID、MAC、Security、Signal Power 等等，最多儲存 64 組，如希望在 Log 顯示這些資訊，請在 ``WifiMgr.h`` 打開 ``WIFIMGR_SHOW_SCAN_LIST`` 。

**使用範例**

.. code-block:: c

    static WIFI_MGR_SCANAP_LIST  pList[64];

    void scan_ap (void)
    {
        int get_scan_count = 0;
        get_scan_count = WifiMgr_Get_Scan_AP_Info(pList);
    }

 
WifiMgr_Sta_Connect
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int WifiMgr_Sta_Connect(char* ssid, char* password, char* secumode)

  連接熱點。

**參數**

``char* ssid, char* password, char* secumode``

  連接熱點所需要的 SSID、密碼與安全性加密代號

**描述**

使用該 API 連接熱點，輸入欲連接熱點的 SSID、密碼與加密代號，加密代號定義在 ``WifiMgr.h`` ，前缀字為 ``ITE_WIFI_SEC_XXX`` 。如果不知道該熱點之加密型態，一般路由器的加密為 WPA2，可以直接填 "7" 即可，即使填錯也沒關係，該 API 會自行更新。

**使用範例**

.. code-block:: c

    /* Connect Info */
    #define SSID "ITE_GUEST"
    #define PW   "12345678"
    #define SEC  "7"

    void link_ap (void)
    {
        int ret = -1;
        ret = WifiMgr_Sta_Connect(SSID, PW, SEC);
    }
 
WifiMgr_Sta_HostAP_Switch
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int WifiMgr_Sta_HostAP_Switch(WIFI_MGR_SETTING wifiSetting)

  STA 模式與 HostAP 模式切換。

**參數**

``WIFI_MGR_SETTING  WifiSetting``

  應用層 WiFi 數據結構，存在 INI 的資料會將對應的資訊如 SSID、PW 寫入該結構中。

**描述**

不重開機即可讓兩種模式互相切換

**使用範例**

.. code-block:: c

    void NetworkWifiModeSwitch(void)
    {
        int ret;
        ret = WifiMgr_Sta_HostAP_Switch(gWifiSetting);
    }
 
回調函數
-------------------

原型
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: c

    typedef enum tagWIFIMGR_STATE_CALLBACK_E
    {
        WIFIMGR_STATE_CALLBACK_CONNECTION_FINISH = 0,
        WIFIMGR_STATE_CALLBACK_CLIENT_MODE_DISCONNECT_30S,
        WIFIMGR_STATE_CALLBACK_CLIENT_MODE_RECONNECTION,
        WIFIMGR_STATE_CALLBACK_CLIENT_MODE_TEMP_DISCONNECT,
        WIFIMGR_STATE_CALLBACK_CLIENT_MODE_CONNECTING_FAIL,
        WIFIMGR_STATE_CALLBACK_CLIENT_MODE_CONNECTING_CANCEL,
        WIFIMGR_STATE_CALLBACK_CLIENT_MODE_SLEEP_SAVE_INFO,
        WIFIMGR_STATE_CALLBACK_CLIENT_MODE_SLEEP_CLEAN_INFO,
        WIFIMGR_STATE_CALLBACK_SWITCH_CLIENT_SOFTAP_FINISH,
        WIFIMGR_STATE_CALLBACK_MAX,
    } WIFIMGR_STATE_CALLBACK_E;

描述
^^^^^^^^^^^^^^^^^^^^^^^^^^^

- WIFIMGR_STATE_CALLBACK_CONNECTION_FINISH

  意義、狀態建議如下

  - 意義：Sta/AP mode 連線完成，Sta mode連線完成表示已經連線上路由器，AP mode 連線完成表示 AP 已經開啟。
  - 狀態建議：如果有功能需要再連線完成時反應，可在此處添加代碼。

- WIFIMGR_STATE_CALLBACK_CLIENT_MODE_DISCONNECT_30S

  意義、狀態建議如下

  - 意義：Sta mode 連線的路由器因某種原因斷線(比如說關機)，中斷連線超過 30 秒，觸發此 callback。
  - 狀態建議：可以中斷連線或是等一段時間再重新連線。

- WIFIMGR_STATE_CALLBACK_CLIENT_MODE_TEMP_DISCONNECT

  意義、狀態建議如下

  - 意義：Sta mode 連線的路由器因某種原因斷線(比如說關機)，中斷連線未超過 30 秒，觸發此 callback。
  - 狀態建議：無。

- WIFIMGR_STATE_CALLBACK_CLIENT_MODE_RECONNECTION

  意義、狀態建議如下

  - 意義：Sta mode 連線的路由器因某種原因斷線(比如說關機)，中斷連線未超過 30 秒，重新連接上路由器，觸發此 callback。
  - 狀態建議：無。

- WIFIMGR_STATE_CALLBACK_CLIENT_MODE_RECONNECTION

  意義、狀態建議如下

  - 意義：Sta mode 連線的路由器因某種原因斷線(比如說關機)，中斷連線未超過 30 秒，重新連接上路由器，觸發此 callback。
  - 狀態建議：無。

- WIFIMGR_STATE_CALLBACK_CLIENT_MODE_CONNECTING_FAIL

  意義、狀態建議如下

  - 意義：Sta mode 連線的路由器因某種原因連線失敗，觸發此 callback。
  - 狀態建議：提示連線錯誤。

- WIFIMGR_STATE_CALLBACK_CLIENT_MODE_CONNECTING_CANCEL

  意義、狀態建議如下

  - 意義：Sta mode 連線的路由器連線中，裝置主動中斷連線，觸發此 callback。
  - 狀態建議：無。

- WIFIMGR_STATE_CALLBACK_CLIENT_MODE_SLEEP_SAVE_INFO

  意義、狀態建議如下

  - 意義：板子進入睡眠模式時會儲存目前已連線的 SSID / PW / SEC。
  - 狀態建議：提示資訊。

- WIFIMGR_STATE_CALLBACK_CLIENT_MODE_SLEEP_CLEAN_INFO

  意義、狀態建議如下

  - 意義：呼叫 ``WifiMgr_Terminat`` 時把 ``gWifiSetting`` 的資訊全部清空。
  - 狀態建議：提示資訊。

- WIFIMGR_STATE_CALLBACK_SWITCH_CLIENT_SOFTAP_FINISH

  意義、狀態建議如下

  - 意義：Sta/AP 兩種模式切換的結束通知。
  - 狀態建議：切換之後的代碼判斷。

簡易範例程式
----------------------------

.. code-block:: c

    #include <pthread.h>
    #include <sys/ioctl.h>
    #include "openrtos/FreeRTOS.h"
    #include "openrtos/task.h"

    void* TestFunc(void* arg)
    {
        pthread_t task_ap;
        pthread_attr_t attr_ap;

        printf("====>NGPL: init itp\n");
        itpInit();
        usleep(5*100*1000);

        gWifiSetting.wifiCallback = CallbackFucntion;
        PreSettingWifi();

    #ifdef CFG_NET_WIFI_SDIO_POWER_ON_OFF_USER_DEFINED
        WifiPowerOn();
    #endif

        WifiMgr_Sta_Switch(1);
        WifiMgr_Init(WIFIMGR_MODE_CLIENT, 0, gWifiSetting);

        for (;;)
        {
            sleep(1);
        }
    }

    int main(void)
    {
        pthread_t task;
        pthread_attr_t attr;

        pthread_attr_init(&attr);
        pthread_create(&task, &attr, TestFunc, NULL);

        /* Now all the tasks have been started - start the scheduler. */
        vTaskStartScheduler();

        /* Should never reach here! */
        return 0;
    }

.. _wifi_complete_example:

完整範例展示
----------------------------

在 ``<sdk_root>/project/test_wifi_sdio`` 目錄下附有一個 WiFi 範例主程式 ``test_wifi_ngpl_lwip`` 。該程式展示了一個簡單的 WiFi 初始化流程，並且有將一些副程式可供測試，例如 PING、Socket Client 測試、DNS 解析等等，只需要在目錄下的 ``test_config.h`` 開啟對應功能。客戶也可以在主程式中加入自己的程式碼、完成想做的實驗。

一些定義請集中在 ``test_config.h`` 做修改：

連線資訊
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: c

    /* Connect Info */
    #define SSID "ITE_GUEST"
    #define PW   "12345678"
    #define SEC  "7"

測試功能開關
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: c

    #define test_iperf          0 // Make sure you have iperf lib and open HTTP
    #define tcp_client_test     0
    #define test_connect        0 // Don't OPEN while test iperf or socket
    #define test_scan_ap        0
    #define test_dns            0
    #define test_ping           0

硬體連接
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

如使用 Daughter board 外接模組，GPIO 的接法請參考文件 ``IT986x_MINI USER GUIDE_SDIO.pdf``，也請重新檢查 Kconfig 的 GPIO 編號設定。

KConfig 設定
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

請執行 ``<sdk_root>/build/openrtos/test_wifi_sdio.cmd`` ，所有設定都已經設定好，欲知詳情可參考目錄下的 Kconfig 或 :ref:`wifi_kconfig` ，如無特別需求即可直接編譯:

驗證結果
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  .. |build_icon| image:: /_static/qconf_build_icon.png
    :alt: build icon
    :width: 14
    :height: 14

  .. |run_icon| image:: /_static/qconf_run_icon.png
    :alt: run icon
    :width: 14
    :height: 14

1. 點擊 |build_icon| 按鈕建置專案。確保：

  - USB to SPI 轉換板已正確安裝。

    - 若您尚未將 USB to SPI 轉換板與 PC 連接，請參考 :ref:`usb2spi_connect2pc` 。
    - 若您尚未將 USB to SPI 轉換板與 EVB 連接，請參考 :ref:`usb2spi_connect2board` 。
    - 若您尚未安裝 USB to SPI 轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

  - EVB 已經切換為 Co-operative Mode。

    .. note::

       若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

2. 天線已安裝
3. 重置 EVB 的電源。
4. 等程式建置完成，點擊 |run_icon| 按鈕即可透過 USB to SPI 轉換板啟動系統。
5. 執行 Tera Term，如 SSID 與密碼皆正確無誤，測試程式會自動連上該熱點，最終取得 IP
6. 留意大部分的測試功能需要取得 IP 後才得以進行，例如 TEST_PING、TEST_DNS

  .. image:: /_static/console_wifi_log.png

.. note::

  1. 不定時會新增或修改 Kconfig 的選項或功能，不一定與上圖一模一樣。
  2. 如果 WiFi Driver 初始化失敗，可先檢查 GPIO 有無設定錯誤，或可觀察 Log 中的 CMD53 是否有回報 SD ERR。
  3. 若有未知錯誤無法處理，請聯絡 iTE 的 FAE 轉交給負責 RD。


問題排除
------------------------------------------------

我們收集了一些客戶經常反應的疑問，發生錯誤或奇怪現象時對初步判斷有一定程度的幫助

1. Q：8189FTV 初始化失敗
   
  A：請檢查 Kconfig 設定如 GPIO、IT986x 需要開啟 "SD1 WIFI Card Power Enable"

2. Q：有甚麼方式可以知道 SDIO 成功接到模組
   
  A：可以觀察一開始的 CMD53 讀取值，如下圖

  .. image:: /_static/console_wifi_cmd_log.png

  或是可以在某個位址寫任意值進暫存器再讀取，測試是否正確，以下是簡單範例

  .. code-block:: c

    struct sdio_func *wifi_sdio_func = NULL;
    void cmd_test(void)
    {
        wifi_writeb(wifi_sdio_func, 0x1, 0x2a);
        printf("test read 0x1 is 0x%x \n", wifi_readb(func, 0x1)); // If the value is 0x2a, it is correct
    }

3. Q：掃描後取得的 SSID 很少、或是訊號強度很弱(大多在 30% 以下)
   
  A：請接上天線，有無天線會差很多

4. Q：能跟 Ethernet 同時開啟嗎
   
  A：可以，雙 MAC 的 Kconfig 設定有寫在 Pre-Setting，檔名為 ``_config_xxx_indoor_ngpl_wifi_ethernet`` ，讀取完直接編譯即可

5. Q：發生未知的問題如何排查
   
  A：請用測試程式 test_wifi_sdio 確認問題，使用方式請見 :ref:`wifi_complete_example` ，並收集 Log 資訊

6. Q：支援 STA + HostAP 模式同時開啟嗎
   
   A：不支援 STA + HostAP 模式同時開啟


.. raw:: latex

    \newpage
