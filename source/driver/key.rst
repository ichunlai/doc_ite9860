.. _key:

Key
=======

功能描述
----------------------

Key 是 SoC 最基本的輸入裝置，依據輸入介面來區分有 GPIO、SPI、I2C、USB、PS/2、RF… ，各式各樣的型態，本節僅適用於使用 GPIO 為輸入介面的 KEY，關於其他 I/O 介面的 KEY，將以另文專述。ITE SOC 的Key 模組提供三種型式，最多可支援 64 組 KEY。

ITE 原生的 SDK 可支援三種型式的 Key 模組。

- 第一種是一般的 GPIO KEY，特色是 KEY 的線路簡單，即一個 GPIO PIN 控制一組 KEY，缺點是會占用較多的 GPIO PIN。
- 第二種 N * N HEX KEY，特色是使用 2N 組的 GPIO 去控制 N2 組 KEY。有關 HEX KEY 的相關訊息請參考[Hex Keypad Datasheet](http://www.winpicprog.co.uk/pic_tutorial9.htm)。
- 第三種是 ITE 自行開發的 Castor3 KEY，特色是使用 N 組 GPIO 控制 N2 組 KEY，但線路也是三種 KEY 裡面最複雜的。

假設同樣使用 8 組 GPIO，第一種 GPIO KEY 只能控制 8 組KEY，第二種 HEX KEY 能控制 16 組 KEY，而第三種 Castor3 KEY 能支援最多的 64 組 KEY，這也是目前 SDK 能支援最多的 KEY 總數

這三種 KEY 模組所對應的 Driver 都放在 ``＜sdk_root>/sdk/driver/itp/keypad/`` 目錄下，其中第一種 GPIO KEY 對應的 Driver 是 ``itp_keypad_gpio.c``、 第二種 HEX KEY 對應的 Driver 是 ``itp_keypad_hex.c``、第三種 Castor3 KEY 對應的 Driver 是 ``itp_keypad_castor3.c`` 。

相關的 KConfig 設定
----------------------------------------------------------------

在 KCONFIG 中要指定選用的 KEY 模組，首先必須勾選 “Keypad Enable”，然後在 “Keypad Module” 欄位中輸入 KEY 模組所對應的 Driver 檔案名稱(EX: ``itp_keypad_castro3.c``)，然後依據專案需求，適當地設定相關 KEYPAD 參數(如圖所示)，以下是各 KCONFIG 設定的說明 。

  .. image:: /_static/qconf_peripheral_keypad.png
 
- :menuselection:`Peripheral --> Keypad Enable`

  決定是否要啟用 KEYPAD 模組。

- :menuselection:`Peripheral --> Keypad Enable -->  Keypad module`

  設定所要使用的 KEYPAD 模組名稱。

- :menuselection:`Peripheral --> Keypad Enable --> Keypad Press Key interval(ms)`

  設定發送每個 KEY Event 的間隔時間。

- :menuselection:`Peripheral --> Keypad Enable --> Keypad Repeat Key Enable`

  啟用 repeat key 功能，當手指按壓某 KEY 不放的時候，系統將會重複不斷發送 KEY Event。

- :menuselection:`Peripheral --> Keypad Enable --> Keypad mapping table`

  設定 KEYPAD 的 MAPPING TABLE 檔(放在 ``<sdk_root>/sdk/target/keypad`` 目錄中)。Driver 回報上層的值是 INT 0 ~ N (N 指最大 KEY 總數 - 1)，上層依據此 Mapping Table 翻譯成相對應的 Key Code。



KEY GPIO 相關 KConfig 設定
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

關於設定 KEY 的 GPIO PIN，則要到 :menuselection:`GPIO --> Keypad Pins` 的欄位做設定(如下圖所示)。

.. image:: /_static/qconf_gpio_keypad.png
 
1. GPIO KEY

  按照 KEY 的順序填寫 GPIO 順序，EX: KEY0 使用 GPIO 51、KEY1 使用 GPIO 58、KEY2 使用 GPIO 31，則 “Keypad Pins: 51, 58, 31”

2. HEX KEY

  按照先 COLUMN 後 RAW 的順序填寫 GPIO 順序。EX: column_1 使用 GPIO 31、column_2 使用 GPIO 30、column_3 使用 GPIO 29、column_4 使用 GPIO 28、raw_1 使用 GPIO 40、raw _2 使用 GPIO 41、raw _3 使用 GPIO 42、raw _4 使用 GPIO 43，則 “Keypad Pins: 31, 30, 29, 28, 40, 41, 42, 43”

3. Castor3 KEY

  按照 GPIO SCAN 的順序填寫 GPIO 順序。EX: 第一次 SCAN 時，沒有 GPIO 設 OUTPUT 0，第二次SCAN 時 GPIO22 設 OUTPUT 0，第三次 SCAN 時 GPIO21 設 OUTPUT 0，第四次 SCAN 時 GPIO33 設OUTPUT 0，第五次 SCAN 時 GPIO16 設 OUTPUT 0；則設定 “Keypad Pins: 22, 21, 33, 16”

相關的程式碼檔案
----------------------------------------------------------------

.. flat-table::
  :header-rows: 1
   
  * - 路徑
    - 描述
  * - ``<sdk_root>/sdk/driver/itp/itp_keypad.c``
    - | Keypad ITP Driver，主要目的是定義三個 API，做為下層 driver 實作的規範。即:
      | ``itpKeypadInit();``
      | ``itpKeypadGetMaxLevel();``
      | ``itpKeypadProbe();``
  * - ``<sdk_root>/sdk/driver/itp/keypad/itp_keypad_xxx.c``
    - | ``itp_keypad_xxx.c`` 是 Keypad 的底層 driver，主要目的是實作以下三個 API， 
      | ``itpKeypadInit();``
      | ``itpKeypadGetMaxLevel();``
      | ``itpKeypadProbe();``
 
相關的 API 描述
---------------------------------------

DEVICE ID : ITP_DEVICE_KEYPAD (參考 ``<sdk_root>/sdk/include/ite/itp.h`` )

本節描述用來操作 KEYPAD 裝置的相關 API。KEYPAD driver 提供依據 POSIX 規範實作的 API，用戶可以透過 ``itpKeypadInit()``/``itpKeypadGetMaxLevel()``/``itpKeypadProbe()`` 等函式對 KEYPAD 裝置進行操作。

itpRegisterDevice
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void itpRegisterDevice(ITPDeviceType type, const ITPDevice *device)

  註冊裝置，即 ``itpRegisterDevice(ITP_DEVICE_ KEYPAD, &itpDeviceKeypad);`` 。

**參數**

``ITPDeviceType type``

  裝置類型。完整裝置類型列表請見 ``<sdk_root>/sdk/include/ite/itp.h`` 。與 KEYPAD 裝置相關的裝置類型是 ``ITP_DEVICE_KEYPAD`` 。

``const ITPDevice *device``

  裝置標識符。與 KEYPAD 裝置相關的裝置標識符有 ``itpDeviceKeypad`` 。

**描述**

此函式可用來將 KEYPAD 裝置註冊到核心裝置列表中，使上層 AP 可以透過 ``ioctl``/``read``/``write`` 等函式來操作 KEYPAD 的功能。本函式會在系統初始化期間被 ``itpInit()`` 函式呼叫 (見 ``<sdk_root>/sdk/driver/itp_init_openrtos.c`` )，因此若在您的專案中有呼叫過 ``itpInit()`` 函式，將無須再執行此註冊函式。

底下展示如何註冊 KEYPAD 裝置到系統核心，並初始化 KEYPAD 裝置。

.. code-block:: c

    itpRegisterDevice(ITP_DEVICE_KEYPAD, &itpDeviceKeypad); 
    ioctl(ITP_DEVICE_KEYPAD, ITP_IOCTL_INIT, NULL);
 
ioctl
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr)

  控制裝置

**參數**

``int file``

  裝置標識符。其值為 ``ITPDeviceType`` 列表中的值。與 KEYPAD 裝置相關的裝置標識符有 ``ITP_DEVICE_KEYPAD`` 。

``unsigned long request``

  request 參數用來選擇要對裝置運行的操作功能，並將取決於所指定的裝置。

``void *ptr``

  ptr 參數表示對此特定裝置執行請求的功能所需的額外訊息。ptr 的型別取決於特定的控制請求，但它可以是整數，也可以是指向裝置特定資料結構的指標。

**描述**

``ioctl()`` 函數用於在 KEYPAD 裝置上執行各種控制功能。request 參數和可選的第三個參數（具有不同的型別）將傳遞給與 file 關聯的 KEYPAD 裝置的相應部分並由其解釋。

適用於 KEYPAD 裝置的 ``ioctl()`` 命令，其參數以及適用於每個單獨命令的錯誤狀態如下所述。

- ITP_IOCTL_INIT

  用來初始化 KEYPAD 裝置。ptr 參數為 NULL。

- ITP_IOCTL_PROBE 

  回報 key 的總數給上層。

- ITP_IOCTL_GET_MAX_LEVEL

  偵測 key event，若偵測到有 key 輸入，則將 event 塞入 queue 中，等待 ``read()`` 取用。

read
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int read(int __fd, void *__buf, size_t __size)

  從 KEYPAD 讀取資料。相關範例可參考 ``<sdk_root>/sdk/share/sdl/video/castor3/SDL_castor3keyboard.c`` 。

**參數**

``int __fd``

  欲執行的 KEYPAD Device。EX: ``ITP_DEVICE_KEYPAD`` 。

``void *__buf``

  讀取後寫回的位置，KEYPAD 的 buffer size 即是 ``ITP Event Size(sizeof (ITPKeypadEvent))`` ， 完整裝置類型列表請見 ``<sdk_root>/sdk/include/ite/itp.h`` 。

``size_t __ size``

  欲讀取之資料長度，即 ``ITP Event Size(sizeof (ITPKeypadEvent))`` 。

**描述**

使用者可透過此函式從 KEYPAD 裝置讀回資料，底下展示如何使用函式。

.. code-block:: c

    ITPKeypadEvent ev;
    
    if (read(ITP_DEVICE_KEYPAD, &ev, sizeof(ITPKeypadEvent)) == sizeof(ITPKeypadEvent))
    {
        if (ev.flags & ITP_KEYPAD_DOWN)
        {
            printf("GOT KEYPAD-DOWN\n");
        }
        else if (ev.flags & ITP_KEYPAD_UP)
        {
            printf("GOT KEYPAD-UP\n");
        }
    }

簡易範例程式
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: c

    /*
    Add the following statements in the "Kconfig" file to build the SDL library.
    
    config BUILD_SDL
        def_bool y
    
    Simple DirectMedia Layer (SDL) is a cross-platform software development library designed to provide a hardware abstraction layer for computer multimedia hardware components. Software developers can use it to write high-performance computer games and other multimedia applications that can run on many operating systems such as Android, iOS, Linux, macOS, and Windows. https://en.wikipedia.org/wiki/Simple_DirectMedia_Layer。
    
    This example demos how ITE SDK uses SDL library to get the KEY EVENT.
    */
    
    #include "SDL/SDL.h"
    
    int TouchEvent_test(void)
    {
        SDL_Event ev;    
    
        /* SDL initial */
        if (SDL_Init(SDL_INIT_VIDEO) < 0)
            printf("Couldn't initialize SDL: %s\n", SDL_GetError());
    
        while (SDL_PollEvent(&ev))
        {
            switch (ev.type)
            {
            case SDL_KEYDOWN:
                switch (ev.key.keysym.sym)
                {   
                case SDLK_UP:
                    printf("key direction up\n");
                    break;
    
                case SDLK_DOWN:
                    printf("key direction down\n");
                    break;
    
                case SDLK_LEFT:
                    printf("key direction left\n");
                    break;
    
                case SDLK_RIGHT:
                    printf("key direction right\n");
                    break; 
        	       }
            }
            SDL_Delay(1);
        }
    }


.. raw:: latex

    \newpage
