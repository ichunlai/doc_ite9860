.. _gpio:

GPIO
==========

功能描述
----------------
IT986x 的 GPIO 控制器（ITGPIO）是用戶可編程的通用 I/O 控制器。它用於從系統和裝置輸入/輸出數據。每個 GPIO pin 均可被編程為輸入或輸出。

每個 GPIO pin 也可以作為中斷輸入。它支持上升沿、下降沿、雙沿和高/低電位中斷檢測類型。

相關的KConfig 設定
-----------------------------

無。

相關的程式碼檔案
-----------------------------

.. flat-table::
   :header-rows: 1
   
   * - 路徑
     - 描述
   * - ``<sdk_root>/sdk/include/ith/ith_gpio.h``
     - API 原型定義。

相關的 API 描述
-----------------------------

本節描述用來操作GPIO pin的相關API。

ithGpioEnable
^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioEnable(unsigned int pin)

   將某個 pin 切換作為 GPIO 使用。

**參數**

``unsigned int pin``
       
   GPIO pin 編號。

**描述**

將 pin 對應的 GPIO pin 切換作為 GPIO 使用。


ithGpioSetIn
^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioSetIn(unsigned int pin)

   設定 GPIO pin 為輸入模式。

**參數**

``unsigned int pin``
       
   GPIO pin 編號。

**描述**

將 pin 對應的 GPIO pin 設定為輸入模式。


ithGpioSetOut
^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioSetOut(unsigned int pin)

   設定 GPIO pin 為輸出模式。

**參數**

``unsigned int pin``
       
   GPIO pin 編號。

**描述**

將 pin 對應的 GPIO pin 設定為輸出模式。


ithGpioSet
^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioSet(unsigned int pin)

   設定 GPIO pin 輸出高電位。

**參數**

``unsigned int pin``
       
   GPIO pin 編號。

**描述**

將 pin 對應的 GPIO pin 設定輸出高電位。僅在該 GPIO pin 為輸出狀態時才有效。

ithGpioClear
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioClear(unsigned int pin)

   設定 GPIO pin 輸出低電位。

**參數**

``unsigned int pin``
       
   GPIO pin 編號。

**描述**

將 pin 對應的 GPIO pin 設定輸出低電位。僅在該 GPIO pin 為輸出狀態時才有效。


ithGpioGet
^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: uint32_t ithGpioGet(unsigned int pin)

   獲取 GPIO pin 的輸入為高電位( 1 )或低電位( 0 )。

**參數**

``unsigned int pin``
       
   GPIO pin 編號。

**描述**

獲取 pin 對應的 GPIO pin 的輸入為高電位( 1 )或低電位( 0 )。若該 GPIO pin 被設定為輸出模式，則呼叫此函式可獲得該 GPIO pin 目前輸出為高電位( 1 )或低電位( 0 )。

**回傳值**

- 0：低電位 
- 1：高電位

ithGpioCtrlEnable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioCtrlEnable(unsigned int pin, ITHGpioCtrl ctrl)

   設定 GPIO pin 的控制模式。

**參數**

``unsigned int pin``
       
   GPIO pin 編號。

``ITHGpioCtrl ctrl``

   要啟用或設置的控制模式。

**描述**

啟用 pin 對應的 GPIO pin 的各種控制模式。ctrl 參數代表要啟用的控制模式，可為以下數值之一：

- ITH_GPIO_PULL_ENABLE

  啟用 pin 對應的 GPIO pin 的 pull up/down 功能。

- ITH_GPIO_PULL_UP

  當 pin 對應的 GPIO pin 已啟用 pull up / down 功能時，將 pin 對應的 GPIO pin 設定為 pull up 模式。

- ITH_GPIO_INTR_LEVELTRIGGER

  將 pin 對應的 GPIO pin 設定為中斷輸入，採用 level trigger 模式。

- ITH_GPIO_INTR_BOTHEDGE

  當 pin 對應的 GPIO pin 被設定為中斷輸入且採用 edge trigger 模式時，啟用 both edge trigger 模式。

- ITH_GPIO_INTR_TRIGGERFALLING

  當 pin 對應的 GPIO pin 被設定為中斷輸入且採用 edge trigger 模式時，將該 pin 設定為 falling edge trigger 模式。

- ITH_GPIO_INTR_TRIGGERLOW 

  當 pin 對應的 GPIO pin 被設定為中斷輸入且採用 level trigger 模式時，將該 pin 設定為 low level trigger。

ithGpioCtrlDisable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioCtrlDisable(unsigned int pin, ITHGpioCtrl ctrl)
   
   設定 GPIO pin 的控制模式。

**參數**

``unsigned int pin``

   GPIO pin 編號。

``ITHGpioCtrl ctrl``

   要禁用或清除的控制模式。

**描述**

禁用或清除 pin 對應的 GPIO pin 的各種控制模式。ctrl 參數代表要禁用的控制模式，可為以下數值之一：

- ITH_GPIO_PULL_ENABLE

   禁用 pin 對應的 GPIO pin 的 pull up / down 功能。

- ITH_GPIO_PULL_UP

   當 pin 對應的 GPIO pin 已啟用 pull up/down 功能時，將 pin 對應的 GPIO pin 設定為 pull down 模式。

- ITH_GPIO_INTR_LEVELTRIGGER

   將 pin 對應的 GPIO pin 設定為中斷輸入，採用 edge trigger 模式。

- ITH_GPIO_INTR_BOTHEDGE

   當 pin 對應的 GPIO pin 被設定為中斷輸入且採用 edge trigger 模式時，禁用 both edge trigger 模式。

- ITH_GPIO_INTR_TRIGGERFALLING

   當 pin 對應的 GPIO pin 被設定為中斷輸入且採用 edge trigger 模式時，將該 pin 設定為 rising edge trigger。

- ITH_GPIO_INTR_TRIGGERLOW 

   當 pin 對應的 GPIO pin 被設定為中斷輸入且採用 level trigger 模式時，將該 pin 設定為 high level trigger。

ithGpioRegisterIntrHandler
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioRegisterIntrHandler(unsigned int pin, ITHGpioIntrHandler handler, void *arg)

**參數**

``unsigned int pin``

   GPIO pin 編號。

``ITHGpioIntrHandler handler``

   要註冊的 ISR 函式指標。

``void *arg``

   要傳入 ISR 函式的參數。

**描述**

註冊 GPIO 的 ISR。

ithGpioEnableIntr
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioEnableIntr(unsigned int pin)
    
**參數**

``unsigned int pin``

   GPIO pin 編號。
  
**描述**

啟用 GPIO 的中斷功能。
 
ithGpioDisableIntr
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioDisableIntr(unsigned int pin)
    
**參數**

``unsigned int pin``

   GPIO pin 編號。
  
**描述**

關閉 GPIO 的中斷功能。
 
ithGpioClearIntr
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioClearIntr(unsigned int pin)
    
**參數**

``unsigned int pin``

   GPIO pin 編號。
  
**描述**

清除 GPIO 的中斷旗標。

底下說明中斷設定的流程：

- 建議在設定前使用 ``ithEnterCritical()`` 保護整個中斷設定區塊，避免被其他中斷干擾。
- 清除先前的中斷訊號，使用 ``ithGpioClearIntr(pin)`` 清除此 GPIO pin 的中斷訊號。
- 註冊 ISR，使用 ``ithGpioRegisterIntrHandler()`` 來註冊 ISR。
- 設定中斷方式，使用 ``ithGpioCtrlEnable()`` 以及 ``ithGpioCtrlDisable()`` 來設定中斷的型式(如 edge trigger / level trigger, both edge / single edge, …)。
- 啟用 GPIO 中斷的IRQ，使用 ``ithIntrEnableIrq(ITH_INTR_GPIO)`` 來啟用 GPIO 的 IRQ。
- 啟用此 GPIO pin 的中斷功能，使用 ``ithGpioEnableIntr()`` 來啟用此 GPIO pin 的中斷功能。
- 使用 ``ithExitCritical()`` ，離開 critical section。
 
ithGpioSetMode
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioSetMode(unsigned int pin, ITHGpioMode mode)

**參數**

``unsigned int pin``

   GPIO pin 編號。

``ITHGpioMode mode``

   設定 GPIO 複用功能模式，可使用以下參數值設定：
   
   - ITH_GPIO_MODE0
   - ITH_GPIO_MODE1
   - ITH_GPIO_MODE2
   - ITH_GPIO_MODE3
   - ITH_GPIO_MODE4

**描述**

設定 GPIO pin 的複用功能模式。因為 IT986x 系列 pin 腳有限的關係，每個 pin 腳還可以切換為其他功能(稱做複用功能)。下表列出了每個 GPIO pin 對應的複用功能模式。

  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | Pin          | Mode 0       | Mode 1       | Mode 2          | Mode 3       | Mode 4       |
  +==============+==============+==============+=================+==============+==============+
  | GPIO0        | GPIO0        |                                                              |
  +--------------+--------------+                                                              +
  | GPIO1        | GPIO1        |                                                              |
  +--------------+--------------+                      USB2SPI_Debug                           +
  | GPIO2        | GPIO2        |                                                              |
  +--------------+--------------+                                                              +
  | GPIO3        | GPIO3        |                                                              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO4        | GPIO4        |              Read_Log (Default use UART0_Tx)                 |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO5        | GPIO5        | AXISPI_CS0   |                 |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO6        | GPIO6        | AXISPI_D0    | SD0_D0          |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO7        | GPIO7        | AXISPI_D1    | SD0_D1          |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO8        | GPIO8        | AXISPI_D2    | SD0_D2          |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO9        | GPIO9        | AXISPI_D3    | SD0_D3          |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO10       | GPIO10       | AXISPI_CLK   | SD0_D3          |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO11       | GPIO11       | AXISPI_CS1   | SD1_CLK         |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO13       | GPIO13       |              | SD0_CMD         |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO14       | GPIO14       |              | SD0_CLK         |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO15       | GPIO15       |              | SD0_D0          | SPI1_CLK     |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO16       | GPIO16       |              | SD0_D1          | SPI1_CS0     |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO17       | GPIO17       |              | SD0_D2          | SPI1_DO      |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO18       | GPIO18       |              | SD0_D3          | SPI1_DI      |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO19/XAIN0 | GPIO19       | WO1_D0       | SD1_D1          | SPI0_CLK     | RX_ZD1       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO20/XAIN1 | GPIO20       | WO1_D1       | SD1_CMD         | SPI0_CS0     | RX_ZD2       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO21/XAIN2 | GPIO21       | IrDA_RXL     | WI0_Out(Debug)  | SPI0_DO      | RX_ZD3       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO22/XAIN3 | GPIO22       | IrDA_TX      | WI1_Out(Debug)  | SPI0_DI      | IIS_AMCLK    |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO23/XAIN4 | GPIO23       | WO0_D0       |                 | SPI0_CS1     | IIS_ZCLK     |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO24/XAIN5 | GPIO24       | WO0_D1       |                 | SPI0_CLK     | IIS_ZWS      |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO25/XAIN6 | GPIO25       | WO1_D0       |                 | SPI0_CS0     | IIS_ZDO      |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO26/XAIN7 | GPIO26       | WO1_D1       |                 |              | IIS_ZDI      |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO27       | GPIO27       |  MDIO        | SD0_D1          | SPI0_CLK     |              | 　
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO28       | GPIO28       |  MDC         | SD0_CMD         | SPI0_CS0     |              | 　
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO29       | GPIO29       |  TXD1        | SD0_CLK         | SPI0_DO      |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO30       | GPIO30       |  TXD0        |                 |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO31       | GPIO31       |  TXEN        |                 |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO32       | GPIO32       |  TXC         | SD1_CLK         |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO33       | GPIO33       |  RX_CRS_DV   | SD1_CMD         | SPI0_DI      |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO34       | GPIO34       |  RXD0        | SD1_D0          | SPI1_CLK     | WO0_D0       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO35       | GPIO35       |  RXD1        | SD1_D1          | SPI1_CS0     | WO0_D1       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO36       | GPIO36       |  RXER        | SD1_D2          | SPI1_DO      | WO1_D0       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO37       | GPIO37       |  INTB        | SD1_D3          | SPI1_DI      | WO1_D1       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO38       | GPIO38       |  LDCLK       | VD0             | SPI1_CLK     |              | 
  +--------------+--------------+--------------+-----------------+--------------+--------------+　
  | GPIO40       | GPIO40       |  LD1         | VD2             | SPI1_DO      |              | 
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO41       | GPIO41       |  LD2         | VD3             | SPI1_DI      |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+ 
  | GPIO42       | GPIO42       |  LD3         | VD4             | SPI1_CS1     | SD1_CLK      |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO43       | GPIO43       |  LD4         | VD5             | SD1_CMD      |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+ 
  | GPIO44       | GPIO44       |  LD5         | VD6             | SD1_D0       |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+ 
  | GPIO45       | GPIO45       |  LD6         | VD7             | SD1_D1       |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+ 
  | GPIO46       | GPIO46       |  LD7         | VD8             | SD1_D2       |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+ 
  | GPIO47       | GPIO47       |  LD8         | VD9             | WO0_D0       | SD1_D3       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO48       | GPIO48       |  LD9         | VD10            | WO0_D1       | SD1_D4       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO49       | GPIO49       |  LD10        | VD11            | WO1_D0       | SD1_D5       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO50       | GPIO50       |  LD11        | VD12            | WO1_D1       | LDCLK        |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO51       | GPIO51       |  LD12        | VD13            | SD0_CLK      | SD1_D6       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO52       | GPIO52       |  LD13        | VD14            | SD0_CMD      | SD1_D7       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO53       | GPIO53       |  LD14        | VD15            | SD0_D0       |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+  　
  | GPIO54       | GPIO54       |  LD15        | VD16            | SD0_D1       |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+  　
  | GPIO55       | GPIO55       |  LD16        | VD17            | SD0_D2       | RX_ZCLK      |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO56       | GPIO56       |  LD17        | VD18            | SD0_D3       | RX_ZWS       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO57       | GPIO57       |  LD18        | VD19            | SD0_D4       | RX_ZD0       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO58       | GPIO58       |  LD19        | VD20            | SD0_D5       | RX_AMCLK     |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO59       | GPIO59       |  LD20        | VD21            | SD0_D6       | LDCLK        |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO60       | GPIO60       |  LD21        | VD22            | SD0_D7       | RX_ZCLK      |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO61       | GPIO61       |  LD22        | VD23            | SD1_D1       | RX_ZWS       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO62       | GPIO62       |  LD23        | VD24            | SD1_CMD      | RX_ZD0       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO63       | GPIO63       |  LD24        | VD25            | SD1_CLK      | RX_AMCLK     |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO64       | GPIO64       |  LD25        |                 |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+ 
  | GPIO65       | GPIO65       |  LD26        |                 |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+ 
  | GPIO66       | GPIO66       |  LD27        |                 |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+ 
  | GPIO67       | GPIO67       |  LD28        |                 |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+ 

簡易範例程式
-----------------------------

底下為 GPIO 範例程式碼。

GPIO output
^^^^^^^^^^^^^^^^^^^^^^^^

底下展示將 ``CFG_GPIO_TEST_OUTPUT_PIN`` 對應的 pin 腳設定為輸出模式，並每隔一秒切換輸出的 high / low 狀態。

.. code-block:: c

   #include "ite/itp.h" // for all ith driver (include GPIO) & MACRO

   void main(void)
   {
       int gpioPin = CFG_GPIO_TEST_OUTPUT_PIN;
       int i = 0;
       
       itpInit();
   
       //initial GPIO
       ithGpioSetOut(gpioPin);
       ithGpioSetMode(gpioPin, ITH_GPIO_MODE0);
       
       //
       while(1)
       {
           if (i++ & 0x1)
           {
               ithGpioClear(gpioPin);
           }
           else
           {
               ithGpioSet(gpioPin);
           }
           printf("current GPIO[%d] state=%x, index=%d\n”, gpioPin, ithGpioGet(gpioPin), i);
           usleep(1000 * 1000);  // wait for 1 second
       }
   
       return NULL;
   }

GPIO input
^^^^^^^^^^^^^^^^^^^^^^^^

底下展示將 ``CFG_GPIO_TEST_OUTPUT_PIN`` 對應的 pin 腳設定為輸入模式，並不斷輪詢該 pin 的 high / low 狀態。

.. code-block:: c

    #include "ite/itp.h"    // for all ith driver (include GPIO) & MACRO

    static void _gpioPinInit(void)
    {
        ithGpioSetMode(gpioPin, ITH_GPIO_MODE0);
        ithGpioSetIn(gpioPin);
        ithGpioEnable(gpioPin);
    }
    
    void main(void)
    {
        int gpioPin = CFG_GPIO_TEST_INPUT_PIN;
        int lastPinStatus = 0;
    
        itpInit();
        // initial GPIO
        _gpioPinInit();
    
        while(1)
        {
            // polling gpio pin
            if (ithGpioGet(gpioPin) != lastPinStatus)
            {
                // GPIO status has changed
                lastPinStatus = ithGpioGet(gpioPin);
                if (lastPinStatus)
                {
                    printf("The lasted GPIO state is HIGH\n”);
                }
                else
                {
                    printf("The lasted GPIO state is LOW\n”);
                }
            }
        }
        return NULL;
    }

GPIO interrupt
^^^^^^^^^^^^^^^^^^^^^^^^

底下展示將 ``CFG_GPIO_TEST_OUTPUT_PIN`` 對應的 pin 腳設定為中斷輸入模式，並採用 both edge trigger。

.. code-block:: c

    #include "ite/itp.h"    // for ITH_GPIO_MODE0 & GPIO ith driver

    static char g_GPIO_INTR = 0;
    
    void _gpio_isr(void* data)
    {
        g_GPIO_INTR = 1;
    }
    
    void _initGpioPin(int pin)
    {
        ithGpioSetMode(pin, ITH_GPIO_MODE0);    // Set the GPIO pin as mode 0.
        ithGpioSetIn(pin);  // Set the GPIO pin as input pin.
        ithGpioCtrlEnable(pin, ITH_GPIO_PULL_ENABLE);   // Enable the GPIO pin's pull function
        ithGpioCtrlEnable(pin, ITH_GPIO_PULL_UP);       // Enable the GPIO pin's pull up function
        ithGpioEnable(pin); // Enable GPIO pin
    }
    
    void _initGpioIntr(int pin)
    { 
        ithEnterCritical();     // To prevent from being interrupted
        ithGpioClearIntr(pin);  // Clear the interrupt
        ithGpioRegisterIntrHandler(pin, _gpio_isr, (void*)pin); // Register the GPIO pin's interrupt handler
        ithGpioCtrlDisable(pin, ITH_GPIO_INTR_LEVELTRIGGER);    // Use edge trigger interrupt mode
        ithGpioCtrlEnable(pin, ITH_GPIO_INTR_BOTHEDGE);         // Use Both edge trigger
        ithIntrEnableIrq(ITH_INTR_GPIO);    // Enable the GPIO pin's interrupt (to the interrupt controller)
        ithGpioEnableIntr(pin);             // Enable the GPIO pin's interrupt
        ithExitCritical();      // unlock spinlock
    }
    
    void _initGpio(int pin)
    {
        _initGpioPin(pin);  
        _initGpioIntr(pin);
    }
    
    void main(void)
    {
        int gpioPin = CFG_GPIO_TEST_INTR_PIN;
        int i = 0;
    
        itpInit();
    
        _initGpio(gpioPin);
        
        while(1)
        {
            // polling "g_GPIO_INTR"
            if (g_GPIO_INTR)
            {
                unsigned int gpioState = ithGpioGet(gpioPin) ? 1 : 0;
                printf("current GPIO[%d] state = %d \n”, gpioPin, gpioState);
                g_GPIO_INTR = 0;
                ithGpioClearIntr(gpioPin);
            }
            usleep(1000);   // sleep for 1 ms
        }
    
        return NULL;
    }

.. raw:: latex

    \newpage
