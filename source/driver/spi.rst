﻿.. _spi:

SPI
=======

功能描述
--------------------

SPI 是 Serial Peripheral Interface 的縮寫，中文意思是串列週邊介面，該介面是由 Motorola 公司設計發展的同步串列介面，通常被用來與週邊晶片做通訊，如： Storage、Sensor 和 Bridge 等等。它是一個主從式架構，普遍有一個 Master (主設備)端和一個(或多個) Slave (從設備)端，而裝置之間使用全雙工模式通信，內部硬體結構之相關詳述如下。

.. figure:: /_static/diagram_spi.png

  結構圖

接腳名稱及意義
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- SCLK：時脈信號，由 Master 產生並控制。
- SS：Slave 選擇信號，由 Master 控制。Slave 只有在 SS 信號為低電位時，才會對Master 的操作指令有反應。
- MOSI：Master 數據輸出，Slave 數據輸入。
- MISO：Master 數據輸入，Slave 數據輸出。

.. figure:: /_static/diagram_spi_master_to_multi_slaves.png

  Master 端連接多個 Slave 端

.. figure:: /_static/diagram_spi_timing.png

  時序圖以及工作模式


相關的 KConfig 設定
----------------------------------------------------------------

- :menuselection:`Peripheral --> SPI_Enable`

  決定是否要啟用 SPI 裝置，並選擇欲開啟的 SPI 模組。

  .. image:: /_static/qconf_peripheral_check_spi_enable.png


- :menuselection:`Peripheral --> SPI_Enable --> SPI# Enable`

  選擇欲開啟的 SPI 模組。

  .. image:: /_static/qconf_peripheral_check_spi0_enable.png

- :menuselection:`GPIO --> SPI#`

  設定 SPI GPIO，目前 HW 設計為特定 PIN number，可選擇的 PIN number 可參考 SPI 腳位的註解。

  .. image:: /_static/qconf_gpio_spi.png


相關的程式碼檔案
----------------------------


.. flat-table::
  :header-rows: 1
  
  * - 路徑
    - 描述
  * - ``<sdk_root>/sdk/driver/itp/itp_spi.c``
    - 最上層函式，主要提供 POSIX 規範實作的 API，供上層 AP 透過 ``open()``、``close()``、``ioctl()`` 等函式對裝置進行操作。
  * - | ``<sdk_root>/sdk/include/ssp/*``
      | ``<sdk_root>/sdk/driver/spi/*``
    - 底層函式，未來可能會變動。


相關的 API 描述
---------------------------------------

本節描述用來操作 SPI 裝置的相關 API。SPI driver 提供依據 POSIX 規範實作的 API，用戶可以透過 ``open()``/``read()``/``write()``/``close()``/``ioctl()`` 等函式對 SPI 裝置進行如讀寫檔案般的操作。若需要參考 SPI slave 之實作流程，在 :ref:`spi_simple_example` 中有提供一個簡單的範例，需要注意的是，software SPI slave 銜接是需要客戶提供協定且客製化的。

itpRegisterDevice
^^^^^^^^^^^^^^^^^^^^

.. c:function:: void itpRegisterDevice(ITPDeviceType type, const ITPDevice *device)

  註冊裝置。

**參數**

``ITPDeviceType type ITP_DEVICE_SPI``

  裝置類型。完整裝置類型列表請見 ``<sdk_root>/sdk/include/ite/itp.h`` 。與 SPI 裝置相關的裝置類型有 ``ITP_DEVICE_SPI`` 以及 ``ITP_DEVICE_SPI1`` 。

``const ITPDevice *device``

  裝置標識符。與 SPI 裝置相關的裝置標識符有 ``itpDeviceSpi0`` 以及 ``itpDeviceSpi1`` 。

**描述**

此函式可用來將 SPI# 裝置註冊到核心裝置列表中，使上層 AP 可以透過 ``ioctl``/``read``/``write`` 等函式來操作 SPI 的功能。本函式會在系統初始化期間被 ``itpInit()`` 函式呼叫 (見 ``<sdk_root>/sdk/driver/itp_init_openrtos.c`` )，因此若在您的專案中有呼叫過 ``itpInit()`` 函式，將無須再執行此註冊函式。
 

ioctl
^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr)

  控制裝置。

**參數**

``int file``

  裝置標識符。其值為 ITPDeviceType 列表中的值。與 SPI 裝置相關的裝置標識符有 ``ITP_DEVICE_SPI`` 以及 ``ITP_DEVICE_SPI1`` 。

``unsigned long request``

  request 參數用來選擇要對裝置運行的操作功能，並將取決於所指定的裝置。

``void *ptr``

  ptr 參數表示對此特定裝置執行請求的功能所需的額外訊息。ptr 的型別取決於特定的控制請求，但它可以是量化整數，也可以是指向裝置特定資料結構的指標。

**描述**

``ioctl()`` 函數用於在 SPI 裝置上目前只有 INIT 功能。

- ITP_IOCTL_INIT

  用來初始化 SPI 裝置，底下展示如何透過預設設定來初始化 SPI0 裝置。

  .. code-block:: c

    itpRegisterDevice(ITP_DEVICE_SPI, &itpDeviceSpi0);
    ioctl(ITP_DEVICE_SPI, ITP_IOCTL_INIT, (void *)0);
 
read
^^^^^^^^^^^^

.. c:function:: int read(int file, char *ptr, int len)

  從 RX 讀取資料。

**參數**

``int file``

  欲執行的 SPI 埠。EX: ITP_DEVICE_SPI。

``char *ptr``

  讀取結構的陣列頭位址，包含讀寫方式、指令內容以及實際記憶體之收送資訊。

``int len``

  讀取結構陣列的大小，代表做多少次讀取行為。

**描述**

使用者可透過此函式從 SPI0 埠中讀回資料，底下展示如何使用該函式。

.. code-block:: c

    // initialize SPI, the method below could be found in itpInit();
    // Register SPI device
    itpRegisterDevice(ITP_DEVICE_SPI, &itpDeviceSpi0);

    // Do initialization
    ioctl(ITP_DEVICE_SPI, ITP_IOCTL_INIT, NULL);

    // Do SPI Read operation
    fd = open(":spi0", O_RDONLY);
    SpiInfo.readWriteFunc = ITP_SPI_PIO_READ;
    SpiInfo.cmdBuffer = &command;
    SpiInfo.cmdBufferSize = 1;
    SpiInfo.dataBuffer = &recv_buf;
    SpiInfo.dataBufferSize = 1;
    read(fd, &SpiInfo, 1);
    close(fd);
 
write
^^^^^

.. c:function:: int write(int file, char*ptr, int len)

  從 TX 發送資料。

**參數**

``int file``

  欲執行的 SPI 埠。EX: ITP_DEVICE_SPI。

``char *ptr``

  寫入結構的陣列頭位址，包含讀寫方式、指令內容以及實際記憶體之收送資訊。

``int len``

  寫入結構陣列的大小，代表做多少次寫入行為。

**描述**

使用者可透過此函式從 SPI0 埠中寫入資料，底下展示如何使用該函式。

.. code-block:: c

    // initialize SPI, the method below could be found in itpInit();
    // Register SPI device
    itpRegisterDevice(ITP_DEVICE_SPI, &itpDeviceSpi0);

    // Do initialization
    ioctl(ITP_DEVICE_SPI, ITP_IOCTL_INIT, NULL);

    // Do SPI Write operation
    fd = open(":spi0", O_WRONLY);
    SpiInfo.readWriteFunc = ITP_SPI_PIO_WRITE;
    SpiInfo.cmdBuffer = &command;
    SpiInfo.cmdBufferSize = 1;
    SpiInfo.dataBuffer = &send_buf;
    SpiInfo.dataBufferSize = 1;
    write(fd, &SpiInfo, 1);
    close(fd);
 


.. _spi_simple_example:

簡易範例程式
----------------------------------------------------------------

.. code-block:: c

    #include "ite/itp.h"

    void main(void)
    {
        int fd = 0;
        ITPSpiInfo SpiInfo = {0};
        uint8_t command[5] = {0};
        uint8_t id2[3]  = {0};

        itpRegisterDevice(ITP_DEVICE_SPI, &itpDeviceSpi0);
        ioctl(ITP_DEVICE_SPI, ITP_IOCTL_INIT, NULL);

        printf("Start SPI test!\n");

        fd = open(":spi0", O_RDONLY);
        if (!fd) 
            printf("--- open device spi0 fail ---\n");
        else 
            printf("fd = %d\n", fd);

        command[0] = 0x9F;

        SpiInfo.readWriteFunc = ITP_SPI_PIO_READ;
        SpiInfo.cmdBuffer = &command;
        SpiInfo.cmdBufferSize = 1;
        SpiInfo.dataBuffer = &id2;
        SpiInfo.dataBufferSize = 3;

        while (1)
        {
            read(fd, &SpiInfo, 1);
            printf("SpiInfo 0x%x, 0x%x, 0x%x\n", id2[0],id2[1], id2[2]);
        }

        close(fd);
    }




.. raw:: latex

    \newpage
