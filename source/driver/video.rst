.. _video:

Video
=============

功能描述
------------------------

IT986x 支援播放 video 功能，包含與 ITU 整合的 video 播放 widget、開機動畫播放、以及一般多媒體檔案播放功能。

- ITU video 播放 widget：

  使用 GUIDesigner 可以建立 video widget 來播放 video，我們可以在 video widget 的 FilePath 欄位指定要播放的檔案路徑，此外也提供了 Playing 和 Repeat 欄位供使用者使用。

  .. image:: /_static/gui_editor_video_widget.png

  - Playing：
  
	當使用者把 Playing 設成 True 時，只要切換到該 UI 頁面就會自動播放指定的 video；如果設成 False ，則可以依照使用者需求另外呼叫 API 進行播放。
  
  - Repeat：
  
	當使用者把 Repeat 設成 True 時，指定的 video 將會進行循環播放。

- 開機動畫播放：

  使用者可以利用 GUIDesigner 設定 video widget 來播放開機動畫。

- 多媒體播放：

  使用者可以直接呼叫 API 來進行 video 播放。

支援格式
^^^^^^^^^^^^^^^^^^^^^^

IT986x 支援的壓縮格式和封裝格式如下：
	
格式一
""""""""""""""""""

- 壓縮格式：Video：H.264，AUDIO：MP3、AAC
- 封裝格式：MP4、MKV

  .. note:: 
    
    IT986x 所支援的 H264 codec 只支援 reference frame = 1 的編碼方式，所以使用者要播放的 video 必須經過特定的 converter 轉換才能播放，我們提供了兩種 tool ( ``freemaker``、``ffmpeg.exe`` )供使用者進行轉換。

- Freemake：

  GUI 介面，可從https://www.freemake.com/free_video_converter/ 下載。

  1. 開啟 Freemake Video Converter 後，選擇 "Video" 選項。
  2. 載入要轉檔的 Video 後，選擇 "to MP4" 選項或 "to MKV" 選項。

	.. image:: /_static/video_converter_to_mp4.png
   
  3. 選擇設定按鈕。

	.. image:: /_static/video_converter_to_mp4_setting.png
   
  4. 在彈出 Video 設定視窗後，使用者可以依照需求設定 frame size、bitrate、codec。
  5. 設定完成後按 "OK" 鍵。

	.. image:: /_static/video_converter_to_mp4_setting_ok.png
   
  6. 按 "Convert" 鍵開始進行轉檔。
 
	.. image:: /_static/video_converter_to_mp4_done.png

- ffmpeg.exe：

  CMD介面，使用的指令參數範例如下：

  .. code-block:: shell

	ffmpeg.exe -i "filename" -c:v libx264 -profile:v main -level:v 3.0 -x264opts b-pyramid=0:ref=1:bframes=0 -c:a mp3 output.mp4

	
格式二
""""""""""""""""""

- 壓縮格式：Video：MJPEG，AUDIO：MP3、AAC
- 封裝格式：AVI

.. note::

  JPEG 格式不支援 progressive jpeg

.. note::
	
  使用者可以利用 ``ffmpeg.exe`` 將圖檔(jpg、png、bmp)製作成 mjpeg 格式的 video，使用的指令參數範例如下：

  .. code-block:: shell

	ffmpeg.exe -i *.jpg -c:v mjpeg -qscale:v 0 output.avi

.. note:: 
	
	因為 MJPEG 格式的檔案容量比較大，播放時讀取檔案會比較耗時，所以建議使用者可以利用 ffmpeg.exe 將 AVI 檔轉換為 H264 壓縮格式的 MP4 或 MKV 檔。

IT986x 支援的 video 解析度如下：

- IT9862、IT9863：支援的最大解析度為 480 x 272。
- IT9866：支援的最大解析度為 1280 x 720。
- IT9868、IT9869：支援的最大解析度為 1920 x 1080。

.. note::
	
	IT986x 支援的 video 解析度，會因晶片記憶體容量大小而有所限制。

相關的 Kconfig 設定
---------------------------------

- :menuselection:`Video --> Video Enable`

  開啟 Video Enable，即可讓 IT986x 支援播放 video 功能。

相關的程式碼檔案
---------------------------------

.. flat-table::
  :header-rows: 1  

  * - 路徑
    - 描述
  * - ``sdk/itu/share/itu/itu_video.c``
    - ITU video 相關 API。
  * - | ``sdk/itu/share/itu/itu_framefunc.c``
      | ``sdk/driver/itv/it9860/itv.c``
      | ``sdk/share/ffmpeg/castor3player.c``
    - 多媒體播放相關 API 實作。
  * - ``project/ctrlboard/layer_videoplayer.c``
    - 多媒體播放參考使用範例。

 
ITU video 相關 API 描述
------------------------------

ituVideoPlay
^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ituVideoPlay(ITUVideo* video, int percentage)

**參數**

``ITUVideo* video``

  Video widget 類型。請見 ``<sdk_root>/sdk/itu/include/itu.h`` 。使用者可以呼叫 ``ituSceneFindWidget(ITUScene *scene, const char *name)`` 取得指定的 video widget 類別。

``int percentage``

  percentage 的值範圍 0 ~ 100。檔案將從使用者設定的 percentage 值換算百分比例開始播放，percentage 設為 0 即會從頭播放。

**描述**

此函式可用來播放 video widget 設定的影片。當使用者預設的 Playing 欄位為 False 時，必須呼叫此函式才會開始播放影片，此時會將 Playing 欄位設為 True。
 
ituVideoStop
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ituVideoStop(ITUVideo* video)

**參數**

``ITUVideo* video``

  Video widget 類型。請見 ``<sdk_root>/sdk/itu/include/itu.h`` 。使用者可以呼叫 ``ituSceneFindWidget(ITUScene *scene, const char *name)`` 取得指定的 video widget 類別。

**描述**

此函式可用來停止 video widget 的影片。
 
ituVideoPause
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ituVideoPause(ITUVideo* video, int percentage)

**參數**

``ITUVideo* video``

  Video widget 類型。請見 ``<sdk_root>/sdk/itu/include/itu.h``。使用者可以呼叫 ``ituSceneFindWidget(ITUScene *scene, const char *name)`` 取得指定的 video widget 類別。

**描述**

此函式可用來暫停 video widget 的影片。影片暫停後，如要返回播放狀態，只要再呼叫 ``ituVideoPlay()`` 進行播放即可
 
ituVideoGoto
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ituVideoGoto(ITUVideo* video, int percentage)

**參數**

``ITUVideo* video``

  Video widget 類型。請見 ``<sdk_root>/sdk/itu/include/itu.h`` 。使用者可以呼叫 ``ituSceneFindWidget(ITUScene *scene, const char *name)`` 取得指定的 video widget 類別。

``int percentage``

  percentage 的值範圍 0 ~ 100。檔案播放時將依照使用者設定的 percentage 值換算百分比例跳到該時間點播放。

**描述**

此函式可用來控制 video widget 的影片，直接跳到 percentage 百分比例換算的時間點播放。
 
ituVideoSpeedUpDown
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ituVideoSpeedUpDown(ITUVideo* video, float speed)

**參數**

``ITUVideo* video``

  Video widget 類型。請見 ``<sdk_root>/sdk/itu/include/itu.h`` 。使用者可以呼叫 ``ituSceneFindWidget(ITUScene *scene, const char *name)`` 取得指定的 video widget 類別。

``float speed``

  speed 的值範圍 0.5 ~ 3。檔案播放時將依照使用者設定的 speed 值來快播或慢播，speed = 1 為正常速度，speed = 0.5 為 1/2 倍速，speed = 2 為 2 倍速，以此類推。

**描述**

此函式可用來控制 video widget 的影片，依照 speed 的值進行快播或慢播。
 
ituVideoSetOnStop
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:macro:: ituVideoSetOnStop(video, onStop)
	
**參數**

``ITUVideo* video``

  Video widget類型。請見 ``<sdk_root>/sdk/itu/include/itu.h`` 。使用者可以呼叫 ``ituSceneFindWidget(ITUScene *scene, const char *name)`` 取得指定的 video widget 類別。

``onStop``

  onStop 是一個 callback function。

**描述**

此函式用來設定檔案結束播放時的 callback function。當檔案播放結束時會呼叫 onStop 對應的 callback function，讓使用者可以在檔案播放完後做對應的工作。
 
itu_framefuncInit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int ituFrameFuncInit(void)

**描述**

此函式會做 video hardware 和 display buffer 的初始化，使用 video 相關函式前必須先呼叫此函式。
 
itu_framefuncExit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int ituFrameFuncExit(void)

**描述**

此函式會做 video hardware 和 display buffer 的反初始化
 

.. _video_play_api:

多媒體播放相關 API 描述
------------------------

itv_set_video_window
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void itv_set_video_window(uint32_t startX, uint32_t startY, uint32_t width, uint32_t height)

**參數**

``uint32_t startX``

  設定 video window 的 x 座標。

``uint32_t startY``

  設定 video window 的 y 座標。

``uint32_t width``

  設定 video window 的寬。

``uint32_t height``

  設定 video window 的高。

**描述**

此函式會設定 video window 的範圍，使用者所播放的 video 會在這個範圍內顯示。
 
mtal_pb_init
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int mtal_pb_init(cb_handler_t callback)

**參數**

``cb_handler_t callback``

  Video widget 類型。請見 ``<sdk_root>/sdk/itu/include/itu.h`` 。使用者可以呼叫 ``ituSceneFindWidget(ITUScene *scene, const char *name)`` 取得指定的 video widget 類別。

**描述**

此函式會進行多媒體播放初始化，並註冊 callback function，當使用者要進入多媒體播放介面必須呼叫此函式。
 
mtal_pb_exit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int mtal_pb_exit(void)

**描述**

此函式會對多媒體播放反初始化，當使用者要離開多媒體播放介面必須呼叫此函式。
 
mtal_pb_select_file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int mtal_pb_select_file(MTAL_SPEC *spec)

**參數**

``MTAL_SPEC *spec``

  ``MTAL_SPEC`` 類型。請見 ``<sdk_root>/sdk/share/ffmpeg/castor3player.h`` ，此類型其中一個member： ``srcname`` 必須代入使用者要播放的檔案路徑。

**描述**

此函式的功能是讓使用者選擇要播放的檔案。
 
mtal_pb_play
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int mtal_pb_play(void)

**描述**

此函式的功能是讓使用者開始播放檔案。

- 呼叫函式 ``mtal_pb_select_file`` 後，可以呼叫此函式進行檔案播放。
- 呼叫函式 ``mtal_pb_pause`` 使檔案暫停時，可以呼叫此函式使檔案繼續播放。
 
mtal_pb_pause
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int mtal_pb_pause(void)

**描述**

此函式的功能是讓使用者暫停正在播放的檔案。

當檔案正在播放時，呼叫此函式可以暫停播放。
當檔案暫停播放時，呼叫此函式可以繼續播放 (此時與呼叫函式 ``mtal_pb_play`` 效果相同)。
 
mtal_pb_stop
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int mtal_pb_stop(void)

**描述**

此函式的功能是讓使用者停止正在播放的檔案。


 
mtal_pb_get_total_duration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int mtal_pb_get_total_duration(int *totaltime)

**參數**

``int *totaltime``

  回傳檔案的總時間長度，單位是秒。

**描述**

此函式的功能是取得正在播放的檔案的總時間長度。


 
mtal_pb_get_total_duration_ext
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int mtal_pb_get_total_duration_ext(int *totaltime, char *filepath)

**參數**

``int *totaltime``

  回傳檔案的總時間長度，單位是秒。

``char *filepath``

  要取得檔案總時間長度的檔案路徑。

**描述**

此函式的功能是取得檔案的總時間長度。

和 ``mtal_pb_get_total_duration_ext`` 不同的是，此函式是在不播放該檔案的情況下，需要取得該檔案的總時間長度。
 
mtal_pb_get_current_time
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int mtal_pb_get_current_time(int *currenttime)

**參數**

``int *currenttime``

  回傳檔案目前的時間長度，單位是秒。

**描述**

此函式的功能是取得檔案目前播放到的時間長度


.. raw:: latex

	\newpage
