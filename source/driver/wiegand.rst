.. _wiegand:

Wiegand
===========

功能描述
---------------------

Wiegand 介面是門禁系統或保全系統中非接觸讀卡機常見的溝通介面。IT986x 的 Wiegand 模組提供標準 Wiegand 的操作方式用來讀取感應卡中的 Wiegand code，IT986x 最多可支援兩組 Wiegand 介面，每個介面都提供的標準 Wiegand 26、Wiegand 34 及 Wiegand 37 協定，使用者也可以自己撰寫符合特定系統的協定長度。
	
Wiegand 輸入格式
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

IT986x 支援三種輸入格式：

1. Wiegand 26

  Wiegand 26 協定總共有 26 位二進制數字，其中

  - bit0 為 bit1 ~ bit12 的偶同位檢查位元。
  - bit1 ~ bit8 位元為設備碼(Facility code)，共 8 位元。
  - bit9 ~ bit24 位元為使用者資料(User Data)，共 16 位元。
  - bit25 為 bit13 ~ bit24 的奇同位檢查位元。
      
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  | 0 |	1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  | P | F | F | F | F | F | F | F | F | D | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | P  |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  | P | E | E | E | E | E | E | E | E | E | E  | E  | E  |    |    |    |    |    |    |    |    |    |    |    |    |    |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  |   |   |   |   |   |   |   |   |   |   |    |    |    | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | P  |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  
  - E: ven parity
  - O: dd parity
  - F: acility Code
  - D: User Data
  - P: Parity bit

2. Wiegand 34

  Wiegand 34 協定總共有 34 位二進制數字，其中

  -	bit0 為 bit1 ~ bit16 的偶同位檢查位元。
  -	bit1 ~ bit8 位元為設備碼(Facility code)，共 8 位元。
  -	bit9 ~ bit32 位元為使用者資料(User Data)，共 24 位元。
  -	bit33 為 bit17 ~ bit32 的奇同位檢查位元。

  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31 | 32 | 33 |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  | P | F | F | F | F | F | F | F | F | D | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | P  |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  | P | E | E | E | E | E | E | E | E | E | E  | E  | E  | E  | E  | E  | E  |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  |   |   |   |   |   |   |   |   |   |   |    |    |    |    |    |    |    | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | P  |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+

  - E: Even parity
  - O: Odd parity
  - F: Facility Code
  - D: User Data
  - P: Parity bit

3. Wiegand 37

  Wiegand 37 協定總共有 37 位二進制數字，其中

  -	bit0 為 bit1 ~ bit18 的偶同位檢查位元。
  -	bit1 ~ bit35 位元為使用者資料(User Data)，共 35 位元。
  -	bit36 為 bit18 ~ bit35 的奇同位檢查位元。

  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31 | 32 | 33 | 34 | 35 | 36 |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  | P | D | D | D | D | D | D | D | D | D | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | P  | 
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+ 
  | P | E | E | E | E | E | E | E | E | E | E  | E  | E  | E  | E  | E  | E  | E  | E  |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  |   |   |   |   |   |   |   |   |   |   |    |    |    |    |    |    |    |    | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | P  |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+

  - E: Even parity
  - O: Odd parity
  - D: Data
  - P: Parity bit 

相關的 KConfig 設定
----------------------------------------------------------------

- :menuselection:`Peripheral --> Wiegand Enable`

  決定是否要啟用 Wiegand 功能，若要啟用 Wiegand 相關功能都必須啟用此選項。

- :menuselection:`Peripheral --> Wiegand Enable --> Wiegand#`

  提供兩組 Wiegand 介面，可以同時啟動，也可以擇一啟動。

- :menuselection:`Peripheral --> Wiegand Enable --> Wiegand# --> Bit Count`

  設定輸入格式，有 Wiegand 26、Wiegand 34 及 Wiegand 37，也可自定位元數客製化格式。

- :menuselection:`Peripheral --> Wiegand Enable --> Wiegand# --> Bit Count --> Customize --> Customize Module`

  設定更換客製化驅動檔案。

- :menuselection:`Peripheral --> Wiegand Enable --> Wiegand# --> WIEGAND0_GPIO0`

  設定 Data0 的 GPIO pin 號碼。

- :menuselection:`Peripheral --> Wiegand Enable --> Wiegand# --> WIEGAND0_GPIO1`

  設定 Data1 的 GPIO pin 號碼。

相關的程式碼檔案
----------------------------------------------------------------

.. flat-table::
  :header-rows: 1
  
  * - 路徑
    - 描述
  * - ``sdk/driver/itp/itp_wiegand.c``	
    - 參考 API 原型。

 
相關的 API 描述
---------------------------------------

本節描述用來操作 Wiegand 裝置的相關 API。Wiegand driver 提供依據 POSIX 規範實作的 API，用戶可以透過 ``open()``/``read()``/``write()``/``ioctl()`` 等函式對 Wiegand 裝置進行如讀寫檔案般的操作。

itpRegisterDevice
^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void itpRegisterDevice(ITPDeviceType type, const ITPDevice *device)

  註冊裝置。

**參數**

``ITPDeviceType type``

  裝置類型。Wiegand 裝置相關的裝置類型有 ``ITP_DEVICE_WIEGAND0`` ~ ``ITP_DEVICE_WIEGAND1`` 。

``const ITPDevice *device``

  裝置標識符。Wiegand 裝置相關的裝置標識符有 ``itpDeviceWiegand0`` ~ ``itpDeviceWiegand1`` 。

**描述**

此函式可用來將 Wiegand 裝置註冊到核心裝置列表中。

 
ioctl
^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr)

**參數**

``int file``

  裝置標識符。其值為 ``ITPDeviceType`` 列表中的值。與 Wiegand 裝置相關的裝置標識符有 ``ITP_DEVICE_ WIEGAND0`` ~ ``ITP_DEVICE_ WIEGAND1`` 。

``unsigned long request``

  request 參數用來選擇要對裝置運行的操作功能，並將取決於所指定的裝置。

``void *ptr``

  ptr 參數表示對此特定裝置執行請求的功能所需的額外訊息。ptr 的型別取決於特定的控制請求，但它可以是整數，也可以是指向裝置特定資料結構的指標。

**描述**

``ioct()`` 函數用於在 Wiegand 裝置上執行各種控制功能。適用於 Wiegand 裝置的 ``ioctl()`` 命令，如下所述。

- ITP_IOCTL_INIT

  用來初始化 Wiegand 裝置。

- ITP_IOCTL_ENABLE

  用來啟動 Wiegand 裝置。

- ITP_IOCTL_SET_BIT_COUNT

  用來設定 Wiegand 協定的模式。

- ITP_IOCTL_SET_GPIO_PIN

  用來設定 Wiegand Data 的 GPIO pin 號碼。

read
^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int read(int __fd, void *__buf, size_t __nbyte)

**參數**

``int __fd``

  欲執行的Wiegand裝置。EX: ``ITP_DEVICE_ WIEGAND0`` 。

``void *__buf`` 

  讀取後寫回的位置

``size_t __nbyte``

  值為 0。

**描述**

使用者可透過此函式從 Wiegand 裝置中來讀取感應卡中的 Wiegand code。
 
範例程式
---------------------

.. code-block:: c 

    #include "ite/itp.h"    // for ITP_DEVICE_WIEGAND0 & ITP_DEVICE_WIEGAND1

    int main(void)
    {
        int bit_count;
        int wiegand_gpio[2];

        // wiegand 0
        itpRegisterDevice(ITP_DEVICE_WIEGAND0, &itpDeviceWiegand0);
        ioctl(ITP_DEVICE_WIEGAND0, ITP_IOCTL_INIT, NULL);
        bit_count = WIEGAND0_BIT_COUNT;
        ioctl(ITP_DEVICE_WIEGAND0, ITP_IOCTL_SET_BIT_COUNT, &bit_count);
        wiegand_gpio[0] = CFG_WIEGAND0_GPIO0;
        wiegand_gpio[1] = CFG_WIEGAND0_GPIO1;
        ioctl(ITP_DEVICE_WIEGAND0, ITP_IOCTL_SET_GPIO_PIN, wiegand_gpio);
        ioctl(ITP_DEVICE_WIEGAND0, ITP_IOCTL_ENABLE, NULL);

        // wiegand 1
        itpRegisterDevice(ITP_DEVICE_WIEGAND1, &itpDeviceWiegand1);
        ioctl(ITP_DEVICE_WIEGAND1, ITP_IOCTL_INIT, NULL);
        bit_count = WIEGAND1_BIT_COUNT;
        ioctl(ITP_DEVICE_WIEGAND1, ITP_IOCTL_SET_BIT_COUNT, &bit_count);
        wiegand_gpio[0] = CFG_WIEGAND1_GPIO0;
        wiegand_gpio[1] = CFG_WIEGAND1_GPIO1;
        ioctl(ITP_DEVICE_WIEGAND1, ITP_IOCTL_SET_GPIO_PIN, wiegand_gpio);
        ioctl(ITP_DEVICE_WIEGAND1, ITP_IOCTL_ENABLE, NULL);

        while(1)
        {
            char* card_id0, card_id1;

            read(ITP_DEVICE_WIEGAND0, &card_id0, 0);
            read(ITP_DEVICE_WIEGAND1, &card_id1, 0)

            usleep(10000);
        }
    }

.. raw:: latex

    \newpage
