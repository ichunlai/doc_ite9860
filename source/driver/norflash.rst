.. _norflash:

NOR Flash 
================

功能描述
---------------------------

SPI NOR flash 是 IT986x 的其中一種外接儲存設備，主要用來儲存 Bootloader、Image 和 FAT file system 等。實體層的溝通協議採用 SPI，使用者可以根據 KConfig 決定哪一個 SPI 模組做為傳輸媒介，而 NOR flash driver 的底層是呼叫 SPI driver 來存取 NOR flash，相關的 SPI 介紹可以參考 :ref:`spi`，另外，IT986x 支援高速的 AXISPI 與低速的 SPI(SPI0 and SPI1)，SPI NOR flash 可以接在 AXISPI，也可以接在 SPI，但只有 AXISPI 支援 NOR booting 的功能。由於現今 NOR flash 規格不一，使用前也必須查看 AC/DC CHARACTERISTICS 的電氣特性是否相符，以及確認 NOR CMD 是否與 Mask ROM 和 NOR flash driver 的定義一致，對於已在 IT986x 驗證過的 NOR 型號，可以參考 :ref:`norflash_support_list` 。相關 SPI 模組以及儲存分區的詳述如下。

- AXISPI

  較高速的 SPI 控制器，SPI clock 最高可達 66 Mhz 至 80 Mhz，並且支援 QUAD 運作模式。最高理論值之傳輸速率為 (80 Mhz / 8) * 4 = 40 MB/s，而 IT986x 僅支援 AXISPI 的NOR booting。

- SPI

  較低速的 SPI 控制器，IT986x 提供兩組 SPI 控制器(SPI0 and SPI1)，SPI clock 最高可達 20 Mhz，但僅支援 Single 運作模式。最高理論值之傳輸速率為 (20 Mhz / 8) = 2.5 MB/s，而 SPI 控制器不支援 NOR booting 功能。

- Reserved Area

  此區域主要做為存放 Bootloader、Image、backup-pkg、MAC address 以及使用者自行定義的資料。

- Disk Partition

  此區域為 file system 所管控的區域。

相關的 KConfig 設定
----------------------------------------------------------------

- :menuselection:`Storage --> NOR Device`

  決定是否要啟用 SPI NOR flash 裝置。

- :menuselection:`Storage --> NOR Device --> NOR device use axispi`

  選擇 NOR 所使用的 SPI 模組。勾選代表選擇 AXISPI，反之則代表選擇 SPI0 and SPI1，而 SPI0 and SPI1 的詳細設定，可以參考 :ref:`spi` 。

- :menuselection:`Storage --> NOR Device --> NOR device use axispi --> NOR enable QUADMODE`

  若 SPI NOR flash 支援 QUAD 模式，可勾選來開啟 AXISPI QUAD 模式，反之則代表選擇 AXISPI Single 模式。Single 和 QUAD 模式的 AXISPI divider 設定為 2。

- :menuselection:`Storage --> NOR Device --> NOR device use axispi --> NOR enable QUADMODE --> NOR enable DTRMODE`

  若 SPI NOR flash 支援 DTR 模式，可勾選來開啟 AXISPI DTR 模式，反之則代表選擇 AXISPI QUAD 模式。DTR 模式的 AXISPI divider 設定為 4。

- :menuselection:`Peripheral --> AXISPI_Enable`

  決定是否要啟用 AXISPI 裝置。

- :menuselection:`GPIO --> AXISPI#`

  設定 AXISPI GPIO，目前 HW 設計為特定 PIN number，可選擇的 PIN number 可參考 AXISPI 腳位的註解。

相關的程式碼檔案
----------------------------------------------------------------

.. flat-table::
   :header-rows: 1
   
   * - 路徑
     - 描述
   * - ``<sdk_root>/sdk/driver/itp/itp_nor.c``
     - 最上層函式，主要提供 POSIX 規範實作的 API，供上層 AP 透過 ``open()``、``lseek()``、``read()``、``write()``、``close()``、``ioctl()`` 等函式對裝置進行操作。

   * - | ``<sdk_root>/sdk/include/nor``
       | ``<sdk_root>/sdk/driver/nor``
     - 底層函式，內含 SPI NOR flash 驅動程式。
 
相關的 API 描述
---------------------------------------

本節描述用來操作 SPI NOR flash 裝置的相關 API。NOR flash driver 提供依據 POSIX 規範實作的 API，用戶可以透過 ``open()``/``lseek()``/``read()``/``write()``/``close()``/``ioctl()`` 等函式對 NOR 裝置進行如讀寫檔案般的操作。

itpRegisterDevice
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void itpRegisterDevice(ITPDeviceType type, const ITPDevice *device);

  註冊裝置。

**參數**

``ITPDeviceType type`` 

  裝置類型。完整裝置類型列表請見 ``<sdk_root>/sdk/include/ite/itp.h`` 。與 NOR 裝置相關的裝置類型為 ``ITP_DEVICE_NOR`` 。

``const ITPDevice *device``

  裝置標識符。與 NOR 裝置相關的裝置標識符為 ``itpDeviceNor`` 。

**描述**

此函式可用來將 NOR flash 裝置註冊到核心裝置列表中，使上層 AP 可以透過 open/close/ioctl/read/write 等函式來操作 NOR 的功能。本函式會在系統初始化期間被 ``itpInit()`` 函式呼叫 (見 ``<sdk_root>/sdk/driver/itp_init_openrtos.c`` )，因此若在您的專案中有呼叫過 ``itpInit()`` 函式，將無須再執行此註冊函式。
 
ioctl
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr);

  控制裝置。

**參數**

``int file``

  裝置標識符。其值為 ``ITPDeviceType`` 列表中的值。與 NOR 裝置相關的裝置標識符為 ``ITP_DEVICE_NOR`` 。

``unsigned long request``

  request 參數用來選擇要對裝置運行的操作功能，並將取決於所指定的裝置。

``void *ptr``

  ptr 參數表示對此特定裝置執行請求的功能所需的額外訊息。ptr 的型別取決於特定的控制請求，但它可以是量化整數，也可以是指向裝置特定資料結構的指標。

**描述**

``ioctl()`` 函數用於在 NOR 裝置上目前有 INIT、GET_BLOCK、GET_GAP 和 FLUSH 等功能。

- ITP_IOCTL_INIT

  用來初始化 NOR 裝置，底下展示如何透過預設設定來初始化 NOR 裝置。

  .. code-block:: c

    itpRegisterDevice(ITP_DEVICE_NOR, &itpDeviceNor);
    ioctl(ITP_DEVICE_NOR, ITP_IOCTL_INIT, (void *)0);

- ITP_IOCTL_GET_BLOCK_SIZE

  用來取得 NOR 裝置的 Block size，底下展示 API 的使用方式。

  .. code-block:: c

    itpRegisterDevice(ITP_DEVICE_NOR, &itpDeviceNor);
    ioctl(ITP_DEVICE_NOR, ITP_IOCTL_GET_BLOCK_SIZE, &blocksize);

- ITP_IOCTL_GET_GAP_SIZE

  用來取得 NOR 裝置的 Gap size，底下展示 API 的使用方式。

  .. code-block:: c

    itpRegisterDevice(ITP_DEVICE_NOR, &itpDeviceNor);
    ioctl(ITP_DEVICE_NOR, ITP_IOCTL_GET_GAP_SIZE, &gapsize);

- ITP_IOCTL_FLUSH

  若開啟 FAT NOR cache 的情況下，可以透過這個 API 將 cache 改動資料寫入 NOR

  .. code-block:: c

    itpRegisterDevice(ITP_DEVICE_NOR, &itpDeviceNor);
    ioctl(ITP_DEVICE_NOR, ITP_IOCTL_FLUSH, NULL);
 
read
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int read(int file, char *ptr, int len);

  從 RX 讀取資料。

**參數**

``int file``

  欲執行的 NOR 埠。EX: ``ITP_DEVICE_NOR`` 。

``char *ptr``

  由 NOR 讀取資料後的記憶體寫入位址。

``int len``

  由 NOR 讀取的長度，單位為 Block。

**描述**

使用者可透過此函式從 NOR 埠中讀回資料，底下展示如何使用該函式。

.. code-block:: c

    // initialize NOR, the method below could be found in itpInit();
    // Register NOR device
    itpRegisterDevice(ITP_DEVICE_NOR, &itpDeviceNor);

    // Do initialization
    ioctl(ITP_DEVICE_NOR, ITP_IOCTL_INIT, NULL);

    // Do NOR Read operation
    fd = open(":nor", O_RDONLY);
    ioctl(fd, ITP_IOCTL_GET_BLOCK_SIZE, &blocksize);
    temp = malloc(blocksize);
    read(fd, temp, 1);
    close(fd);
    free(temp);
 
write
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int write(int file, char*ptr, int len);

  從 TX 發送資料。

**參數**

``int file``

  欲執行的 NOR 埠。EX: ``ITP_DEVICE_NOR`` 。

``char *ptr``

  寫入 NOR 前的記憶體讀取位址。

``int len``

  寫入 NOR 的長度，單位為 Block。

**描述**

使用者可透過此函式從 NOR 埠中寫入資料，底下展示如何使用該函式。

.. code-block:: c

    // initialize NOR, the method below could be found in itpInit();
    // Register NOR device
    itpRegisterDevice(ITP_DEVICE_NOR, &itpDeviceNor);

    // Do initialization
    ioctl(ITP_DEVICE_NOR, ITP_IOCTL_INIT, NULL);

    // Do NOR Write operation
    fd = open(":nor", O_WRONLY);
    ioctl(fd, ITP_IOCTL_GET_BLOCK_SIZE, &blocksize);
    temp = malloc(blocksize);
    write(fd, temp, 1);
    close(fd);
    free(temp);

簡易範例程式
---------------------------------------

.. code-block:: c

    #include "ite/itp.h"

    void main(void)
    {
        int fd = 0;
        uint32_t blocksize = 0;
        uint8_t *temp = 0;

        itpRegisterDevice(ITP_DEVICE_NOR, &itpDeviceNor);
        ioctl(ITP_DEVICE_NOR, ITP_IOCTL_INIT, NULL);

        printf("Start NOR test!\n");

        fd = open(":nor", O_RDONLY);
        if (!fd) printf("--- open device NOR fail ---\n");
        else printf("fd = %d\n", fd);

        if (ioctl(fd, ITP_IOCTL_GET_BLOCK_SIZE, &blocksize))
        printf("get block size error\n");

        temp = malloc(blocksize);

        while (1)
        {
            read(fd, temp, 1);
            printf("NorInfo 0x%x, 0x%x, 0x%x\n", temp[0], temp[1], temp[2]);
        }

        close(fd);
        free(temp);
    }

完整範例展示
---------------------------------------

在 ``<sdk_root>/project/test_nor`` 目錄下附有一個 SPI NOR flash 的驗證程式。該程式展示了三個範例，包括透過 AXISPI 做 NOR 的全區讀寫檢查、透過 SPI 做 NOR 的全區讀寫檢查以及 SPI Slave 的簡易協定之測試。

硬體連接
^^^^^^^^^^^^^^^^^^^^^

底下是 IT986x 與 SPI NOR flash 的連接方式，另外也提供了 IT986x 上 SPI0 (Master) 與 SPI1(Slave) 對接的方式。

- NOR on AXISPI 之連接。
 
  .. image:: /_static/e_circuit_norflash_axispi.png

- NOR on SPI 之連接。
 
  .. image:: /_static/e_circuit_norflash_spi.png

- SPI0 (Master) 與 SPI1 (Slave) 之連接。
 
  .. image:: /_static/e_circuit_spi_master_spi_slave.png

KConfig 設定
^^^^^^^^^^^^^^^^^^^^^

請執行 ``<sdk_root>/build/openrtos/test_nor.cmd`` ，按照以下步驟設定。

- NOR on AXISPI 之驗證。
 
  .. image:: /_static/qconf_storage_nor.png

  .. image:: /_static/qconf_peripheral_axispi.png

  .. image:: /_static/qconf_gpio_axispi.png
 
- NOR on SPI 之驗證。

  .. image:: /_static/qconf_storage_spi_nor.png

  .. image:: /_static/qconf_peripheral_spi.png

  .. image:: /_static/qconf_gpio_spi_nor.png
 
- SPI0(Master) 與 SPI1(Slave) 之驗證。
 
  .. image:: /_static/qconf_storage_spi_nor.png

  .. image:: /_static/qconf_peripheral_spi.png

  .. image:: /_static/qconf_gpio_spi_master_spi_slave.png

  .. image:: /_static/qconf_spi_slave_test_spi_bist.png
 
驗證結果
^^^^^^^^^^^^^^^^^^^^^

  .. |run_icon| image:: /_static/qconf_run_icon.png
    :alt: run icon
    :width: 14
    :height: 14

  .. |build_icon| image:: /_static/qconf_build_icon.png
    :alt: build icon
    :width: 14
    :height: 14

1. 點擊 |build_icon| 按鈕建置專案。確保：

  - USB to SPI 轉換板已正確安裝。
  
    - 若您尚未將 USB to SPI 轉換板與 PC 連接，請參考 :ref:`usb2spi_connect2pc` 。
    - 若您尚未將 USB to SPI 轉換板與 EVB 連接，請參考 :ref:`usb2spi_connect2board` 。
    - 若您尚未安裝 USB to SPI 轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。
  
  - EVB 已經切換為 Co-operative Mode。
  
    .. note:: 
  
       若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

2. 重置 EVB 的電源。
3. 執行 Tera Term 並監測板端log。
4. 等程式建置完成，點擊 |run_icon| 按鈕即可透過 USB to SPI 轉換板啟動系統。
5. NOR on AXISPI之驗證結果。
 
  .. image:: /_static/teraterm_nor_axispi.png

6. NOR on SPI之驗證結果。

  .. image:: /_static/teraterm_nor_spi.png
 
7. SPI0(Master) 與 SPI1(Slave) 之驗證結果。
 
  .. image:: /_static/teraterm_spi_master_spi_slave.png
 


.. raw:: latex

    \newpage
