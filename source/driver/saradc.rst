.. _saradc :

SAR ADC
========


功能描述
--------------------

SAR ADC 提供類比信號至數位信號的轉換功能，能夠透過 8 個 channel 去偵測不同的感應器。在一般應用上，使用者可以將數位信號的輸出儲存在記憶體內，而它也能夠透過計算數位輸出之平均的方式，去偵測和蒐集特定輸出範圍的資訊。理論上來說，每一筆數位輸出以 12 位元來表示，而這個輸出則會根據輸入電壓的大小等比例地變動，範圍介於 0V 至 3.3V 間。相關詳述如下。

- 取樣率

  - ADC Clock (Hz)：WCLK / ADC Divider
  - ADC Conversion Time (sec)：12 / ADC Clock
  - ADC Sample Rate (Hz)：1 / ADC Conversion Time

  .. note::

    實際使用的 WCLK 頻率可透過 `ithGetBusClock()` 獲得。

- 規格

  - Input Channel：

    Configurable 8 channels share the ADC.

  - Analog Range：

    0.3V to 3.0V.

  - Sample Rate：

    Up to 1 MHz of sample rate.

  - 12-bit Resolution：

    | Effective 5-bit resolution without calibration.
    | Effective 6-bit resolution with external calibration.
    | Effective 6-bit resolution with internal calibration.

- 記憶體輸出格式

  .. flat-table::
    :widths: 1 1 6

    * - :cspan:`2` 8 byte
    * - 1 byte
      - 1 byte
      - 6 byte
    * - raw_1_low (0 ~ 7 bit)
      - raw_1_high (0 ~ 7 bit)
      - raw data


相關的 KConfig 設定
----------------------------------------------------------------

-	:menuselection:`Peripheral --> SARADC Enable`

  決定是否要啟用 SAR ADC 裝置，並設定有效的 XAIN，該值必須介於 0x0 至 0xff 之間，由低位元到高位元分別代表對應的 GPIO Pin。

-	:menuselection:`Peripheral --> SARADC Enable --> SARADC Calibration Enable`

  決定是否要啟用 SAR ADC 校準功能。

-	:menuselection:`Peripheral --> SARADC Enable --> SARADC Calibration Enable --> Calibration Reference Type`

  決定 SAR ADC 的校準模式，分別為外部電壓之校準模式和內部分壓之校準模式。

  -	外部電壓之校準模式：

    參考外部電壓來實現校準功能，同時必須設定校準 XAIN 以及校準時所參考的輸入電壓。XAIN 必須介於 0x0 至 0xff 之間，由低位元到高位元分別代表對應的 GPIO Pin；而輸入電壓必須介於 0 至 3.3 之間，由低索引到高索引分別代表對應的校準 GPIO Pin。

  -	內部分壓之校準模式：

    參考內部分壓來實現校準功能，同時必須設定校準 XAIN 以及校準時所參考的輸入電壓。XAIN 必須介於 0x0 至 0x1f 之間，由低位元到高位元分別代表對應的參考電壓 0.0V、0.825V、1.8V、2.475V 及 3.3V；而輸入電壓必須介於 0 至 3.3 之間，由低索引到高索引分別代表對應的校準控制信號。

-	:menuselection:`GPIO --> XAIN#`

  設定 SAR ADC GPIO，目前 HW 設計為固定 PIN number，無法任意更動。用不到的 SAR ADC GPIO 不會與其他 GPIO 功能衝突，亦不能設置為 “-1”。


相關的程式碼檔案
----------------------------

.. flat-table::
  :header-rows: 1

  * - 路徑
    - 描述
  * - | ``<sdk_root>/sdk/include/saradc/*``
      | ``<sdk_root>/sdk/driver/saradc/*``
      | ``<sdk_root>/sdk/driver/ith/<chip_family_id>/ith_saradc.c``
    - 底層函式，內含 SAR ADC 重置相關行為以及驅動程式。

相關的 API 描述
---------------------------------------

本節描述用來操作 SAR ADC 裝置的相關 API。

mmpSARInitialize
^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: SARADC_RESULT mmpSARInitialize(SARADC_MODE_AVG modeAVG, SARADC_MODE_STORE modeStore, SARADC_AMPLIFY_GAIN amplifyDriving, SARADC_CLK_DIV divider);

  SAR ADC 初始化 API。

**參數**

``SARADC_MODE_AVG modeAVG``

  modeAVG 代表是否開啟 engine 的 AVG 及 INTR 偵測模組，一般都是設為開啟，才能取得完整的功能。完整的類型列表請見 ``<sdk_root>/sdk/include/saradc/<chip family id>/saradc_type.h`` 。

``SARADC_MODE_STORE modeStore``

  modeStore 代表在記憶體中存放 RAW 或 AVG 的數位信號(AVG 預設為 32 筆 RAW data 之平均)。完整的類型列表請見 ``<sdk_root>/sdk/include/saradc/<chip family id>/saradc_type.h`` 。

``SARADC_AMPLIFY_GAIN amplifyDriving``

  amplifyDriving 代表 I/O driving，可以根據類比電路的阻抗而調整，一般狀況設定為一倍即可。完整的類型列表請見 ``<sdk_root>/sdk/include/saradc/<chip family id>/saradc_type.h`` 。

``SARADC_CLK_DIV divider``

  divider 代表 engine 的工作時脈之控制，可以藉此更改取樣率。完整的類型列表請見 ``<sdk_root>/sdk/include/saradc/<chip family id>/saradc_type.h`` 。


**回傳值**

函式返回值。完整的返回類型列表請見 ``<sdk_root>/sdk/include/saradc/<chip family id>/saradc_error.h`` 。初始成功將返回 ``SARADC_SUCCESS`` 。


mmpSARTerminate
^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: SARADC_RESULT mmpSARTerminate(void)

  SAR ADC 解構 API。重置 engine 和關閉工作時脈。

**回傳值**

函式返回值。完整的返回類型列表請見 ``<sdk_root>/sdk/include/saradc/<chip family id>/saradc_error.h`` 。解構成功將返回 ``SARADC_SUCCESS`` 。


mmpSARConvert
^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: SARADC_RESULT mmpSARConvert(SARADC_PORT hwPort, uint16_t wbSize, uint16_t *avg);

  SAR ADC 取樣 API。呼叫後，engine 寫完指定的記憶體區間，便會停止取樣。

**回傳值**

函式回傳值。完整的回傳類列表請見 ``<sdk_root>/sdk/include/saradc/<chip family id>/saradc_error.h`` 。取樣成功將返回 ``SARADC_SUCCESS`` 。

**參數**

``SARADC_PORT hwPort``

  hwPort 代表從第幾個 channel(GPIO) 進行取樣功能。完整的類型列表請見 ``<sdk_root>/sdk/include/saradc/<chip family id>/saradc_type.h`` 。

``uint16_t wbSize``

  wbSize 代表記憶體的總取樣量，單位為 Byte，必須為 512B 以上。

``uint16_t *avg``

  avg 可以在取樣結束後，回傳 wbSize 中所有數位輸出的平均值。


簡易範例程式
----------------------

.. code-block:: c

    #include "ite/itp.h"
    #include "saradc/saradc.h"

    void main(void)
    {
        SARADC_RESULT result = SARADC_SUCCESS; // SAR ADC error number
        int channel = 0; // channel number
        uint16_t writeBuffer_len = 512, calibrationOutput = 0; // SAR ADC average buffer and average output

        // setting of clock, average, store type on memory, amplifier level and initialization
        result = mmpSARInitialize(
          SARADC_MODE_AVG_ENABLE,
          SARADC_MODE_STORE_RAW_ENABLE,
          SARADC_AMPLIFY_1X,
          SARADC_CLK_DIV_9);
        if (result) printf("mmpSARInitialize() error (0x%x) !!\n", result);

        while (1) // conversion loop of SAR ADC
        {
            if (result = mmpSARConvert(channel, writeBuffer_len, &calibrationOutput))
                printf("mmpSARConvert() error (0x%x) !!\n", result);
            else
                printf("Calibration Output:%d\n", calibrationOutput);
        }

        result = mmpSARTerminate(); // reset engine and disable clock
        if (result) printf("mmpSARTerminate() error (0x%x) !!\n", result);
    }


完整範例展示
----------------------

在 ``<sdk_root>/project/test_saradc`` 目錄下附有一個 SAR ADC 範例程式。該程式展示一個 XAIN_0 至 XAIN_7 持續循序的取樣功能。預設每 XAIN 收滿 256 筆 RAW data 時，返回 256 筆 RAW data 的平均值，並往下一個 XAIN 繼續取樣。

硬體連接
^^^^^^^^^^^^^^^^^^^^^^^^

底下我們使用一個 16-Keypad 的外接分壓器來模擬類比信號，根據輸入電壓所對應的數位輸出平均值，進而判斷 SAR ADC 是否能正確達到類比轉數位之目的。

- 將分壓器的地線接入 EVB 之接地端。
- 將分壓器的電源線接入 EVB 之 3.3V 供應端。
- 將分壓器的分壓輸出線接入 EVB 之 XAIN#。該範例接入 EVB XAIN_3(GPIO_22)。

  .. image:: /_static/e_circuit_saradc.png

KConfig 設定
^^^^^^^^^^^^^^^^^^^

請執行 ``<sdk_root>/build/openrtos/test_saradc.cmd`` ，按照以下步驟設定。

- 勾選 :menuselection:`Peripheral --> SARADC Enable` 。有效的 XAIN 設為 0xff。
- 勾選 :menuselection:`Peripheral --> SARADC Enable --> SARADC Calibration Enable --> Internal Voltage Reference` 。校準 XAIN 設為 0xe(0.825V, 1.8V, 2.475V)，校準參考電壓則使用統計過後的最佳值(0.0V、0.847V、1.822V、2.395V、3.3V)。

  .. image:: /_static/qconf_peripheral_saradc.png

- 將 :menuselection:`GPIO --> XAIN#` 依序設為 19 ~ 26。

  .. image:: /_static/qconf_gpio_xain.png

驗證結果
^^^^^^^^^^^^^^^^

  .. |run_icon| image:: /_static/qconf_run_icon.png
    :alt: run icon
    :width: 14
    :height: 14

  .. |build_icon| image:: /_static/qconf_build_icon.png
    :alt: build icon
    :width: 14
    :height: 14

1. 點擊 |build_icon| 按鈕建置專案。確保：

  - USB to SPI 轉換板已正確安裝。

    - 若您尚未將 USB to SPI 轉換板與 PC 連接，請參考 :ref:`usb2spi_connect2pc` 。
    - 若您尚未將 USB to SPI 轉換板與 EVB 連接，請參考 :ref:`usb2spi_connect2board` 。
    - 若您尚未安裝 USB to SPI 轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

  - EVB 已經切換為 Co-operative Mode。

    .. note::

       若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

2. 重置 EVB 的電源。
3. 執行 Tera Term 並監測板端 log。
4. 等程式建置完成，點擊 |run_icon| 按鈕即可透過 USB to SPI 轉換板啟動系統。
5. 測試結果，在每次的 XAIN_3 取樣前，按下不同按鍵。

 .. image:: /_static/teraterm_xain_example_log.png


.. raw:: latex

    \newpage