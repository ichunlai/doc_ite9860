.. _nandflash:

NAND Flash 
================

功能描述
---------------------------

NAND 作為資料儲存裝置，在硬體上有 AXISPI 與 SPI 作為 I/O 介面 (IT986X 不支援 Parallel 介面的 NAND)，軟體上有作為 Boot-Loader 與 boot-Image 等 raw Data 的保留區(Reserved Area) 以及擁有檔案系統的磁碟儲存區(Disk Area)。由於現今 NAND 規格不一，使用前也必須查看 AC/DC Characteristics 的電氣特性是否相符，以及確認 NAND CMD 是否與 Mask ROM 和 NAND driver 的定義一致，對於已在 IT986x 驗證過的 NAND 型號，可以參考 :ref:`nandflash_support_list`。

NAND 模組的軟體架構，基本上是建立在檔案系統與 FTL(Flash Translation Layer) 上，檔案系統透過 FTL 呼叫 NAND 模組的 driver，而 NAND Driver 是遵循 FTL 所制訂的 API 所編寫的。但 SDK 不提供 FTL 的原始碼，僅提供 Library。另外 AP 層也可透過 ITP 的 API 呼叫 NAND 的 Driver，對 NAND Flash 進行低階的讀寫操作。通常是用在存取 Bootloader 與 Boot-Image 這類非頻繁讀寫的資料，因為透過 ITP 介面讀寫 NAND Flash 是沒有受到 FTL 的平均抹除演算法的保護，若太過頻繁的讀寫容易導致 NAND Flash 出現壞塊。

由於 NAND Flash 是以 SPI 作為介面，所以 Driver 的底層是呼叫 SPI 的 API 存取 NAND Flash。IT986X Chip 本身支援高速的 AXISPI 與低速的一般 SPI(SPI0/SPI1)。依據專案的應用，SPI NAND 可以接在 AXISPI，也可以接在一般 SPI0/SPI1 上，但只有 AXISPI 支援 NAND Boot 的功能。

以下是關於 AXISPI、SPI、與資料分區規劃的介紹：

- AXISPI：

  較高速的 SPI 控制器，SPI Clock 最高可支援 66 ~ 80MHZ，並且支援 QUAD I/O 運作模式，最高理論值速度可達 (80Mhz / 8) * 4 = 40 MB/s。實際速度約 6 ~ 10MB/s。IT986X 只支援 AXISPI 的 NAND BOOTING。

- SPI0/SPI1：

  較低速的 SPI 控制器，IT986X 提供兩組 SPI 控制器 (SPI0 & SPI1)。SPI Clock 最高可支援 20MHZ，single I/O 運作模式，最高理論值速度 (20Mhz / 8) = 2.5 MB/s。實際速度約 1.8 ~ 2.2MB/s。 SPI 控制器不支援開機功能。

- Reserved Area：

  資料分區上分成 "Reserved Area" 以及 "Disk Area"，"Reserved Area" 是不受 File System 管控的區域，此區域主要做為存放 Bootloader、Boot-Image、backup-pkg、MAC address、或是其他 USER 自行定義的資料。此區有簡易的壞塊管理機制，所以沒有誤用壞塊的問題。但本區域不受平均損耗的演算法保護，所以應避免過於頻繁的讀寫操作，這正好適合類似 Bootloader 或是 Boot-Image 等這類每次開機只讀一次或是鮮少更新的資料。Reserved Area 有一個限制，就是無法動態調整 Reserved Area 的 Size，例如某顆 NAND 規劃 Reserved Area 為 16MB，這顆 NAND 就只能一直維持 16MB，不能中途改成 12MB 或是 24MB，如要修改 Reserved Area Size，則必須更換另一顆全新的 NAND。所以專案在一開始的開發階段，請先規劃較大的 Reserved Area Size，或是精算一下所需的 Reserved Area Size，以免頻繁更換全新的 NAND，影響開發進度。

  - Bootloader：Bootloader 與 NAND 開機有關，Bootloader 起始位置固定放在 NAND Block0 的Page0 (即 position = 0x000000)。IT986X 會依據 BOOT-CONFIG 來決定開機的 DEVICE。若開機過程中發現 NAND 裡面沒有存放 Bootloader 則回報開機失敗。
  
  - Boot-Image：此為專案的應用程式，固定放在 Reserved Area 中的某個指定位置。USER 應依據專案內容，提供足夠的空間來存放此 Boot-Image。因 NAND 有 Bad Block 的問題，通常建議規劃 2 倍的 Boot-Image size 來存放 Boot-Image (EX: Boot-Image size=2MB，BOOTLOADER=350KB，則RESERVED AREA 建議為 4MB + 1MB 取整數 8MB，BOOT-IMAGE POSITION 設為 1MB (0x100000) 位置，這樣 BOOT-IMAGE 膨脹成 6MB 時仍可使用)。

- Storage(disk/partition)：

  本區域為 FILE SYSTEM 所管控的區域，FILE SYSTEM 內含 NAND 的壞塊管理與平均損耗(WEAR LEVELING)的功能，以免 NAND 過早出現壞塊問題。 

相關的 KConfig 設定
----------------------------------------------------------------

關於 NAND 的應用規劃，有單顆 NAND 的應用(即使用 AXISPI 連接 SPI NAND)，以及 NOR + NAND 的應用(有三種組合，即 ”AXISPI-NOR + AXISPI-NAND”、”AXISPI-NOR + SPI0-NAND”、”AXISPI-NOR + SPI1-NAND”)

關於一般單顆 SPI NAND 的應用配置，需要先啟用 AXISPI 並設定 AXISPI 的 GPIO PIN，然後再勾選 NAND Device 。本節將著重 NAND 的配置說明。

.. image:: /_static/qconf_storage_nand.png
 
- :menuselection:`Storage --> NAND Device`

  決定是否要啟用 NAND 裝置，若要啟用 UART0 ~ 5 之間任一裝置都必須啟用此選項，否則將無法出現個別 UART 裝置的詳細設定選項。

- :menuselection:`Storage --> NAND Device --> NAND Interface Type`

  請先忽略此選項，目前 SDK 只有 SPI NAND 可選。

- :menuselection:`Storage --> NAND Device --> SPI NAND Choise SPI Bus`

  決定 SPI NAND 所搭配的 SPI 模組，單顆 SPI NAND 請選擇使用 AXISPI。

- :menuselection:`Storage --> NAND Device --> NAND Page Size`

  設定 NAND 的 Page Size (單位 Byte)。

- :menuselection:`Storage --> NAND Device --> NAND Block Size`

  設定 NAND 的 Block Size (單位 page)。

- :menuselection:`Storage --> NAND Device --> Enable SPI NAND Boot`

  設定是否開啟 AXISPI NAND 的開機功能。

- :menuselection:`Storage --> NAND Device --> Enable SPI NAND Boot --> Select SPI NAND QUAD Mode Address Type`

  設定 SPI NAND 在 Quad Mode 時所使用的 SPI address 格式，一種是使用三個字節(適用於兆易創新的GD5FxGQ4xB/GD5FxGQ4xE 與華邦的 W25N01GVxxIG，以及美光、旺宏、華邦、東芝、芯天下等 SPI NAND)，一種是使用四個字節(適用於兆易創新的 GD5FxGQ4xC / GD5FxGQ4xF，以及華邦的 W25N01GVxxIT)，這項設定將決定 NAND BOOT 能否成功使用 Quad Mode 開機。但不會導致開機失敗，只會影響開機速度。

以上設定可以透過 ``<sdk_root>/build/_presettings`` 目錄下的檔案做一次設定。設定方式可直接點選 "LOAD" 圖示(如下圖)

.. image:: /_static/qconf_load.png
 
或是從 "FILE" 選單中點選 "LOAD" (如下圖)

.. image:: /_static/qconf_file_load.png
 
到 ``<sdk_root>/build/_presettings`` 目錄下載入 ``_config_spi_nand_boot`` 這個檔案。下載之後會自動設定 AXISPI 與 SPI NAND 的相關設定，且會開啟 NAND BOOT 功能。

NOR + NAND 的配置規劃
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- AXISPI-NOR + SPI0-NAND

  需同時啟用 AXISPI 與 SPI0 (參閱 AXISPI 設定與 SPI0 的設定)，再來配置 NOR 跟 NAND。關於 NOR 的配置，請參考 :ref:`norflash` 。關於 NAND 的配置需取消 Enable SPI NAND Boot 的勾選，並點選 :menuselection:`Storage --> NAND Device --> SPI NAND Choice SPI Bus` 即可。快速載入 _PRESETTINGS 功能要選 _config_spi_nand 這個檔案(不含 NAND BOOTING 的功能的預設檔)。然後在 "Enable SPI NAND Boot" 這個選項中選擇 "SPI NAND SPI0" 。

- AXISPI-NOR + SPI1-NAND

  需同時啟用 AXISPI 與 SPI0 (參閱 AXISPI 設定與 SPI0 的設定)，再來配置 NOR 跟 NAND。關於 NOR 的配置，請參考 :ref:`norflash` 。關於 NAND 的配置需取消 "Enable SPI NAND Boot" 的勾選，並點選 :menuselection:`Storage --> NAND Device --> SPI NAND Choise SPI Bus` 即可。快速載入 _presettings 功能要選 _config_spi_nand 這個檔案(不含 NAND Booting 的功能的預設檔)。然後在 "Enable SPI NAND Boot" 這個選項中選擇 "SPI NAND SPI1"。

File System 與 NAND的分區規劃
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 
.. image:: /_static/qconf_file_system_nand.png

上圖展示一般 SPI NAND 的分區規劃，其中 NAND reserved Size: 0x1000000 規劃表示 NAND 的前面 16MB 的空間規劃為 Reserved Area，而這 16MB 的空間是不受 File System 所管控的。此區域只能透過 ITP 的 NAND Driver 才能正確存取。此區域至少會存在兩種資料，一是 Bootloader，二是  boot-Image。Bootloader 位置固定在 0，而 boot-Image 的位置設定在 :menuselection:`Upgrade --> Upgrade Image --> Image position` 。一般預設在 3MB(0x300000) 的位置。
 
.. image:: /_static/qconf_upgrade_upgrade_image.png

NAND 的 Reserved Area 區域之後的所有分區全都屬於 NAND Disk Partition 的範圍。上圖顯示此區域一共分割 Partition0 ~ Partition3，一共 4 個 Partition，這是為了對應底下的 4 個 The drive of xxxxx disk partition: A ~ D，partition0 對應 A，partition1 對應 B，partition2 對應 C，partition3 對應 D。各 Partiton 的 Size 都有相對應的 Size 設定值，其 Size 總和當然是不能超過 NAND Disk Partition 的容量(即 NAND 的總容量減去 reserved Area 的 size)。各 partition size 請以 Block Size 為單位(即 0X20000 or 0x40000，正確數據以 USER 所使用的 NAND 的規格而定)做規劃。一旦 Size 設定有誤，會導致 File System 因總和大於實際 NAND 能提供的 Size 而 Partition 失敗，或是比實際 NAND 能提供的 Size 小而浪費空間。

相關的程式碼檔案
---------------------------

.. flat-table::
   :header-rows: 1
   
   * - 路徑
     - 描述
   * - ``<sdk_root>/sdk/driver/itp/itp_nand.c``
     - 最上層函式，主要提供 POSIX 規範實作的 API，供上層 AP 透過 ``open()``、``close()``、``ioctl()`` 等函式對裝置進行操作。
   * - ``<sdk_root>/sdk/driver/nand/*.*``
     - NAND driver 相關程式
   * - ``<sdk_root>/sdk/driver/nand/nf_spi_nand.c``
     - SPI NAND 底層 DRIVER，往上承接 FTL layer 往下呼叫 ``spinfdrv.c`` 的 API。
   * - ``<sdk_root>/sdk/driver/ith/spinfdrv.c``
     - SPI NAND 底層 DRIVER，往上承接 ``nf_spi_nand.c`` 的 API，往下呼叫 SPI 的 API 實作 SPI NAND 的 Read/Write/Erase/Read ID 等 command。
   * - ``<sdk_root>/sdk/driver/spi/it9860/axispi/mmp_axispi.c`` 
     - AXISPI driver 相關程式
   * - ``<sdk_root>/sdk/driver/spi/it9860/spi/mmp_spi.c``
     - SPI driver 相關程式

 
相關的 API 描述
---------------------------------------

DEVICE ID : ITP_DEVICE_NAND (參考 ``<sdk_root>/sdk/include/ite/itp.h`` )

本節描述用來操作 NAND 裝置的相關 API。NAND driver 提供依據 POSIX 規範實作的 API，用戶可以透過 ``open()``/``read()``/``write()``/``ioctl()`` 等函式對 NAND 裝置進行如讀寫檔案般的操作。

itpRegisterDevice
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void itpRegisterDevice(ITPDeviceType type, const ITPDevice *device)

  註冊裝置，即 ``itpRegisterDevice(ITP_DEVICE_NAND, &itpDeviceNand);`` 。
	
**參數**

``ITPDeviceType type``

  裝置類型。完整裝置類型列表請見 ``<sdk_root>/sdk/include/ite/itp.h`` 。與 NAND 裝置相關的裝置類型是 ``ITP_DEVICE_NAND`` 。

``const ITPDevice *device``

  裝置標識符。與 NAND 裝置相關的裝置標識符有 ``itpDeviceNand`` 。
	
**描述**

此函式可用來將 NAND 裝置註冊到核心裝置列表中，使上層 AP 可以透過 ioctl/read/write 等函式來操作 NAND 的功能。本函式會在系統初始化期間被 ``itpInit()`` 函式呼叫 (見 ``<sdk_root>/sdk/driver/itp_init_openrtos.c`` )，因此若在您的專案中有呼叫過 ``itpInit()`` 函式，將無須再執行此註冊函式。

底下展示如何註冊 NAND 裝置到系統核心，並初始化 NAND 裝置。

.. code-block:: c

    itpRegisterDevice(ITP_DEVICE_NAND, &itpDeviceNand); 
    ioctl(ITP_DEVICE_NAND, ITP_IOCTL_INIT, NULL);
 
ioctl
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr)

  控制裝置

**參數**

``int file``

  裝置標識符。其值為 ``ITPDeviceType`` 列表中的值。與 UART 裝置相關的裝置標識符有 ``ITP_DEVICE_NAND`` 。

``unsigned long request``

  request 參數用來選擇要對裝置運行的操作功能，並將取決於所指定的裝置。

``void *ptr``

  ptr 參數表示對此特定裝置執行請求的功能所需的額外訊息。ptr 的型別取決於特定的控制請求，但它可以是整數，也可以是指向裝置特定資料結構的指標。

**描述**

``ioctl()`` 函數用於在 NAND 裝置上執行各種控制功能。request 參數和可選的第三個參數（具有不同的型別）將傳遞給與 file 關聯的 UART 裝置的相應部分並由其解釋。

適用於 NAND 裝置的 ``ioctl()`` 命令，其參數以及適用於每個單獨命令的錯誤狀態如下所述。

- ITP_IOCTL_INIT

  用來初始化 NAND 裝置。ptr 參數為 NULL。

- ITP_IOCTL_GET_BLOCK_SIZE 

  呼叫此函式可以取得 NAND 在 driver 中定義的 Block Size，ptr 參數傳入變數位置(EX: &BlockSize)。

- ITP_IOCTL_FLUSH

  若 KCONFIG 有開啟 FAT file system 功能，則會將 FAT cache 內的資料強制寫回 NAND 中，目前的 SDK 已將 NAND Flush 的機制整合在 File Close 之中，所以每當執行 File System 的 File Close，亦同時完成了 NAND Flush 的動作。

read
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int read(int __fd, void *__buf, size_t __nBlk)

  從 RX 讀取資料。

**參數**

``int __fd``

  欲執行的 NAND Device。EX: ``ITP_DEVICE_NAND`` 。

``void *__buf``

  讀取後寫回的位置，NAND 的 buffer size 最小單位是 Block Size，也就是 Buffer Size 要為 Block Size 的整數倍，Block Size 要由 ``ioctl()`` 的 ``ITP_IOCTL_GET_BLOCK_SIZE`` 取得，此 Size 不會剛好是 0X20000 的整數倍，而是類似 0X1FFFFC 或是 0X3FFFFC，請以此 Size 配置 buffer Size，程式可參閱 ``<sdk_root>/project/bootloader/boot.c`` 的 ``LoadImage()``或是本節最後所附的 ITP NAND Read/Write 範例。

``size_t __nBlk``

  欲讀取之資料長度，單位是 Block Size (參閱前面 ``void *__buf`` 的說明)。

**描述**

使用者可透過此函式從 SPI NAND 讀回資料

底下展示如何使用函式。

.. code-block:: c

    int fd = -1;
    uint8_t *buffer;
    uint32_t blocksize = 0;
    uint32_t pos = 0;
    int RdLen = 1;  // read one block
    
    fd = open(":nand", O_RDONLY, 0);    // open NAND device
    ioctl(fd, ITP_IOCTL_GET_BLOCK_SIZE, &blocksize);    // get NAND block size
    buffer = (uint8_t *)malloc(blocksize);  // locate a buffer with “blocksize”
    lseek(fd, pos, SEEK_SET);   // seek position to block 0
    read(fd, buffer, RdLen);	// read one block data(one block has “blocksize” bytes)
    close(fd);	// close NAND device

write
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int write(int __fd, const void *__buf, size_t __nbyte)

  將資料寫入SPI NAND。

**參數**

``int __fd``

  欲執行的 NAND Device。EX: ``ITP_DEVICE_NAND`` 。

``void *__buf``

  欲送出資料的位置，NAND 的 buffer size 最小單位是 Block Size，也就是 Buffer Size 要為 Block Size 的整數倍，Block Size 要由 ``ioctl()`` 的 ``ITP_IOCTL_GET_BLOCK_SIZE`` 取得，此 Size 不會剛好是 0X20000 的整數倍，而是類似 0X1FFFFC 或是 0X3FFFFC，請以此 Size 配置 buffer Size，程式可參閱 ``<sdk_boot>/project/bootloader/boot.c`` 的 ``LoadImage()`` 或是本節最後所附的 ITP NAND Read/Write 範例。

``size_t __nbyte``

  欲送出之資料長度，單位是 Block Size (參閱前面 ``void *__buf`` 的說明)。

**描述**

使用者可透過此函式從特定 UART 埠中讀回資料

底下展示如何使用函式。

.. code-block:: c

    int fd = -1;
    uint8_t *buffer;
    uint32_t blocksize = 0;
    uint32_t pos = 0;
    int WtLen = 1;	// read one block

    fd = open(":nand", O_RDONLY, 0);    // open NAND device
    ioctl(fd, ITP_IOCTL_GET_BLOCK_SIZE, &blocksize);	// get NAND block size
    buffer = (uint8_t *)malloc(blocksize);	// locate a buffer with “blocksize”
    memset(buffer, 0x00, blocksize);		// set write data
    lseek(fd, pos, SEEK_SET);	// seek position to block 0
    write(fd, buffer, WtLen);	// read one block data(one block has “blocksize” bytes)
    close(fd);  // close NAND device

簡易範例程式
--------------------------------

.. code-block:: c

    #include "ite/itp.h"

    void main(void)
    {
        int fd = 0;
        uint32_t blocksize = 0;
        uint8_t *temp = 0;

        itpRegisterDevice(ITP_DEVICE_NAMD, &itpDeviceNand);
        ioctl(ITP_DEVICE_NAND, ITP_IOCTL_INIT, NULL);
    
        printf("Start NAND test!\n");

        fd = open(":nand", O_RDONLY);
        if (!fd)
            printf("--- open device NAND fail ---\n");
        else
            printf("fd = %d\n”, fd);
    
        if (ioctl(fd, ITP_IOCTL_GET_BLOCK_SIZE, &blocksize))
            printf("get block size error\n”);
    
        temp = malloc(blocksize);

        while (1)
        {
            read(fd, temp, 1);
            printf("SpiInfo 0x%x, 0x%x, 0x%x\n”, temp[0], temp[1], temp[2]);
        }

        close(fd);
        free(temp);
    }

.. raw:: latex

    \newpage
