.. _remote_ir:

Remote IR
==============

功能描述
--------------------

Remote IR (Infrared)是指一種遠程紅外線控制技術；有效傳輸距離達 8 公尺，傳輸速率達 2MBps，發射角度達 120 度，可應用在桌上型電腦、筆記型電腦、數位相機、行動電話等產品。

目前紅外線遙控編碼協議有相當多種，如 NEC、Phlips RC5、RC6、RC-MM、Toshiba、Sharp、JVC、Sony SIRC 等，但主流以 NEC 為大宗。在 test_ir 範例中即是採用 NEC protocol。

.. image:: /_static/_remote_ir_protocol.png

IT986x 支援多達 4 組 IR (IR0 ~ 3) 和三種操作模式 (Interrupt、DMA、FIFO)。

在初始化時，使用者可透過修改寫入 ITH_IR_RX_PRESCALE_REG 暫存器的 prescale 值，達到更改取樣條件，以下為 prescale 公式：

.. math::

  prescale = sampleRate * busClk / precision - 1;

其中

- **precision** 可根據需求調整，其意義為調整取樣時間。
- **sampleRate** 可根據需求調整，其意義為幾個 clock 取樣一次。

目前 Default 設定為 sampleRate = 10，precision = 1000000，即代表 10us 取樣一次。

從 it986x 開始，加入了 IR 頻率偵測之功能。此功能的運作原理為：當 IR 訊號反向時，紀錄從上次至此有幾個 clock 週期震盪，搭配 clock 頻率推導出此 Rx 接收到之頻率。

相關的 KConfig 設定
----------------------------------------------------------------

- :menuselection:`Peripheral --> IrDA Enable`

  決定是否要啟用 IrDA 裝置，若要啟用 IR0 ~ 3 之間任一裝置都必須啟用此選項，否則將無法出現個別 IrDA 裝置的詳細設定選項。

- :menuselection:`Peripheral --> IrDA Enable --> IR# Enable`

  決定是否要啟用 IrDA# 裝置。勾選此選項後，系統才會在 ``itpInit()`` 函式內透過 ``itpRegisterDevice()`` 函式註冊對應 IrDA# 裝置並進行初始化動作。

- :menuselection:`Peripheral --> IrDA Enable --> IR# Enable --> IR# mode`

  決定 IrDA# 裝置的操作模式。分別為 FIFO、Interrupt、DMA 和模式。

  - FIFO 模式: 又稱為 Polling 模式。採用此模式運行的 IrDA 驅動程式採用不斷輪詢裝置的狀態發送數據或接收數據。通常使用在收送速度不快的情況下。在中斷關閉的情況下要發送資料時，只能使用此模式。
  - Interrupt 模式: IrDA 模組在作為接收端時，可在每收到一個 character 的資料或收到的資料有錯誤時就收到中斷。在作為發送端時，可在 IrDA FIFO 空的時候就收到中斷通知。通常使用在收送速度中等的情況下。
  - DMA 模式: IrDA 模組在作為接收端時，可在收到某個數量的資料才觸發中斷。在作為發送端時，類似Interrupt 模式，但可以在發送更大量的資料後才收到中斷。通常使用在收送速度要求較高的情況下。

- :menuselection:`Peripheral --> IrDA Enable --> IR# Enable --> IR# Rx Modulation Enable` 

  當 Rx 接收時有載波，則需要勾選此功能，目前 Default 率波範圍在 20 ~ 60k。

- :menuselection:`Peripheral --> IrDA Enable --> IR# Enable --> IR# Rx sample Enable`

  透過 sampling 功能可以得到目前 Rx 訊號的最高、低，平均和最新頻率(it9860 後才有) 。

- :menuselection:`Peripheral --> IrDA Enable --> IR# Enable --> IR# Tx Modulation Enable`

  當 Tx 發送時需有載波，則需要勾選此功能，目前 Default 率波範圍在 39k。

- :menuselection:`Peripheral --> IrDA Enable --> IR# Enable --> IR# protocol`

  通訊編碼選擇，Default 為常見的 NEC protocol。

- :menuselection:`GPIO --> IR# TX Pin`

  指定該 IrDA 模組要使用哪個 GPIO pin 作為 TX pin。

- :menuselection:`GPIO --> IR# RX Pin`

  指定該 IrDA 模組要使用哪個 GPIO pin 作為 TX pin。

相關的程式碼檔案
----------------------------------------------------------------

.. flat-table::
  :header-rows: 1
   
  * - 路徑
    - 描述
  * - ``<sdk_root>/sdk/driver/itp/itp_ir.c``
    - 最上層函式，主要提供 POSIX 規範實作的 API，供上層 AP 透過 ``open()``、``close()``、``ioctl()`` 等函式對裝置進行操作。
  * - | ``<sdk_root>/sdk/driver/irda/{chip}/irda.c``
      | ``<sdk_root>/sdk/include/irda/irda.h``
      | ``<sdk_root>/sdk/chip/{chip}/include/ith/ith_ir.h``
      | ``<sdk_root>/sdk/driver/ith/ith_ir.c``
    - 底層函式，未來可能會變動。

 
相關的 API 描述
---------------------------------------

本節描述用來操作 IrDA 裝置的相關 API。IrDA driver 提供依據 POSIX 規範實作的 API，用戶可以透過 ``open()``/``read()``/``write()``/``ioctl()`` 等函式對 IrDA 裝置進行如讀寫檔案般的操作。

itpRegisterDevice
^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void itpRegisterDevice(ITPDeviceType type, const ITPDevice *device)

  註冊裝置。

**參數**

``ITPDeviceType type``

  裝置類型。完整裝置類型列表請見 ``<sdk_root>/sdk/include/ite/itp.h`` 。與 IrDA 裝置相關的裝置類型有 ``ITP_DEVICE_IR0`` ~ ``ITP_DEVICE_IR3`` 。

``const ITPDevice *device``

  裝置標識符。與 IR 裝置相關的裝置標識符有 ``itpDeviceIr0`` ~ ``itpDeviceIr3`` 。

**描述**

此函式可用來將 IrDA# 裝置註冊到核心裝置列表中，使上層 AP 可以透過 ``ioctl``/``read``/``write`` 等函式來操作 IrDA 的功能。本函式會在系統初始化期間被 ``itpInit()`` 函式呼叫 (見 ``<sdk_root>/sdk/driver/itp_init_openrtos.c`` )，因此若在您的專案中有呼叫過 ``itpInit()`` 函式，將無須再執行此註冊函式。

 
ioctl
^^^^^^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr)

  控制裝置

**參數**

``int file``

  裝置標識符。其值為 ``ITPDeviceType`` 列表中的值。與 IrDA 裝置相關的裝置標識符有 ``ITP_DEVICE_IR0`` ~ ``ITP_DEVICE_IR3`` 。

``unsigned long request``

  request 參數用來選擇要對裝置運行的操作功能，並將取決於所指定的裝置。

``void *ptr``

  ptr 參數表示對此特定裝置執行請求的功能所需的額外訊息。ptr 的型別取決於特定的控制請求，但它可以是整數，也可以是指向裝置特定資料結構的指標。

**描述**

``ioct()`` 函數用於在 IrDA 裝置上目前只有 INIT 功能。

- ITP_IOCTL_INIT

  用來初始化 IrDA 裝置，其中相關參數設定皆在建置前的 Kconfig 設定完成。
  底下展示如何透過預設設定來初始化 IR0 裝置。

  .. code-block:: c
  
    itpRegisterDevice(ITP_DEVICE_IR0, &itpDeviceIr0);
    ioctl(ITP_DEVICE_IR0, ITP_IOCTL_INIT, (void *)0);

 
read
^^^^^^^^^^

.. c:function:: int read(int file, char *ptr, int len)

  從 RX 讀取資料。

**參數**

``int file``

  欲執行的 IR 埠。EX: ``ITP_DEVICE_IR0`` 。

``char *ptr``

  讀取後寫回的位置

``int len``

  預留開發用

**描述**

使用者可透過此函式從特定 IR 埠中讀回資料。

底下展示如何使用函式。

.. code-block:: c

    // initialize IrDA, the method below could be found in itpInit();
    ITPKeypadEvent ev;
    // Register device Remote control
    itpRegisterDevice(TEST_PORT, &TEST_DEVICE);

    // Do initialization
    ioctl(TEST_PORT, ITP_IOCTL_INIT, (void *) 0);
    // initialize IrDA end.
    for (;;)
    {
    	if (read(TEST_PORT, &ev, sizeof (ITPKeypadEvent)) == sizeof (ITPKeypadEvent))
    		printf("key: time=%lld.%ld,code=%d,down=%d,up=%d,repeat=%d,flags=0x%X/r\n", 
    		ev.time.tv_sec,
    		ev.time.tv_usec / 1000,
    		ev.code,
    		(ev.flags & ITP_KEYPAD_DOWN) ? 1 : 0,
    		(ev.flags & ITP_KEYPAD_UP) ? 1 : 0,
    		(ev.flags & ITP_KEYPAD_REPEAT) ? 1 : 0,
    		ev.flags);
    	usleep(33000);
    }

由於底層會將收到的 IR code 送到 IR 自有的 RxQueue中，下此函數會將 RxQueue 中的值取出來。
 
write
^^^^^^^^^^^

.. c:function:: int write(int file, char*ptr, int len)

  從 TX 發送資料。

**參數**

``int file``

  欲執行的 IR 埠。EX: ``ITP_DEVICE_IR0`` 。

``char *ptr``

  送出時資料的位置

``int len``

  預留開發用

**描述**

使用者可透過此函式從特定 IR 埠中讀回資料。

底下展示如何使用函式。

.. code-block:: c

  ITPKeypadEvent ev;
  ev.code = 0;
  ev.code = TxBuf[cnt++];
  write(TEST_PORT, &ev, sizeof (ITPKeypadEvent));


iteIrClearClkSample
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrClearClkSample(ITHIrPort port)

**參數**

``ITHIrPort port``

  欲執行的 IR 埠。EX: ``ITH_IR0_BASE`` 。

**描述**

使用者可透過此函式從特定 IR 埠中清除採樣資料並重新開始採樣。

 
iteIrGetFreqFast
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetFreqFast (ITHIrPort port)
    
**參數**

``ITHIrPort port``

  欲執行的 IR 埠。EX: ``ITH_IR0_BASE`` 。

**描述**

使用者可透過此函式從特定 IR 埠中取得最快頻率。

 
iteIrGetFreqSlow
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetFreqSlow (ITHIrPort port)
    
**參數**

``ITHIrPort port``

  欲執行的 IR 埠。EX: ``ITH_IR0_BASE`` 。

**描述**

使用者可透過此函式從特定 IR 埠中取得最慢頻率。

 
iteIrGetFreqAvg
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetFreqAvg (ITHIrPort port)
    
**參數**

``ITHIrPort port``

  欲執行的 IR 埠。EX: ``ITH_IR0_BASE`` 。

**描述**

使用者可透過此函式從特定 IR 埠中取得平均頻率。

 
iteIrGetFreqNew
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetFreqNew (ITHIrPort port)
    
**參數**

``ITHIrPort port``

  欲執行的 IR 埠。EX: ``ITH_IR0_BASE`` 。

**描述**

使用者可透過此函式從特定 IR 埠中取得最新頻率。

 
iteIrGetHighDCFast
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetHighDCFast(ITHIrPort port)
    
**參數**

``ITHIrPort port``

  欲執行的 IR 埠。EX: ``ITH_IR0_BASE`` 。

**描述**

使用者可透過此函式從特定 IR 埠中取得最快頻率波型高電位週期。

 
iteIrGetLowDCFast
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetLowDCFast (ITHIrPort port)
    
**參數**

``ITHIrPort port``

  欲執行的 IR 埠。EX: ``ITH_IR0_BASE`` 。

**描述**

使用者可透過此函式從特定 IR 埠中取得最快頻率波型低電位週期。

 
iteIrGetHighDCSlow
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetHighDCSlow (ITHIrPort port)
    
**參數**

``ITHIrPort port``

  欲執行的 IR 埠。EX: ``ITH_IR0_BASE`` 。

**描述**

使用者可透過此函式從特定 IR 埠中取得最慢頻率波型高電位週期。

 
iteIrGetLowDCSlow
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetLowDCSlow (ITHIrPort port)
    
**參數**

``ITHIrPort port``

  欲執行的 IR 埠。EX: ``ITH_IR0_BASE`` 。

**描述**

使用者可透過此函式從特定 IR 埠中取得最慢頻率波型低電位週期。

 
iteIrGetHighDCAvg
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetHighDCAvg (ITHIrPort port)
    
**參數**

``ITHIrPort port``

  欲執行的 IR 埠。EX: ``ITH_IR0_BASE`` 。

**描述**

使用者可透過此函式從特定 IR 埠中取得平均頻率波型高電位週期。

 
iteIrGetLowDCAvg
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetLowDCAvg (ITHIrPort port)
    
**參數**

``ITHIrPort port``

  欲執行的 IR 埠。EX: ``ITH_IR0_BASE`` 。

**描述**

使用者可透過此函式從特定 IR 埠中取得平均頻率波型低電位週期。

 
iteIrGetHighDCNew
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetHighDCNew (ITHIrPort port)
    
**參數**

``ITHIrPort port``

  欲執行的 IR 埠。EX: ``ITH_IR0_BASE`` 。

**描述**

使用者可透過此函式從特定 IR 埠中取得最新頻率波型高電位週期。

 
iteIrGetLowDCNew
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetLowDCNew (ITHIrPort port)
    
**參數**

``ITHIrPort port``

  欲執行的 IR 埠。EX: ``ITH_IR0_BASE`` 。

**描述**

使用者可透過此函式從特定 IR 埠中取得最新頻率波型低電位週期。
 
範例展示
----------------------------------

在 ``<sdk_root>/project/test_ir`` 目錄下有三個 Ir 範例程式。
此範例程式分成三個小測試項：

1. 測試 Rx 接收 NEC Ir 通訊規之遙控器訊號。
2. 測試 Tx 模擬 NEC Ir 通訊規格打出訊號給 Rx 接收。
3. 測試偵測頻率功能。

硬體連接
^^^^^^^^^^^^^^^^^^^^^^

底下我們使用一個 USB to TTL 的轉換板與 PC 連接，透過 UART1 來看測試結果。

- 測試一：將該 IR 接收器接到 GPIO24，並供電，在搭配 NEC 遙控器一起測試。

  .. image:: /_static/e_circuit_ir_test1.png

- 測試二：將該 IR 接收器接到 GPIO24，並供電；再將 IR 發射器接到 GPIO14，並供電。

  .. image:: /_static/e_circuit_ir_test2.png

- 測試三：透過杜邦線將 GPIO24 和 GPIO14 連接。

  .. image:: /_static/e_circuit_ir_test3.png

KConfig 設定
^^^^^^^^^^^^^^^^^^

請執行 ``<sdk_root>/build/openrtos/test_ir.cmd`` ，按照以下步驟設定:

- 勾選 :menuselection:`Peripheral --> IrDA Enable` 。
- 勾選 :menuselection:`Peripheral --> IrDA Enable --> IR0 Enable` 。
- 透過 :menuselection:`Peripheral --> IrDA Enable --> IR0 Enable --> IR0 mode` 選擇 "IR0 Interrupt Enable" 。

  .. image:: /_static/qconf_peripheral_ir.png

測試一：

- 勾選 :menuselection:`Peripheral --> IrDA Enable --> IR0 Enable --> IR0 RX Modulation Enable` 。
- 選擇 :menuselection:`Peripheral --> IrDA Enable --> IR0 Enable --> IR0 Protocol：ir_nec.inc` 。

  .. image:: /_static/qconf_peripheral_ir_test1.png

- 選擇 :menuselection:`Test IR --> Test IR --> test IR Rx` 。

  .. image:: /_static/qconf_testir_test_ir_rx.png

測試二：

- 勾選 :menuselection:`Peripheral --> IrDA Enable --> IR0 Enable --> IR0 RX Modulation Enable` 。
- 勾選 :menuselection:`Peripheral --> IrDA Enable --> IR0 Enable --> IR0 TX Modulation Enable` 。
- 選擇 :menuselection:`Peripheral --> IrDA Enable --> IR0 Enable --> IR0 Protocol：ir_nec.inc` 。

  .. image:: /_static/qconf_peripheral_ir_test2.png
      
- 選擇 :menuselection:`Test IR --> Test IR --> test IR Tx sig to Rx` 。

  .. image:: /_static/qconf_testir_test_ir_tx.png

測試三：

- 勾選 :menuselection:`Peripheral --> IrDA Enable --> IR0 Enable --> IR0 RX sample Enable` 。

  .. image:: /_static/qconf_peripheral_ir_test3.png


接下來設定 IR0 使用到的 pin。

- 將 ``GPIO --> IR0 RX Pin`` 設為 24。
- 將 ``GPIO --> IR0 TX Pin`` 設為 14。

  .. image:: /_static/qconf_gpio_ir.png

- 選擇 :menuselection:`Test IR --> Test IR --> test IR sampling function` 。

  .. image:: /_static/qconf_testir_test_ir_sample.png

驗證結果
^^^^^^^^^^^^^^^^^^^^^^^

  .. |build_icon| image:: /_static/qconf_build_icon.png
    :alt: build icon
    :width: 14
    :height: 14

  .. |run_icon| image:: /_static/qconf_run_icon.png
    :alt: run icon
    :width: 14
    :height: 14

1. 點擊 |build_icon| 按鈕建置專案。確保：

  - USB to SPI 轉換板已正確安裝。
  
    - 若您尚未將 USB to SPI 轉換板與 PC 連接，請參考 :ref:`usb2spi_connect2pc` 。
    - 若您尚未將 USB to SPI 轉換板與 EVB 連接，請參考 :ref:`usb2spi_connect2board` 。
    - 若您尚未安裝 USB to SPI 轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。
  
  - EVB 已經切換為 Co-operative Mode。
  
    .. note:: 
  
       若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

2. 重置 EVB 的電源。
3. 等程式建置完成，點擊 |run_icon| 按鈕即可透過 USB to SPI 轉換板啟動系統。
4. 執行 Tera Term。
  
測試一：
""""""""""""""""""""""

IR Rx: 須有遙控器(NEC protocol為例)

當按下遙控器的 “1” 鍵時，在 Console 上須要看到 "IR code: 0x11” 的訊息，若是按下遙控器 “2” 則會看到 0x12。

.. image:: /_static/teraterm_ir_test1.png

測試二：
""""""""""""""""""""""

IR Tx sig to Rx: 透過自身的TX送資料給自身的RX。

Console 須看到等於或近似於在 ``test_ir.c`` 中的 ``TxBuf[]`` 的值。
此範例目前 ``TxBuf[]`` 內的值為 NEC protocol 之 0x11，並且會每 500ms 發送一次。

.. image:: /_static/teraterm_ir_test2.png

測試三：
""""""""""""""""""""""

IR sampling function: 透過 PWM 打 38khz 送資料給自身的 Rx，並且透過 sample function 偵測頻率。Console 須看到等於或近似於 38khz，從結果來看不夠精準是由於採樣率不夠高造成誤差導致，調高 sample rate 可以提升。

.. image:: /_static/teraterm_ir_test3.png





.. raw:: latex

    \newpage
