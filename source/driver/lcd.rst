.. _lcd:

LCD
==============

功能描述
--------------------

IT986x 系列晶片提供一個 LCD controller。該 controller 支援的輸入介面有

- RGB interface

  支援最高解析度為1920x1080。
  
  支援 16/18/24 bpp (RGB565/RGB666/RGB8888) 介面。

- CPU interface

  - 80-Type
  - 68-Type

- LVDS

  支援最高解析度為 1366 x 768 pixel。

- MIPI

  支援最高解析度為 1920 x 1080 pixel。

ITE 針對點屏部分提供了一個 ``LCD.exe`` 工具，有關該工具的詳盡操作說明，請參閱：

- ITE AP SoC-Panel Tool 指南 IT9860 RGB_SPI.pdf
- ITE AP SoC-Panel Tool 指南 IT9860 CPU.pdf
- ITE AP SoC-Panel Tool 指南 IT9860 LVDS.pdf
- ITE AP SoC-Panel Tool 指南 IT9860 MIPI.pdf

本節主要針對 SDK 內提供的 LCD 相關操作函式做說明。

相關的 KConfig 設定
----------------------------------------------------------------

- :menuselection:`Screen --> LCD enable`

  啟用 LCD 裝置。

- :menuselection:`Screen --> LCD/TV out initial script`

  用來選擇 LCD 初始化設定檔案。該檔案內容包含一堆暫存器位址和數值配對，用來初始化 LCD controller 的初始暫存器設定。這些初始化設定檔一般放置在路徑 ``<sdk_root>/sdk/target/lcd/`` 下面。可使用 LCD tool 來產生此設定檔。

- :menuselection:`Screen --> LCD enable --> LCD width`

  LCD 水平方向的有效資料區間，其表示螢幕真正顯示的水平像素個數。

- :menuselection:`Screen --> LCD enable --> LCD height`

  LCD 垂直方向的有效資料區間，其表示螢幕真正顯示的垂直像素個數。

- :menuselection:`Screen --> LCD enable --> LCD pitch`

  畫面上一條水平線所佔用的記憶體位元組數。此設定應填入的值為 LCD width x LCD Byte-Per-Pixel。譬如 LCD width = 480，LCD Byte-Per-Pixel = 2，則此設定應填入的值為 480 x 2 = 960。

- :menuselection:`Screen --> LCD enable --> LCD Byte-Per-Pixel`

  畫面上一個像素所佔用的記憶體位元組數。若畫面上每個像素的格式是 RGB565，則此設定應填入的值為  2。若畫面上每個像素的格式是 RGB8888，則此設定應填入的值為 4。

相關的程式碼檔案
----------------------------

.. flat-table::
   :header-rows: 1
   
   * - 路徑
     - 描述
   * - | ``<sdk_root>/sdk/driver/ith/it9860/ith_lcd.c``
       | ``<sdk_root>/sdk/driver/ith/it9860/ith_lcd.h``
     - 最上層函式。


相關的 API 描述
---------------------------------------

本節描述用來操作 LCD controller 的相關 API。

ithLcdDisable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithLcdDisable(void)

**描述**

此函式會將 LCD disable。在進入 power saving mode 時會呼叫此函式(若有啟用 power saving mode 的話)。
 

ithLcdEnable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithLcdEnable( void)

**描述**

此函式會將 LCD enable。在離開 power saving mode 時會呼叫此函式(若有啟用 power saving mode 的話)。
 
ithLcdSetRotMode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithLcdSetRotMode(ITHLcdScanType type, ITHLcdRotMode mode)
	
**參數**

``ITHLcdScanType type``

  LCD 旋轉方向horizontal或vertical設定。

``ITHLcdRotMode mode``

  LCD 旋轉模式設定。

**描述**

用來旋轉 LCD 模組顯示的畫面，IT986x 支援 Rotate 180、Flip 及 Mirror 幾種模式。
 
ithLcdGetPanelType
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: ITHLcdPanelType ithLcdGetPanelType(void)

**描述**

用來取得目前 LCD panel 種類。
 
**回傳值**

LCD type。

完整範例展示
----------------------

這裡將使用 Standard EVB 來點亮 TL068HWXH08 6.86" MIPI 螢幕為例。TL068HWXH08 6.86" MIPI 螢幕對應的 initial script 位於 ``<sdk_root>/sdk/target/lcd/IT9860_MIPI_TL068HWXH08_EK79030_480x1280_4LANE_byteclk53.txt`` 。該螢幕的有效顯示寬度為 1280，有效顯示高度為 480。本例將點亮螢幕並在整個畫面上定期切換顯示各種不同顏色。

硬體部分
^^^^^^^^^^^^^^^^^

請先將螢幕與 EVB 透過排線連接。請連接至下圖黃色箭頭所指的 socket。

  .. image:: /_static/evb_lcd_example.png

KConfig 設定
^^^^^^^^^^^^^^^^^

請執行 ``<sdk_root>/build/openrtos/test_lcd.cmd`` ，按照以下步驟設定:

- 勾選 :menuselection:`Screen --> LCD Enable` 。
- 在 :menuselection:`Screen --> LCD Enable --> LCD width` 填入 1280 (即 TL068HWXH08 6.86" MIPI 螢幕的有效顯示寬度)。
- 在 :menuselection:`Screen --> LCD Enable --> LCD height` 填入 480 (即 TL068HWXH08 6.86" MIPI 螢幕的有效顯示高度)。
- 在 :menuselection:`Screen --> LCD Enable --> LCD pitch` 填入 2560。
- 在 :menuselection:`Screen --> LCD Enable --> LCD Bytes-Per-Pixel` 填入 2。
- 雙擊 :menuselection:`Screen --> LCD Enable --> LCD initial script` ，選擇 ``<sdk_root>/sdk/target/lcd/IT9860_MIPI_TL068HWXH08_EK79030_480x1280_4LANE_byteclk53.txt`` 。

  .. image:: /_static/qconf_lcd_example_screen_lcd.png

TL068HWXH08 6.86" MIPI螢幕是採用PWM作為背光。因此還需要設定背光。背光的設定步驟如下：

- 勾選 :menuselection:`Screen --> Backlight Enable:` 。
- 在 :menuselection:`GPIO --> Backlight PWM pin:` 填入 63。
- 在 :menuselection:`GPIO --> Backlight PWM NUMBER:` 填入 1。
- 在 :menuselection:`GPIO --> LCD Power Enable Pin:` 填入 48。
- 取消勾選 :menuselection:`GPIO --> LCD Power Enable n --> LCD Power Enable Pin Active Low:` 。

  .. image:: /_static/qconf_lcd_example_gpio_lcd.png


驗證結果
^^^^^^^^^^^^^^^^^

1. 點擊 |build_icon| 按鈕建置專案。確保：

  .. |build_icon| image:: /_static/qconf_build_icon.png
    :alt: build icon
    :width: 14
    :height: 14

  - USB to SPI 轉換板已正確安裝。
  
    - 若您尚未將 USB to SPI 轉換板與 PC 連接，請參考 :ref:`usb2spi_connect2pc` 。
    - 若您尚未將 USB to SPI 轉換板與 EVB 連接，請參考 :ref:`usb2spi_connect2board` 。
    - 若您尚未安裝 USB to SPI 轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。
  
  - EVB 已經切換為 Co-operative Mode。
  
    .. note:: 
  
       若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

2. 重置 EVB 的電源。
3. 等程式建置完成，點擊 |run_icon| 按鈕即可透過 USB to SPI 轉換板啟動系統。
4. 確認畫面是否有顯示全螢幕同色色彩。


  .. |run_icon| image:: /_static/qconf_run_icon.png
    :alt: run icon
    :width: 14
    :height: 14


.. raw:: latex

    \newpage
