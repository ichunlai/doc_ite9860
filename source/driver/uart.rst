﻿.. _uart:

Uart
=======

.. _uart_feature:

功能描述
-----------------------------

IT986x 支援多達 6 組UART (UART0 ~ 5) 和三種操作模式(Interrupt、DMA、FIFO)。其中每組 UART 均支援全雙工非同步通訊和以下選項設定：

- **baud rate：** 所支援最高速率與 WCLK 頻率有關。譬如 WCLK 為 99MHz (如果您選擇的是 ``IT9860_396Mhz_DDR2_396Mhz.txt`` 這個 initial script)，則所支援的最高頻率為 99MHz / 16 = 6,187,500 bps。

  .. note::

      實際使用的 WCLK 頻率可透過 ``ithGetBusClock()`` 獲得。

- **word length：** 支援的 word length 與 stop bits 個數的組合請見下表：

  .. flat-table::
     :header-rows: 1

     * - Character Length (Bits)
       - Number of Stop Bits
     * - 5
       - 1
     * - 6
       - 1
     * - 7
       - 1
     * - 8
       - 1
     * - 5
       - 1.5
     * - 6
       - 2
     * - 7
       - 2
     * - 8
       - 2

- **parity bit mode：** 支援No Parity、Odd Parity、Even Parity、Mark、Space。

  除了 PIN 21 ~ 24 不可作為 UART RX 使用外，晶片上任何一個 GPIO 接腳均可做為 UART TX/RX 使用。

  .. note::

      不建議把 GPIO0 ~ 3 作為UART使用。否則您將無法在運行時期除錯程式。

  另外，智能家居領域中經常用到 RS485 serial bus，即是 UART 的一種介面，其在驅動程式底層也是直接呼叫 UART 的 API。

.. _uart_kconfig:

相關的 KConfig 設定
-----------------------------

- :menuselection:`Peripheral --> UART Enable`

  決定是否要啟用 UART 裝置，若要啟用UART0 ~ 5之間任一裝置都必須啟用此選項，否則將無法出現個別 UART 裝置的詳細設定選項。

- :menuselection:`Peripheral --> UART Enable --> UART# Enable`

  決定是否要啟用 UART# 裝置。勾選此選項後，系統才會在 ``itpInit()`` 函式內透過 ``itpRegisterDevice()`` 函式註冊對應 ``UART#`` 裝置並進行初始化動作。

- :menuselection:`Peripheral --> UART Enable --> UART# Enable --> UART# mode`

  決定 UART# 裝置的操作模式。分別為 FIFO、Interrupt、DMA 和模式。

  - FIFO 模式: 又稱為 Polling 模式。採用此模式運行的 UART 驅動程式透過不斷輪詢裝置的狀態發送數據或接收數據。通常使用在收送速度不快的情況下。在中斷關閉的情況下要發送資料時，只能使用此模式。
  - Interrupt 模式: UART 模組在作為接收端時，可在每收到一個 character 的資料或收到的資料有錯誤時就收到中斷。在作為發送端時，可在 UART FIFO 空的時候就收到中斷通知。通常使用在收送速度中等的情況下。
  - DMA 模式: UART 模組在作為接收端時，會使用 DMA LLP (linked list pointer) mode，達到 ring buffer 功能，因此，所有收到的資料會先暫存在此 ring buffer 內，大小目前設定為 8KB，使用者可在 project 之 Kconfig 內加入 CFG_UART_DMA_BUF_SIZE size 即可修改調整大小。使用者可以自行決定何時透過 ``read()`` 將資料取出，不過須注意當資料取出速度小於寫入速度時可能導致 ring buffer 前端資料被覆蓋的狀況。此模式通常使用在收送速度要求較高的情況下。

- :menuselection:`Peripheral --> UART Enable --> UART# Enable --> UART# Baud Rate`

  指定收送的速率。以 bps 為單位。

- :menuselection:`Peripheral --> UART Enable --> UART# Enable --> RS485_# interface enable`

  該 UART 模組是否要作為 RS485 使用。

- :menuselection:`GPIO --> UART# TX Pin`

  指定該 UART 模組要使用哪個 GPIO pin 作為 TX pin。

- :menuselection:`GPIO --> UART# RX Pin`

  指定該 UART 模組要使用哪個 GPIO pin 作為 TX pin。

- :menuselection:`Debug --> UART#`

  指定 Debug 訊息從某個 UART 裝置輸出。Debug 訊息是指程式透過呼叫 ``printf()`` 或 ``ithPrintf()`` 輸出的訊息。 ``printf()`` 在 bootloader 時期會透過 Kconfig 所選的 UART# 註冊到 ITP_DEVICE_STD 上，因此當使用 ``printf()`` 時就會執行 ITP_DEVICE_STD 的 write，也就等同使用 UART# 的 wirte； ``ithPrintf()`` 定義在 ``<sdk_root>/sdk/driver/ith/ith_printf.c`` 內，其中可看出最為重要的函式 ``ithPutcharFunc()`` 會在 ``iteUartInit()`` 中掛上相對應 UART# mode 的 send。

相關的程式碼檔案
----------------------------

.. flat-table::
  :header-rows: 1

  * - 路徑
    - 描述
  * - ``<sdk_root>/sdk/driver/itp/itp_uart.c``
    - 最上層函式，主要提供 POSIX 規範實作的 API，供上層 AP 透過 ``open()``、``close()``、``ioctl()`` 等函式對裝置進行操作。
  * - ``<sdk_root>/sdk/driver/itp/itp_uart_api.c``
    - 建置 Win32 simulator 專案時，用來利用 PC 的 COM port 模擬實際板子上的 RS232 的函式。
  * - | ``<sdk_root>/sdk/driver/uart/{chip}/*``
      | ``<sdk_root>/sdk/include/uart/uart.h``
      | ``<sdk_root>/sdk/include/uart/uart_*.h``
      | ``<sdk_root>/sdk/include/uart/interface/rs485.h``
      | ``<sdk_root>/sdk/include/ith/ith_uart.h``
      | ``<sdk_root>/sdk/driver/ith/ith_uart.c``

    - 底層函式，未來可能會變動。

相關的 API 描述
---------------------------------------

本節描述用來操作 UART 裝置的相關 API。 UART driver 提供依據 POSIX 規範實作的 API，用戶可以透過 ``open()``/``read()``/``write()``/``ioctl()`` 等函式對 UART 裝置進行如讀寫檔案般的操作。

itpRegisterDevice
^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void itpRegisterDevice(ITPDeviceType type, const ITPDevice *device)

   註冊裝置。

**參數**

``ITPDeviceType type``

   裝置類型。完整裝置類型列表請見 ``<sdk_root>/sdk/include/ite/itp.h`` 。與 UART 裝置相關的裝置類型有 ``ITP_DEVICE_UART0`` ~ ``ITP_DEVICE_UART5`` 。

``const ITPDevice *device``

   裝置標識符。與 UART 裝置相關的裝置標識符有 ``itpDeviceUart0`` ~ ``itpDeviceUart5`` 。

**描述**

此函式可用來將 UART# 裝置註冊到核心裝置列表中，使上層 AP 可以透過 ``read()``/``write()``/``ioctl()`` 等函式來操作 UART 的功能。本函式會在系統初始化期間被 ``itpInit()`` 函式呼叫 (見 ``<sdk_root>/sdk/driver/itp_init_openrtos.c`` )，因此若在您的專案中有呼叫過 ``itpInit()`` 函式，將無須再執行此註冊函式。

底下展示如何註冊 UART0 裝置到系統核心，並利用預設設定(baud rate: 115200、character length: 8 bits、Parity: none、Stop bits: 1 bit)初始化 UART0 裝置。

.. code-block:: c

   itpRegisterDevice(ITP_DEVICE_UART0, &itpDeviceUart0);
   ioctl(ITP_DEVICE_UART0, ITP_IOCTL_INIT, NULL);

ioctl
^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr)

   控制裝置

**參數**

``int file``

   裝置標識符。其值為 ITPDeviceType 列表中的值。與 UART 裝置相關的裝置標識符有 ``ITP_DEVICE_UART0`` ~ ``ITP_DEVICE_UART5`` 。

``unsigned long request``

   request 參數用來選擇要對裝置運行的操作功能，並將取決於所指定的裝置。

``void *ptr``

   ptr 參數表示對此特定裝置執行請求的功能所需的額外訊息。ptr 的型別取決於特定的控制請求，但它可以是整數，也可以是指向裝置特定資料結構的指標。

**描述**

``ioctl()`` 函數用於在 UART 裝置上執行各種控制功能。request 參數和可選的第三個參數（具有不同的型別）將傳遞給與 file 參數關聯的 UART 裝置的相應部分並由其解釋。

適用於 UART 裝置的 ``ioctl()`` 命令、其參數以及適用於每個單獨命令的錯誤狀態如下所述。

以下的 ``ioctl()`` 命令適用於所有 UART 裝置：

- **ITP_IOCTL_INIT**

   用來初始化 UART 裝置。必須透過 ptr 參數傳入 UART_OBJ 型別的指標指向 UART 裝置初始化參數結構。UART_OBJ 型別的各個欄位描述如下：

   - ITHUartPort  port

      裝置 handle。值應為 ITH_UART0 ~ ITH_UART5 其中之一。注意此欄位不能填入 ITPDeviceType 列表中的值。

   - ITHUartParity parity

      校驗碼類型。值可以是 ``ITH_UART_NONE``、``ITH_UART_ODD``、``ITH_UART_EVEN``、``ITH_UART_MARK``、``ITH_UART_SPACE`` 其中之一。

   - txPin

      UART TX 對應的 GPIO 接腳編號。填入 -1 表示不啟用 UART TX。填入大於 0 的數字表示使用 GPIO# 作為 UART TX 接腳。譬如填入 5 表示將使用 GPIO5 作為 UART TX 接腳。

   - rxPin

      UART RX 對應的 GPIO 接腳編號。填入 -1 表示不啟用 UART RX。填入大於 0 的數字表示使用 GPIO# 作為 UART RX 接腳。譬如填入 5 表示將使用 GPIO5 作為UART RX接腳。

   - baud

      baud rate。用來設定傳輸速率，以 bps 為單位。所支援最高速率與 WCLK 頻率有關。見 :ref:`uart_feature` 。

   - timeout

      逾時值，以 CPU tick 為單位。0 表示不啟用 timeout 機制。當使用者希望 UART 在送收忙碌時能有額外緩衝時間等待系統正確送出時使用。

   - mode

      操作模式。其值可以是 ``UART_INTR_MODE``、``UART_DMA_MODE``、``UART_FIFO_MODE`` 其中之一。不同操作模式的差異請見 :ref:`uart_kconfig`。

   - forDbg

      設定此 UART 裝置是否為 debug message 輸出裝置。若其值為非 0 值，表示此 UART 裝置將作為 debug message 輸出裝置，則當程式中有呼叫 ``printf()`` 或 ``ithPrintf()`` 之類的函式將透過此裝置輸出訊息。若其值為 0，則此 UART 裝置將不作為 debug message 輸出裝置。

      若透過 Kconfig 定義了 CFG_DBG_UART# 巨集，則將導致系統在啟動階段透過呼叫此函式來初始化對應的 UART# 裝置時，會在內部將對應的 UART# 裝置的 UART_OBJ 型別結構的 forDbg 欄位設定為 1 並初始化該裝置。細節請參考 ``<sdk_root>/sdk/include/uart/uart.h`` 檔案的 ``INIT_UART_OBJ_DEFAULT`` 巨集以及 ``<sdk_root>/sdk/driver/uart/it9860/uart.c`` 檔案內的 ``iteUartInit()`` 函式。

      若將多個 UART 裝置都設定為 debug message 輸出裝置，則只有時序上最後一個設定的裝置會被用作為 debug message 輸出裝置。目前尚未驗證將多個 UART 裝置都設定為 debug message 輸出裝置是否會有任何問題，因此不建議這麼做。

   底下展示如何透過填充 UART_OBJ 型別變數和使用 ``ioctl()`` 函式來初始化 UART0 裝置。

   .. code-block:: c

      UART_OBJ    UartInitParam = {};
      UartInitParam.port      = ITH_UART0;
      UartInitParam.parity    = ITH_UART_NONE;
      UartInitParam.txPin     = 5;
      UartInitParam.rxPin     = 6;
      UartInitParam.baud      = 115200;
      UartInitParam.timeout   = 0;
      UartInitParam.mode      = UART_DMA_MODE;
      UartInitParam.forDbg    = false;

      itpRegisterDevice(ITP_DEVICE_UART0, &TEST_DEVICE);
      ioctl(ITP_DEVICE_UART0, ITP_IOCTL_INIT, (void *)&UartInitParam);


   若 ptr 參數值為 NULL，則將使用預設設定初始化 UART 裝置。預設設定即用戶於 Kconfig 填入的設定值。

   底下展示如何透過預設設定來初始化 UART0 裝置。

   .. code-block:: c

      itpRegisterDevice(ITP_DEVICE_UART0, &TEST_DEVICE);
      ioctl(ITP_DEVICE_UART0, ITP_IOCTL_INIT, NULL);


- **ITP_IOCTL_RESET**

     同 ITP_IOCTL_INIT。只是 ITP_IOCTL_RESET 會先 Terminate UART 裝置在對其重新初始化。因此其 ptr 參數的型別和用途與 ITP_IOCTL_INIT 的 ptr 參數的型別和用途完全相同。

     底下展示如何透過填充 UART_OBJ 型別變數和使用 ``ioctl()`` 函式來重置 UART0 裝置。

     .. code-block:: c

        UART_OBJ UartInitParam  = {};
        UartInitParam.port      = ITH_UART0;
        UartInitParam.parity    = ITH_UART_NONE;
        UartInitParam.txPin     = 5;
        UartInitParam.rxPin     = 6;
        UartInitParam.baud      = 115200;
        UartInitParam.timeout   = 0;
        UartInitParam.mode      = UART_DMA_MODE;
        UartInitParam.forDbg    = false;

        ioctl(ITP_DEVICE_UART0, ITP_IOCTL_RESET, (void *)&UartInitParam);


     底下展示如何透過預設設定來重置UART0裝置。

     .. code-block:: c

         ioctl(ITP_DEVICE_UART0, ITP_IOCTL_RESET, NULL);

- **ITP_IOCTL_REG_UART_CB**

     用來掛載 UART RX 中斷處理函式。只有在該 UART 裝置的操作模式為 Interrupt 模式，此函式才有作用。此處掛載的中斷處理函式會在每收到一個 character 時被呼叫。其 ptr 參數即要掛載的中斷處理函式。

     底下展示如何掛載中斷處理函式，以及在 UART RX 中斷處理函式內通常執行的動作。

     .. code-block:: c

        static sem_t UartSemIntr;

        void UartRxCallback(void)
        {
            sem_post(&UartSemIntr);
        }

        int main(void)
        {
            sem_init(&UartSemIntr, 0, 0);
            ...
            ioctl(TEST_PORT, ITP_IOCTL_REG_UART_CB, (void*)UartRxCallback);

            while(1)
            {
               ...
               sem_wait(&UartSemIntr);
               ...
            }
        }

read
^^^^

.. c:function:: int read(int __fd, void *__buf, size_t __nbyte)

   從UART RX讀取資料。

**參數**

``int __fd``

  裝置標識符。其值為 ITPDeviceType 列表中的值。與 UART 裝置相關的裝置標識符有 ``ITP_DEVICE_UART0`` ~ ``ITP_DEVICE_UART5`` 。

``void *__buf``

  指向接收數據緩衝區的指標。

``size_t __nbyte``

  欲接收的資料最大 bytes 數。

**描述**

此函式用於從 UART RX 讀取最多 __nbyte 個 characters 至 __buf 指向的緩衝區。並回傳實際接收的 bytes 數。

底下展示如何使用函式。

.. code-block:: c

   char getstr[256] = "";
   int len = 0, count = 0;
   len = read(ITP_DEVICE_UART0, getstr + count, 256);
 
write
^^^^^

.. c:function:: int write(int __fd, const void *__buf, size_t __nbyte)

  從 UART TX 發送資料。

**參數**

``int __fd``

  裝置標識符。其值為 ITPDeviceType 列表中的值。與 UART 裝置相關的裝置標識符有 ``ITP_DEVICE_UART0`` ~ ``ITP_DEVICE_UART5`` 。

``void *__buf``

  指向發送數據緩衝區的指標。

``size_t __nbyte``

  欲發送的資料 bytes 數。

**描述**

此函式用於將 __buf 指向的緩衝區內的數據透過 UART TX 發送最多 __nbyte 個 characters。並回傳實際發送的 bytes 數。

底下展示如何使用函式。

.. code-block:: c

   char sendstr[256] = "iTE";
   int len = 0;
   len = write(ITP_DEVICE_UART0, sendstr, 3);

簡易範例程式
----------------------------------------------------------------

.. code-block:: c

    #include "ite/itp.h"

    static sem_t    UartSemIntr;

    void UartCallback(void)
    {
        sem_post(&UartSemIntr);
    }

    int main(void)
    {
        int i;
        char getstr[256];
        char sendtr[256];
        int len = 0;

        /* if in rs485 interface then use RS485_OBJ instead */
        UART_OBJ UartInitParam	= {};
        UartInitParam.port	= ITH_UART0;
        UartInitParam.parity	= ITH_UART_NONE;
        UartInitParam.txPin	= 5;
        UartInitParam.rxPin	= 6;
        UartInitParam.baud	= 115200;
        UartInitParam.timeout	= 0;
        UartInitParam.mode	= UART_INTR_MODE;
        UartInitParam.forDbg	= false;

        printf("Start uart test!\n”);
        sem_init(&UartSemIntr, 0, 0);
        ioctl(TEST_PORT, ITP_IOCTL_RESET, (void*)&UartInitParam);     // use UART structure and reset
        ioctl(TEST_PORT, ITP_IOCTL_REG_UART_CB, (void*)UartCallback); // set callback function

        while(1)
        {
            sem_wait(&UartSemIntr);

            len = read(ITP_DEVICE_UART1,getstr,256);
            if (len > 0)
            {
                printf("uart read: %s\n”,getstr);
                sprintf(sendtr,"uart write: read str = %s",getstr);
                write(ITP_DEVICE_UART1,sendtr,256);
            }
        }
    }


完整範例展示 – TTL
--------------------------------------------

在 ``<sdk_root>/project/test_uart`` 目錄下附有一個 UART 範例程式。該程式展示了一個 UART echo 功能。預設從 UART0 RX 每收滿 9 個字元的資料時，將每筆資料的尾部都加上一個 ``\n`` 字元並透過 UART0 TX 送出去。此外，還使用 UART1 TX 以輸出 debug message。

我們希望從 UART0 RX 每收到一個字元的資料就將資料透過 UART0 TX 送出去，所以請修改 ``<sdk_root>/project/test_uart/test_uart.c`` 檔案內的 UartCommandLen 巨集定義，將：

.. code-block:: c

  #define UartCommandLen 9


改為

.. code-block:: c

  #define UartCommandLen 1


並且我們希望程式不要主動在每筆資料的尾部都加上一個 ``\n`` 字元，因此請將 ``TestFuncUseINTR()``、``TestFuncUseDMA()`` 和 ``TestFuncUseFIFO()`` 函式內的以下語句：

.. code-block:: c

  sendstr[count] = '\n';


改為

.. code-block:: c

  // sendstr[count] = '\n';


硬體連接
^^^^^^^^^^^^^

底下我們使用一個 USB to TTL 的轉換板與 PC 連接，並

- 將該轉換板的 TX (下圖左上方黃色杜邦線所連接的接腳)與 EVB 的 GPIO5 (下圖右方黃色杜邦線所連接的接腳，我們會將其設定為 EVB 的 UART0 RX)連接，
- 將轉換板的 RX (下圖左上方藍色杜邦線所連接的接腳)與 EVB 的 GPIO6 (下圖右方藍色杜邦線所連接的接腳，我們會將其設定為 EVB 的 UART0 RX)連接。
- 將轉換板的 GND (下圖左上方黑色杜邦線所連接的接腳)與 EVB 的 GPIO6 (下圖右上方黑色杜邦線所連接的接腳)連接。

  .. image:: /_static/evb_ttl_example.png

KConfig 設定
^^^^^^^^^^^^^^^^^^^^^^^^^

請執行 ``<sdk_root>/build/openrtos/test_uart.cmd`` ，按照以下步驟設定:

- 勾選 :menuselection:`Peripheral --> UART Enable`。
- 勾選 :menuselection:`Peripheral --> UART Enable --> UART0 Enable`。
- 透過 :menuselection:`Peripheral --> UART0 Enable --> UART0 mode` 選擇 UART0 的操作模式，這裡我們選擇 UART0 DMA Enable。
- 將 :menuselection:`Peripheral --> UART Enable --> UART0 Baud Rate` 設定為 9600。
- 勾選 :menuselection:`Peripheral --> UART Enable --> UART1 Enable`。
- 透過 :menuselection:`Peripheral --> UART1 Enable --> UART1 mode` 選擇 UART1 的操作模式，這裡我們選擇 UART1 Interrupt Enable。
- 將 :menuselection:`Peripheral --> UART Enable --> UART1 Baud Rate` 設定為 115200。

  .. image:: /_static/qconf_peripheral_uart.png

接下來設定 UART0 和 UART1 使用到的 pin。

- 將 :menuselection:`GPIO --> UART0 RX Pin` 設為 5。
- 將 :menuselection:`GPIO --> UART0 TX Pin` 設為 6。
- 將 :menuselection:`GPIO --> UART1 TX Pin` 設為 4。

  .. image:: /_static/qconf_gpio_uart.png

  .. note::

    為何將 UART1 TX pin 設定為 GPIO4 是因為在EVB電路圖中，IT986x 的 GPIO4 與 USB to SPI 轉換板的 RX (在電路圖中標示為 DBG_TX) 相連。

    .. image:: /_static/e_circuit_uart.png

接下來設定 UART1 作為 Debug message 輸出裝置。

- 將 :menuselection:`Debug --> Debug Message Device` 設定為 UART1。

  .. image:: /_static/qconf_check_debug_uart1.png

驗證結果
^^^^^^^^^^^^^^^^^^^^^^^^^

  .. |run_icon| image:: /_static/qconf_run_icon.png
    :alt: run icon
    :width: 14
    :height: 14

  .. |build_icon| image:: /_static/qconf_build_icon.png
    :alt: build icon
    :width: 14
    :height: 14

1. 點擊 |build_icon| 按鈕建置專案。確保：

  - USB to SPI 轉換板已正確安裝。

    - 若您尚未將 USB to SPI 轉換板與 PC 連接，請參考 :ref:`usb2spi_connect2pc` 。
    - 若您尚未將 USB to SPI 轉換板與 EVB 連接，請參考 :ref:`usb2spi_connect2board` 。
    - 若您尚未安裝 USB to SPI 轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

  - EVB 已經切換為 Co-operative Mode。

    .. note::

       若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

2. 重置 EVB 的電源。
3. 等程式建置完成，點擊 |run_icon| 按鈕即可透過 USB to SPI 轉換板啟動系統。
4.	執行 Tera Term，點擊 Tera Term 主選單的 :menuselection:`Setup --> Serial Port` 。Port 的部分請選擇 USB to TTL 轉換板對應的 com port。Speed 的部分請選擇 9600。

  .. image:: /_static/teraterm_set_speed_9600.png

5.	點擊 "New setting"，在 Tera Term 的主畫面中黑色區域輸入任意文字，您會發現畫面將反饋任何您輸入的內容。

  .. image:: /_static/teraterm_uart_echo_example.png

.. attention::

  1. 若您輸入任何內容後，畫面仍一片空白，請重複整個步驟並檢查接線是否正確。
  2. 這裡我們選擇 UART0 的操作模式為 DMA 模式，因此將運行 ``<sdk_root>/project/test_uart/test_uart.c`` 的 ``TestFuncUseDMA()`` 來進行整個測試行為。若您選擇的操作模式為 Interrupt 模式，則將運行 ``<sdk_root>/project/test_uart/test_uart.c`` 的 ``TestFuncUseINTR()`` 。若您選擇的操作模式為 FIFO 模式，則將運行 ``<sdk_root>/project/test_uart/test_uart.c`` 的 ``TestFuncUseFIFO()`` 。



完整範例展示 – RS232
--------------------------------------------

在 ``<sdk_root>/project/test_uart`` 目錄下附有一個 UART 範例程式。該程式展示了一個 UART echo 功能。預設從 UART0 RX 每收滿 9 個字元的資料時，將每筆資料的尾部都加上一個 ``\n`` 字元並透過 UART0 TX 送出去。此外，還使用 UART1 TX 以輸出 debug message。

我們希望從 UART0 RX 每收到一個字元的資料就將資料透過 UART0 TX 送出去，所以請修改 ``<sdk_root>/project/test_uart/test_uart.c`` 檔案內的 UartCommandLen 巨集定義，將：

.. code-block:: c

  #define UartCommandLen 9


改為

.. code-block:: c

  #define UartCommandLen 1


並且我們希望程式不要主動在每筆資料的尾部都加上一個 ``\n`` 字元，因此請將 ``TestFuncUseINTR()``、``TestFuncUseDMA()`` 和 ``TestFuncUseFIFO()`` 函式內的以下語句：

.. code-block:: c

  sendstr[count] = '\n';


改為

.. code-block:: c

  // sendstr[count] = '\n';


硬體連接
^^^^^^^^^^^^^

底下我們使用一個 RS232 轉換板與PC連接，並且

- 將 PC 端連出的 DB9 的 RX 與 TX 連接到該轉換板。
- 將轉換板的 TX 與 EVB 的 GPIO5 連接。
- 將轉換板的 RX 與 EVB 的 GPIO6 連接。

  .. image:: /_static/evb_rs232_example.png

  .. image:: /_static/evb_rs232_connect_example.png


KConfig 設定
^^^^^^^^^^^^^^^^^^^^^^^^^

請執行 ``<sdk_root>/build/openrtos/test_uart.cmd`` ，按照以下步驟設定:

- 勾選 :menuselection:`Peripheral --> UART Enable`。
- 勾選 :menuselection:`Peripheral --> UART Enable --> UART0 Enable`。
- 透過 :menuselection:`Peripheral --> UART0 Enable --> UART0 mode` 選擇 UART0 的操作模式，這裡我們選擇 UART0 DMA Enable。
- 將 :menuselection:`Peripheral --> UART Enable --> UART0 Baud Rate` 設定為 9600。
- 勾選 :menuselection:`Peripheral --> UART Enable --> UART1 Enable`。
- 透過 :menuselection:`Peripheral --> UART1 Enable --> UART1 mode` 選擇 UART1 的操作模式，這裡我們選擇 UART1 Interrupt Enable。
- 將 :menuselection:`Peripheral --> UART Enable --> UART1 Baud Rate` 設定為 115200。

  .. image:: /_static/qconf_peripheral_uart.png

接下來設定 UART0 和 UART1 使用到的 pin。

- 將 :menuselection:`GPIO --> UART0 RX Pin` 設為 5。
- 將 :menuselection:`GPIO --> UART0 TX Pin` 設為 6。
- 將 :menuselection:`GPIO --> UART1 TX Pin` 設為 4。

  .. image:: /_static/qconf_gpio_uart.png

  .. note::

    為何將 UART1 TX pin 設定為 GPIO4 是因為在EVB電路圖中，IT986x 的 GPIO4 與 USB to SPI 轉換板的 RX (在電路圖中標示為 DBG_TX) 相連。

    .. image:: /_static/e_circuit_uart.png

接下來設定 UART1 作為 Debug message 輸出裝置。

- 將 :menuselection:`Debug --> Debug Message Device` 設定為 UART1。

  .. image:: /_static/qconf_check_debug_uart1.png

驗證結果
^^^^^^^^^^^^^^^^^^^^^^^^^

1. 點擊 |build_icon| 按鈕建置專案。確保：

  - USB to SPI 轉換板已正確安裝。

    - 若您尚未將 USB to SPI 轉換板與 PC 連接，請參考 :ref:`usb2spi_connect2pc` 。
    - 若您尚未將 USB to SPI 轉換板與 EVB 連接，請參考 :ref:`usb2spi_connect2board` 。
    - 若您尚未安裝 USB to SPI 轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

  - EVB 已經切換為 Co-operative Mode。

    .. note::

       若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

2. 重置 EVB 的電源。
3.	等程式建置完成，點擊 |run_icon| 按鈕即可透過 USB to SPI 轉換板啟動系統。
4.	執行 Tera Term，點擊 Tera Term 主選單的 :menuselection:`Setup --> Serial Port` 。Port 的部分請選擇 USB to TTL 轉換板對應的 com port。Speed 的部分請選擇 9600。

  .. image:: /_static/teraterm_set_speed_9600.png

5.	點擊 "New setting"，在 Tera Term 的主畫面中黑色區域輸入任意文字，您會發現畫面將反饋任何您輸入的內容。

  .. image:: /_static/teraterm_uart_echo_example.png

.. attention::

  1. 若您輸入任何內容後，畫面仍一片空白，請重複整個步驟並檢查接線是否正確。
  2. 這裡我們選擇 UART0 的操作模式為 DMA 模式，因此將運行 ``<sdk_root>/project/test_uart/test_uart.c`` 的 ``TestFuncUseDMA()`` 來進行整個測試行為。若您選擇的操作模式為 Interrupt 模式，則將運行 ``<sdk_root>/project/test_uart/test_uart.c`` 的 ``TestFuncUseINTR()`` 。若您選擇的操作模式為 FIFO 模式，則將運行 ``<sdk_root>/project/test_uart/test_uart.c`` 的 ``TestFuncUseFIFO()`` 。



完整範例展示 – RS485
--------------------------------------------

在 ``<sdk_root>/project/test_uart`` 目錄下附有一個 UART 範例程式。該程式展示了一個 UART echo 功能。預設從 UART0 RX 每收滿 9 個字元的資料時，將每筆資料的尾部都加上一個 ``\n`` 字元並透過 UART0 TX 送出去。此外，還使用 UART1 TX 以輸出 debug message。

我們希望從 UART0 RX 每收到一個字元的資料就將資料透過 UART0 TX 送出去，所以請修改 ``<sdk_root>/project/test_uart/test_uart.c`` 檔案內的 UartCommandLen 巨集定義，將：

.. code-block:: c

  #define UartCommandLen 9


改為

.. code-block:: c

  #define UartCommandLen 1


並且我們希望程式不要主動在每筆資料的尾部都加上一個 ``\n`` 字元，因此請將 ``TestFuncUseINTR()``、``TestFuncUseDMA()`` 和 ``TestFuncUseFIFO()`` 函式內的以下語句：

.. code-block:: c

  sendstr[count] = '\n';


改為

.. code-block:: c

  // sendstr[count] = '\n';


硬體連接
^^^^^^^^^^^^^

底下我們使用一個 RS485 轉換板與 PC 連接，並且

- 將 PC 端連出的 DB9 透過 RS232toRS485 轉換器連接，再將 DA 與 DB 連接到該轉版。
- 將轉換板的 TX 與 EVB 的 GPIO40 連接。
- 將轉換板的 RX 與 EVB 的 GPIO41 連接。

由於透過此 RS485 轉換版轉換後，訊號會變回 RS232，因此 ENABLE pin 就無需使用。

  .. image:: /_static/evb_rs485_example.png

  .. image:: /_static/evb_rs485_connect_example.png

KConfig 設定
^^^^^^^^^^^^^^^^^^^^^^^^^

請執行 ``<sdk_root>/build/openrtos/test_uart.cmd`` ，按照以下步驟設定:

- 勾選 :menuselection:`Peripheral --> UART Enable`。
- 勾選 :menuselection:`Peripheral --> UART Enable --> UART0 Enable`。
- 透過 :menuselection:`Peripheral --> UART0 Enable --> UART0 mode` 選擇 UART0 的操作模式，這裡我們選擇 UART0 DMA Enable。
- 勾選 :menuselection:`Peripheral --> UART Enable --> RS485_0_interface enable`。
- 將 :menuselection:`Peripheral --> UART Enable --> UART0 Baud Rate` 設定為 115200。
- 勾選 :menuselection:`Peripheral --> UART Enable --> UART1 Enable` 。
- 透過 :menuselection:`Peripheral --> UART1 Enable --> UART1 mode` 選擇 UART1 的操作模式，這裡我們選擇 UART1 Interrupt Enable。
- 將 :menuselection:`Peripheral --> UART Enable --> UART1 Baud Rate` 設定為 115200。


  .. image:: /_static/qconf_peripheral_uart_rs485_example.png

接下來設定 UART0 和 UART1 使用到的 pin。

- 將 :menuselection:`GPIO --> UART0 RX Pin` 設為 40。
- 將 :menuselection:`GPIO --> UART0 TX Pin` 設為 41。
- 將 :menuselection:`GPIO --> UART1 TX Pin` 設為 4。

  .. image:: /_static/qconf_gpio_uart_rs485_example.png

接下來設定 UART1 作為 Debug message 輸出裝置。

- 將 :menuselection:`Debug --> Debug Message Device` 設定為 UART1。

  .. image:: /_static/qconf_check_debug_uart1.png

驗證結果
^^^^^^^^^^^^^^^^^^^^^^^^^

1. 點擊 |build_icon| 按鈕建置專案。確保：

  - USB to SPI 轉換板已正確安裝。

    - 若您尚未將 USB to SPI 轉換板與 PC 連接，請參考 :ref:`usb2spi_connect2pc` 。
    - 若您尚未將 USB to SPI 轉換板與 EVB 連接，請參考 :ref:`usb2spi_connect2board` 。
    - 若您尚未安裝 USB to SPI 轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

  - EVB 已經切換為 Co-operative Mode。

    .. note::

       若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

2. 重置 EVB 的電源。
3.	等程式建置完成，點擊 |run_icon| 按鈕即可透過 USB to SPI 轉換板啟動系統。
4.	執行 Tera Term，點擊 Tera Term 主選單的 :menuselection:`Setup --> Serial Port` 。Port 的部分請選擇 USB to TTL 轉換板對應的 com port。Speed 的部分請選擇 9600。

  .. image:: /_static/teraterm_set_speed_9600.png

5.	點擊 "New setting"，在 Tera Term 的主畫面中黑色區域輸入任意文字，您會發現畫面將反饋任何您輸入的內容。

  .. image:: /_static/teraterm_uart_echo_example.png

.. attention::

  1. 若您輸入任何內容後，畫面仍一片空白，請重複整個步驟並檢查接線是否正確。
  2. 這裡我們選擇 UART0 的操作模式為 DMA 模式，因此將運行 ``<sdk_root>/project/test_rs485/test_rs485.c`` 的 ``TestFuncUseDMA()`` 來進行整個測試行為。若您選擇的操作模式為 Interrupt 模式，則將運行 ``<sdk_root>/project/test_rs485/test_rs485.c`` 的 ``TestFuncUseINTR()`` 。若您選擇的操作模式為 FIFO 模式，則將運行 ``<sdk_root>/project/test_rs485/test_rs485.c`` 的 ``TestFuncUseFIFO()`` 。




.. raw:: latex

    \newpage
