.. _audio:

Audio
==========

功能描述
-------------------

IT986x 提供基本的聲音操作可細分兩部分：上層應用 AUDIO PROCESS FLOW 與底層 AUDIO HW CONTROL，底層所處理的事項封裝在上層應用中，使用者可直接使用上層提供的 API 即可播放音樂、錄製音樂、語音辨識、對講、調整音量、暫停等功能。

IT986x 支援的基本音訊格式為

- sampling rate：8K～48K
- channels：1 ~ 2 bit
- size：16 bit

.. image:: /_static/diagram_audio_flow.png

Audio HW control
^^^^^^^^^^^^^^^^^^^^^^^^^

包含負責 audio data 傳輸收送(read write pointer)的 interface(I2S)、digital/analog 轉換(DAC/ADC)、audio 硬體控制行為(譬如暫停、調整音量大小、mute/unmute)。這部分的軟體屬於後端(底層) driver 硬體控制部分。

.. image:: /_static/diagram_audio_hw_control_flow.png

下圖是一個 I2S DA buffer data 傳輸過程的示意圖。DA buffer 被實作為一個 ring buffer。從檔案讀取的資料被複製到 DA Buffer 中(DA 表示 digital-to-analog)，並更新 WP (即 write pointer)以表示有新資料寫入。RP (即 read pointer)由硬體控制，表示哪些資料以被播放。

.. image:: /_static/diagram_audio_da_buffer.png

下圖是一個 I2S AD buffer data 傳輸過程的示意圖。AD buffer 同樣被實作為一個 ring buffer。WP  (即 write pointer)由硬體自動更新，記錄從外部(mic)接收到的資料存放於 buffer 的位置。RP (即 read pointer)由使用者自行更新，表示已經從 AD buffer 讀取過的最後一筆資料的位置。

.. image:: /_static/diagram_audio_ad_buffer.png

Audio process flow
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

為了開發方便，底層 driver 與資料操作被進一步封裝成較簡化的 API，可供使用者直接呼叫已模組化的功能，使用者也能根據需求自行增加 audio 相關操作流程。例如：播放聲音檔案、錄製聲音檔案、混音、語音識別、通話等。根據應用的不同可再細分成 audio list flow 和 audio mgr 兩種處理流程。audio list flow 主要是針對通話的應用，並且可擴展為更廣泛的聲音處理相關應用(包含聲音檔案播放)。而 audio mgr 只針對聲音檔案播放的應用。其相關的測試範例位於 ``project/test_audio/audioFlow`` 和 ``project/test_audio/audioMgr`` 。

audio list flow
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Audio List flow 是將語音處理過程拆解成功能相異的 filter 並將這些 filter 串接起來形成一個完整的流程(flow)，使用者可自行組合 filter 建立新功能。Audio list flow 所提供的 API 已包含硬體的控制，開發人員依據說明即可操作播放音樂、錄音、音訊處理等基礎功能。

.. image:: /_static/diagram_audio_list_flow.png

audio mgr
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: /_static/diagram_audio_manager.png

相關的 KConfig 設定
-----------------------------

- :menuselection:`AUDIO --> Audio Enabel`

  決定是否要啟用 AUDIO。

- :menuselection:`Peripheral --> I2S Enable --> I2S INTERNAL CODEC`

  啟用並勾選 I2S Internal codec。

- :menuselection:`Peripheral --> Amplifier available` 和 :menuselection:`GPIO --> Amplifier Enable/Disable Pin、Amplifier Mute Pin`

  設定 AMP GPIO PIN，若硬體有另外設計則需設定，IT986x Standard EVB 不須另外設定。

- :menuselection:`AUDIO --> Audio DAC module`

  IT986x 內建一個 internal AD/DA codec，不須另外加掛外部 AD/DA Codec。若需要使用外部的 AD/DA Codec，請在此設定項目點擊2下即可更換預設選擇的 ``itp_dac_ite970.c`` 。

- :menuselection:`AUDIO --> Audio Enable --> Mic single end mode、SPK single ground mode`

  MIC 外部線路請勾選 single-end/differential (依硬體設定勾選)
  SPK 外部線路請勾選 single-ground/differential (依硬體設定勾選)

- :menuselection:`AUDIO --> Audio Enable --> Dialogue Parameter Set`

  對講系統功能選單，適用於 Linphone 系統

- :menuselection:`AUDIO --> Audio mgr --> Music Codec、Audio Mgr Buffer Size Setting`

  選擇要支援的音訊解碼器(wav、mp3、wma)。IT986x 除了主 CPU 之外，另規劃了一顆 arm-lite CPU 用來處理音訊解碼，以減少主 CPU 負擔。

- :menuselection:`AUDIO --> Audio ASR --> Audio ASR LIB module`

  語音識別功能選單，用來選擇語音識別函式庫，預設為 ``libasr_ITE01.a`` 。

相關的程式碼檔案
----------------------------------

.. flat-table::
  :header-rows: 1

  * - 路徑
    - 描述
  * - | ``sdk/driver/i2s/it9860/i2s_9860.c``
      | ``sdk/include/i2s/i2s.h``
      | ``sdk/driver/itp/dac/itp_dac_ite970.c``
    - 底層 i2s driver 函式，提供 I2S 初始化操作、音量調整、AD/DA 資料收送、AD/DA codec driver。
  * - | ``sdk/share/flower/audio/flower_stream.c``
      | ``sdk/share/flower/audio``
    - List flow 架構。提供最上層函式，並對底層 I2S 操作進行封裝，使用者可直接呼叫做基本 API(播放音樂、錄製音樂，語音識別，對講等功能...)，後續開發的架構形式。
  * - ``sdk/share/audio_mgr/audio_mgr.c``
    - Audio mgr 架構。提供最上層函式，並對底層 I2S 操作進行封裝，主要作為 mp3、wma、aac 等需要解碼播放的音樂。

相關 API 描述
-----------------

本節描述用來操作 AUDIO 的相關 API。分為兩部分介紹底層 Audio HW control 與上層應用 Audio process flow，在開發過程時，底層 I2S 的控制項目大多會封裝在上層應用中，如有需求可以自行增加或是直接呼叫底層提供的 API 進行操作。

測試範例檔案請參閱：

- ``project/test_audio/audioHWctrl``
- ``project/test_audio/audioFlow``
- ``project/test_audio/audioMgr``
- ``project/test_audio/test_audio.docx``
 
Audio HW control 相關 API 描述
------------------------------------------

AUDIO 操作第一步驟是 I2S driver 的初始化，I2S 初始化包含 clock 設定 DAC、ADC codec driver 設定等，相關測試程式位於 ``project/test_audio/audioHWctrl`` 。底下將針對 I2S driver 所提供的 API 做進一步的描述。

i2s_init_DAC
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_init_DAC(STRC_I2S_SPEC *i2s_spec)

**參數**

``STRC_I2S_SPEC *i2s_spec``

  I2s 裝置初始設定。其各個欄位描述如下：

  - ``unsigned int channels``

    值 1 表示 mono 聲道，值 2 表示 stereo 聲道。

  - ``unsigned int sample_rate``

    Sampling rate，以 Hz 為單位。譬如 44100 Hz 就填入 44100。

  - ``unsigned int buffer_size``

    DA buffer size，以 bytes 為單位。

  - ``unsigned int is_big_endian``

    存放在 DA buffer 的資料是否是以 big endian 的型式存放。值 0 表示否，其他值表示是。

  - ``unsigned char *base_i2s``

    DA buffer 基底位址。

  - ``unsigned int sample_size``

    存放在 DA buffer 的每個 audio sample 的大小，以 bit 為單位。該值可能是 16、24、32，分別代表 16bits、24bits、32bits。

  - ``unsigned int from_LineIN``

    輸入是否從 line in 輸入。值 0 表示否，其他值表示是。因為此函式只能用來處理輸出，因此此欄位一率填 0。

  - ``unsigned int from_MIC_IN``

    輸入是否從麥克風輸入。值 0 表示否，其他值表示是。因為此函式只能用來處理輸出，因此此欄位一率填 0。

  - ``unsigned int num_hdmi_audio_buffer``

    當輸出是從 hdmi 輸出時，所需要用來暫存從 hdmi 輸出的語音資料的緩衝區個數。若無需從 hdmi 輸出，則此欄位填 0。

  - ``unsigned char *base_hdmi[4]``

    用來暫存從 hdmi 輸出的語音資料的緩衝區。其個數由 num_hdmi_audio_buffer 欄位指定。

  - ``unsigned int is_dac_spdif_same_buffer``

    從 spdif 輸出的語音資料是否與從 dac 輸出的語音資料共用相同的緩衝區。值 0 表示否，其他值表示是。

  - ``unsigned char *base_spdif``

    從 spdif 輸出的語音資料的緩衝區的基底位址。

  - ``unsigned int enable_Speaker``

    是否啟用喇叭輸出。值 0 表示否，其他值表示是。

  - ``unsigned int enable_HeadPhone``

    是否啟用耳機輸出。值 0 表示否，其他值表示是。

  - ``unsigned int postpone_audio_output``

    若此值為 0，則存放在緩衝區的語音資料會在呼叫此函式後立即輸出。若此值為 1，則存放在緩衝區的語音資料  在呼叫此函式後並不會立即輸出。

**描述**

初始化 DAC。

底下展示從耳機或喇叭輸出雙聲道、8KHz、16bits的語音資料的範例程式。

.. code-block:: c

    void initDA() {
        /* init DAC */
        dac_buf = (uint8_t*)malloc(DAC_BUFFER_SIZE);
        memset((uint8_t*) dac_buf, 0, DAC_BUFFER_SIZE);
        memset((void*)&spec_daI, 0, sizeof(STRC_I2S_SPEC));
        spec_daI.channels                 = 2;
        spec_daI.sample_rate              = 8000;
        spec_daI.buffer_size              = DAC_BUFFER_SIZE;
        spec_daI.is_big_endian            = 0;
        spec_daI.base_i2s                 = (uint8_t*) dac_buf;
        spec_daI.sample_size              = 16;
        spec_daI.num_hdmi_audio_buffer    = 0;
        spec_daI.is_dac_spdif_same_buffer = 1;
        spec_daI.enable_Speaker           = 1;
        spec_daI.enable_HeadPhone         = 1;
        spec_daI.postpone_audio_output    = 1;
        spec_daI.base_spdif                = (uint8_t*) dac_buf;
        audio_init_DA(&spec_daI);
        i2s_pause_DAC(1);
    }
 

i2s_init_ADC
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_init_ADC(STRC_I2S_SPEC *i2s_spec)

**參數**

``STRC_I2S_SPEC *i2s_spec``

  I2s 裝置初始設定。其各個欄位描述如下：

  - ``unsigned int channels``

    值 1 表示 mono 聲道，值 2 表示 stereo 聲道。

  - ``unsigned int sample_rate``

    Sampling rate，以 Hz 為單位。譬如 44100Hz 就填入 44100。

  - ``unsigned int buffer_size``

    DA buffer size，以 bytes 為單位。

  - ``unsigned int is_big_endian``

    存放在 DA buffer 的資料是否是以 big endian 的型式存放。值 0 表示否，其他值表示是。

  - ``unsigned char *base_i2s``

    DA buffer 基底位址。

  - ``unsigned int sample_size``

    存放在 DA buffer 的每個 audio sample 的大小，以 bit 為單位。該值可能是 16、24、32，分別代表 16bits、24bits、32bits。

  - ``unsigned int from_LineIN``

    輸入是否從 line in 輸入。值 0 表示否，其他值表示是。

  - ``unsigned int from_MIC_IN``

    輸入是否從麥克風輸入。值 0 表示否，其他值表示是。

  - ``unsigned int num_hdmi_audio_buffer``

    當輸出是從 hdmi 輸出時，所需要用來暫存從 hdmi 輸出的語音資料的緩衝區個數。若無需從 hdmi 輸出，則此欄位填 0。因為此函式只能用來處理輸入，因此此欄位一率填 0。

  - ``unsigned char *base_hdmi[4]``

    用來暫存從 hdmi 輸出的語音資料的緩衝區。其個數由 num_hdmi_audio_buffer 欄位指定。因為此函式只能用來處理輸入，因此此欄位一率填 0。

  - ``unsigned int is_dac_spdif_same_buffer``

    從 spdif 輸出的語音資料是否與從 dac 輸出的語音資料共用相同的緩衝區。值 0 表示否，其他值表示是。因為此函式只能用來處理輸入，因此此欄位一率填 0。

  - ``unsigned char *base_spdif``

    從 spdif 輸出的語音資料的緩衝區的基底位址。因為此函式只能用來處理輸入，因此此欄位一率填 0。

  - ``unsigned int enable_Speaker``

    是否啟用喇叭輸出。值 0 表示否，其他值表示是。因為此函式只能用來處理輸入，因此此欄位一率填0。

  - ``unsigned int enable_HeadPhone``

    是否啟用耳機輸出。值 0 表示否，其他值表示是。因為此函式只能用來處理輸入，因此此欄位一率填 0。

  - ``unsigned int postpone_audio_output``

    若此值為 0，則存放在緩衝區的語音資料會在呼叫此函式後立即輸出。若此值為 1，則存放在緩衝區的語音資料在呼叫此函式後並不會立即輸出。因為此函式只能用來處理輸入，因此此欄位一率填 0。

**描述**

初始化 ADC。

底下展示從麥克風輸入單聲道、8KHz、16bits 的語音資料的範例程式。

.. code-block:: c

    /* init ADC */
    adc_buf = (uint8_t*)malloc(ADC_BUFFER_SIZE);
    memset((uint8_t*) adc_buf, 0, ADC_BUFFER_SIZE);
    memset((void*)&spec_adI, 0, sizeof(STRC_I2S_SPEC));
    spec_adI.channels                   = 1;
    spec_adI.sample_rate                = 8000;
    spec_adI.buffer_size                = ADC_BUFFER_SIZE;
    spec_adI.is_big_endian              = 0;
    spec_adI.base_i2s                   = (uint8_t*) adc_buf;
    spec_adI.sample_size                = 16;
    spec_adI.record_mode                = 1;
    spec_adI.from_LineIN                = 0;
    spec_adI.from_MIC_IN                = 1;
    //i2s_init_ADC(&spec_adI);
    audio_init_AD(&spec_adI);
    audio_pause_AD(1);

 
i2s_get_DA_running
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int i2s_get_DA_running(void)

**描述**

獲取 DAC 是否已被初始化。
 
**回傳值**

若值為 1 表示 DAC 已被初始化。若值為 0 表示 DAC 尚未被初始化。

i2s_get_AD_running
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int i2s_get_AD_running(void)

**描述**

獲取 ADC 是否已被初始化。
 
**回傳值**

若值為 1 表示 ADC 已被初始化。若值為 0 表示 ADC 尚未被初始化。


i2s_deinit_DAC
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_deinit_DAC(void)

**描述**

重置 DAC。
 


i2s_deinit_ADC
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_deinit_ADC(void)

**描述**

重置 ADC。
 

i2s_set_direct_volperc
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_set_direct_volperc(unsigned int volperc)

**參數**

``unsigned int volperc``

  欲設置的播放音量。其範圍為 0 ~ 99。

**描述**

設置 DAC 音量(播放音量)。
 


i2s_ADC_set_rec_volperc
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_ADC_set_rec_volperc (unsigned int recperc)

**參數**

``unsigned int recperc``

  欲設置的錄音音量。其範圍為 0 ~ 99。

**描述**

設置 ADC 音量(錄音音量)。
 

i2s_set_direct_RLvolperc
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_set_direct_RLvolperc(unsigned int volperc, unsigned char RL)

**參數**

``unsigned int volperc``

  欲設置的播放音量。其範圍為 0 ~ 99。

``unsigned char RL``

  值為 0 表示僅設定右聲道的音量，值為 1 表示僅設定左聲道的音量。

**描述**

設置左聲道或右聲道的 DAC 音量(播放音量)。
 

i2s_ADC_set_rec_RLvolperc
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_ADC_set_rec_RLvolperc(unsigned int recperc, unsigned char RL)

**參數**

``unsigned int recperc``

  欲設置的錄音音量。其範圍為 0 ~ 99。

``unsigned char RL``

  值為 0 表示僅設定右聲道的音量，值為 1 表示僅設定左聲道的音量。

**描述**

設置左聲道或右聲道的 ADC 音量(錄音音量)。
 

i2s_get_current_volperc
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: unsigned int i2s_get_current_volperc(void)

**描述**

取得目前 DAC 的音量(播放音量)。
 
**回傳值**

目前 DAC 的音量。其範圍為 0 ~ 99。


i2s_ADC_get_current_volstep
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: unsigned int i2s_ADC_get_current_volstep(void)

**描述**

取得目前ADC的音量(錄音音量)。
 
**回傳值**

目前ADC的音量。其範圍為0~99。

i2s_get_current_RLvolperc
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: unsigned int i2s_get_current_RLvolperc(unsigned char RL)

**參數**

``unsigned char RL``

  值為 0 表示僅取得右聲道的音量，值為 1 表示僅取得左聲道的音量。

**描述**

獲取左聲道或右聲道的 DAC 音量(播放音量)。
 
**回傳值**

左或右聲道的音量。其範圍為0~99。

i2s_ADC_get_rec_RLvolperc
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: unsigned int i2s_ADC_get_current_RLvolstep(unsigned char RL)

**參數**

``unsigned char RL``

  值為 0 表示僅設定右聲道的音量，值為 1 表示僅設定左聲道的音量。

**描述**

設置左聲道或右聲道的 ADC 音量(錄音音量)。
 
**回傳值**

左或右聲道的音量。其範圍為 0 ~ 99。

i2s_pause_DAC
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_pause_DAC(int pause)

**參數**

``int pause``

  值 1 表示暫停播放，值 0 表示開始播放。

**描述**

停止/開始 I2S 傳輸(播放控制)。
 
i2s_pause_ADC
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_pause_ADC(int pause)

**參數**

``int pause``

  值 1 表示暫停錄音，值 0 表示開始錄音。

**描述**

停止/開始 I2S 傳輸(錄音控制)。
 
i2s_mute_volume
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_mute_volume(int mute)

**參數**

``int mute``

  值 1 表示靜音，值 0 表示非靜音。

**描述**

針對聲音播放，進行靜音/非靜音處理。
 
i2s_mute_ADC
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_mute_ADC (int mute)

**參數**

``int mute``

  值1表示靜音，值0表示非靜音。

**描述**

針對聲音錄製，進行靜音/非靜音處理 。
 
I2S_DA32_GET_RP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: uint32_t I2S_DA32_GET_RP(void)

**描述**

取得 DA buffer 的 read pointer 數值。
 
**回傳值**

DA buffer 的 read pointer 數值。

I2S_DA32_GET_WP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: uint32_t I2S_DA32_GET_WP(void)

**描述**

取得 DA buffer 的 write pointer 數值。
 
**回傳值**

DA buffer 的 write pointer 數值。

I2S_DA32_SET_WP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void I2S_DA32_SET_WP(uint32_t data32)

**參數**

``uint32_t data32``

  DA buffer 的 write pointer 數值。

**描述**

設定 DA buffer 的 write pointer 數值。
 
 
I2S_AD32_GET_RP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: uint32_t I2S_AD32_GET_RP(void)

**描述**

取得 AD buffer 的 read pointer 數值。
 
**回傳值**

AD buffer 的 read pointer數值。

I2S_AD32_SET_RP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void I2S_AD32_SET_RP(uint32_t data32)

**參數**

``uint32_t data32``

  AD buffer的read pointer數值。

**描述**

設定 AD buffer 的 read pointer 數值。


 
I2S_AD32_GET_WP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: uint32_t I2S_AD32_GET_WP(void)

**描述**

取得 AD buffer 的 write pointer 數值。
 
**回傳值**

AD buffer 的 write pointer 數值。

Audio list flow 相關 API 描述
-----------------------------------------------

Audio list flow 所提供的 API 已包裝硬體的控制，software programmer 依據說明即可操作播放音樂、錄音、音訊處理等基礎功能。請參考測試程式 ``project/test_audio/audioFlow`` 。底下將針對audio list flow 所提供的 API 做進一步的描述。

.. image:: /_static/diagram_audio_list_flow.png


IteStreamInit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: IteFlower *IteStreamInit(void)

**描述**

初始化 IteFlower。

要使用 Audio list flow 所有提供的 API 之前，需要先初始化 IteFlower，否則後續的功能無法正常運作。


**回傳值**

IteFlower 的指標。
 

flow_start_sound_play
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void flow_start_sound_play(IteFlower *f,const char* filepath,PlaySoundCase mode)

**參數**

``IteFlower *f``

  IteFlower 的指標。

``const char* filepath``

  播放檔案的路徑。

``PlaySoundCase mode``

  Repeat 重複播放、Normal 只播放一次。

**描述**

開始播放 wav 檔。
 
.. image:: /_static/diagram_audio_sound_play.png

flow_stop_sound_play
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void flow_stop_sound_play(IteFlower *f)

**參數**

``IteFlower *f``

  IteFlower 的指標

**描述**

結束播放 wav 檔。

 
PlayFlowPause
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void PlayFlowPause(IteFlower *f,bool pause)

**參數**

``IteFlower *f``

  IteFlower 的指標

``bool pause``

  值 true 表示暫停播放，值 0 表示繼續播放。

**描述**

暫停/繼續播放 wav。
 

PlayFlowMix
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void PlayFlowMix(IteFlower *f, const char *fileinsert)

**參數**

``IteFlower *f``

  IteFlower 的指標。

``const char *fileinsert``

  混音的音檔路徑。

  若需要連續混入音檔，可以空白作為分隔檔案路徑。

  Ex：``fileinsert="a:/1.wav a:/2.wav a:/9.wav`` 。

**描述**

調用 ``flow_start_sound_play`` 播放背景音樂時需要混入短語音。背景音樂與短語音格式須為 wav 檔，且格式相同(sampling rate、channels、bitsize) 。
 

flow_start_sound_record
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void flow_start_sound_record(IteFlower *f,const char* filepath,int rate)

**參數**

``IteFlower *f``

  IteFlower 的指標。

``const char* filepath``

  儲存錄音檔案的路徑。

``int rate``

  設定錄音的取樣頻率。

**描述**

開始錄製音檔。

.. image:: /_static/diagram_audio_sound_record.png

 
flow_stop_sound_record
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void flow_stop_sound_record(IteFlower *f)

**參數**

``IteFlower *f``

  IteFlower 的指標。

**描述**

結束錄製音檔。

 
flow_start_asr
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void flow_start_asr(IteFlower *f,Callback_t func)

**參數**

``IteFlower *f``

  IteFlower 的指標。

``Callback_t func``

  上層回呼函數。

**描述**

開始語音識別。

當語音識別成功觸發事件，回呼函數進行上層的操作。

.. image:: /_static/diagram_audio_sound_asr.png
 
flow_stop_asr
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void flow_stop_asr(IteFlower *f)

**參數**

``IteFlower *f``

  IteFlower 的指標。

**描述**

結束語音識別。

 
flow_start_soundrw
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void flow_start_soundrw(IteFlower *f)

**參數**

``IteFlower *f``

  IteFlower 的指標。

**描述**

開始 Mic 收音 --> Spk 放音。

.. image:: /_static/diagram_audio_soundrw.png
 
 
flow_stop_soundrw
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void flow_stop_soundrw(IteFlower *f)

**參數**

``IteFlower *f``

  IteFlower 的指標。

**描述**

結束 Mic 收音 --> Spk 放音


flow_start_udp_audiostream
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void flow_start_udp_audiostream(IteFlower *f, int rate, AudioCodecType type, const char *rem_ip, unsigned short rem_port)

**參數**

``IteFlower *f``

  IteFlower 的指標。

``int rate``

  取樣頻率：8K。

``AudioCodecType type``

  編解碼格式(ulaw、alaw ...)。

``const char *rem_ip``

  remorte 的 IP 位址。

``unsigned short rem_port``

  remort 的 port 號。

**描述**

開始 UDP 傳輸雙向溝通。

.. image:: /_static/diagram_audio_udp_audiostream.png

 
flow_stop_udp_audiostream
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: uint8_t flow_stop_udp_audiostream(IteFlower *f)

**參數**

``IteFlower *f``

  IteFlower 的指標。

**描述**

結束 UDP 傳輸雙向溝通。


 
Audio mgr 相關 API 描述
---------------------------------------------

Audio mgr 所提供的 API 已包裝硬體的控制，software programmer 依據說明即可操作播放音樂 mp3、wma、aac、wav 等需要解碼的音樂格式。請參考測試程式 ``project/test_audio/audioMgr`` 。底下將針對 audio mgr 所提供的 API 做進一步的描述。

.. image:: /_static/diagram_audio_manager.png

AudioInit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void AudioInit(void)

**描述**

初始化 Audio mgr。

要使用 Audio mgr 所有提供的 API 之前，需要先初始化 Audiomgr，否則後續的功能無法正常運作。
 
AudioPlay
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int AudioPlay(char* filename, AudioPlayCallback func)

**參數**

``char* filename``

  播放檔案的路徑。

``AudioPlayCallback func``

  回呼函數。

**描述**

播放需要解碼的音樂格式檔案。EX：mp3、aac、wma、wav…。

**回傳值**

-1 代表開啟檔案失敗，0 代表正常開啟檔案。

 
AudioStop
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void AudioStop(void)

**描述**

停止播放。
 

AudioSetVolume
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void AudioSetVolume(int level)

**參數**

``int level``

  欲設置的播音音量。其範圍為 0 ~ 99。

**描述**

設定播音音量。

 
AudioMute
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void AudioMute(void)

**描述**

設定為靜音。
 

AudioUnMute
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void AudioUnMute (void)

**描述**

取消靜音。


簡易範例程式
-----------------------------

此範例演示播放位於USB隨身碟中檔名為 ``Music8k1c.wav`` 的音樂檔案。此範例的完整程式碼位於 ``project/test_audio/audioFlow/test_flower_play.c`` 。

.. code-block:: c

    #include <sys/ioctl.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <pthread.h>
    #include <unistd.h>
    #include "openrtos/FreeRTOS.h"
    #include "flower.h"

    //======================================================//
    IteFlower *flower = NULL;

    /*********************/
    /*   FC 1: A->B->C   */
    /*********************/
    void* TestFunc(void* arg)
    {
        itpInit();
        sleep(2);
        flower = IteStreamInit();

        //IteFChain fc1=s->fc;
        const char *filename="a:/Music8k1c.wav";

        flow_start_sound_play(flower,filename,Normal);

        while(1) {
            sleep(1);
            if(flower->audiostream==NULL) break;
        }

        flow_stop_sound_play(flower);

        printf("Test Task done\n");
    }
 
完整範例展示
-----------------------------

在 ``project/test_audio/audioHWctr``、``project/test_audio/audioFlow``、``project/test_audio/audioMgr`` 提供一般 audio 所常用的操控、包含播音、錄音、同時播放音功能。透過這些範例可以初步檢查 audio 是否能正常運作，以及 audio 相關的基本 API 應用。

硬體連接
^^^^^^^^^^^^^

1. 左右聲道接上 MIC 與 SPK。

   .. image:: /_static/e_circuit_audio_connect_speaker.png

2. 執行 ``<sdk_root>/build/openrtos/test_audio.cmd`` 。
3. 點選想要測試的項目。

  .. image:: /_static/qconf_audio_test.png

.. |run_icon| image:: /_static/qconf_run_icon.png
  :alt: run icon
  :width: 14
  :height: 14

.. |build_icon| image:: /_static/qconf_build_icon.png
  :alt: build icon
  :width: 14
  :height: 14

驗證結果
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. 點擊 |build_icon| 按鈕建置專案。確保：

  - USB to SPI 轉換板已正確安裝。

    - 若您尚未將 USB to SPI 轉換板與 PC 連接，請參考 :ref:`usb2spi_connect2pc` 。
    - 若您尚未將 USB to SPI 轉換板與 EVB 連接，請參考 :ref:`usb2spi_connect2board` 。
    - 若您尚未安裝 USB to SPI 轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

  - EVB 已經切換為 Co-operative Mode。

    .. note::

       若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

2. 執行 Tera Term 或任何 Serial Port 軟體。
3. 點擊 |run_icon| 按鈕即可透過 USB to SPI 轉換板啟動系統。


底下針對三個測試項目做進一部的說明。

 
test audio HW control
""""""""""""""""""""""""

在Test audio HW control項目中，包含四個子測試範例。其中：

.. image:: /_static/qconf_audio_test_all.png

- HW iis play：

  測試聲音播放。其完整測試程式碼位於 ``project/test_audio/audioHWctrl/test_main_simple.c`` 。正常的話在系統啟動後應可以在喇叭聽到一段長嗶聲後面接著一小段語音。

- HW usbwav play：

  測試從 USB 隨身碟中播放聲音檔(僅能播放 PCM wav 檔)。其完整測試程式碼位於 ``project/test_audio/audioHWctrl/test_main_usb_play.c`` 。請將檔名為 ``test.wav`` 的聲音檔放置於 USB 隨身碟中，系統啟動後即可從喇叭聽到聲音。

- HW usbwav record：

  測試錄音至 USB 隨身碟中。其完整測試程式碼位於 ``project/test_audio/audioHWctrl/test_main_usb_record.c`` 。系統啟動後會立即錄音約 8 秒的時間，並將錄製下來的聲音存放於 USB 隨身碟中(以 wav 檔案的格式)。

- HW louder：

  測試 MIC-IN-SPK-OUT。 ``project/test_audio/audioHWctrl/test_main_louder.c`` 。系統啟動後，麥克風與喇叭同時運作，形成擴音器效果，測試 AD 和 DA 同時運作。
 
test audio FLOW
""""""""""""""""""""""""

在 Test audio FLOW 項目中，包含五個子測試範例。其中：

.. image:: /_static/qconf_audio_test_audio_flow.png

- FLOWER play：

  測試由 USB 隨身碟中播放聲音檔(僅能播放PCM wav檔)。其完整測試程式碼位於 ``project/test_audio/audioFlow/test_flower_play.c`` 。請將檔名為 ``Music8k1c.wav`` 的測試音檔置於USB隨身碟中，系統啟動後即可從喇叭聽到聲音。

- FLOWER usbrec：

  測試錄音至 USB 隨身碟中。其完整測試程式碼位於 ``project/test_audio/audioFlow/test_flower_usbrec`` 。插入 USB 隨身碟，系統啟動後會立即錄音約 8 秒的時間，並將錄製下來的聲音存放於 USB 隨身碟中(以 wav 檔案的格式，其檔案路徑為 ``a:/rec.wav`` )。

- FLOWER mix：

  將 USB 隨身碟中的多個聲音檔案進行混音操作。其完整測試程式碼位於 ``project/test_audio/audioFlow/test_flower_mix.c`` 。

  將以下聲音檔放入 USB 隨身碟中，插入 USB 隨身碟，系統啟動後即可聽到聲音。

  - 主要聲音檔： ``a:/Music8k1c.wav`` (背景音樂)。
  - 要混音的檔案： ``a:/0.wav``、``a:/1.wav``、``a:/2.wav``、``a:/3.wav`` (其內容為簡短的語音)。

- FLOWER asr：

  這是一個語音識別的範例程式。其完整測試程式碼位於 ``project/test_audio/audioFlow/test_flower_asr.c`` 。欲使用此程式，請在系統啟動後，念出以下中文指令：

  .. flat-table::

    * - 小陽管家
      - 打開窗簾
      - 打開空調
    * - 電影模式
      - 關閉窗簾
      - 關閉空調
    * - 在家模式
      - 停止窗簾
      - 最小風量
    * - 工作模式
      - 打開燈光
      - 中等風量
    * - 離家模式
      - 關閉燈光
      - 最大風量
    * - 打開網絡
      - 調亮一點
      - 自動風量
    * - 關閉網絡
      - 調暗一點
      - 調高風量
    * -
      -
      - 調低風量
    * -
      -
      - 調低溫度
    * -
      -
      - 調高溫度

　   　
  若能成功識別將在輸出訊息中顯示所識別到的指令，將顯示如下訊息。

  .. image:: /_static/teraterm_audio_test_log.png


- FLOWER louder：

  測試 MIC-IN-SPK-OUT。其完整測試程式碼位於 ``project/test_audio/audioFlow/test_main_louder.c`` 。系統啟動後，將從麥克風收音，從喇叭發出聲音，形成擴音器效果。
 
test audio MGR
""""""""""""""""""""""""

在 Test audio FLOW 項目中，只包含一個子測試範例。其中：

.. image:: /_static/qconf_audio_test_audio_mgr.png

- AUDIOMGR play：

  測試由 USB 隨身碟中播放聲音檔，這裡展示播放一個 MP3 檔案。其完整測試程式碼位於 ``project/test_audio/audioHWctrl/test_main_louder.c`` 。請將 ``test.mp3`` 放置於 USB 隨身碟中，系統啟動後即可聽到聲音。

.. raw:: latex

    \newpage
