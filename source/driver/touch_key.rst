.. _touch_key:

Touch Key
==============

功能描述
---------------------------

Key 是 SOC 最基本的輸入裝置，依據輸入介面來區分有 GPIO、SPI、I2C、USB、PS/2、RF…，各式各樣的型態，本節所討論的是專指以 I2C 為輸入介面的 KEY，包含 I2C 介面的機械按鍵、觸控按鍵、觸控滑條等都是適用於本文的輸入裝置。至於其他 I/O 介面的 KEY、或是 I2C 介面的 Touch Panel 隨附的 Touch Key，都不在本文的適用範圍內(這兩種的 I2C 介面的輸入裝置有另文專述)。此外，ITE SOC 的 Touch Key 模組主要是使用 IT7236。IT7236 是電容式觸控按鍵控制器，最多可提供 16 組 Key Number。目前實際應用是以 5 組 & 16 組( 4 X 4 數字鍵)的 Touch Key 為主。

相關 Touch Key 的 code 請參考 ``<sdk_root>/sdk/driver/itp/itp_keypad.c`` 以及 ``<sdk_root>/sdk/driver/itp/keypad/itp_keypad_xxxx.c`` (xxxx 為 TOUCH KEY 的模組名稱)。

關於 Touch Key 的 event 的傳遞，與一般 GPIO Key 的 event 相同，都是由上層 AP 透過呼叫 SDL 的 API 來 polling Touch key 的 event。關於 SDL 的 polling event 與 API 呼叫請參考 SDL 說明文件，或是參考 ``<sdk_root>/project/doorbell_indoor/scene.c`` 。

本模組也可透過幾個簡單的程式修改，支援其他 KEY 控制器的輸入裝置。

相關的 KConfig 設定
----------------------------------------------------------------

在 KCONFIG 中要指定選用的 TOUCH KEY 模組，首先必須勾選 "I2C0/I2C1/I2C2/I2C3 Enable" 以及 "Keypad Enable"，然後在 "Keypad Module" 欄位中輸入 KEY 模組所對應的 Driver 檔案名稱(EX:  ``itp_keypad_it7236.c`` )，如圖所示：

.. image:: /_static/qconf_peripheral_i2c0_enable.png

.. image:: /_static/qconf_peripheral_keypad_enable.png


- :menuselection:`Peripheral --> Keypad Enable`

  決定是否要啟用 KEYPAD 模組。

- :menuselection:`Peripheral --> Keypad Enable --> Keypad module`

  設定所要使用的 KEYPAD 模組名稱。

- :menuselection:`Peripheral --> Keypad Enable --> Keypad Press Key interval(ms)`

  設定發送每個 KEY Event 的間隔時間。

- :menuselection:`Peripheral --> Keypad Enable --> Keypad Repeat Key Enable`

  啟用 repeat key 功能，當手指按壓某 KEY 不放的時候，系統將會重複不斷發送 KEY Event。

- :menuselection:`Peripheral --> Keypad Enable --> Keypad mapping table`

  設定 KEYPAD 的 MAPPING TABLE 檔(放在 ``<sdk_root>/sdk/target/keypad`` 目錄中)。Driver 回報上層的值是 INT 0 ~ N (N 指最大 KEY 總數 - 1)，上層依據此 Mapping Table 翻譯成相對應的 Key Code。

- :menuselection:`Peripheral --> Keypad Enable --> Enable the setting of the total keypad number`

  啟用 KEY 總數的設定(I2C 介面)。

- :menuselection:`Peripheral --> Keypad Enable --> Touch Key total number`

  設定 KEY 的總數(應用在 I2C 介面的 keypad)。

- :menuselection:`Peripheral --> Keypad Enable --> Touch Key use I2C interface`

  設定 KEYPAD 是否使用 I2C 介面。

KEY GPIO 相關 KConfig 設定
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

有些 Touch Key 需要使用額外的 GPIO pin 來偵測 INT 訊號，以便通知 driver 何時該透過 I2C BUS 去讀取控制器的 Register，所以需要在 KCONFIG 中設定 Touch Key 的 GPIO pin，系統就會依據此設定的 GPIO 做為 Touch Key 的 INT pin。關於設定 Touch Key 的 INT pin，同樣是到 KCONFIG 的 :menuselection:`GPIO --> Keypad Pins` 的欄位做設定(如下圖所示)。

.. image:: /_static/qconf_gpio_touch_key.png

相關的程式碼檔案
----------------------------------------------------------------

.. flat-table::
  :header-rows: 1

  * - 路徑
    - 描述
  * - ``<sdk_root>/sdk/driver/itp/itp_keypad.c``
    - | Keypad ITP Driver，主要目的是定義三個 API，做為下層 driver 實作的規範。即：
      |
      | ``itpKeypadInit();``
      | ``itpKeypadGetMaxLevel();``
      | ``itpKeypadProbe();``
  * - ``<sdk_root>/sdk/driver/itp/keypad/itp_keypad_xxx.c``
    - | ``itp_keypad_xxx.c`` 是 Keypad 的底層 driver，主要目的是實作以下三個 API，
      |
      | ``itpKeypadInit();``
      | ``itpKeypadGetMaxLevel();``
      | ``itpKeypadProbe();``

目前已經有實作的 Touch Key driver 有 ``itp_keypad_it7236.c`` 。
 
相關的 API 描述
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

ITP Driver 則是依據 POSIX 規範實作的 API，可以使用 OPEN / READ / WRITE / LSEEK / CLOSE / IOCTL 等函式對 I/O DEVICE 進行如讀寫檔案般的操作。以下是基於這種規範所實做的 Keypad ITP API。

DEVICE ID 為 ITP_DEVICE_KEYPAD (請參考 ``<sdk_root>/sdk/include/ite/itp.h`` )。

本節描述用來操作 KEYPAD 裝置的相關 API。KEYPAD driver 提供依據 POSIX 規範實作的 API，用戶可以透過 ``itpKeypadInit()``/``itpKeypadGetMaxLevel()``/``itpKeypadProbe()`` 等函式對 KEYPAD 裝置進行操作。

itpRegisterDevice
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void itpRegisterDevice(ITPDeviceType type, const ITPDevice *device)

  註冊裝置，即 ``itpRegisterDevice(ITP_DEVICE_KEYPAD, &itpDeviceKeypad);`` 。

**參數**

``ITPDeviceType type``

  裝置類型。完整裝置類型列表請見 ``<sdk_root>/sdk/include/ite/itp.h`` 。與 KEYPAD 裝置相關的裝置類型是 ITP_DEVICE_KEYPAD。

``const ITPDevice *device``

  裝置標識符。與 KEYPAD 裝置相關的裝置標識符有 ``itpDeviceKeypad`` 。

**描述**

此函式可用來將 KEYPAD 裝置註冊到核心裝置列表中，使上層 AP 可以透過 ``ioctl``/``read``/``write`` 等函式來操作 KEYPAD 的功能。本函式會在系統初始化期間被 ``itpInit()`` 函式呼叫 (見 ``<sdk_root>/sdk/driver/itp_init_openrtos.c`` )，因此若在您的專案中有呼叫過 ``itpInit()`` 函式，將無須再執行此註冊函式。

底下展示如何註冊 KEYPAD 裝置到系統核心，並初始化 KEYPAD 裝置。

.. code-block:: c

    itpRegisterDevice(ITP_DEVICE_KEYPAD, &itpDeviceKeypad);
    ioctl(ITP_DEVICE_KEYPAD, ITP_IOCTL_INIT, NULL);

ioctl
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr)

  控制裝置

**參數**

``int file``

  裝置標識符。其值為 ``ITPDeviceType`` 列表中的值。與 KEYPAD 裝置相關的裝置標識符有 ``ITP_DEVICE_KEYPAD`` 。

``unsigned long request``

  request 參數用來選擇要對裝置運行的操作功能，並將取決於所指定的裝置。

``void *ptr``

  ptr 參數表示對此特定裝置執行請求的功能所需的額外訊息。ptr 的型別取決於特定的控制請求，但它可以是整數，也可以是指向裝置特定資料結構的指標。

**描述**

``ioctl()`` 函數用於在 KEYPAD 裝置上執行各種控制功能。request 參數和可選的第三個參數（具有不同的型別）將傳遞給與 file 關聯的 KEYPAD 裝置的相應部分並由其解釋。

適用於 KEYPAD 裝置的 ``ioctl()`` 命令，其參數以及適用於每個單獨命令的錯誤狀態如下所述。

- ITP_IOCTL_INIT

  用來初始化 KEYPAD 裝置。ptr 參數為 NULL。

- ITP_IOCTL_PROBE

  回報 key 的總數給上層。

- ITP_IOCTL_GET_MAX_LEVEL

  偵測 key event，若偵測到有 key 輸入，則將 event 塞入 queue 中，等待 ``read()`` 取用。

read
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int read(int __fd, void *__buf, size_t __size)

  從 KEYPAD 讀取資料。相關範例可參考 ``<sdk_root>/sdk/share/sdl/video/castor3/SDL_castor3keyboard.c`` 。

**參數**

``int __fd``

  欲執行的 KEYPAD Device。EX: ``ITP_DEVICE_KEYPAD`` 。

``void *__buf``

  讀取後寫回的位置，KEYPAD 的 buffer size 即是 ``ITP Event Size(sizeof (ITPKeypadEvent))`` ， 完整裝置類型列表請見 ``<sdk_root>/sdk/include/ite/itp.h`` 。

``size_t __ size``

  欲讀取之資料長度，即 ``ITP Event Size(sizeof (ITPKeypadEvent))`` 。

**描述**

使用者可透過此函式從 KEYPAD 裝置讀回資料，底下展示如何使用函式。

.. code-block:: c

    ITPKeypadEvent ev;

    ioctl(ITP_DEVICE_KEYPAD, ITP_IOCTL_PROBE, NULL);

    if (read(ITP_DEVICE_KEYPAD, &ev, sizeof(ITPKeypadEvent)) == sizeof(ITPKeypadEvent))
    {
        if (ev.flags & ITP_KEYPAD_DOWN)
        {
            printf("GOT KEYPAD-DOWN\n");
        }
        else if (ev.flags & ITP_KEYPAD_UP)
        {
            printf("GOT KEYPAD-UP\n");
        }
    }

有關 Touch Key driver 的 code 請參考 ``<sdk_root>/sdk/driver/itp/keypad/itp_keypad_it7236.c`` 。此 Touch Key 控制器使用 INT 與 I2C BUS 來偵測並讀取 Touch Key Data，經過判讀之後，將 Key Number 傳送至上層 SDL。目前 IT7236 的 Driver 可直接支援 5 組與 16 組 KEY 的應用，其他不是 5 組與 16 組的 Touch Key 應用，可能需要透過適當修改才能使用。

關於 TOUCH KEY 的 DRIVER 實作，即為實作 ``itpKeypadGetMaxLevel()``、``itpKeypadInit()``、``itpKeypadProbe()`` 這三個 Function。其中 ``itpKeypadGetMaxLevel()`` 是回傳要偵測的 key 的總數。``itpKeypadInit()`` 是實作此 Touch Key 的初始化。 ``itpKeypadProbe()`` 則是實作 keypad 的偵測，並回傳 keypad mapping table 相對應的值。此回傳值必須是 0 ~ (KEYPAD總數 - 1) 之間的整數。keypad mapping table 則是定義在 Kconfig 的 Peripheral 的 Keypad Enable 底下的 Keypad mapping table 選項中(KEYPAD_MAPPING_TABLE)。檔案則放在 ``<sdk_root>/sdk/target/keypad/`` 目錄下。此 Keypad mapping table 檔案將會在 ``<sdk_root>/sdk/share/sdl/video/castor3/SDL_castor3keypad.c`` 這裡被讀取，轉換成 SDL 的 scancode (參考 ``<sdk_root>/sdk/include/SDL/SDL_scancode.h`` )。SDL 的 scancode 最後會被轉成 SDL 的 keycode (參考 ``<sdk_root>/sdk/share/sdl/events/SDL_keyboard.c`` )，提供上層 AP 透過 SDL API 擷取 key event (請參考本文件之範例)。所以 Keypad mapping table 內容的編寫即是 SDL 的 scancode 與 ``itpKeypadProbe()``回傳值的對應表。
 
簡易範例程式
---------------------------

1. 透過SDL讀取Touch Key Event
2. 建置SDL library的方式是在“Kconfig”檔案中加入以下設定

  .. code-block:: shell

    config BUILD_SDL
 	    def_bool y

  SDL（Simple DirectMedia Layer）是一套開放原始碼的跨平台多媒體開發函式庫。SDL提供了數種控制圖像、聲音、輸出入的函式。有關SDL的相關訊息可參考[維基百科] (https://zh.wikipedia.org/wiki/SDL)。

  此範例展示 ITE 的 SDK 運用 SDL 的所提供的函式，獲得底層 KEY 的 EVENT。

.. code-block:: c

    #include "SDL/SDL.h"

    int TouchEvent_test(void)
    {
        SDL_Event ev;

	/* SDL initial */
        if (SDL_Init(SDL_INIT_VIDEO) < 0)
            printf("Couldn't initialize SDL: %s\n", SDL_GetError());

        while (SDL_PollEvent(&ev))
        {
            switch (ev.type)
            {
            case SDL_KEYDOWN:
                switch (ev.key.keysym.sym)
                {
                case SDLK_UP:
                    printf("key direction up\n");
                    break;

                case SDLK_DOWN:
                    printf("key direction down\n");
                    break;

                case SDLK_LEFT:
                    printf("key direction left\n");
                    break;

                case SDLK_RIGHT:
                    printf("key direction right\n");
                    break;
        	       }
            }
            SDL_Delay(1);
        }
    }


.. raw:: latex

    \newpage
