.. _4g:

4G
============

ITE 平台支援 USB 介面的 4G 模塊，有些 4G 模塊需要撥號流程才能上網，有些不需要撥號流程就能直接上網。有些 4G 模塊可使用 UART 介面來執行撥號流程，有些只能透過 USB 介面來撥號。目前 ITE 支援 USB 的 OPTION 驅動來執行撥號，及透過 USB 的 ECM 驅動來執行上網的功能。

ECM 的上層應用和 Ethernet 共用，所以使用 ECM 時不能打開 "Enable Ethernet module" 。

.. image:: /_static/qconf_uncheck_network_enable_ethernet_module.png
 
目前支援的 4G 模塊有：广和通 L718(中興微)、有方 Neoway N720(Qualcomm)、合宙 Air720SL(Marvell)。

L718 使用方式
---------------------------------

FIBOCOM L718 4G 模塊的撥號流程可透過 UART 介面來設定，也可透過 USB OPTION 驅動來設定。若使用 USB OPTION 驅動來撥號，撥號的程序可參考 ``<sdk_root>/project/test_ecm/test_dial_io_mode.c`` 裡面的 ``Dial_io_mode_L718()`` 。設定可參考 ``<sdk_root>/project/test_ecm/Kconfg.L718`` 。

使用前需先確認 4G 模塊有接上天線(ANT_MAIN)，且有插上SIM卡。

FIBOCOM L718 4G 模塊一上電時會先進入 DL 模式，過一段時間才會進入 ECM 模式，所以此模塊有一個額外設定的 delay 時間如下圖，目的用來確定模塊已經進入 ECM 模式後才開始對它進行列舉。

.. image:: /_static/qconf_network_delay_time.png
 
若使用PC透過UART來撥號，指令如下：

- 若L718 4G模塊為第一次使用，需先跑過下面流程

  | # 確認數據端口能通信
  | ATE0
  | OK
  | 
  | # 確認SIM卡能被正常識別
  | AT+CPIN?
  | READY
  | 
  | # 查看當前時間、當前環境、當前天線的條件下的SIM卡所屬營運商網路制式的信號質量參考
  | # 若第一個參數 < 12，需接天線，如果只用一根天線，要接MAIN_ANT，最好選信號好的地方
  | AT+CSQ
  | +CSQ: 22,99
  | OK
  | 
  | # 確認數據服務是否可用
  | AT+CGREG?
  | +CGREG: 1,1
  | OK
  | 
  | # 檢查網路模式，7表示是4G
  | AT+COPS?
  | +COPS: 0,0,"T Star",7
  | OK
  | 
  | # 設置APN
  | AT+CGDCONT=1,"IP","internet"
  | OK
  | 
  | # ECM撥號
  | AT+GTRNDIS=1,1
  | OK
  | 
  | # 確認撥號成功，且取得IP
  | AT+GTRNDIS?
  | +GTRNDIS: 1,1,10.149.198.114,172.24.9.21,10.9.121.102
  | OK

- 若非第一次使用，每次測試前請先確認模塊已撥號成功

  | # 確認撥號是否成功使用
  | AT+GTRNDIS?
  | 
  | L718回應如下即已經撥號成功
  | +GTRNDIS: 1,1,10.105.9.201,172.24.9.21,10.9.121.102
  | OK
  | 
  | L718回應如下即尚未撥號成功
  | +GTRNDIS: 0
  | OK

N720 使用方式
---------------------------------

Neoway N720 4G 模塊的撥號流程只能透過 USB OPTION 驅動來設定。設定可參考 ``<sdk_root>/project/test_ecm/Kconfg.N720`` 。

撥號的程序可參考 ``<sdk_root>/project/test_ecm/test_dial_io_mode.c`` 裡面的 ``Dial_io_mode_N720()`` 。

使用前需先確認有接上天線，且有插上 SIM 卡。

Air720SL 使用方式
---------------------------------

Air720SL 4G 模塊不需執行撥號流程，直接就可以 ECM 上網。設定可參考 ``<sdk_root>/project/test_ecm/Kconfig.Air720SL`` 。

使用前需先確認有接上天線，且有插上 SIM 卡。

若 Air720SL 不在 ECM 模式下，可在 PC 上先下指令 AT + SETUSB = 2，然後重啟模塊，之後就會進入 ECM 模式，這個參數會被保存。


.. raw:: latex

    \newpage
