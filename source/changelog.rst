.. _changelog:

修訂記錄
****************************************************************
.. list-table::
   :widths: 20 80
   :header-rows: 1

   * - 修訂日期
     - 版本
     - 修訂說明

   * - 2021/04/01
     - V1.0
     - 初建版本 V1.0。
   * - 2022/01/06
     - V1.0.1
     - 加入索引。修正打字錯誤。
   * - 2022/01/07
     - V1.1
     - 新增 UVC 章節。
