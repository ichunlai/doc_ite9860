﻿.. _faq:

常見問題
********************************

專案編譯後，因某種原因更換了整個SDK的路徑，就無法再成功編譯專案?
---------------------------------------------------------------------

假設您目前編譯的是 MY_PROJECT 專案，請先移除

    * ``build\openrtos\codec``
    * ``build\openrtos\codecex``
    * ``build\openrtos\MY_PROJECT\.config``

以外的所有資料夾和檔案。再執行 ``MY_PROJECT_ALL.cmd`` (注意編譯路徑不要有中文或空格等文字)即可正確編譯。

如何新增函式庫至SDK?
---------------------------------------------------------------------

#. 在路徑 ``sdk/share/`` 下以 ``{lib_name}`` 為名稱建立資料夾。
#. 將程式碼中所有 ``*.c; *.h`` 檔置入 ``{lib_name}`` 資料夾。
#. 在 ``{lib_name}`` 資料夾中建立 ``CMakeLists.txt`` 檔案，其內容為：

    .. code-block:: shell

	add_library({lib_name} STATIC
	    file1.c
	    file2.c
	    file3.h
	    file4.h
	    …
	)

#. 修改在路徑 ``sdk/`` 底下的 ``build.cmake`` ，加入：

    .. code-block:: shell

	ITE_LINK_LIBRARY_IF_DEFINED_CFG_BUILD_LIB({lib_name})

#. 之後在使用上，只要在 ``project`` 裡的 ``Kconfig`` 加入以下語句，編譯時即可連結函式庫：

    .. code-block:: kconfig 

	config BUILD_{lib_name}
	    def_bool y


.. note:: 
	
	若是要在 ``Kconfig`` 增加函式庫的選項，除了在 ``sdk/Kconfig`` 新增語句，如 ``config {lib_name}_ENABLE …`` (可參考 ``sdk/Kconfig`` 其他編輯範例) 之外，還需在 ``sdk/def.cmake`` 加入：

	.. code-block:: kconfig 

	    if (DEFINED CFG_{lib_name}_ENABLE)
	        set(CFG_BUILD_{lib_name} 1)
	    endif()

	之後在使用上，只需要於 ``Kconfig`` 勾選即可，不需要在 ``project`` 裡的 ``Kconfig`` 加入描述。


.. _multiple_toolchain:

若要同時並存多個 toolchain，該怎麼做？
----------------------------------------

若您需要同時針對 IT985X 系列和 IT986X 系列晶片開發軟體，您可能會碰到需要切換 toolchain 的問題。因為 IT985X 系列必須使用 ``ITEGCC.####.##.##.gcc-4.8.5.7z`` 版本的 toolchain 來開發軟體，而 IT986X 系列則使用更新版本的 toolchain。在這種情況下，若能讓兩個版本的 toolchain 並存，便可免除切換 toolchain 的困擾。

要同時並存多個 toolchain，請遵循以下步驟：

1. 首先，根據您建置專案的平台，修改 toolchin 目錄的名稱

    * 如果您的平台是 Windows
    
        將 ``c:\ITEGCC`` 更名，譬如更名為 ``c:\ITEGCC.2020.01.14.gcc-5.4.0``。

    * 如果您的平台是 Linux

        將 ``/opt/ITEGCC`` 更名，譬如更名為 ``/opt/ITEGCC.2020.01.14.gcc-5.4.0``。

2. 接著，您需要修改 sdk 內以下檔案的內容：

    * ``/build/openrtos/common.cmd``
    * ``/build/linux/common.sh`` 
    * ``/openrtos/toolchain.cmake``
    * ``/sdk/target/debug/fa626/libcodesize.cmd.in`` (非必要修改)
    * ``/sdk/target/debug/sm32/libcodesize.cmd.in`` (非必要修改)

   在這些檔案中搜尋 ``ITEGCC`` 字串並將其修改為 ``ITEGCC.2020.01.14.gcc-5.4.0``。

其餘版本的 toolchain 可以按照上述方式修改並安裝。