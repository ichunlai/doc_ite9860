﻿.. _driver:

週邊裝置介紹與相關 API
************************

本章將針對 IT986x 系列晶片所支援的週邊以及操作函式做細部說明，並給予測試程式範例。

.. toctree::
   :glob:

   driver/gpio.rst
   driver/uart.rst
   driver/lcd.rst
   driver/pwm.rst
   driver/i2c.rst
   driver/spi.rst
   driver/canbus.rst
   driver/saradc.rst
   driver/remote_ir.rst
   driver/key.rst
   driver/touch_key.rst
   driver/norflash.rst
   driver/nandflash.rst
   driver/sd.rst
   driver/wifi.rst
   driver/4g.rst
   driver/wiegand.rst
   driver/alt_cpu.rst
   driver/video.rst
   driver/capture.rst
   driver/audio.rst