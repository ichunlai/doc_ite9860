.. _uvc:

UVC
=============

功能描述
-------------------------------------

IT986x SDK 已預先移植好 libuvc library (https://github.com/libuvc/libuvc) 以支援符合 USB device class 規範之鏡頭的使用，該函式庫可支援由 USB-IF（USB開發者論壇）組織定義的最新的UVC 1.5 版本規範。已移植好的函式庫可支援的鏡頭輸出格式如下：

.. flat-table::
  :header-rows: 1

  * - 支援格式
    - 驅動支援
    - 範例
  * - YUV
    - Y
    - Y
  * - MJPEG
    - Y
    - Y
  * - H264
    - Y
    - Y
  * - RGB
    - Y
    - Y

其中”驅動支援”欄位標示為 `Y` 表示晶片內建硬體支援可將該格式轉碼為RGB565以供LCD輸出顯示。”範例”欄位標示為 `Y` 表示在test_uvc範例內有現成程式碼範例供參考。

相關Kconfig 設定
----------------------------------------------------------------

- :menuselection:`Peripheral --> USB Enable`

  啟用USB 模組。

- :menuselection:`Peripheral --> USBHCC Disable`

  關閉 HCC 模組。

- :menuselection:`Video --> USB Video Class Enable`

  啟用 USB 影像裝置模式。


相關的程式碼檔案
----------------------------

.. flat-table::
  :header-rows: 1

  * - 路徑
    - 描述
  * - ``<sdk_root>/sdk/driver/uvc``
    - 已預先移植好 libuvc library。
  * - ``<sdk_root>/project/test_uvc``
    - UVC 簡易範例測試程式。僅供用戶快速測試是否支援某鏡頭，且並未整合 UI 顯示。針對輸出格式為 H264 的鏡頭不支援即時畫面顯示。
  * - | ``<sdk_root>/sdk/share/flower/video/filter_uvc.c``
    - 支援 YUV or MJPEG or H264 UVC 輸入格式的完整播放器範例程式。可自由擴充支援串接錄影或拍照功能。實際專案建議參考此範例程式再自行修改。


重點函式描述
---------------------------------------
IT986x SDK 內建的 UVC library 是移植自 libuvc library (https://github.com/libuvc/libuvc)。完整 API 描述請參考 https://ken.tossell.net/libuvc/doc/modules.html 。底下僅針對較重要的函式進行描述。


uvc_init
^^^^^^^^^^^^^^^^^^^^^^
.. c:function:: uvc_error_t uvc_init(uvc_context_t **pctx)

   Initializes the UVC context.


uvc_open
^^^^^^^^^^^^^^^^^^^^^^
.. c:function:: uvc_error_t uvc_open(uvc_device_t *dev, uvc_device_handle_t **devh)

   Open a UVC device.


uvc_start_streaming
^^^^^^^^^^^^^^^^^^^^^^
.. c:function:: uvc_error_t uvc_start_streaming(uvc_device_handle_t *devh, uvc_stream_ctrl_t *ctrl, uvc_frame_callback_t *cb, void *user_ptr, uint8_t flags)

   Begin streaming video from the camera into the callback function.


uvc_get_stream_ctrl_format_size
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. c:function:: uvc_error_t uvc_get_stream_ctrl_format_size(uvc_device_handle_t *devh, uvc_stream_ctrl_t *ctrl, enum uvc_frame_format cf, int width, int height, int fps)

   Get a negotiated streaming control block for some common parameters.


uvc_set_ae_mode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. c:function:: uvc_error_t uvc_set_ae_mode(uvc_device_handle_t *devh, uint8_t mode)

   Sets camera's auto-exposure mode.


uvc_stop_streaming
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. c:function:: void uvc_stop_streaming(uvc_device_handle_t *devh)

   Closes all streams, ends threads and cancels pollers.


uvc_close
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. c:function:: void uvc_close(uvc_device_handle_t *devh)

   Closes stream, frees handle and all streaming resources.


uvc_exit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. c:function:: void uvc_exit(uvc_context_t *ctx)

   Closes the UVC context, shutting down any active cameras.


範例程式說明
---------------------------------------

在 IT986x SDK 中包含了兩個與 UVC 有關的範例程式: ``test_uvc`` 和 ``test_uvc_player`` 。其中 ``test_uvc`` 的程式碼較小，主要用於在剛拿到一個新鏡頭，想要快速確認是否能支援該鏡頭的輸出顯示時使用。欲快速理解 libuvc library 的使用方法也可從 trace 此範例開始。 ``test_uvc_player`` 則包含完整使用範例，該範例支援 YUV、MJPEG、H264 的輸入格式，並能將輸入內容與 UI 結合一起輸出到 LCD 顯示。在實際專案中，建議參考 ``test_uvc_player`` 的做法。底下將針對兩個範例做詳細描述。

test_uvc 範例程式說明
---------------------------------------

``test_uvc`` 範例程式位於 ``<sdk_root>/project/test_uvc`` 。該範例程式展示將支援 UVC 的鏡頭的輸入畫面，經過適當的格式轉換，並放到 LCD frame buffer 以輸出顯示。

該程式主要針對快速確認是否能支援某鏡頭的輸出顯示所撰寫，因此在電路板連接不同的 UVC Camera 時，需經過適當修改才能正常運行。底下將描述如何運行該範例程式並經過適當修改使其能正確輸出畫面。

硬體連接
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

請按照下圖連接好 LCD Panel、UVC Camera、USB to SPI 轉換板以及電源線。

.. image:: /_static/e_circuit_uvc.png

KConfig 設定
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

請執行 ``<sdk_root>/build/openrtos/test_uvc.cmd`` ，按照以下步驟設定:

- 勾選 :menuselection:`Screen --> LCD Enable` 。

  啟用 LCD 裝置。其餘 LCD 相關設定請參考 :ref:`lcd` 章節。建議在運行此範例之前先運行過 :ref:`lcd` 章節的範例程式，以確定您的 LCD 設定正確。

- 勾選 :menuselection:`Graphics --> JPEG H/W enable or not` 。

  若所連接的 UVC Camera 的輸入格式為 MJPEG (Motion JPEG)，務必勾選此選項以啟用硬體加速。

- 勾選 :menuselection:`Peripheral --> UART Enable` 。
- 勾選 :menuselection:`Peripheral --> UART Enable --> UART0 Enable` 。
- 透過 :menuselection:`Peripheral --> UART0 Enable --> UART0 mode` 選擇 UART0 的操作模式，這裡我們選擇 UART0 Interrupt Enable。
- 將 :menuselection:`Peripheral --> UART Enable --> UART0 Baud Rate` 設定為 115200。
- 將 :menuselection:`GPIO --> UART0 TX Pin` 設為 4。
- 透過 :menuselection:`Debug --> Debug Message Device` 選擇使用 UART0 做為 Debug 輸出。

  這裡主要是將 UART0 設定做為 Debug 輸出使用。

  .. note::

    為何將 UART0 TX pin 設定為 GPIO4 是因為在EVB電路圖中，IT986x 的 GPIO4 與 USB to SPI 轉換板的 RX (在電路圖中標示為 DBG_TX) 相連。

    .. image:: /_static/e_circuit_uart.png

- 取消勾選 :menuselection:`Peripheral --> USBHCC Enable` 。
- 勾選 :menuselection:`Peripheral --> USB0 Enable` 。
- 取消勾選 :menuselection:`Peripheral --> USB0 Enable --> USB Device Mode` 。
- 勾選 :menuselection:`Video --> USB Video Class Enable` 。

運行範例
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  .. |run_icon| image:: /_static/qconf_run_icon.png
    :alt: run icon
    :width: 14
    :height: 14

  .. |build_icon| image:: /_static/qconf_build_icon.png
    :alt: build icon
    :width: 14
    :height: 14

1. 點擊 |build_icon| 按鈕建置專案。確保：

  - USB to SPI 轉換板已正確安裝。

    - 若您尚未將 USB to SPI 轉換板與 PC 連接，請參考 :ref:`usb2spi_connect2pc` 。
    - 若您尚未將 USB to SPI 轉換板與 EVB 連接，請參考 :ref:`usb2spi_connect2board` 。
    - 若您尚未安裝 USB to SPI 轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

  - EVB 已經切換為 Co-operative Mode。

    .. note::

       若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

2. 重置 EVB 的電源。
3. 等程式建置完成，點擊 |run_icon| 按鈕即可透過 USB to SPI 轉換板啟動系統。
4. 執行 Tera Term，點擊 Tera Term 主選單的 :menuselection:`Setup --> Serial Port` 。Port 的部分依照 USB to SPI 轉換板連接至PC時所顯示的 Serial Port 為主。Speed 的部分請填入 115200，並再次確認 Device Manufacturer 是否顯示為 "FTDI"。

   .. image:: /_static/teraterm_setup_serial_port_dialog.png

5. 若系統成功偵測到該 USB Camera 裝置，將在 Tera Term 上依序顯示 ``Device found`` --> ``Device opened`` 。訊息如下：

   .. code-block:: shell

      Found uvc_driver usb driver!
       find driver: (config #1, interface 1)

       Found uvc_driver usb driver!
      Chip: 0x960, gChipRevision: 0x0
      Init memory status-----------------------
      max system bytes =     496544
      system bytes     =     496544
      in use bytes     =     495256
      -----------------------------------------
      UVC initialized
      Device found
      unsupported descriptor subtype: 3
      unsupported descriptor subtype: 13
      unsupported descriptor subtype: 3
      unsupported descriptor subtype: 13
      Device opened
      DEVICE CONFIGURATION (0c45:64ab/[none]) ---
      Status: idle
      VideoControl:
              bcdUVC: 0x0100

6. 若系統成功偵測到該 Camera 支援 UVC 協定，將顯示以下訊息。其中第6行和第8行表示該 Camera 支援 motion jpeg 格式的視訊輸出格式，第45和第47行表示該 Camera 支援 RAW YUY2 的視訊輸出格式。

   .. code-block:: shell
      :linenos:
      :emphasize-lines: 6,8,15,19,20,23,27,28,31,35,36,39,43-45,47,54,58,61,65,66,69,73,74,77,81-83

      VideoControl:
              bcdUVC: 0x0100
      VideoStreaming(1):
              bEndpointAddress: 129
              Formats:
              MJPEGFormat(1)
                        bits per pixel: 0
                        GUID: 4d4a5047000000000000000000000000 (MJPG)
                        default frame: 1
                        aspect ratio: 0x0
                        interlace flags: 00
                        copy protect: 00
                              FrameDescriptor(1)
                                capabilities: 00
                                size: 1600x1200
                                bit rate: 153623560-614494240
                                max frame size: 3840589
                                default interval: 1/20
                                interval[0]: 1/20
                                interval[1]: 1/5
                              FrameDescriptor(2)
                                capabilities: 00
                                size: 1280x1024
                                bit rate: 104881160-419524640
                                max frame size: 2622029
                                default interval: 1/20
                                interval[0]: 1/20
                                interval[1]: 1/5
                              FrameDescriptor(3)
                                capabilities: 00
                                size: 1280x960
                                bit rate: 98327560-393310240
                                max frame size: 2458189
                                default interval: 1/20
                                interval[0]: 1/20
                                interval[1]: 1/5
                              FrameDescriptor(4)
                                capabilities: 00
                                size: 640x480
                                bit rate: 24599560-147597360
                                max frame size: 614989
                                default interval: 1/30
                                interval[0]: 1/30
                                interval[1]: 1/5
              UncompressedFormat(2)
                        bits per pixel: 16
                        GUID: 5955593200001000800000aa00389b71 (YUY2)
                        default frame: 1
                        aspect ratio: 0x0
                        interlace flags: 00
                        copy protect: 00
                              FrameDescriptor(1)
                                capabilities: 00
                                size: 1600x1200
                                bit rate: 122880000-122880000
                                max frame size: 3840000
                                default interval: 1/4
                                interval[0]: 1/4
                              FrameDescriptor(2)
                                capabilities: 00
                                size: 1280x1024
                                bit rate: 104857600-188743680
                                max frame size: 2621440
                                default interval: 1/9
                                interval[0]: 1/9
                                interval[1]: 1/5
                              FrameDescriptor(3)
                                capabilities: 00
                                size: 1280x960
                                bit rate: 98304000-176947200
                                max frame size: 2457600
                                default interval: 1/9
                                interval[0]: 1/9
                                interval[1]: 1/5
                              FrameDescriptor(4)
                                capabilities: 00
                                size: 640x480
                                bit rate: 24576000-147456000
                                max frame size: 614400
                                default interval: 1/30
                                interval[0]: 1/30
                                interval[1]: 1/20
                                interval[2]: 1/5
      END DEVICE CONFIGURATION

   其中的 size 和 interval 欄位表示該 Camera 支援的輸出解析度以及 frame rate。從上面的資訊我們可以得知該 Camera 支援的輸出格式如下表：


   .. flat-table::
     :header-rows: 1

     * - Color Format
       - Width
       - Height
       - Frame Rate
     * - MJPEG
       - 1600
       - 1200
       - 20
     * -
       -
       -
       - 5
     * -
       - 1280
       - 1024
       - 20
     * -
       -
       -
       - 5
     * -
       - 1280
       - 960
       - 20
     * -
       -
       -
       - 5
     * -
       - 640
       - 480
       - 30
     * -
       -
       -
       - 5
     * - YUY2
       - 1600
       - 1200
       - 4
     * -
       - 1280
       - 1024
       - 9
     * -
       -
       -
       - 5
     * -
       - 1280
       - 960
       - 9
     * -
       -
       -
       - 5
     * -
       - 640
       - 480
       - 30
     * -
       -
       -
       - 20
     * -
       -
       -
       - 5

7. 根據上述資訊使用 ``uvc_get_stream_ctrl_format_size()`` 函式設定欲採用的 color format、width、height 和 frame rate。譬如底下程式碼 (該程式碼擷取自 ``<sdk_root>/project/test_uvc/example.c`` ) highlight 的部分展示如何要求 camera 的輸出大小為 640x480、frame rate 為 30 fps、color format 為 Motion JPEG 的視訊串流 。

   .. code-block:: c
      :linenos:
      :emphasize-lines: 43-46

      int uvc_main()
      {
          uvc_context_t       *ctx;
          uvc_device_t        *dev;
          uvc_device_handle_t *devh;
          uvc_stream_ctrl_t   ctrl;
          uvc_error_t         res;
          pthread_t           pJpegDecodeThread, pDisplayThread;

          ...

          res = uvc_init(&ctx);

          ...

          /* Locates the first attached UVC device, stores in dev */
          res = uvc_find_device(ctx, &dev, 0, 0, NULL); /* filter devices: vendor_id, product_id, "serial_num"        */

          if (res < 0)
          {
              uvc_perror(res, "uvc_find_device"); /* no devices found */
          }
          else
          {
              puts("Device found");

              /* Try to open the device: requires exclusive access */
              res = uvc_open(dev, &devh);
              if (res < 0)
              {
                  uvc_perror(res, "uvc_open"); /* unable to open device */
              }
              else
              {
                  puts("Device opened");
                  /* Print out a message containing all the information that libuvc
                   * knows about the device */
                  uvc_print_diag(devh, stderr);

                  ...

                  /* Try to negotiate a 640x480 30 fps YUYV stream profile */
                  res = uvc_get_stream_ctrl_format_size(
                      devh, &ctrl,            /* result stored in ctrl */
                      UVC_FRAME_FORMAT_MJPEG, /* Motion JPEG. try _COMPRESSED */
                      640, 480, 30);          /* width, height, fps */

                  //res = uvc_get_stream_ctrl_format_size(
                  //    devh, &ctrl,           /* result stored in ctrl */
                  //    UVC_FRAME_FORMAT_YUYV, /* YUV 422, aka YUV 4:2:2. try _COMPRESSED */
                  //    640, 480, 25);         /* width, height, fps */

                  //res = uvc_get_stream_ctrl_format_size(
                  //    devh, &ctrl,            /* result stored in ctrl */
                  //    UVC_FRAME_FORMAT_H264,  /* YUV 422, aka YUV 4:2:2. try _COMPRESSED */
                  //    1920, 1080, 30);        /* width, height, fps */

8. 若步驟 7 的設定是正確的，則在呼叫 uvc_start_streaming() 時所傳入的 callback function 將會在系統每收到一張來自 Camera 的 frame 時被呼叫。譬如底下程式碼所採用的 callback function 為 ``_uvcFrameCb`` 。您可以在 ``_uvcFrameCb`` 內利用 printf 輸出任意訊息來判斷步驟 7 的設定是否正確。

   .. code-block:: c

      res = uvc_start_streaming(devh, &ctrl, _uvcFrameCb, NULL, 0);

9. 根據上面步驟所設定的 color format，修改 ``<sdk_root>/project/test_uvc/example.c`` 以選擇適當的解碼和顯示流程。

   .. note::

      ``test_uvc`` 範例程式未包含 H264 的解碼流程。若 Camera 的輸出視訊串流格式為 H264 ，請參考 ``test_uvc_player`` 範例程式。

   - 選擇初始化佇列

     .. flat-table::
       :header-rows: 1

       * - Color Format
         - Code
       * - UVC_FRAME_FORMAT_YUYV
         - ``packetQueueInit(&gtYuyvQueue, _yuyvPktRelease, 4, sizeof(YuyvPkt));``
       * - UVC_FRAME_FORMAT_MJPEG
         - | ``packetQueueInit(&gtJpegInputQueue, _jpegInputPktRelease, 4, sizeof(JpegInputPkt));``
           | ``packetQueueInit(&gtJpegOutputQueue, _jpegOutputPktRelease, 4, sizeof(JpegOutputPkt));``


   - 選擇初始化執行緒

     .. flat-table::
       :header-rows: 1

       * - Color Format
         - Code
       * - UVC_FRAME_FORMAT_YUYV
         -
       * - UVC_FRAME_FORMAT_MJPEG
         - | ``pthread_create(&pJpegDecodeThread, NULL, _jpegThread, 0);``
           | ``pthread_create(&pDisplayThread, NULL, _displayThread, 0);``

   - 選擇結束函式

     .. flat-table::
       :header-rows: 1

       * - Color Format
         - Code
       * - UVC_FRAME_FORMAT_YUYV
         - ``packetQueueEnd(&gtYuyvQueue);``
       * - UVC_FRAME_FORMAT_MJPEG
         - | ``pthread_join(pJpegDecodeThread, NULL);``
           | ``pthread_join(pDisplayThread, NULL);``
           | ``packetQueueEnd(&gtJpegInputQueue);``
           | ``packetQueueEnd(&gtJpegOutputQueue);``

10. 點擊 |build_icon| 按鈕重新建置專案。等程式建置完成，點擊 |run_icon| 按鈕透過 USB to SPI 轉換板啟動系統。若一切設定正確，應該可以從 LCD 看到來自 Camera 的即時輸出畫面。該畫面將在播放 2 分鐘之後自動停止。


test_uvc_player 範例程式說明
---------------------------------------

``test_uvc_player`` 範例程式位於 ``<sdk_root>/project/test_uvc_player`` 。該範例程式展示如何使用 ITE 開發的 flower player 架構來播放來自 UVC Camera 的輸出視訊，同時展示了如何整合 UI 呈現。有關 flower player 架構的詳細說明，請看 ``<sdk_root>/project/test_flower`` 。本節僅針對 uvc player 進行說明。

``test_uvc_player`` 的關鍵程式碼位於 ``<sdk_root>/project/test_uvc_player/layer_uvc_player.c`` 。此範例支援 YUV、MJPEG、H264 三種輸入格式。Flower player 是由多個 filter 組成。不同的播放格式將包含不同的 filter，而 filter 和 filter 之間透過 pin 相連。底下針對三種輸入格式描述其內部包含的 filter 以及 filter 之間的連接狀態。

YUV
^^^^^

``test_uvc_player`` 範例程式針對 YUV 輸入格式提供顯示串流範例，架構圖如下：

.. graphviz::

   digraph yuv {
     rankdir=LR
     node [shape=rect]
     "UVC filter" -> "Display filter" [label= "YUYV Stream"]
   }
　
相關的 API 如下：

flow_start_uvc_yuv
"""""""""""""""""""""""""""
.. c:function:: uint8_t flow_start_uvc_yuv(IteFlower *f, int width, int height, int fps)

   Build YUV format filter chain.


flow_stop_uvc_yuv
"""""""""""""""""""""""""""
.. c:function:: uint8_t flow_stop_uvc_yuv(IteFlower *f)

   Destroy YUV format filter chain.


MJPEG
^^^^^

``test_uvc_player`` 範例程式針對 MJPEG 輸入格式提供顯示即錄影串流範例，架構圖如下：

.. graphviz::

   digraph mjpeg {
     rankdir=LR
     node [shape=rect]
     "UVC filter" -> "MJPEG DEC filter" [label= "MJPEG Stream"]
     "UVC filter" -> "AVI filter" [label= "MJPEG Stream"]
     "MJPEG DEC filter" -> "Display filter"
   }

　
相關的 API 如下：

flow_start_uvc_mjpeg
"""""""""""""""""""""""""""
.. c:function:: uint8_t flow_start_uvc_mjpeg(IteFlower *f, int width, int height, int fps);

   Build Motion JPEG format filter chain.


flow_stop_uvc_mjpeg
"""""""""""""""""""""""""""
.. c:function:: uint8_t flow_stop_uvc_mjpeg(IteFlower *f)

   Destroy Motion JPEG format filter chain.

flow_start_uvc_mjpeg_record
"""""""""""""""""""""""""""
.. c:function:: void flow_start_uvc_mjpeg_record(IteFlower *f, char* file_path, int width, int height, int fps)

   Save all the incoming mjpeg streams into a file.

flow_stop_uvc_mjpeg_record
"""""""""""""""""""""""""""
.. c:function:: void flow_stop_uvc_mjpeg_record(IteFlower *f, char* file_path, int width, int height, int fps)

   Stop recording and close file.

H264
^^^^^

``test_uvc_player`` 範例程式針對 H264 輸入格式提供顯示即錄影串流範例，架構圖如下：

.. graphviz::

   digraph h264 {
     rankdir=LR
     node [shape=rect]
     "UVC filter" -> "H264 DEC filter" [label= "H264 Stream"]
     "UVC filter" -> "AVI filter" [label= "H264 Stream"]
     "H264 DEC filter" -> "Display filter"
   }
　
相關的 API 如下：

flow_start_uvc_h264
"""""""""""""""""""""""""""
.. c:function:: uint8_t flow_start_uvc_h264(IteFlower *f, int width, int height, int fps);

   Build h264 format filter chain.

flow_stop_uvc_h264
"""""""""""""""""""""""""""
.. c:function:: uint8_t flow_stop_uvc_h264(IteFlower *f)

   Destroy h264 format filter chain.

flow_start_uvc_h264_record
"""""""""""""""""""""""""""
.. c:function:: void flow_start_uvc_h264_record(IteFlower *f, char* file_path, int width, int height, int fps)

   Save all the incoming h264 streams into a file.

flow_stop_uvc_h264_record
"""""""""""""""""""""""""""
.. c:function:: void flow_stop_uvc_h264_record(IteFlower *f, char* file_path, int width, int height, int fps)

   Stop recording and close file.



硬體連接
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

請按照下圖連接好 LCD Panel、UVC Camera、USB to SPI 轉換板以及電源線。

.. image:: /_static/e_circuit_uvc.png

如果需要測試錄影功能還需要插入 SD Card 子模組 (預設使用 COM3)

.. image:: /_static/e_circuit_uvc_sd.png

KConfig 設定
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

請執行 ``<sdk_root>/build/openrtos/test_uvc_player_all.cmd`` ，按照以下步驟設定:

- 勾選 :menuselection:`Screen --> LCD Enable` 。

  啟用 LCD 裝置。其餘 LCD 相關設定請參考 :ref:`lcd` 章節。建議在運行此範例之前先運行過 :ref:`lcd` 章節的範例程式，以確定您的 LCD 設定正確。

- 勾選 :menuselection:`Graphics --> JPEG H/W enable or not` 。

  若所連接的 UVC Camera 的輸入格式為 MJPEG (Motion JPEG)，務必勾選此選項以啟用硬體加速。

- 勾選 :menuselection:`Peripheral --> UART Enable` 。
- 勾選 :menuselection:`Peripheral --> UART Enable --> UART0 Enable` 。
- 透過 :menuselection:`Peripheral --> UART0 Enable --> UART0 mode` 選擇 UART0 的操作模式，這裡我們選擇 UART0 Interrupt Enable。
- 將 :menuselection:`Peripheral --> UART Enable --> UART0 Baud Rate` 設定為 115200。
- 將 :menuselection:`GPIO --> UART0 TX Pin` 設為 4。
- 透過 :menuselection:`Debug --> Debug Message Device` 選擇使用 UART0 做為 Debug 輸出。

  這裡主要是將 UART0 設定做為 Debug 輸出使用。

  .. note::

    為何將 UART0 TX pin 設定為 GPIO4 是因為在EVB電路圖中，IT986x 的 GPIO4 與 USB to SPI 轉換板的 RX (在電路圖中標示為 DBG_TX) 相連。

    .. image:: /_static/e_circuit_uart.png

- 取消勾選 :menuselection:`Peripheral --> USBHCC Enable` 。
- 勾選 :menuselection:`Peripheral --> USB0 Enable` 。
- 取消勾選 :menuselection:`Peripheral --> USB0 Enable --> USB Device Mode` 。
- 勾選 :menuselection:`Video --> USB Video Class Enable` 。
- 勾選 :menuselection:`Video --> Video Enable` 。

其餘選項與 UI 有關，按照專案預設值即可。要注意的是必須在 ``<sdk_root>/project/test_uvc_player/Kconfig`` 加入以下語句。注意該語句必須放在 ``source "$CMAKE_SOURCE_DIR/sdk/Kconfig"`` 語句之前。

.. code-block:: kconfig

   config BUILD_FLOWER
       def_bool y


運行範例
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

在運行 ``test_uvc_player`` 範例程式之前，請先修改 ``<sdk_root>/project/test_uvc_player/layer_uvc_player.c`` 檔案。將以下程式碼 highlight 的部分根據所連接的 UVC Camera 支援的格式做適當的設定。

.. code-block:: c
   :emphasize-lines: 15-18

   #include <assert.h>
   #include <stdio.h>
   #include <stdlib.h>
   #include <string.h>
   #include "scene.h"
   #include "ctrlboard.h"
   #ifdef __OPENRTOS__
   #include "flower.h"
   #endif
   #include "ite/itv.h"

   ...

   //User define uvc camera
   #define UVC_WIDTH     640
   #define UVC_HEIGHT    480
   #define UVC_FPS       30
   #define UVC_FORMAT    2  //  0 : H264 1:MJPEG 2:YUV

接著請按照以下步驟執行：

1. 點擊 |build_icon| 按鈕建置專案。確保：

  - USB to SPI 轉換板已正確安裝。

    - 若您尚未將 USB to SPI 轉換板與 PC 連接，請參考 :ref:`usb2spi_connect2pc` 。
    - 若您尚未將 USB to SPI 轉換板與 EVB 連接，請參考 :ref:`usb2spi_connect2board` 。
    - 若您尚未安裝 USB to SPI 轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

  - EVB 已經切換為 Co-operative Mode。

    .. note::

       若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

2. 請按照 ref:`write_image_to_norflash` 一節的說明將 ``<sdk_root>/build/openrtos/test_uvc_player/project/test_uvc_player/ITE_NOR.ROM`` 燒錄至 NOR Flash 。

3. 重置 EVB 的電源。
4. 系統啟動後會在 LCD 螢幕上看到以下畫面：

   .. image:: /_static/ui_uvc_main.png

5. 點擊螢幕上的 USB Camera 圖示後進入 uvc_player layer 。

   - 點擊 "Play/Pause" 開始播放/結束播放。
   - 點擊 "Save/Stop" 開始錄影/結束錄影。
   - 點擊 "Back" 回上一頁。

   .. image:: /_static/ui_uvc_player.png
