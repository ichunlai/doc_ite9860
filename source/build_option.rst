.. gui_designer:

建置選項設定
***************************

當您計畫開始編譯專案並執行 ``<sdk_root>/build/openrtos/<project_name>.cmd`` 或 ``<sdk_root>/build/openrtos/<project_name>_all.cmd`` 後，會開啟一個 qconf 設定與建置工具。您可以在這個視窗內更改專案的建置選項以及編譯專案。

基本操作
=================

qconf 設定與建置工具常用的有以下區塊。

.. image:: /_static/qconf_intro.png

1.	儲存設定 (儲存編譯選項設定)
2.	建置專案 (建置並編譯軟體)
3.	設定分類 (每個設定的分類)
4.	設定選單 (每個分類項的選單)
5.	設定說明 (每個分類項的說明)
6.	建置專案輸出畫面 (編譯過程的輸出訊息)。

在設定分類的第一項 "Develop Environment" 之下，有以下兩個選單：

1.	Develop mode (開發模式：選擇此項目，會顯示出所有分類項與選單)
2.	Release mode (簡易模式：選擇此項目，只會顯示出常用選單)

如下圖：選擇開發模式，可顯示出較多分類項。

.. image:: /_static/qconf_devel_mode_setting.png

選擇簡易模式，則只顯示出常用的部份。

.. image:: /_static/qconf_release_mode_setting.png

有些選單設定會與其他選項有關，當某一選項啟用後，才會顯示出對應額外選單。

譬如下圖：當 :menuselection:`System --> Watch Dog enable` 選項被勾選時，在 "Watch Dog enable" 選項下方會出現 Watch Dog 相關細部設定。
  
.. image:: /_static/qconf_watchdog_enable.png

當 :menuselection:`System --> Watch Dog enable` 選項未被勾選時，那些 Watch Dog 相關細部設定將被隱藏起來。

.. image:: /_static/qconf_watchdog_hidden.png

儲存完整設定說明
=================

前一節簡要地說明了設定與建置工具如何操作，如欲儲存完整的設定說明，可點擊 :menuselection:`File --> Save XML As…` ，這將輸出 ``kconfig.xml`` 檔案。

.. image:: /_static/qconf_save_xml.png

可利用 Excel 開啟 ``kconfig.xml`` 檔案，就可以看到所有設定選項的樹狀結構，並有每個選項說明。

.. image:: /_static/qconf_kconfig_xml.png

搜尋設定
=================

點擊工具列上的 |search icon| ，或者使用 :kbd:`Ctrl-F` 組合鍵，會開啟一個視窗。在這個視窗內輸入要搜尋的關鍵字，譬如搜尋 ``IIC``。可以搜尋設定選項內與此關鍵字相關的設定。

.. |search icon| image:: /_static/qconf_search_icon.png
  :alt: search icon
  :width: 14
  :height: 14

.. image:: /_static/qconf_search_dialog.png
    
搜尋輸出視窗
=================

當您點擊 |build_icon| 按鈕開始建置專案，會在輸出窗格中顯示建置過程的訊息。在此視窗中按下滑鼠右鍵，開啟右鍵選單，點擊 "Search String"。

.. |build_icon| image:: /_static/qconf_build_icon.png
  :alt: build icon
  :width: 14
  :height: 14

.. image:: /_static/qconf_search_string.png

在 "Search string" 欄位輸入想搜尋的字串即可。

.. image:: /_static/qconf_search_string_dialog.png


可使用組合鍵 :kbd:`Ctrl-N` 搜尋下一個符合的字串。


選項與 CMake 變數對應關係
==========================

任何選項的設置都會產生對應的 CMake 變數定義。以 RTC 為例，可從設定說明窗格中看到 :menuselection:`Peripheral --> RTC Enable` 選項與 RTC_ENABLE 關鍵字有關聯。當點擊 |save_icon| 按鈕儲存設定時，qconf會在所產生的 ``<sdk_root>/build/openrtos/<project_name>/config.cmake`` 檔案內寫入 ``set(CFG_RTC_ENABLE y)`` 語句。注意原本的 RTC_ENABLE 關鍵字被加上了前綴字串 ``CFG_`` 。因此只要在適當的 CMake script 檔案內包含該 ``config.cmake`` 檔案便可以取得該設定值。目前 ITE SDK 統一在 ``<sdk_root>/CMakeLists.txt`` 檔案內利用 ``include(build/$ENV{CFG_BUILDPLATFORM}/${CMAKE_PROJECT_NAME}/config.cmake)`` 語句將該 ``config.cmake`` 檔案包含進來。

.. |save_icon| image:: /_static/qconf_save_icon.png
  :alt: save icon
  :width: 14
  :height: 14

.. image:: /_static/qconf_rtc_setting.png   

.. note::

   在 ``<sdk_root>/build/openrtos/build.cmd`` 內可以看到以下命令：

   .. code-block:: shell

      qconf --fontsize 11 --prefix "CFG_" --cmakefile %CFG_PROJECT%/config.cmake --cfgfile %CFG_PROJECT%/.config %PRESETTINGS% "%CMAKE_SOURCE_DIR%/project/%CFG_PROJECT%/Kconfig"

   其中 ``--prefix "CFG_"`` 表示所有輸出的設定項目的關鍵字前面都要加上 ``CFG_`` 前綴字串。 ``--cmakefile %CFG_PROJECT%/config.cmake`` 表示 qconf 在儲存設定時將輸出以 CMake script language 編寫的檔案至 ``%CFG_PROJECT%/config.cmake`` 。 ``--cfgfile %CFG_PROJECT%/.config`` 表示將從 ``%CFG_PROJECT%/.config`` 讀取先前的設定值以及在儲存設定時將設定值存入 ``%CFG_PROJECT%/.config`` 檔案。 ``"%CMAKE_SOURCE_DIR%/project/%CFG_PROJECT%/Kconfig"`` 表示 qconf 所顯示的所有選項是來自 ``%CMAKE_SOURCE_DIR%/project/%CFG_PROJECT%/Kconfig`` 檔案的內容。

選項詳細說明
==========================

請參考各個驅動程式測試專案的詳細說明。

