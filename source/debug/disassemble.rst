.. disassemble:

產生組合語言程式碼
======================================

請開啟 qconf 設定與建置工具，勾選 :menuselection:`Debug --> Output debug files` 。點擊 |build_icon| 按鈕重新建置專案。

.. |build_icon| image:: /_static/qconf_build_icon.png
  :alt: build icon
  :width: 14
  :height: 14

.. image:: /_static/qconf_check_output_debug_files.png

建置完成後會產生一個反組譯程式碼位於 ``<sdk_root>/build/openrtos/<project_name>/project/<project_name>/<project_name>.dbg`` 。  

.. image:: /_static/file_struct_disassemble_result.png

