.. _memory_leak:

Memory leak
===================================

請先參考 :ref:`monitor_memory_usage` 開啟 Runtime Heap statistic 功能，看看是否在特定操作下，出現記憶體一直增長的問題。先確認是否有 memory leak 的問題。接著按照以下步驟操作。

- 透過 qconf 設定與建置工具，取消勾選 :menuselection:`System --> Internal Settings --> CPU write-back cache enable` 以關閉 CPU cache write back 功能。

   .. image:: /_static/qconf_uncheck_writeback.png

- 勾選 :menuselection:`Debug --> Enable Malloc Debug Library`，利用Rmalloc的輸出顯示觀看配置記憶體的來源，以幫忙分析 leak 的可能來源，再透過 code trace 去進一步定位錯誤邏輯的部位。

   .. image:: /_static/qconf_check_enable_malloc_debug_library.png
