.. _run_time_debug:

Run Time Debug Capability
=======================================

ITE SDK 提供數種運行時期除錯協助功能以供開發者除錯程式用。

Runtime Statistic 功能
-----------------------------------

此功能主要是讓系統定期透過 debug message 裝置輸出目前系統狀態。可顯示的狀態有：

- Heap 的使用狀況
- 目前 DRAM 頻寬的利用狀態
- GPIO 的 mapping 設定
- 與 FAT 相關的統計資料

可透過勾選 qconf 設定與建置工具的 :menuselection:`Debug --> Enable Runtime Statistic` 選項底下相對應的選項來開啟上述功能。後面我們將描述各個功能的詳細設定步驟和運行效果。請根據輸出的相關資訊查看系統是否有異常。請注意由於開啟太多 debug 功能會嚴重影響效能，建議通常只開啟 "Heap statistic"，其他的部分再有異常可以拿來輔助 debug，當問題解決後建議關閉。


Debug Message
------------------------------

要輸出除錯訊息，必須先設定 debug message 輸出裝置。可透過 qconf 設定與建置工具選擇 debug message 輸出裝置。通常最為建議使用的是 UART interface，其次是 Print buffer。若採用 Print buffer，需使用 project 的 binary 輸出目錄下的 ``print.cmd`` 透過 USB to SPI 轉換板去獲取 output debug message。

要設定 debug message 輸出裝置，請執行 ``<sdk_root>/build/openrtos/my_project_all.cmd`` 以開啟 qconf 設定與建置工具。並執行以下步驟：

- 將 :menuselection:`Develop Environment --> Develop Environment` 切換為 "Develop Mode"。

   .. image:: /_static/qconf_switch_to_develop_mode.png

- 點擊 :menuselection:`Debug --> Debug Message Device` 。即可選擇要輸出 debug message 的裝置。

   .. image:: /_static/qconf_debug_debug_meg_device.png

若您選擇採用 Print Buffer 輸出 debug message，要看到所輸出的 debug message，您必須執行 ``<sdk_root>/build/openrtos/<project_name>/project/<project_name>/print.cmd`` (請注意這個程式與 USB TO SPI 工具程式互斥，不能同時開啟)。

.. image:: /_static/file_struct_print_cmd.png

Task 列表
-----------------------

此功能可讓系統定期透過 debug message device 輸出目前所有 Task 的狀態。要開啟此功能，請執行 ``<sdk_root>/build/openrtos/my_project_all.cmd`` 以開啟 qconf 設定與建置工具。並執行以下步驟：

- 勾選 :menuselection:`OpenRTOS --> Use Trace Facility` 。

   .. image:: /_static/qconf_check_use_trace_facility.png

- 接著勾選 :menuselection:`Debug --> Enable Runtime Statistic` 和 :menuselection:`Debug --> Enable Runtime Task List Statistic` 。

   .. image:: /_static/qconf_check_enable_runtime_task_list_statistic.png

輸出範例如下：

.. code-block:: shell

    TASKLIST:
    Tmr Svc         X       5       9795    3
    _raProbeHandler R       1       19805   19
    MainTask        R       1       9488    1
    LinphoneTask    R       1       11522   10
    itv_hw_overlay_ R       1       19905   16
    DriveProbeTask  R       1       19905   6
    IDLE            R       0       19968   2

其中第一欄是 Task Name，第二欄是 Task 狀態：

.. code-block::

    X: RUNNING
    B: BLOCKED
    R: READY
    D: DELETED
    S: SUSPENDED

第三欄是 Proirity (數值越大 priority 越高)。第四欄是目前 Stack 的最高使用量，單位是 4 個 bytes。第五欄是 Task 的流水號。詳細說明可以參考 https://www.freertos.org/a00021.html#vTaskList。

.. _monitor_memory_usage:

觀測 Memory 使用狀況
----------------------------------

此功能可讓系統定期透過 debug message device 輸出目前 Heap 的使用狀態。步驟如下：

- 勾選 :menuselection:`Debug --> Enable Runtime Statistic` 和 :menuselection:`Debug --> Enable Runtime Statistic --> Enable Runtime Heap Statistic` 。

   .. image:: /_static/qconf_check_enable_runtime_heap_statistic.png

   輸出範例如下：

   .. code-block:: shell

      HEAP newlib : usage = 23395928 / 42745088(54 % ), addr = 0x13B8300

其中 usage 表示目前 heap 使用量 / heap 容量 (使用百分比)，addr 表示 heap 的起始地址。

- 若勾選 :menuselection:`Debug --> Enable Malloc Debug Library` (需要先取消勾選:menuselection:`System --> Internal Settings --> CPU write-back cache enable` )：

   .. image:: /_static/qconf_check_enable_malloc_debug_library.png

   可以輸出每個記憶體配置的詳細資訊，範例如下：

   .. code-block:: shell

      <MALLOC_STATS>       1 x        4 Bytes in .../port_thread.c : 50, generations : 4433
      <MALLOC_STATS>       1 x        8 Bytes in .../linphonecore.c : 397, generations : 3555
      <MALLOC_STATS>       1 x       19 Bytes in .../linphonecore.c : 3175, generations : 3557
      <MALLOC_STATS>       1 x       22 Bytes in .../linphonecore.c : 1249, generations : 3572
      <ALLOC_STATS>        6 x        8 Bytes in .../lpconfig.c : 72, generations : 3360 3379 3446 ...
      <MALLOC_STATS>       2 x        4 Bytes in .../lpconfig.c : 73, generations : 3466 3473

   其中第一行表示在 ``port_thread.c, line. 50`` 的地方，程式配置了 1 個 4 bytes 的記憶體。Generations 表示配置的流水號。詳細說明可以參考 http://www.hexco.de/rmdebug/。

觀測 CPU 使用率
----------------------------------

此功能可讓系統定期透過 debug message 裝置輸出目前每個 Task 的 CPU 使用率。使用率的統計是每次輸出後重新計算。要啟用此功能，需要勾選 :menuselection:`OpenRTOS --> Use Trace Facility` 與 :menuselection:`OpenRTOS --> Generate Run Time statistics` ， 

.. image:: /_static/qconf_check_use_trace_facility_and_generate_run_time_statistics.png

以及 :menuselection:`Debug --> Enable Runtime Statistic` 與 :menuselection:`Debug --> Enable Runtime Statistic --> Enable Runtime Task Time Statistic` 。

.. image:: /_static/qconf_check_enable_runtime_task_time_statistic.png

輸出範例如下： 

.. code-block:: shell

    IDLE            37268           74
    itv_hw_overlay_ 1               <1
    ms_ticker_run   1045            2
    MainTask        11202           22
    ms_ticker_run   0               <1
    USBEX_ThreadFun 1               <1
    UsbHostDetectHa 0               <1
    tcpip_thread    69              <1
    NetworkTask     6               <1

其中第一欄是 Task Name，第二欄是花費的時間，單位是 ms。第三欄是統計時間內佔 CPU 時間的百分比。

