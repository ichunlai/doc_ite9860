.. _use_spitool_rw_memory.rst:

使用 USB To SPI Tool 讀寫暫存器、讀寫記憶體、傾印 ROM
=======================================================================

可利用 ITE USB To SPI Tool 讀寫暫存器或讀寫記憶體來取得或控制系統狀態，藉此在運行時期監看一些變數或暫存器的變化狀況，可以拿來輔助 debug。

有關如何利用 USB To SPI Tool 讀寫暫存器、讀寫記憶體和傾印ROM的詳細步驟，請見 :ref:`usb_spi_tool_read_reg` 、 :ref:`usb_spi_tool_rw_memory` 、 :ref:`dump_norflash` 、 :ref:`dump_nandflash` 。
