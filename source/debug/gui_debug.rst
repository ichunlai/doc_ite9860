﻿.. _gui_debug:

使用 GUI 介面除錯韌體
=========================

環境安裝
-----------------

安裝 Eclipse
^^^^^^^^^^^^^^^^^^^^^^^^

1. 首先下載 `Eclipse CDT 2019-09 <https://www.eclipse.org/downloads/download.php?file=/oomph/epp/2019-09/R/eclipse-inst-win64.exe>`_ 並安裝。

   .. image:: /_static/eclipse_download_page.png

   .. caution::

      即使有更新的版本，仍請使用版本 2019-09。目前 ITE 釋出的 Eclipse 修正檔僅針對Eclipse CDT 2019-09 版做修正，除非有針對 Eclipse 更新版本的修正檔，否則請勿升級Eclipse。

2. 若安裝過程中出現下圖畫面，請先到 https://www.java.com/ 下載 jre 1.8.0 (64 Bit) 並安裝。

   .. image:: /_static/eclipse_prompt_install_jre.png

   .. attention::

      若您的 Windows 是 64 位元，卻安裝了 32 位元版本的 JRE，則安裝過程中可能會出現找不到 JRE 的狀況。

3. 安裝完 jre 後，回來安裝 eclipse。記得選擇安裝 "Eclipse IDE for C/C++ Developers" 。

   .. image:: /_static/eclipse_select_install_cdt.png

4. 選擇安裝路徑。建議不要使用預設值，將原本路徑改為 ``D:\eclipse_cpp_2019_09`` 。Eclipse 安裝程式會自動建立 ``D:\eclipse_cpp_2019_09\eclipse`` 目錄。

   .. image:: /_static/eclipse_install_path.png

5. 安裝完成後，點擊 Eclipse 圖示執行。

6. 選擇 workspace，使用預設位置即可。

   .. image:: /_static/eclipse_workspace_path.png

7. 建議將 eclipse 安裝目錄加到 PATH 系統環境變數。譬如：

   .. image:: /_static/eclipse_add_env_variable.png


安裝 Eclipse 修正程式
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. 先將 ``eclipse-cpp-2019-09-patch.zip`` 解壓縮到特定目錄，譬如 ``D:\patch`` 。您可以在該目錄看到以下檔案：

   .. image:: /_static/eclipse_patch_file_list.png

2. 在啟動 eclipse 後，點擊 :menuselection:`Help --> Install New Software…`

   .. image:: /_static/eclipse_install_new_software.png

3. 點擊 "Add" 按鈕。

   .. image:: /_static/eclipse_install_add_patch.png

4. 點擊 "Local…" 按鈕，選擇 ``D:\patch`` 後，點擊 "Add" 按鈕。

   .. image:: /_static/eclipse_patch_path.png

5. 先取消勾選 "Group items by category" ，然後點擊 "Select All" ，最後點擊 "Next >" ：

   .. image:: /_static/eclipse_uncheck_group_items.png

6. 選擇 "Update my installation to be compatible with the items being installed" 。點擊 "Next >"。

   .. image:: /_static/eclipse_update_software.png

7. 點擊 "Next >" 。

   .. image:: /_static/eclipse_patch_install_details.png

8. 選擇 "I accept the terms of the license agreement" ，點擊 "Finish"。

   .. image:: /_static/eclipse_accept_license.png

9. 出現 "Security Warning" 視窗，點擊 "Install anyway" 。

   .. image:: /_static/eclipse_security_warnings.png

10. 安裝完成後，會出現詢問您是否重啟 IDE 的畫面，點擊 "Restart Now" 。

   .. image:: /_static/eclipse_restart_now.png

確認已成功安裝修正程式
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

安裝後我們要確認是否 Eclipse 是否已安裝了修正檔，或者有時候我們不知道某平台是否已經安裝了修正檔，則可以透過 :menuselection:`Help --> About Eclipse IDE --> Installation Details --> Installed Software` ， 確認 "C/C++ DSF GDB Debugger Integration" 版本是否為 9.9.0.201912201609 以及 "C/C++ GDB Hardware Debugging" 版本是否為 9.9.0.201912201608 。

   .. image:: /_static/eclipse_installed_software.png


除錯專案
-----------------

這裡假設我們要除錯 my_project 專案。 my_project 專案的相關描述請看 :ref:`start_a_new_project` 。

要利用 GDB 除錯程式，您必須確保：

- USB to SPI 轉換板已正確安裝。

  - 若您尚未將 USB to SPI 轉換板與 PC 連接，請參考 :ref:`usb2spi_connect2pc` 。
  - 若您尚未將 USB to SPI 轉換板與 EVB 連接，請參考 :ref:`usb2spi_connect2board` 。
  - 若您尚未安裝 USB to SPI 轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

- EVB 已經切換為 Co-operative Mode。

  .. note::

     若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

接著請執行以下步驟：

1. 要使用 GDB 除錯程式，必須要修改編譯設定。因此要先執行 ``<sdk_root>/build/openrtos/my_project_all.cmd`` 以開啟 qconf 設定和建置視窗。

2. 將 :menuselection:`Develop Environment --> Develop Environment` 切換為 "Develop Mode"。

   .. image:: /_static/qconf_switch_to_develop_mode.png

3. 取消勾選 :menuselection:`System --> Watch Dog enable`。

   .. image:: /_static/qconf_uncheck_watchdog.png

4. 取消勾選 :menuselection:`System --> Internal Settings --> Memory Debug enable`。

   .. image:: /_static/qconf_uncheck_memory_debug.png

5. 取消勾選 :menuselection:`Screen --> Shows Logo on Bootloader`。

   .. image:: /_static/qconf_uncheck_show_logo_on_bootloader.png

6. 將 :menuselection:`SDK --> Build Type` 切換為 "Debug" 或 "DebugRel"。

   .. image:: /_static/qconf_switch_to_debugrel.png

   點擊 |build_icon| 建置專案。

   若先前從未將映像檔燒錄至 NOR Flash，請參考 :ref:`write_image_to_norflash` 將產生出來的映像檔先燒錄至 NOR Flash 內。

7. 執行 ``<sdk_root>/build/openrtos/my_project/project/my_project/jp2.cmd``。

   .. image:: /_static/gdb_jp2.png

8. 執行 ``/build/openrtos/my_project/project/my_project/console.exe``，在console視窗內輸入 ``eclipse.exe`` 路徑並執行它。如果先前有將eclipse安裝目錄加入到PATH系統環境變數，這裡只要打 ``eclipse.exe`` 即可。

   .. caution::

      務必從此 console 視窗啟動 eclipse，否則eclipse可能會出現找不到toolchain或symbol file之類的情形。

   .. image:: /_static/eclipse_execute_eclipse.png

.. |build_icon| image:: /_static/qconf_build_icon.png
  :alt: build icon
  :width: 14
  :height: 14

9. 開啟 eclipse 之後，點擊 :menuselection:`file --> new --> Makefile Project with Existing Code` 。

   .. image:: /_static/eclipse_new_makefile_project.png

10. "Project Name" 欄位請輸入你想要的專案名稱。 "Existing Code Location" 欄位請選擇 SDK 根目錄。這裡我們假設 SDK 根目錄位於 ``d:\ite_sdk`` 。 "Toolchain for Indexer Settings" 欄位選擇 "<none>" (這裡沒選對也沒差，因為我們不在 eclipse IDE 內建置專案)。設定完成後，點擊 "Finish" 。

   .. image:: /_static/eclipse_new_project_toolchain.png

11. 若成功會在 "Project Explorer" 看到 ``ite_sdk`` 專案。

   .. image:: /_static/eclipse_project_explorer.png

12. 專案建立成功後，點擊 :menuselection:`Run --> Debug Configurations…` 。

   .. image:: /_static/eclipse_run_debug_configuration.png

13. 雙擊 "GDB Hardware Debugging" 。

   .. image:: /_static/eclipse_debug_configuration_type.png

14. 這裡的步驟較多。首先在 "Name" 欄位填入任何你想要的名字，自己能識別就好。然後在 "Project" 欄位填入第 14 步所填的 Project Name。 "C/C++ Application:" 欄位填入 "my_project" 執行檔路徑，譬如 ``D:\ite_sdk\build\openrtos\my_project\project\my_project\my_project`` 。記得要選擇沒有副檔名的那個檔案。

   .. note::

      注意這個路徑是檔案路徑不是目錄路徑。

   .. note::

      您也可以在 "C/C++ Application:" 欄位填入 ``D:\ite_sdk\build\openrtos\${env_var:CFG_PROJECT}\project\${env_var:CFG_PROJECT}\${env_var:CFG_PROJECT}``。這將會自動採用環境變數 ``CFG_PROJECT`` 的值來決定執行檔路徑。我們較推薦採用這種方法。

   最後請選擇 "Disable auto build"。

   .. image:: /_static/eclipse_debug_configuration_main.png

15. 接下來切換到 "Debugger" 標籤頁。在 "GDB Command:" 欄位填入：

   .. code-block::

      C:\ITEGCC\bin\arm-none-eabi-gdb.exe D:\ite_sdk\build\openrtos\my_project\project\my_project\my_project

   .. note::

      這邊有三點需要注意的：

      1. 若只填 ``C:\ITEGCC\bin\arm-none-eabi-gdb.exe`` ，在後面 debug 階段會出現 "No symbol table is loaded. Use the \"file\" command." 訊息。
      2. ``D:\ite_sdk\build\openrtos\my_project\project\my_project\my_project`` 是檔案路徑，不是目錄路徑。
      3. ``C:\...\arm-none-eabi-gdb.exe`` 與 ``D:/...my_project`` 應填在同一行，並以空格分隔。

   - 勾選 "Use remote target" 。
   - "JTAG Devices:" 欄位選擇 "OpenOCD (via socket)"。
   - "Port number:" 欄位填入 ``3333``。

   .. image:: /_static/eclipse_debug_configuration_debugger.png

16. 切換到 "Startup" 標籤頁，取消勾選 "Load Image" 和 "Load symbols"。

   .. image:: /_static/eclipse_debug_configuration_startup.png

17. 切換到 "Source" 標籤頁，點擊 "Add" 按鈕。

   .. image:: /_static/eclipse_debug_configuration_source.png

18. 選擇 "Path Mapping"。點擊 "OK"。

   .. image:: /_static/eclipse_debug_configuration_path_mapping.png

19. "Name:" 欄位隨便填，點擊 "Add"。

   .. image:: /_static/eclipse_debug_configuration_path_mapping_detail.png

20. 在 "Compilation path:" 欄位填入 ``/cygdrive/d`` ，在 "Local file system path:" 欄位填入 ``D:\`` 。這裡假設您的 SDK 根目錄是在 D 槽，若是在其他槽，請自行修改。點擊 "OK" 關閉對話框。

   .. image:: /_static/eclipse_debug_configuration_path_mapping_done.png

21. 點擊 "Apply" ，接著點擊 "Debug"。

22. 點擊 :menuselection:`Windows --> Show View --> Debug Console` 。您應該會看到類似這樣的視窗，左邊顯示 "program counter" 停在 0x0，以及目前位於 ``_start()`` 函式內。實際上 ``_start()`` 位於 ``<sdk_root>/openrtos/boot/fa626/startup.S`` ，因為是用組合語言撰寫，所以顯示 "No source available for …"。若您的專案建置時， :menuselection:`SDK --> Build Type` 是採用 Debug 選項編譯，則在這裡將可看到組合語言程式碼。底下的 "Debugger Console" 會顯示 gdb 輸出的訊息。

   .. image:: /_static/eclipse_debug_break.png

   您可以在 "Debugger Console" 輸入 ``b BootInit``、``c`` 看看程式會不會停在 BootInit function。如果會就表示成功了。

   .. image:: /_static/eclipse_debug_break_at_bootinit.png

.. note::

   之後要重新debug 專案，只要確認：

   1. 先運行專案建置目錄下的 ``jp2.cmd`` 。
   2. 確定是透過專案建置目錄下的 ``console.cmd`` 來開啟 eclipse。
   3. 選擇 eclipse 選單的 :menuselection:`Run --> Debug History --> ite_sdk` 就可以再次進行debug。
