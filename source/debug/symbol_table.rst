.. _symbol_table:

產生 Symbol Table
================================================================

當 image 編譯完後，會在 build 目錄下產生 ``<project_name>.symbol`` 檔案。如下圖所示：

.. image:: /_static/file_struct_symbol_table.png

Symbol 檔案裏面會陳述各個變數以及函數放置的位置，和每個 section 被分配的範圍與大小。可參考這個 table file 透過 USB to SPI 轉換板去讀相對應記憶體位置來查看目前狀況。   

