.. _back_trace:

Back Trace
===================================

當系統當機時會顯示如下圖的訊息。可將第二行所顯示的位址資訊填入 backtrace tool 以推斷出最後造成當機的程式位置。

.. image:: /_static/teraterm_core_dump.png

操作方法如下：

1. 執行 ``<sdk_root>/build/openrtos/<project_name>/project/<project_name>/backtrace.cmd`` 。
2. 填入位址資訊，如下圖所示：

   .. image:: /_static/console_backtrace_fill_address.png

3. 鍵入 Enter 將出現程式根據所輸入位址資訊推斷出相應的原始碼位址。其中顯示的越上面的行的資訊越接近系統當掉時的位址。

   .. image:: /_static/console_backtrace_result.png
