.. _run_time_error:

Run Time Errors
================================================================

當系統出現異常時，往往會 dump 如下資訊：

.. image:: /_static/teraterm_core_dump.png

可根據畫面中第一行所顯示的 XXX Error 以及第二行所顯示的位址資訊推斷出軟體中可能有問題的位置。其中第一行顯示的錯誤訊息的詳細細節，請見 :ref:`run_time_exception_error` 。如何從第二行顯示的位址訊息推斷出軟體中可能有問題的位置，請見 :ref:`back_trace` 。

.. _run_time_exception_error:

Run time exception error
----------------------------------------

底下描述當系統當機時顯示的錯誤訊息的詳細說明：

- Data Abort Error：
  
  通常為讀寫到不合法位置(如 0xFFFF0000)，或是有 alignment 的問題。(如讀取/寫入一個 Integer pointer 但是給的位置卻是 0x10003 不在 4 byte alignment 上)。

- Divide by Zero Error：

  發生分母為 0 之異常除 0 問題。

- Undefined Error：

  CPU 從記憶體 Fetch 指令但是解譯時發現不認識該指令而拋出的異常.

- Prefetch Abort Error：

  當 Fetch 指令為無效位置時，會觸發此異常。

- Memory Debug Error：

  當開啟某區段記憶位置寫入保護，該區段被任何 HW 寫入時(CPU、Graphic Engine、H264 decoder等)，則會觸發異常。(請注意將 write back 模式關閉，因為該功能不是透過 ARM 的 Exception 而是外部中斷導致，所以在 write back mode 下相對的 backtrace 較為不精準)。

Error 判斷準則
---------------------------------

通常 Data abort、Divde by Zero 正常情況可以透過 ``backtrace.cmd`` 看到完整的 call stack，所以比較容易推斷是哪段 code 寫錯或是邏輯。但是如 Undefined Error、Prefetch Abort Error 撇去 HW 的問題外，純軟體上，常發生下列錯誤：

- 目前的 thread 的 stack 空間被其他 thread 錯誤邏輯或是越位而蓋到相關記憶體位置。
- 錯誤的 global 變數溢位導致覆寫到其他 global 變數(如 array 大小定義 4，結果寫超過array 大小)，導致使用其他變數的線程發生異常行為。
- Thread 間共享的 global 變數沒有用 mutex 保護，當發生 race condition 時，導致行為異常。
- 對 HW Engine 下的參數錯誤，或是配置記憶體使用不合法，如記憶體釋放後仍傳遞給 HW 做使用，導致寫到別的記憶體區段。



