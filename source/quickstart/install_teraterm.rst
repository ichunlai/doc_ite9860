﻿.. _install_teraterm:

安裝除錯訊息工具軟體
=========================

Tera Term 是一個知名的終端機工具，可用來顯示 PC 從 UART 收到的訊息。因此我們利用它來接收並顯示來自 EVB 的除錯訊息。

安裝 Tera Term 的步驟如下：

1. 從 Tera Term 官方網站 https://osdn.net/projects/ttssh2/releases/ 下載並執行最新版本的安裝程式 (譬如 ``teraterm-4.105.exe``)。

   .. note:: 以下的安裝畫面擷取自 4.105 版本的 Tera Term，新的版本的畫面可能略有不同，請自行調整。

2. 點擊 "Run" 按鈕。

   .. image:: /_static/teraterm_install_init.png

3.	點擊 "Yes" 按鈕。

   .. image:: /_static/teraterm_user_access_control.png

4. 選擇 "I accept the agreement"後，點擊 "Next >" 按鈕。

   .. image:: /_static/teraterm_install_license.png

5. 選擇安裝路徑後點擊 "Next >" 按鈕。

   .. image:: /_static/teraterm_install_path.png

6. 直接點擊 "Next >" 按鈕。

   .. image:: /_static/teraterm_install_select_component.png

7. 選擇語言設定後點擊 "Next >" 按鈕。

   .. image:: /_static/teraterm_install_language.png

8. 採用預設值並點擊 "Next >" 按鈕。

   .. image:: /_static/teraterm_install_menu_name.png

9. 採用預設選項並點擊 "Next >" 按鈕。

   .. image:: /_static/teraterm_install_select_tasks.png

10. 點擊 "Install" 按鈕開始安裝。

   .. image:: /_static/teraterm_install_ready.png

11. 安裝完成。勾選 "Launch Tera Term"，點擊 "Finish" 按鈕即可執行Tera Term。

   .. image:: /_static/teraterm_install_finish.png

12. 選擇 Serial 和 Port ，其中 Port 是依照 USB to SPI 轉換板連接至PC時所顯示的 Serial Port 為主。設定完成後點擊 "OK" 按鈕關閉視窗。

   .. image:: /_static/teraterm_select_serial_port.png

13. 點擊 :menuselection:`Setup --> Serial port...`

   .. image:: /_static/teraterm_setup_serial_port.png

14. Speed 欄位請填入 115200，並再次確認 Device Manufacturer 是否顯示為 "FTDI"。若不是請重新選擇 Port 。設定完成後點擊 "OK" 按鈕關閉視窗。

   .. image:: /_static/teraterm_setup_serial_port_dialog.png

15. 點擊 :menuselection:`Setup --> Terminal...`，並在開啟的視窗做以下設定。設定完成後點擊 "OK" 按鈕關閉視窗。注意 :menuselection:`New-line --> Receive` 的部分要改為 "LF" 或 "Auto"。

   .. image:: /_static/teraterm_setup_terminal.png

16. 點擊 :menuselection:`Setup --> Save setup...` 將跳出儲存檔案對話框以提示您要將剛剛做好的設定儲存到哪。選擇適當的位置後點擊 "Save" 儲存。之後執行 Tera Term 時皆會採取目前設定。

   .. image:: /_static/teraterm_setup_save.png

17. 之後若您需要將從 UART 收到的訊息轉存到檔案中，您可以點擊 :menuselection:`File --> Log...`。這將跳出儲存檔案對話框以提示您要將收到的訊息轉存到哪。若您需要記錄每條訊息收到的時間，您可以勾選 "Timestamp" 選項。

   .. image:: /_static/teraterm_save_log.png

.. raw:: latex

    \newpage