﻿.. _install_usb_to_spi_driver:

安裝 USB to SPI 轉接板驅動程式
=======================================

若您需要燒錄韌體、利用 gdb 除錯程式、在運行時期讀取晶片上的暫存器狀態、利用設定與建置工具將映像檔直接寫入晶片的記憶體運行…等等情況下，您都需要透過USB to SPI轉換板將PC與EVB之間相連來完成這些操作。為了讓USB to SPI轉換板能夠正常運作，您需要為該轉換板安裝驅動程式。

.. _install_ftdi_driver:

安裝 FTDI 原廠驅動程式
-------------------------------------------------

USB to SPI 轉接板上的主晶片是 FTDI 公司生產的 ft2232d，因此我們將安裝該公司所提供的驅動程式。

.. note:: 

   * 若您的系統是 Windows 10，系統會自動安裝驅動程式，您可以跳過此步驟。
   * 若您不需要透過 Usb to SPI 轉接板接收 EVB 透過 uart 送出的除錯訊息，您也可以跳過此步驟。

1. 首先，將 USB to SPI 轉接板插入 PC 上的任一 USB 插槽。
2. 從 ``Tools.7z`` (若不知道 ``Tools.7z`` 從哪裡取得請看 :ref:`download_sdk` ) 取出 ``CDM20828_Setup.exe`` 並執行。

   .. hint::
   
      您也可以直接從 FTDI 的網站 (https://www.ftdichip.com/Drivers/D2XX.htm) 下載最新的驅動程式來安裝。

3. 點擊 "Extract" 按鈕。

   .. image:: /_static/ftdi_driver_install_init.png

4. 點擊 "Next" 按鈕。

   .. image:: /_static/ftdi_driver_install_2nd.png

5. 安裝中。

   .. image:: /_static/ftdi_driver_install_ing.png

6. 安裝完成。點擊 "Finish" 關閉視窗。

   .. image:: /_static/ftdi_driver_install_complete.png

7. 請開啟 Device Manager 。您將會發現系統多了兩個 USB Serial Port 裝置。

   .. image:: /_static/ftdi_driver_install_device_manager.png
   
   .. note::
   
      這兩個 USB Serial Port 不一定會位於 COM3 或 COM4，這取決於您的平台。無論如何，編號較大的 Port 通常被 EVB 用來輸出除錯訊息。編號較小的 Port 則被作為 spi 或 jtag 使用。


安裝 WinUSB 驅動程式
---------------------------------------------

由於 FTDI 公司提供的驅動程式在 Usb to SPI 轉接板作為 jtag 使用時非常不穩定，常導致 gdb 使用到一半斷線。因此 ITE 改採用 WinUSB 驅動程式來驅動 ft2232d 的第一個介面 [#]_ ，並將對晶片的細部控制動作移到AP層處理。

.. [#] ft2232d 晶片包含兩個介面，皆可作為 spi/jtag/uart 使用。不過在ITE的平台上第一個介面固定作為 spi/jtag 使用。第二個介面固定作為 uart 使用。

為了讓使用者更容易安裝 WinUSB 驅動程式，ITE在SDK的 ``<sdk_root>/tool/bin/`` 目錄下提供了一個名稱為 ``usbtospi_driver.exe`` 的程式。該程式可以自動找出 ft2232d 的第一個介面並使用 WinUSB 驅動程式取代原本的驅動程式。

WinUSB 驅動程式的安裝步驟如下：

1. 首先，將USB to SPI轉接板插入PC上的任一USB插槽。
2. 執行 ``/tool/bin/usbtospi_driver.exe``。執行 ``usbtospi_driver.exe`` 後會出現 User Account Control 對話框，點擊 "Yes"。

   .. image:: /_static/winusb_user_access_control.png

3. 安裝中。

   .. image:: /_static/winusb_install_console.png

4. 安裝完成後，將顯示 ``[INFO] Success,Press Enter to exit this program`` 訊息即表示安裝完成。請敲擊鍵盤上的 Enter 鍵關閉此視窗。

   .. image:: /_static/winusb_install_success.png

5. 請開啟 Device Manager 。您將會發現系統多了一個 Dual RS232 裝置。

   .. image:: /_static/winusb_install_ite_usb_device.png

   .. note::   

      若您先前有按照 :ref:`install_ftdi_driver` 所描述的步驟安裝了FTDI原廠驅動程式，您會發現在裝置管理員中少了一個 COM port (原本是兩個 COM port)，那是因為 USB to SPI 轉接板上的第一個介面的驅動程式已被 WinUSB 驅動程式取代，並重新命名為 Dual RS232。
      
      .. figure:: /_static/winusb_install_before.png   

         僅安裝 FTDI原廠驅動程式的情況。
      
      .. figure:: /_static/winusb_install_after.png
      
         安裝 FTDI原廠驅動程式後，又安裝了 WinUSB 驅動程式的情況。
      
   .. note::   

      您也可以使用 zadig (https://zadig.akeo.ie/) 程式來安裝 WinUSB 驅動程式。使用 zadig 程式來安裝 WinUSB 驅動程式的步驟比較複雜，且有可能出錯。除非您很確定您在做什麼，否則不建議使用 zadig 程式來安裝。


.. raw:: latex

    \newpage