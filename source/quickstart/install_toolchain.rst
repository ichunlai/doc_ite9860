﻿.. _install_toolchain:

安裝Toolchain
=========================

在Windows下安裝Toolchain
--------------------------

請直接將下載後的toolchain壓縮檔 (譬如 ``ITEGCC.2018.04.13.gcc-5.4.0_windows.7z``) 使用 7-zip (https://www.developershome.com/7-zip/) 解壓縮到 ``C:\`` 。您應該可以在 ``C:\ITEGCC`` 目錄下看到以下內容：

    .. image:: /_static/quickstart_itegcc.png

這樣就完成在在Windows下安裝Toolchain的任務。

.. TODO 若需要同時存在多個編譯器時該怎麼辦？

.. 在Linux下安裝Toolchain
.. --------------------------
..
.. TBD


.. note::

    若有需要多套 toolcahin 同時存在，請看 ":ref:`multiple_toolchain`"

.. raw:: latex

    \newpage