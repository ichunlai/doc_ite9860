.. _start_a_new_project:

開始一個新專案
=========================

請直接將下載後的 SDK 壓縮檔 (譬如 ``20200715_Standard_SDK_v2430_5f74e6_20200722.7z``) 使用 7-zip (https://www.developershome.com/7-zip/) 解壓縮到任意目錄。假設您將其解壓縮到 ``D:\``，則您應該可以在 ``D:\20200715_Standard_SDK_v2430_5f74e6_20200722`` 目錄下看到以下內容：

   .. image:: /_static/quickstart_sdk_file_list.png

請將 ``ite_sdk.7z`` 解壓縮到 ``D:\``，您在 ``D:\ite_sdk`` 目錄下看到以下內容：

   .. image:: /_static/quickstart_sdk_file_list_detail.png

其中：

.. flat-table::
    :widths: 20 80

    * - ``/build``
      - 包含用來建置專案所需的批次檔和 shell script。最終建置的成果也會放置在此目錄中。
    * - ``/data``
      - 包含所有專案共用的 UI 資源檔，譬如字型檔和圖示等。
    * - ``/doc`` 
      - 與 SDK 有關的文件。
    * - ``/project``
      - 範例專案，以及未來任何新專案的存放位置。
    * - ``/sdk``
      - 為軟體的驅動程式與相關系統架構主體 source code 的存放位置。
    * - ``/tool``
      - 編譯時可能會用到的相關工具軟體存放位置。

有關 SDK 的詳細描述請看 :ref:`sdk_architecture` 。

.. attention::

   SDK 的路徑不能包含空格或任何特殊字元 (包含中文)。

產生一個新專案
--------------------------

這裡我們將展示如何快速建立一個能在 IT986X standard EVB 上運行的專案。要建立一個新專案，請先執行 SDK 目錄下的 ``wizard.exe`` 程式。此工具能協助開發者快速地從現有專案克隆一份副本並重新命名專案名稱，稍後您可以基於此專案加入自己所需的額外功能。

1. ``wizard.exe`` 的執行畫面如下，請點擊 "Next >" ：

    .. image:: /_static/quickstart_wizard_init.png

2. 請在畫面上方的 "Project name:" 欄位輸入所要建立的專案名稱 (譬如： ``my_project``)。"Project:" 欄位請勾選 "Export project"，底下的 "Project name:" 選擇 "standard_project"。設定完畢後，請點擊點擊 "Next >" 按鈕。

    .. image:: /_static/quickstart_wizard_project_details.png

   其中上圖紅框圈起來的 "Select a Kconfig" 選項須依據PCB來選取。若不知如何選取可以先採用預設。

3. 請點擊 "Finish" ：

    .. image:: /_static/quickstart_wizard_finish.png

4. 完成後，您將在 SDK 底下的 ``project`` 目錄內看到新增了一個 ``my_project`` 目錄 (這裡假設您在前一步填入的專案名稱是 ``my_project`` )。

    .. image:: /_static/quickstart_wizard_My_Indoor.png

   產生的專案目錄裡面的内容簡介如下：

    .. flat-table::
        :widths: 35 65

        * - ``/itu`` 目錄
          - 人機界面資料，分不同的解析度，有xml和itu兩種格式的檔案。
        * - ``/media`` 目錄
          - 系統啟動時的開機動畫，和專案所需要多媒體資料。
        * - ``/sounds`` 目錄
          - 包含按鍵聲音檔。
        * - ``/web`` 目錄
          - 包含Web Server所使用的html頁面，提供使用者透過 web 頁面設置參數和升級韌體的功能。
        * - ``/CMakeLists.txt`` 檔案
          - 包含在專案內的檔案列表，以 cmake script 語法寫成。
        * - ``/kconfg`` 檔案
          - 提供組態設定選項，以 kconfig script 語法寫成。
        * - 基礎功能公用檔案
          - 如 ``upgrade.c``、``network.c``、``photo.c``、``video.c``、``string.c``、``storage.c``、``screen.c``、``scne.c``。
        * - UI 邏輯相關檔案
          - 所有名稱以 ``layer_`` 開頭的檔案，可自行編寫開發。
        * - ``function_table.c``
          - Function註冊，可自行編寫開發。
        * - ``external.c``
          - 通訊執行緒線程，可自行編寫開發。
        * - ``external_process.c``
          - 自定義event，可自行編寫開發。

   此外，還會在 ``<sdk_root>/build/openrtos`` 建立兩個批次檔以及在 ``<sdk_root>/build/win32`` 下建立一個批次檔。

   * ``<sdk_root>/build/openrtos/my_project.cmd``
   * ``<sdk_root>/build/openrtos/my_project_all.cmd``
   * ``<sdk_root>/build/win32/my_project_all.cmd``

   您可以執行 ``<sdk_root>/build/openrtos/my_project_all.cmd`` 以彈出設定和建置視窗。

.. note:: 
	
    關於 wizard 工具程式的更詳細說明，請看 ":ref:`wizard`" 

設定與建置專案
--------------------------

請執行 ``<sdk_root>/build/openrtos/my_project_all.cmd`` 以開啟設定和建置視窗。

   .. image:: /_static/qconf_My_Indoor.png

點擊 |build_icon| 建置專案。

.. |build_icon| image:: /_static/qconf_build_icon.png
  :alt: build icon
  :width: 14
  :height: 14

提取編譯完成後的映像
----------------------------------

若編譯成功，所產生的映像檔將置於 ``<sdk_root>/build/openrtos/my_project/project/my_project``。整個建置過程將產生兩個映像檔，分別是 ``XXX.PKG`` 及 ``XXX.ROM`` 。其中XXX會因為專案不同而名稱不同，以 my_project 專案而言，所產生的映像檔名稱為 ``ITEPKG03.PKG`` 與 ``ITE_NOR.ROM``。

.. note:: 
	
   實際產生的映像檔名稱可透過 qconf 修改。其中 PKG 檔案的實際名稱由 :menuselection:`Upgrade --> Upgrade package filename` 設定選項決定。而 ROM 檔案的實際名稱由 :menuselection:`Upgrade --> Generate NOR image` 選項決定。

   .. image:: /_static/qconf_my_project_pkg_filename.png

``*.PKG`` 檔案大小較小，用於 firmware 升級。當您透過 USB pen drive 或透過 EVB 上的網頁介面來升級電路版內的韌體時將採用此格式。若您是透過 USB pen drive 更新韌體，請務必將 USB pen drive Format 成 FAT32 格式。

``*.ROM`` 檔案大小跟 NOR flash size 一樣大，其中還包含 bootloader 和 UI 所需資源檔等完整資料。當 NOR flash 內容為空時您須先透過 nor writer 或使用 SDK 根目錄下的 ``<sdk_root>/tool/bin/usb_spi_tool.exe`` 透過 USB to SPI 轉換板來燒錄韌體 (詳細方法請看 :ref:`write_image_to_norflash` )。

其實還有另外一種映像檔 - ``XXX.bin``，這種檔案只包含未壓縮的 raw binary code。可直接寫入記憶體位址 0x0 執行。由於寫 NOR flash 通常比寫 RAM 還慢很多，因此在開發時期通常會先將 ROM 檔燒錄到 NOR flash，之後若 UI 等相關資源檔案沒有變動，可透過 USB to SPI 轉換板將 bin 檔直接寫入記憶體執行。這樣會比燒錄 NOR flash 快上許多。

.. flat-table:: 各種映像檔格式的比較表
   :widths: 20 80

   * - ``*.bin``
     - 只包含未壓縮的 raw binary code。可直接寫入記憶體位址0x0執行。僅在開發時期使用。
   * - ``*.PKG``
     - 可包含完整韌體內容(不包含空白區域)，也可選擇性只包含需要更新的資源(譬如只包含UI資源)。
   * - ``*.ROM``
     - 包含完整韌體內容(韌體、UI 資源、bootloader 等，甚至包含空白區域)。

接下來需要做一些燒錄 NOR flash 的準備工作。

.. _usb2spi_connect2pc:

將 USB to SPI 轉換板與 PC 連接
----------------------------------------------------

請將 USB to SPI 轉換板透過 Mini USB 與 PC 連接。

   .. image:: /_static/usb2spi_connect2pc.png

.. note::

   若連接正確 Green/Red LED 會亮燈。

.. _usb2spi_connect2board:

將 USB to SPI 轉換板與 EVB 連接
----------------------------------------------

在斷電狀態下，將 USB to SPI 轉換板之 8PIN 訊息線橋接至 IT9860 Standard EVB 之對應介面。

   .. image:: /_static/usb2spi_connect2board.png

.. _usb2spi_switch2cooperative:

將 EVB 切換為 Co-operative Mode (包含切換為 NOR Boot Mode 的說明)
-------------------------------------------------------------------------------------------

Co-operative Mode
^^^^^^^^^^^^^^^^^^^

當板子處於 co-operative mode ，在上電時， IT986x 內部的 CPU core 將處於未啟動的狀態。此時使用者可以利用 ``<sdk_root>/tool/bin/usb_spi_tool.exe`` 透過 USB to SPI 轉換板讀寫板端的 NOR/NAND flash 、 SoC 內部的記憶體和暫存器。此模式經常用在開發階段。

要將 EVB 切換為 co-operative mode ，請在斷電狀態下，按照 :numref:`usb2spi_cooperative_jump` 將黃框標示處靠上方的兩根排針使用 jump 將其短路。

.. figure:: /_static/usb2spi_cooperative_jump.png
   :name: usb2spi_cooperative_jump

   將黃框處上面兩個排針插上 jump 使其短路即為 Co-operative Mode。

NOR Boot Mode
^^^^^^^^^^^^^^^^^^^

當板子處於 NOR Boot mode ，在上電時， IT986x 內部的 CPU core 將立即從內部的 maskrom 擷取指令來執行。此模式主要用於最終產品階段。

要將 EVB 切換為 NOR Boot mode ，請在斷電狀態下，按照 :numref:`usb2spi_norboot_jump` 將黃框標示處排針上的 jump 移除。

.. figure:: /_static/usb2spi_norboot_jump.png
   :name: usb2spi_norboot_jump

   將黃框處排針上的 jump 移除即為 NOR Boot Mode。

.. note:: 

   若您使用的電路板不是 EVB ，請跟 H/W 人員確認如何切換 BOOT_CFG 的設定。

各種啟動模式的設定如下：

.. flat-table::
   :widths: 30 30 40
   :header-rows: 1
   
   * - BOOT_CFG1	
     - BOOT_CFG0
     -
   * - 0
     - 0 
     - SD Card Boot    
   * - 0
     - 1 
     - NOR Flash Boot    
   * - 1
     - 0 
     - SPI NAND Boot   
   * - 1
     - 1 
     - Co-operative mode

您可以透過 ``usb_spi_tool.exe`` 來確認 EVB 目前是處於 Co-operative mode 或 NOR Boot mode 。步驟如下：

1.	先將板子上電，接著執行 ``<sdk_root>/tool/bin/usb_spi_tool.exe`` 。
2.	檢查 usb_spi_tool 程式標題列。

   若標題列中間顯示為 Co-operative mode ，則 EVB 目前處於 Co-operative mode 。

   .. image:: /_static/usb2spi_cooperative_title.png

   若標題列中間顯示為NOR Booting mode ，則 EVB 目前處於 NOR Booting mode 。

   .. image:: /_static/usb2spi_norboot_title.png

   .. attention::

      當 EVB 處於 NOR Boot 模式下並上電，將立即啟動儲存在 NOR Flash 內的韌體。當韌體處於運行的狀態下，則不是 ``usb_spi_tool.exe`` 軟體上呈現的所有功能都能使用。主要是有些功能可能會受到運行中的韌體的干擾。

.. _write_image_to_norflash:

燒錄映像檔至 NOR Flash
--------------------------

.. _usb2spi_connect:

本節將介紹如何使用 SDK 根目錄下的 ``<sdk_root>/tool/bin/usb_spi_tool.exe`` 燒錄映像檔至 NOR flash 。在使用 usb_spi_tool 之前，您必須確保：

- USB to SPI 轉換板已正確安裝。

  - 若您尚未將 USB to SPI 轉換板與 PC 連接，請參考 :ref:`usb2spi_connect2pc` 。
  - 若您尚未將 USB to SPI 轉換板與 EVB 連接，請參考 :ref:`usb2spi_connect2board` 。
  - 若您尚未安裝 USB to SPI 轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

- EVB 已經切換為 Co-operative Mode。

  .. note:: 

     若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

在確認完硬體連接一切正常之後，就可以開始執行燒錄軟體。步驟如下：

1.	先將板子上電後，再執行 ``<sdk_root>/tool/bin/usb_spi_tool.exe`` ，並確認視窗標題欄位的 CHIP ID 顯示為 9860。

   .. image:: /_static/usb2spi_software_init.png
   
   .. note:: 若 USB to SPI Tool 與基板或 PC 之間的連接不正確，會出現以下讀取不到 ``CHIP ID`` 畫面：

      .. image:: /_static/usb2spi_connect_fail.png

2. 切換至 SPI Writer 標籤頁。

   .. image:: /_static/usb2spi_spi_writer_tag.png

3. 點擊 "GET ID" 按鍵，確認可以正常讀取和識別 NOR flash 資訊。

   .. image:: /_static/usb2spi_get_nor_info.png

4. 選擇欲燒錄韌體映象檔 - ``xxx.ROM``  (選擇該 ROM 檔案路徑)。選定後，點擊 "Burn" 開始燒錄。

   .. image:: /_static/usb2spi_select_rom_path.png

   燒錄中的畫面：

   .. image:: /_static/usb2spi_writting_norflash.png

5. 燒錄完成後會顯示 ``Spi burn Success!!`` 訊息。

   .. image:: /_static/usb2spi_write_norflash_success.png

從 NOR Flash 啟動系統
--------------------------

1. 在斷電狀態下，將 EVB 切為 NOR Boot mode。

   .. note:: 

      若您無法確定 EVB 是否已經切換為 NOR Boot Mode ，請參考 :ref:`usb2spi_switch2cooperative`。 

2. 重新上電。

3.	若成功啟動應該可以透過 Tera Term 看到如下訊息： 

   .. image:: /_static/usb2spi_norboot_success.png

   .. note:: 

      若您不知道如何使用 Tera Term 查看 EVB 輸出的訊息，請參考 :ref:`teraterm_show_debug_msg` 。

   .. note:: 

      若您的 EVB 有連接 TL068HWXH08 6.86\" MIPI 螢幕，卻無法在螢幕上看到任何畫面。請檢查 qconf 設定與建置工具的 :menuselection:`GPIO --> Backlight PWN Pin` 選項必須設定為 63。

      .. image:: /_static/qconf_gpio_pwm_pin_setting.png

      背光控制部分對應的硬體電路：

      .. image:: /_static/e_circuit_backlight.png

      若您的觸控面板沒有反應，請檢查qconf設定與建置工具的

      1. :menuselection:`GPIO --> Touch INT Pin` 選項必須設定為 60。
      2. :menuselection:`GPIO --> Touch INT Pin Active High` 選項必須為取消勾選狀態。
      3. :menuselection:`GPIO --> Touch Reset Pin` 選項必須為 59。
      4. :menuselection:`GPIO --> IIC1 CLK GPIO Pin` 選項必須為 62。
      5. :menuselection:`GPIO --> IIC1 DATA GPIO Pin` 選項必須為 61。

      .. image:: /_static/qconf_gpio_touch_panel_setting.png

      觸控面板對應的硬體電路：

      .. image:: /_static/e_circuit_touch_panel.png


透過 USB to SPI 轉換板啟動系統
---------------------------------

"透過 USB to SPI 轉換板啟動系統" 被稱為 SPI Boot Mode，而 "從 NOR Flash 啟動系統" 被稱為 NOR Boot Mode。SPI Boot Mode 跟 NOR Boot Mode 相比，省略了一些步驟和時間，因此很適合開發時期使用。

要使用 NOR Boot Mode，我們必須：

1. 先調整 EVB 上的 jump 以切換到 Co-operative Mode。	
2.	將新韌體透過燒錄軟體寫入到 NOR flash。
3.	將 EVB 斷電。
4.	調整 EVB 上的 jump 以切換到 NOR Boot Mode。
5.	重新上電。

而要使用 SPI Boot Mode，我們只需要將電路板上的 jump 保持在 Co-operative Mode 的設定，重置 EVB 的電源，然後點擊 qconf 設定與建置工具的 "Run" 按鈕即可。底下將描述使用 SPI Boot Mode 的詳細步驟：


- USB to SPI 轉換板已正確安裝。

  - 若您尚未將 USB to SPI 轉換板與 PC 連接，請參考 :ref:`usb2spi_connect2pc` 。
  - 若您尚未將 USB to SPI 轉換板與 EVB 連接，請參考 :ref:`usb2spi_connect2board` 。
  - 若您尚未安裝 USB to SPI 轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

- EVB 已經切換為 Co-operative Mode。

  .. note:: 

     若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

接著請執行以下步驟：

1.	要使用 SPI Boot Mode 需要修改一些編譯設定。請執行 ``<sdk_root>/build/openrtos/my_project_all.cmd`` 以開啟qconf設定與建置工具。

2. 請先取消勾選 :menuselection:`Screen --> Shows Logo on Bootloader`，再點擊 |build_icon| 按鈕重新建置專案。

   .. image:: /_static/qconf_uncheck_show_logo_on_bootloader.png

3. 重置 EVB 的電源。

4. 等程式建置完成，點擊 |run_icon| 即可透過 USB to SPI 轉換板啟動系統。

.. |run_icon| image:: /_static/qconf_run_icon.png
  :alt: run icon
  :width: 14
  :height: 14

.. TODO 描述點擊run後的背後執行細節。

.. _teraterm_show_debug_msg:

在 PC 上顯示 EVB 透過 UART 送出來的除錯訊息
--------------------------------------------------

要在PC上顯示 EVB 透過UART送出來的除錯訊息，您必須確保：

- USB to SPI 轉換板已正確安裝。

  - 若您尚未將 USB to SPI 轉換板與 PC 連接，請參考 :ref:`usb2spi_connect2pc` 。
  - 若您尚未將 USB to SPI 轉換板與  EVB  連接，請參考 :ref:`usb2spi_connect2board` 。
  - 若您尚未安裝 USB to SPI 轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

接著請執行以下步驟：

1.	開啟Tera Term。若尚未安裝Tera Term或對設定尚有疑問請閱讀 :ref:`install_teraterm` 。

   .. image:: /_static/teraterm_init.png

2. 將 EVB 重置或重新上電。若 Tera Term 設定正確應該可以從 Tera Term 的畫面上看到開機過程的除錯訊息。

   .. image:: /_static/teraterm_boot_message.png

利用 GDB 除錯程式
--------------------------------------

要利用 GDB 除錯程式，您必須確保：

- USB to SPI 轉換板已正確安裝。

  - 若您尚未將 USB to SPI 轉換板與 PC 連接，請參考 :ref:`usb2spi_connect2pc` 。
  - 若您尚未將 USB to SPI 轉換板與 EVB 連接，請參考 :ref:`usb2spi_connect2board` 。
  - 若您尚未安裝 USB to SPI 轉換板驅動程式，請參考 :ref:`install_usb_to_spi_driver` 。

- EVB 已經切換為 Co-operative Mode。

  .. note:: 

     若您無法確定 EVB 是否已經切換為 Co-operative Mode，請參考 :ref:`usb2spi_switch2cooperative` 。

接著請執行以下步驟：

1. 要使用 GDB 除錯程式，必須要修改編譯設定。因此要先執行 ``<sdk_root>/build/openrtos/my_project_all.cmd`` 以開啟 qconf 設定和建置視窗。

2. 將 :menuselection:`Develop Environment --> Develop Environment` 切換為 "Develop Mode"。

   .. image:: /_static/qconf_switch_to_develop_mode.png

3. 取消勾選 :menuselection:`System --> Watch Dog enable`。

   .. image:: /_static/qconf_uncheck_watchdog.png

4. 取消勾選 :menuselection:`System --> Internal Settings --> Memory Debug enable`。

   .. image:: /_static/qconf_uncheck_memory_debug.png

5. 取消勾選 :menuselection:`System --> Internal Settings --> CPU write-back cache enable`。

   .. image:: /_static/qconf_uncheck_writeback.png

6. 取消勾選 :menuselection:`Screen --> Shows Logo on Bootloader`。

   .. image:: /_static/qconf_uncheck_show_logo_on_bootloader.png

7. 將 :menuselection:`SDK --> Build Type` 切換為 "Debug" 或 "DebugRel"。

   .. image:: /_static/qconf_switch_to_debugrel.png

   點擊 |build_icon| 建置專案。

   若先前從未將映像檔燒錄至 NOR Flash，請參考 :ref:`write_image_to_norflash` 將產生出來的映像檔先燒錄至 NOR Flash 內。

8. 執行 ``<sdk_root>/build/openrtos/my_project/project/my_project/jp2.cmd``。

   .. image:: /_static/gdb_jp2.png

9. 執行 ``<sdk_root>/build/openrtos/my_project/project/my_project/gdb.cmd``，即可開啟 gdb console 視窗。您可以在 ``(gdb)`` 命令提示符下輸入 ``c`` 直接運行專案。

   .. image:: /_static/gdb_console_init.png

   .. note:: 
   
      繼續運行程式：
   
         .. code-block:: shell
   
            (gdb) c
   
      在 BootInit 函式進入點設置一個中斷點：
   
         .. code-block:: shell
   
            (gdb) b BootInit
            (gdb) where 
   
   .. note:: 
   
      請參考 `Debugging with GDB <https://sourceware.org/gdb/current/onlinedocs/gdb/index.html>`_ 以獲取更多 gdb 命令資訊。

.. note::

   您也可以參考 :ref:`gui_debug` 所描述的方法，使用GUI介面來除錯韌體。

利用 Visual Studio 在 PC 上模擬運行專案
------------------------------------------------------------

為縮短軟體開發人員開發與除錯的時間，ITE SDK 提供了在 win32 下利用 Visual Studio 2013 在 Windows 環境下模擬專案運行。利用此方式不僅可確保軟體運作獨立性，並且減少簡易修改下放到 EVB/Demo Board 測試的時間。方法如下：

1. 請先安裝VS2013。詳細步驟在此不贅述。
2. 請執行 ``<sdk_root>/build/win32/my_project.cmd`` 以開啟 qconf 設定和建置工具。
3. 出現 qconf 設定和建置工具後請點擊 "儲存 |save_icon| " 並關閉視窗，系統會彈出如下訊息：

   .. |save_icon| image:: /_static/qconf_save_icon.png
     :alt: build icon
     :width: 14
     :height: 14

   .. image:: /_static/win32_build_message.png

   訊息顯示了該程式會配置所有運行時期需要的環境。其中顯示系統偵測到VS 2013，並在windows系統下產生3個虛擬磁碟機 ``A:``、``B:``、``T:``，分別對應建置選單下的 ``private``、``public``、``temp`` 三個目錄。成功後按任一鍵關閉此視窗。此時在 ``<sdk_root>/build/win32`` 下產生了相對應的資料夾，於該資料夾內找到 ``my_project.sln`` 後利用 visual studio 2013 將其打開。

   .. image:: /_static/win32_build_sln.png

4. 在 Visaul Studio 2013 下編譯並運行。成功的話將會出現如下畫面：

   .. image:: /_static/win32_simulate.png

.. note:: 

   若在上次執行 ``<sdk_root>/build/win32/my_project.cmd`` 後電腦曾經重新啟動過，先前的運行時期環境將不再存在，請先執行 ``<sdk_root>/build/win32/my_project.cmd`` 再次重建運行時期環境。再利用 Visual Studio 開啟 ``my_project.sln`` 檔案。
   
.. TODO 似乎沒有解釋得很清楚。

