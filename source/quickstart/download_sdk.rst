﻿.. _download_sdk:

下載 SDK、Toolchain 和相關工具程式
====================================

SOC官網註冊
-------------

欲下載SDK和相關Toolchain之前，您必須先到ITE SOC官網 (https://soc.ite.com.tw/en) 註冊帳號。

1. 請先前往 https://soc.ite.com.tw/en/login，點擊 "+ Registered" ：

    .. image:: /_static/quickstart_register_new_account.png

.. raw:: latex

    \newpage

2. 填入註冊資訊後，點擊 "+ Next" ：

    .. image:: /_static/quickstart_fill_new_account_data.png

3. 系統將會寄一封E-Mail到您的電子郵件信箱，信裡面包含驗證碼。請輸入所收到的驗證碼。

4. 若驗證碼輸入正確，您將看到以下畫面，表示您已經註冊成功。請通知FAE人員做帳號開通並等待一段時間。

    .. image:: /_static/quickstart_register_success.png

.. raw:: latex

    \newpage

登入官網
-------------

1. 帳號開通後，請前往 https://soc.ite.com.tw/en/login，輸入帳號和密碼，點擊 "+ Login" ：

    .. image:: /_static/quickstart_login.png

2. 若登錄成功，您將看到以下畫面：

    .. image:: /_static/quickstart_login_success.png

下載 SDK、Toolchain 和相關工具程式
------------------------------------

1. 在登入後的畫面，點擊上方的 "Developer" ：
2. 點擊 "IT986x Series" 並點擊 "Download" ：

    .. image:: /_static/quickstart_download_sdk.png


3. 選擇 "SDK Standard" (如果您要開發的應用是一般產品) 或 "SDK Video Intercom" (如果您要開發的應用是可視對講之類的產品)，並點擊 "Files" 圖示：

    .. image:: /_static/quickstart_download_standard_sdk.png

4. 這將列出所有相關檔案的名單，其中 ``ITEGCC`` 是 toolchain， ``V 2.4.3.0`` 是 SDK， ``Toolkit`` 則包含其他相關工具程式。點擊 "Download" 圖示下載檔案。

    .. image:: /_static/quickstart_download_file_list.png

.. raw:: latex

    \newpage