﻿.. _sdk_architecture:

ITE SDK架構介紹
********************

SDK目錄介紹
==================

/build
--------------------------------

包含用來建置專案所需的批次檔和 shell script。最終建置的成果也會放置在此目錄中。當您需要新增一個專案時，您必需在此目錄新增幾個針對建置新專案所需的批次檔和 shell script。此目錄裡面又包含 ``_presettings``、``openrtos``、``win32`` 三個子目錄。

_presettings
^^^^^^^^^^^^^^^^^^^^

存放 Kconfig 預設定值。有關 Kconfig 的詳細說明請參考 :ref:`kconfig`。

openrtos
^^^^^^^^^^^^^^^^^^^^

包含用來建置能在OpenRTOS™ (https://www.freertos.org/openrtos.html) 上運行的專案所需的批次檔。最終建置的成果也會放置在此目錄中。此目錄內的批次檔主要分為六種：

- ``<project_name>.cmd``

  每個專案都有一個對應的 ``<project_name>.cmd`` 。點擊此檔案將顯示 qconf 建置與設定工具，以便您開始設定和建置專案。

  此檔案底層機制主要是先將CFG_PROJECT環境變數的值設為 ``<project_name>`` ，接著呼叫 ``build.cmd`` 。其中CFG_PROJECT環境變數被用來產生建置目錄和尋找 ``<sdk_root>/project`` 目錄下同名的子目錄。因此正確的命名 ``<project_name>`` 很重要。

  舉例來說， ``my_project.cmd`` 會將 ``CFG_PROJECT`` 環境變數的值設為 ``my_project`` 。在建置時建置系統會尋找 ``<sdk_root>/project/my_project`` 目錄下的檔案，並建立 ``<sdk_root>/build/openrtos/my_project`` 目錄。

  要自己手工建立一個新專案，可以複製一份 ``build_projects.cmd`` 並將其重新命名為 ``<project_name>.cmd``。

- ``<project_name>_all.cmd``

  ``<project_name>_all.cmd`` 的作用只是比 ``<project_name>.cmd`` 額外設置兩個環境變數 – BOOTLOADER 和 ARMLITECODEC，使 ``<project_name>_all.cmd`` 比 ``<project_name>.cmd`` 多建置了 bootloader 和 codec 專案。

- ``build.cmd``

  此檔案將 BUILD_CMD 環境變數設置為存放實際建置命令的批次檔，即 ``<sdk_root>/build/openrtos/post_build.cmd`` 。並將RUN_CMD環境變數設置為存放實際運行專案命令的批次檔，即 ``<sdk_root>/build/openrtos/<project_name>/project/<project_name>/exec.cmd`` 。最後執行qconf或mconf啟動設定和建置介面。

  qconf 會在啟動時讀取 BUILD_CMD 環境變數和 RUN_CMD 環境變數的值，並在您點擊 |build_icon| 按鈕時執行 BUILD_CMD 環境變數所參照的批次檔，以及在您點擊 |run_icon| 按鈕時執行RUN_CMD環境變數所參照的批次檔。

.. |run_icon| image:: /_static/qconf_run_icon.png
  :alt: run icon
  :width: 14
  :height: 14

- ``common.cmd``

  主要目的是設置PATH環境變數，以便在建置過程中無須輸入完整路徑便能找到所需的toolchain、工具軟體以及相關的dll。

- ``post_build.cmd``

  實際用來建置專案的批次檔。此檔案首先利用<sdk_root>/tool/bin/cmake.exe來產生makefile，再利用<sdk_root>/tool/bin/make.exe建置專案。

- ``console.cmd``

  工具類批次檔，方便您在命令列模式下運行toolchain或工具程式時使用。此批次檔首先利用common.cmd設置PATH環境變數，再顯示Windows命令列視窗。

  會需要先利用common.cmd設置PATH環境變數的原因是因為有些工具軟體是在cygwin環境下編譯而成，這些軟體在執行時需要載入cygwin相關的dll檔。這些dll檔案位於<sdk_root>/tool/bin目錄。若您沒有將<sdk_root>/tool/bin目錄加入PATH環境變數，則您將無法運行這些在cygwin環境下編譯而成的軟體。而common.cmd提供了臨時將<sdk_root>/tool/bin目錄加入PATH環境變數的解決方案。
	
win32
^^^^^^^^^^^^^^^^^^^^

包含用來建置能在Windows™上運行的專案所需的批次檔。最終建置的成果也會放置在此目錄中。

/data
--------------------------------

包含所有專案共用的UI資源檔。

/doc
--------------------------------

包含與SDK有關的文件。

/openrtos
--------------------------------

包含openrtos原始程式碼，以及啟動程式碼、CPU相關程式碼。

/project
--------------------------------

包含各個專案特有的程式碼。

/sdk
-------------------------------

包含驅動程式、共享函式庫、第三方程式。

/tool
-------------------------------

包含編譯過程中所需要的輔助工具。

/win32
-------------------------------

包含要建置win32時，所需的底層函式庫。

CMake
================================

ITE SDK 採用 CMake 作為其建置系統產生器。有關 CMake 如何使用的訊息，請見 CMake 官網文件(https://cmake.org/documentation/)。

.. _kconfig:

Kconfig
==================

ITE SDK 採用 Kconfig 來建構其設定選項。有關 Kconfig 語法細節，請見 https://www.kernel.org/doc/Documentation/kbuild/kconfig-language.txt。

.. _build_flow:

建置流程
==================

底下描述當您透過 qconf 建置與設定工具點擊 |build_icon| 按鈕建置專案時，底層實際發生的事情。

.. |build_icon| image:: /_static/qconf_build_icon.png
  :alt: build icon
  :width: 14
  :height: 14

1. 使用 CMake 產生 makefile。見 ``<sdk_root>/build/openrtos/post_build.cmd``。

   .. code-block:: shell

      if exist CMakeCache.txt (
          cmake.exe -G"Unix Makefiles" "%CMAKE_SOURCE_DIR%"
      ) else (
          cmake.exe -G"Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE="%CFG_TOOLCHAIN_FILE%" "%CMAKE_SOURCE_DIR%"
      )
    
   其中 ``-DCMAKE_TOOLCHAIN_FILE="%CFG_TOOLCHAIN_FILE%"`` 選項用在當您需要使用 CMake 來 cross compile 時必須透過該選項指定 toolchain 相關訊息。當 CMake 執行過一次後並產生了 ``CMakeCache.txt`` 檔案， CMake 便可以從該檔案取得 toolchain 相關訊息，因此無須指定 ``-DCMAKE_TOOLCHAIN_FILE="%CFG_TOOLCHAIN_FILE%"`` 選項。

   CMake 產生的 makefile 位於 ``<sdk_root>/build/openrtos/<project_name>/Makefile`` 。

2. 使用 make 產生 elf format binary code - ``<project_name>`` 。見 ``<sdk_root>/build/openrtos/post_build.cmd`` 。所產生的 elf format binary code 位於 ``<sdk_root>/build/openrtos/<project_name>/project/<project_name>/<project_name>`` 。

3. 使用 objcopy 移除 elf format binary code 的 elf header，以產生 raw binary code without any header 。見 ``<sdk_root>/sdk/build.cmake`` 。

   .. code-block:: shell

      add_custom_command(
          TARGET ${CMAKE_PROJECT_NAME}
          POST_BUILD
          COMMAND ${OBJCOPY}
          ARGS -O binary ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME} ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME}.bin
      )

   最後產生的檔案位於 ``<sdk_root>/build/openrtos/<project_name>/project/<project_name>/<project_name>.bin`` 。

4. 使用 ``<sdk_root>/tool/bin/mkrom.exe`` 產生 ROM 檔。見 ``<sdk_root>/sdk/build.cmake`` 。

   .. code-block:: shell

      add_custom_command(
          TARGET ${CMAKE_PROJECT_NAME} POST_BUILD
          COMMAND mkrom
          ARGS ${args} ${PROJECT_SOURCE_DIR}/sdk/target/ram/${CFG_RAM_INIT_SCRIPT} ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME}.bin ${CMAKE_CURRENT_BINARY_DIR}/kproc.sys
      )

   mkrom的使用範例如下：

   .. code-block::

       mkrom -z -b 512K init.scr input.bin output.rom

   上述命令輸入 ``init.scr`` 和 ``input.bin`` 給 mkrom 並要求 mkrom 輸出 ``output.rom`` 。 ``-z`` 選項用來指定要壓縮 ``input.bin`` 。輸出的 ROM 檔結構如下：

   .. image:: /_static/file_struct_rom.png

   所產生的檔案位於 ``<sdk_root>/build/openrtos/<project_name>/project/<project_name>/kproc.sys`` 。此檔案可燒錄至 NOR flash 內，透過 NOR Boot Mode 啟動。

   .. note::
   
      在 NOR Boot Mode 下， ITE chip 內的 Mask Rom code 會從 NOR flash 的 0x0 位置處尋找 ITE ROM header 並驗證該 header 的格式是否正確。接著從該 header 找到 initial script 的位址，載入該 initial script 並根據 initial script 的內容對適當的暫存器寫值。最後，根據 header 的內容找出 compressed binary code 在 NOR flash 中的位址並將其解壓縮後載入到記憶體 0x0 的位址。接著讓 CPU 從記憶體 0x0 的位址開始擷取指令並執行。
      
      若缺少正確的 ITE ROM header ， Mask Rom code 將無法找到 binary code 在 NOR flash 中的位址並將其載入到記憶體。因此系統也將無法啟動。

   .. note::

      Initial script 是一個使用 C like script language 撰寫的 script，內容包含一堆暫存器位址和暫存器數值配對，用來設定 Chip 的初始 clock ，以及初始化 memory controller 的設定。

   建置 ROM 時實際使用的 initial script 可在 qconf 設定與建置工具中的 :menuselection:`System --> RAM initial script` 欄位中設定。

   .. image:: /_static/qconf_init_script.png

5. 使用 ``<sdk_root>/tool/bin/pkgtool.exe`` 打包出 pkg 檔。命令如下：

   .. code-block::

       D:/ite_sdk/tool/bin/pkgtool.exe 
           -o ITEPKG03.PKG 
           --nor-unformatted-data2 D:/ite_sdk/build/openrtos/my_project/project/my_project/kproc.sys 
           --nor-unformatted-data2-pos 0x80000 
           --nor-unformatted-size 0x2D0000 
           --nor-partiton0-dir D:/ite_sdk/build/openrtos/my_project/data/private 
           --nor-partiton1-dir D:/ite_sdk/build/openrtos/my_project/data/public 
           --nor-partiton2-dir D:/ite_sdk/build/openrtos/my_project/data/temp 
           --nor-partiton0-size 0xDC0000 
           --nor-partiton1-size 0x40000 
           --nor-partiton2-size 0xF0000 
           --nor-partiton3-size 0 
           --partition 
           --version 1.0 
           --nor-unformatted-data1 D:/ite_sdk/build/openrtos/my_project/project/bootloader/kproc.sys 
           --key 0

   其中 ``-o ITEPKG03.PKG`` 表示輸出名稱為 ``ITEPKG03.PKG`` 的檔案。 ``--nor-unformatted-data1 …/bootloader/kproc.sys --nor-unformatted-data2 ../my_project/kproc.sys --nor-unformatted-data2-pos 0x80000`` 表示將 ``…/bootloader/kproc.sys`` 放置在 0x0 的位置，將 ``../my_project/kproc.sys`` 放置在 0x80000 的位置。 ``--nor-unformatted-size 0x2D0000`` 表示 Reserved Area 大小為0x2D0000 bytes。也表示 File System Area 從 0x2D0000 的位置開始。``--nor-partiton0-dir D:\ite_sdk\build\openrtos\my_project\data\private --nor-partiton0-size 0xDC0000`` 表示 File System Area 中第 1 個 partition 的內容來自 ``D:\ite_sdk\build\openrtos\my_project\data\private`` ，大小為 0xDC0000。

   .. note::

      有關Reserved Area和File System Area的詳細描述見 :ref:`norflash_space_layout`。

6. 利用 ``<sdk_root>/tool/bin/pkgtool.exe`` 將 pkg 檔轉換為 rom 檔。命令如下：

   .. code-block:: 

       D:\ite_sdk\tool\bin\pkgtool.exe -s 0x1200000 -o ITE_NOR.ROM -n ITEPKG03.PKG

   其中 ``-s 0x1200000 -o ITE_NOR.ROM`` 表示輸出檔案名稱為 ``ITE_NOR.ROM`` ，輸出大小為 0x1200000 bytes。 ``-n ITEPKG03.PKG`` 表示輸入檔案名稱為 ``ITEPKG03.PKG`` 。

.. _norflash_space_layout:

NOR flash 儲存空間佈局
============================

NOR flash 完整空間佈局
---------------------------

此 SDK 預設使用 NOR flash 作為系統儲存空間，並將儲存空間劃分為保留區和檔案系統兩大區域。其中保留區用於存放 Bootloader、MAC Address 與 Firmware Image 。除了保留區以外的剩餘空間則供檔案系統使用，並可分出最多四個磁碟分割區。 :numref:`file_struct_rom_full` 顯示的是室內機的 NOR flash 儲存空間佈局，採用下列 ``pkgtool.exe`` 命令打包而成。在規劃時需注意各區域的大小與位置，不能發生互相重疊情況。由於 NOR flash 地址對齊限制，每個位置須要與 64 KBytes 對齊。

.. code-block:: 

    D:\ite_sdk\tool\bin\pkgtool.exe 
        -o ITEPKG03.PKG 
        --nor-unformatted-data2 D:\ite_sdk\build\openrtos\my_project\project\my_project\kproc.sys 
        --nor-unformatted-data2-pos 0x80000 
        --nor-unformatted-size 0x2D0000 
        --nor-partiton0-dir D:\ite_sdk\build\openrtos\my_project\data\private 
        --nor-partiton1-dir D:\ite_sdk\build\openrtos\my_project\data\public 
        --nor-partiton2-dir D:\ite_sdk\build\openrtos\my_project\data\temp 
        --nor-partiton0-size 0xDC0000 
        --nor-partiton1-size 0x40000 
        --nor-partiton2-size 0xF0000 
        --nor-partiton3-size 0 
        --partition 
        --version 1.0 
        --nor-unformatted-data1 D:\ite_sdk\build\openrtos\my_project\project\bootloader\kproc.sys 
        --key 0

.. note::

    MAC address放置的位址不是透過PKG tool指定的。

.. figure:: /_static/file_struct_rom_full.png
   :name: file_struct_rom_full

   NOR flash 儲存空間的佈局

保留區內儲存不適合放到檔案系統的資料，目前包含 Bootloader、MAC Address 與 Firmware Image。保留區的大小由 qconf 設定與建置工具裡的 :menuselection:`File System --> NOR reserved size` 的值決定。如 :numref:`qconf_reserved_area_size` 所示。

.. figure:: /_static/qconf_reserved_area_size.png
   :name: qconf_reserved_area_size

   保留區的大小

Bootloader
^^^^^^^^^^^^^^^^^^^^^^

一定是放在位置 0x0 的地方，才可以進行開機。 Bootloader 的大小可以從 ``<sdk_root>/build/openrtos/<project_name>/project/bootloader/kproc.sys`` 的檔案大小獲得。

MAC Address
^^^^^^^^^^^^^^^^^^^^^^

通常會放在 Bootloader 與 Firmware Image 之間，由 qconf 設定與建置工具裡的 :menuselection:`Network --> Enable Network --> Enable Ethernet module --> MAC address data is stored in static storage --> Position of MAC address data` 來指定儲存的位置，如 :numref:`qconf_mac_addr_store_pos` 所示。MAC address 佔用 8 個bytes。

.. figure:: /_static/qconf_mac_addr_store_pos.png
   :name: qconf_mac_addr_store_pos

   MAC Address儲存位置設定

Firmware Image
^^^^^^^^^^^^^^^^^^^^^^

位置由 qconf 設定與建置工具裡的 :menuselection:`Upgrade --> Upgrade Image --> Image position` 來指定，如 :numref:`qconf_fw_img_pos` 所示。Firmware Image 的大小可以從 ``<sdk_root>/build/openrtos/<project_name>/project/<project_name>/kproc.sys`` 的檔案大小獲得。

.. figure:: /_static/qconf_fw_img_pos.png
   :name: qconf_fw_img_pos

   Firmware Image 儲存位置設定

檔案系統分割區
----------------------------

檔案系統分割區內的檔案系統採用 FAT 格式。其第一個分割區緊鄰在保留區的最後一個 byte 之後。每個分割區之間不能有空隙，必須相鄰。每個分割區的位置由 qconf 設定與建置工具裡的 :menuselection:`File System --> NOR disk partition # --> The partition # size of NOR disk` 來指定，如 :numref:`qconf_nor_disk_partition` 所示。在最後一個 Partition 裡可指定 size 0 來表示使用剩餘的所有空間。

.. figure:: /_static/qconf_nor_disk_partition.png
   :name: qconf_nor_disk_partition

   內部檔案系統分割區設定

分割區的順序即是磁碟機掛入的順序，我們可以使用預先定義好的磁碟機代號來指定每個分割區的用途，如 :numref:`qconf_disk_parition_id` 所示。

.. figure:: /_static/qconf_disk_parition_id.png
    :name: qconf_disk_parition_id

    磁碟機代號設定

產生NOR flash映像檔
-------------------------

若需要產生 NOR flash 映像檔，請在 qconf 設定與建置工具裡的 :menuselection:`Upgrade --> Generate NOR image --> NOR image size` 欄位指定指定映像檔的大小，並請在 qconf 設定與建置工具裡的 :menuselection:`Upgrade --> Generate NOR image --> Upgrade NOR image filename` 欄位內指定所產生的映像檔名稱。如 :numref:`qconf_nor_image_size` 所示。

.. figure:: /_static/qconf_nor_image_size.png
   :name: qconf_nor_image_size

   NOR flash映像檔大小設定

開機流程
=======================

開機流程會根據您是採用 NOR Boot Mode 或 SPI Boot Mode 而有所不同。底下將針對這兩種 Boot 模式的開機流程進行講解。

.. note::

   若您不知道 NOR Boot Mode 與 SPI Boot Mode 的區別，請見 :ref:`usb2spi_switch2cooperative` 。

NOR Boot Mode 開機流程
------------------------

若系統是採用 NOR Boot Mode 啟動，晶片內部有一個 mask rom 會被映射到記憶體空間 0x0 的位址。該 mask rom 內建啟動程式。在上電時， CPU 會先到該 mask rom 取出第一條指令執行。這過程稱為第一階段啟動。

Mask rom 內的程式會從 NOR Flash 內 0x0 的位址讀取一段資料至記憶體，並檢查該內容是否符合正確的 ITE ROM Header 格式(見 :numref:`file_struct_rom_full` )。若正確，則執行緊鄰在 ITE ROM Header 之後的 initial script。接著將 Compressed Binary Code 解壓縮並放到 RAM 最前面的位置。在解壓縮完成後，RAM 會被重映射到記憶體空間 0x0 的位址，CPU 的 Program Counter 被修改為 0x0。因此 CPU 將從 RAM 最開頭的地方取指令執行。接下來就開始了第二階段啟動。

第二階段啟動所執行的程式碼就是專案實際程式碼，通常從 ``<sdk_root>/openrtos/boot/fa626/startup.S`` 內的程式開始執行。該檔案以組合語言撰寫。在該段程式碼，會設定好初始的中斷向量表，以及各個 ARM 模式下所需要用到的 stack。接著跳到 ``BootInit()`` 函式(位於 ``<sdk_root>/openrtos/boot/init.c`` 內)執行。在執行完 ``BootInit()`` 函式後，就跳到 ``main()`` 函式執行。

SPI Boot Mode 開機流程
-----------------------

若系統是採用 SPI Boot Mode 啟動，RAM 將會被映射到記憶體空間 0x0 的位址。在您點擊 qconf 設定與建置工具上的 |run_icon| 按鈕時，將執行專案專屬建置目錄下的 run.cmd 批次檔。譬如您的專案名稱為 ``my_project``，則 ``<sdk_root>/build/openrtos/my_project/project/my_project/run.cmd`` 將被執行。該批次檔首先透過 ``glamomem -t glamomem.dat -i –q`` 命令執行 initial script。Initial script 的檔案路徑在 ``<sdk_root>/build/openrtos/my_project/project/my_project/glamomem.dat`` 內指定。您可以透過修改 qconf 設定與建置工具的 :menuselection:`Debug --> Debug initial script` 選項來改變這裡要執行的 initial script。

.. attention::

   這裡的 initial script 與 NOR Boot Mode 所執行的 initial script 並不相同。若您要修改 NOR Boot Mode 所執行的 initial script，請看 :ref:`build_flow` 。

.. image:: /_static/qconf_dbg_init_script.png

在執行完 initial script 之後，將執行 ``glamomem -t glamomem.dat -l my_project.bin`` 命令把 ``my_project.bin`` 檔案內容載入到記憶體最前面的位置。接著執行 ``glamomem -t glamomem.dat -R 0x00000001 -a 0xd8300000`` 命令將暫存器 0xd8300000 的值寫為 1 以啟動 CPU 運行。CPU 將從記憶體 0x0 的位址抓取指令執行，接下來的步驟與 NOR Boot Mode 的第二階段啟動相同。