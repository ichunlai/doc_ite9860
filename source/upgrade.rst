.. _upgrade:

韌體更新方法
=======================================

IT986x 提供數種韌體更新方式。本篇介紹所有板端韌體更新方法，供使用者依照當前狀況挑選合適的更新方法。

韌體更新方法整理
----------------------------------------------

韌體更新的目標為：將修改後並重新透過 compiler 建置出的 ``.PKG`` 或 ``.ROM`` 檔燒錄至板子上的Flash內，以達到系統更新。而更新韌體的方法有很多種，分成兩類：

- 開發時期韌體更新
  
  - 透過USB2SPI工具燒錄

- 開機時期韌體更新

  - 隨身碟更新
  - SD卡更新

開發時期韌體更新
----------------------------------------------

透過USB2SPI工具燒錄
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

此方法經常用在第一次燒錄，或是遇到任何更新方法都無法燒錄時的最終手段。詳細步驟請參考 :ref:`write_image_to_norflash` 和 :ref:`write_image_to_nandflash` 。

開機時期韌體更新
----------------------------------------------

開機時期韌體更新機制是透過 bootloader 完成的。因此請先遵循以下步驟以確認您的專案有啟用 bootloader。

1. 請執行 ``<sdk_root>/build/openrtos/<project_name>.cmd`` 或 ``<sdk_root>/build/openrtos/<project_name>_all.cmd`` 以開啟 qconf 設定與建置工具。
2. 點擊 "Develop Environment"。確認 "Enable bootloader or not" 必須已勾選。

   .. image:: /_static/qconf_check_enable_bootloader.png

接著請按照各小節的描述操作。

隨身碟更新
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

此為最常使用也是最基本的更新方法，只要在 qconf 設定與建置工具中點擊 :menuselection:`Peripheral --> USB# Enable` 將對應的 USB port 啟用為 USB Host Mode。

.. image:: /_static/qconf_check_enable_usb0.png

在開機前記得把編譯完的 ``XXX.PKG`` 檔案放進隨身碟中，並將隨身碟插入到對應的 USB port 後再開機。

.. note::

   ``XXX.PKG`` 檔案名稱必須是 qconf 設定與建置工具中 :menuselection:`Upgrade --> Upgrade package filename` 設定選項所設定的名稱。另外，USB 隨身碟必須格式化為 FAT 格式。

   .. image::  /_static/qconf_set_package_filename.png

SD 卡更新
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

設定方式類似隨身碟更新，首先在 qconf 設定與建置工具中點擊 :menuselection:`Storage --> SD# Enable` 啟用對應的 SD device。

.. image:: /_static/qconf_check_enable_sd0.png

接著點擊 "GPIO"，確認與 SD 卡槽相關的 GPIO 設定是否正確。若設定錯誤，系統將無法偵測到 SD 卡槽而導致更新失敗。

.. image:: /_static/qconf_set_sd0_gpio.png

在開機前記得把編譯完的 ``XXX.PKG`` 檔案放進 SD Card 中，並將 SD Card 插入到對應的卡槽後再開機。

.. note::

   檔名必須是 qconf 設定與建置工具中 :menuselection:`Upgrade --> Upgrade package filename` 設定選項所設定的名稱。另外，SD Card 必須格式化為 FAT 格式。
