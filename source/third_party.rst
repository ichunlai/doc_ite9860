.. _third_party:

第三方函式庫介紹
************************

本章將針對 IT986x SDKs 已移植的第三方函式庫的使用做細部說明，並給予測試程式範例。

.. toctree::
   :glob:

   third_party/uvc.rst
