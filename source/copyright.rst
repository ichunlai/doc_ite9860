﻿.. _copyright:

Copyright |copy| 2020 ITE Tech. Inc.

| This is a Preliminary document release. All specifications are subject to change without notice.
| The material contained in this document supersedes all previous material issued for the products herein referenced. Please contact ITE Tech. Inc. for the latest document(s). 
|
| All sales are subject to ITE's Standard Terms and Conditions, a copy of which is included in the back of this document.
|
| ITE is a trademark of ITE Tech. Inc.
| All other trademarks are claimed by their respective owners.
| All specifications are subject to change without notice.
|
| Additional copies of this manual or other ITE literature may be obtained from:
|
|     ITE Tech. Inc.	
|     Marketing Department	
|     3F, No. 13, Chuangsin 1st Rd., 
|     Science Park, Hsinchu 30076, Taiwan, R.O.C.
|
| If you have any marketing or sales questions, please contact:
| ITE Taiwan:
| E-mail:  support@soc.ite.com.tw 
|
| You may also find the local sales representative nearest you on the ITE web site.
|
| To find out more about ITE SoC, visit our World Wide Web at:
| http://soc.ite.com.tw
| 
| Or e-mail support@soc.ite.com.tw for more product information/services 

.. |copy|   unicode:: U+000A9 .. COPYRIGHT SIGN
