..
  負責人: 鴻瑜

.. _touch_panel_develop:

觸控驅動程式的撰寫指南
=====================================

本節針對用戶在使用新的觸控裝置且現有 SDK 不支援時，在需要撰寫驅動程式的狀況下提供一個撰寫指南。

準備工作
------------------------

使用 test_touch project (位於 ``<sdk_root>/project/test_touch`` )作為驗證環境。

1. 建立新的 TP driver (為該驅動命名，譬如： ``tp001`` )
  - 複製一份 ``<sdk_root>/sdk/share/tslib/plugins/ft5316-raw.c`` ，並將檔名改成 ``tp001-raw.c`` (檔名小寫)。
  - 將 ``tp001-raw.c`` 檔案內所有 ``FT5316`` 字串取代為 ``tp001`` 。
2. 建立新的 ``.conf`` 檔案
  - 複製一份 ``<sdk_root>/sdk/target/touch/ft5316.conf`` ，並將檔名改成 ``tp001.conf`` (檔名小寫)。
  - 將 ``tp001.conf`` 檔案內所有 ``FT5316`` 字串取代為 ``tp001`` 。
3. 修改 ``<sdk_root>/sdk/share/tslib/plugins/plugins.h`` 。
 - 新增一行 ``TSLIB_DECLARE_MODULE(tp001);`` 。
4. 在使用 qconf 設定與建置工具建置專案時，記得將設定 :menuselection:`Peripheral --> Touch Enable --> Touch module` 修改為 "tp001" 。

實作 (以 GT911 單指觸控為例)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
PORTING 前
""""""""""""""""""""""""

1. 假設 H/W 線路上的問題都已解決，即 INT 有反應，I2C 有 ACK。

  - 有些 INT 沒有反應是因為沒有 power-on 流程，或是要 program 之後 INT 才有反應。
  - 但 I2C 沒有 ACK，可能是 slave address 錯誤，或是還有執行 power-on 流程(EX: ``GT911`` )。

2. 所謂實作，就是指需要實作以下 6 個 vendor FUNCTION。
  -	``_tpInitSpec_vendor()``
  -	``_tpDoPowerOnSeq_vendor()``
  -	``_tpDoInitProgram_vendor()``
  -	``_tpReadPointBuffer_vendor()``
  -	``_tpParseRawPxy_vendor()``
  -	``_tpParseKey_vendor()``

  其中 ``_tpDoPowerOnSeq_vendor()`` 和 ``_tpDoInitProgram_vendor()`` 和 ``_tpParseKey_vendor()`` 為非必要實作的 function，視 TP 實際應用狀況而定。尤其是 ``_tpParseKey_vendor()`` ，一般 TP 都沒有隨附 TOUCH KEY 的功能，所以可以不必理會此 function。而大部分 TP 都是 POWER-ON 之後即可正常運作，此類 TP 也可不必實作 ``_tpDoPowerOnSeq_vendor()`` 和 ``_tpDoInitProgram_vendor()`` 。而 ``_tpInitSpec_vendor()``、``_tpReadPointBuffer_vendor()``、``_tpParseRawPxy_vendor()`` 為必要實作之 function。

實作 ``_tpInitSpec_vendor()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

依據 ``TP_SPEC`` 結構成員的定義，填入適當的值。底下列出 ``TP_SPEC`` 結構成員說明。

``tpIntPin``

  INT pin 的 GPIO number。

``tpIntActiveState``

  INT active 狀態的設定，1: Active High，0: Active Low。

``tpIntTriggerType``

  中斷觸發的方式，0: edge trigger，1: level trigger。

``tpWakeUpPin``

  Wake-Up pin GPIO number，-1: 表示不使用 Wake-Up pin。

``tpResetPin``

  Reset pin GPIO number，-1: 表示不使用 reset pin。

``tpIntrType``

  INT PIN 發送中斷的行為，ITE 定義了三種 TYPE(如下圖)：
 
  .. image:: /_static/diagram_touch_panel_interrupt_type.png

  - 0：表示 TYPE I，keep state when touch down，手指接觸 TP 時，一直維持 INT ACTIVE 的  狀態，直到手指離開 TP 為止(EX: ``ZT2083`` )。
  - 1：表示 TYPE II，手指接觸 TP 的期間，INT 會持續 HIGH/LOW 變化，直到手指離開 TP 為止 (EX: ``FT5316`` )。
  - 2：表示 TYPE III，當手指接觸 TP 時，INT 會一直 ACTIVE 的狀態，即使手指離開 TP 也不會恢復，除非對 TP 下 clear 命令(或是 read data 之後)才會恢復成 NON-ACTIVE 狀態(EX: ``CYTMA568`` )。
  請依據 TP 實際狀況設定正確的型式。

``tpInterface``

  介面型式，0：I2C，1：SPI，2：其他。

``tpI2cDeviceId``

  I2C device ID(slave address)，假如 TP 是 I2C 介面。

``tpHasTouchKey``

  0：NO touch key，1：TP 自帶觸控按鍵(EX： ``CYTMA568``)。

``tpIntUseIsr``

  0：polling INT signal，1：INT use interrupt。

``tpMaxFingerNum``

  此 TP 原生最大手指數目(非 S/W 限制最大手指數目) 。

``tpIntActiveMaxIdleTime``

  當 INT ACTIVE 的時間超過此設定值時，driver 會強制 INT 恢復成 NOT ACTIVE 狀態，避免有些 TP (EX：tpIntrType = 2 的 TP)無法自行 TOUCH-UP 的 BUG。預設值：33ms，cytma568： 100ms。

``tpMaxRawX``

  TP resolution of X。

``tpMaxRawY``

  TP resolution of Y。

``tpScreenX``

  Screen resolution of X (LCD WIDTH)。

``tpScreenY``

  Screen resolution of Y (LCD HEIGHT) 。

``tpCvtSwapXY``

  TP 座標的 X 軸與 Y 軸置換。

``tpCvtReverseX``

  TP 座標的 X 軸反向。

``tpCvtReverseY``

  TP 座標的 X 軸反向。

``tpCvtScaleX``

  目前暫無效用，預設值為 0。

``tpCvtScaleY``

  目前暫無效用，預設值為 0。

``tpEnTchPressure``

  目前暫無效用，預設值為 0。

``tpSampleNum``

  TP driver 能輸出的最大手指數目(S/W 限制最大手指數目)。
  
  - 0：NO scense。
  - 1：single touch。
  - 2 ~ 10：multi-touch ("tpSampleNum" 必須 <= "tpMaxFingerNum")。

``tpSampleRate``

  設定取樣的速率，時間單位：Millisecond，範圍為 8 ~ 33 ms (120 ~ 30 FPS)。

``tpIdleTime``

  每次檢查 TP 樣本更新的時間間隔，預設值為 2 ms。

  因為 TP driver 會 create 一條 thread 專門 check INT 是否 ACTIVE，tpIdleTime 是此thread 的 sleep time，若沒有 IDLE 2ms，會使此 thread 過度 busy，導致其他 thread 效能降低。若設定時間太長，會使觸控反應不夠即時。

``tpIdleTimeB4Init``

  TP driver 在初始化之前的 IDLE 間隔，預設值為 10 ms。

  原因同 tpIdleTime，只是 TP 初始化之前，沒有觸控須即時反應的問題。

``tpReadChipRegCnt``

  讀取 TP point buffer 所需讀取的 byte 數。

``tpHasPowerOnSeq``

  設定啟用 power-on sequence。

``tpNeedProgB4Init``

  設定啟用初始化編程流程。有些 TP 必須經過初始化編程的流程，TP 才能正常運作(EX： ``GT911`` ) 。

``tpNeedAutoTouchUp``

  設定啟用自動 TOUCH-UP 功能；為要避免某些 TP(tpIntrType = 1 的 TP)漏發 TOUCH-UP EVENT 的問題。

  0：關閉，1：開啟。

``tpIntPullEnable``

  設定開啟 INT PIN 的 CHIP 內部 PULL-UP 功能。有些 TP 的 INT pin 不能有 PULL-UP/PULL-DOWN 電路(EX： ``GT911`` 的 ``INT state`` 會影響 TP 的 I2C slave address)。

  0：關閉 PULL 功能，1：開啟 PULL 功能。

實作 ``_tpDoPowerOnSeq_vendor()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

實作目的：上電初始化。經由正確的上電時序，以便執行 TP 初始化編程，或是直接進入觸控運作狀態。

在實作 power-on sequence 之前，請先參閱各 TP 之 datasheet 的 power-on sequence 說明。並了解 IT9860 GPIO PIN 的控制方式 (請參閱 :ref:`gpio`)。

有一部分的 TP chip 不需要 power-on sequence (EX：FT5316、IT7260、ILI2118A、ST1633、ZET6221 等)，這些 TP chip 可不實作此 function。大部分的 power-on sequence 是指對 reset pin 的信號控制，通常此 reset-pin 的控制流程如下所示：

1. 先讓 reset-pin 保持在 HIGH state A ms。
2. 然後將 reset-pin PULL-LOW B ms。
3. 最後再讓 reset-pin 保持在 HIGH state C ms。

有的 TP chip 會要求 reset 之後要等 C ms (EX： ``GT911`` 要求 50ms 之後，HOST 始可發送 I2C 訊號) 之後才能開始送出第一筆 I2C 的指令，藉由 "reset-pin KEEP-HIGH for C ms" 達到延遲發送 I2C 訊號的目的。

通常透過這種 HIGH-LOW-HIGH 格式的訊號控制，大都能滿足這類 reset pin 的 power-on sequence 的流程 (EX：CYTMA568、FT6436、SIS9255、ST1727、ZET7235 等)。

少部分的 power-on sequence 會有較複雜的信號控制，例如 GT911。根據 GT911 datasheet，power-on 流程需搭配 INT 信號，來決定 I2C 的 slave address (參照下圖)。

.. image:: /_static/diagram_gt911_timing.png

依據 GT911 所給的時序圖，可得 power-on sequence 如下：

1. 一開始 INT & RESET 同時 output 為 LOW 1 ms (datasheet 要求 > 100us)。
2. 然後把 RESET 輸出為 HIGH 55ms。
3. 最後將 INT 設成 GPIO 輸入模式 50ms (datasheet 要求 INT 轉成 INPUT MODE 50ms 以後，才能執行 I2C 指令)。

實作 ``_tpDoInitProgram_vendor()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

實作目的：初始化編程，使用觸控功能之前，必需先存取過 TP CHIP 才能正常運作。有的 TP 是為寫入設定值、configuration、甚至整個 F/W；有的需要讀取 configuration (如 resolution、direction 等)；有的是為喚醒 TP 進入工作模式。

大部分的 TP CHIP 只要 POWER-ON 之後，就可以進行觸控的功能，不需要 PROGRAM，甚至有的 TP，其驅動連 I2C WRITE 都沒執行過，僅少數 TP 需要實作此 function (EX： CYTMA568、GSL3680、GSL1691、gt5688、GT911、ILI2118A 等) 。

以 GT911 為例，必須讀取其 configuration，才能得知其 resolution 以及 direction 以及 INT active HIGH/LOW 的相關資訊。

實作 ``_tpReadPointBuffer_vendor()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

實作目的：讀取 TP register，並回傳 BUFFER DATA。

大部分的 TP chip 的座標資訊都可在一次的 I2C 讀取中完成，僅極少數是需要透過多次讀取才能讀完所需的 register，請應用 S/W 技巧將這些 data 併入一個 buffer 中，傳給下一級函數分析(即 ``_tpParseRawPxy_vendor()`` )。

此BUFFER應包含

1. 資料是否有效
2. 觸控手指數(單指或多指)
3. XY座標
4. EVENT資訊(DOWN/UP等)

以 GT911 為例，每次 INT 拉 LOW 時，必須先 check 此 INT 是否有效，若無效，則需忽略此次的 INT 信號(並維持先前觸控狀態)；若有效，則讀取座標資料("GT911編程指南文件" 之 "三、寄存器列表")，並在最後將 buffer status (0x814E) 寫為 0(參考 "GT911編程指南文件" 之 "五、座標讀取")。

實作 ``_tpParseRawPxy_vendor()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

實作目的：從 ``_tpReadPointBuffer_vendor()`` 接收 buffer，並分析 buffer 的資訊，轉換成 ts_sample 格式。

ts_sample 結構成員(定義在 ``＜sdk_root>/sdk/include/tslib.h`` )描述如下：

- x: X座標值，範圍0 ~ (X resolution - 1) 。
- y: Y座標值，範圍0 ~ (Y resolution - 1) 。
- pressure: 手指觸碰的壓力值，目前僅支援 0 跟 1 (0 代表沒觸碰，1 代表有觸碰) 。
- finger: 此 INT ACTIVE 時的觸碰點數，若是單指觸控的 TP，或是 S/W 強制設定為單指觸控，則僅回報 0 或 1。
- id: 若單指觸控，則 ID 固定為 0，若多指觸控，則依據 datasheet 回報第一指 ID 為 0，第二指 ID 為 1，依此類推。

以 GT911 為例，依據 "GT911編程指南文件" 的 "三、寄存器列表"，從 BUFFER 可解析輸出觸點個數，X/Y 座標、ID 資訊(POINT 1 為 ID 0)。TOUCH-UP/TOUCH-DOWN 的判讀方式依 TP  而定，以GT911 的單指觸控為例，若觸點個數為 0，則為 TOUCH-UP，若 >=1，則為 TOUCH-DOWN。

實作 ``_tpParseKey_vendor()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

實作目的：從 ``_tpReadPointBuffer_vendor()`` 接收 buffer，並分析 buffer 的 button 資訊，轉換成 gTpKeypadValue 格式。若 TP 本身不含 button 功能，則不需實作此 function。

gTpKeypadValue 格式定義如下：

- gTpKeypadValue 為 unsigned int 的 global 變數(32-Bits)。
- key0 is pressed if gTpKeypadValue's bit 0 is 1。
- key1 is pressed if gTpKeypadValue's bit 1 is 1。
- NO key event if gTpKeypadValue = 0 。
- MAX key number: 32 keys。

若 button 資訊與 point 資訊不在原生的 point buffer 中，請在 ``_tpReadPointBuffer_vendor()`` 中，自行應用 S/W 技巧，將 button 資訊帶入 point buffer 中(EX: ``CYTMA568`` )。

關於支援 Touch Panel 上按鍵功能的 KCONFIG 設定方式，可參考說明文件 ``<sdk_root>/project/test_touch/test_touch.docx`` 之 5-b) 。

GT911 雖然在 "GT911編程指南文件" 中有提到 button 的資訊，但沒有在 driver 中實作。關於此 function 的實作範例，可參考 ``<sdk_root>/sdk/share/tslib/plugins/cytma568-raw.c`` 。

目前 TP 附帶的 button 功能，大致可分成兩種形式：

1. Button register (EX： ``CYTMA568`` )

  可從 TP register 直接讀出哪些 button 被按壓。
 
  .. image:: /_static/reg_map_touch_panel_button_reports.png

  
2. Part of TP x/y coordination (EX： ``CYTMA448`` )

按壓那個 button 的資訊判讀來自本身 TP 座標的延伸，如下圖所示，紅框 + 黃框為整個 TP 觸控範圍，紅框定義為傳統觸控範圍，黃框定義為 button 範圍(依座標判讀按壓的 button)。
 
  .. image:: /_static/e_circuit_touch_panel_frame.png

除錯流程
---------------------------

INT
^^^^^^^^^^^^^^^^^^^

查看是否進入 ``_tpReadPointBuffer_vendor()`` 。

- INT ACTIVE STATE 設錯。
- INT TRIGGER TYPE 設錯。
- INT TYPE 設錯。
- INT GPIO 設錯。

READ POINT (假設 SERIES PORT 是 I2C)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- I2C 是否 ACK。

  - I2C SLAVE ADDRESS 錯誤。
  - I2C GPIO PIN 設錯。

- I2C 是否可以讀取 REGISTER 資料。

  - DATA 都是 0 或 0XFF。
  - REGISTER ADDRESS 不對。
  - 指令不對。 
  - ADDRESS 格式不對。
  - 流程不對。

- 所讀資料是否與 DATASHEET 的格式一致。

  - 觸碰面板的四個角落，觀察所讀資料是否有變化，資料有變化的位置與格式是否相符。

PARSE X/Y
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- TOUCH-DOWN/TOUCH-UP。

  - 手指觸碰時，是否可立刻報點。
  - 手指一離開面板時，是否立刻停止報點。
  - 如果暫存器有 EVENT 資訊，DOWN/UP 的 EVENT 資訊是否準確，如果暫存器沒有 EVENT 資訊，是否有其他方式可以判別 DOWN/UP，有的 TP 可以從 INT state 判斷，有的可以從暫存器的 finger 數來判斷。

- X 座標與 Y 座標
  
  - 確認原生座標最大值是否與預期相同 (EX：X_MAX=800，讀到的 X 值是否接近 799) 。
  - 水平方向是否為 X 的 DATA，垂直方向是否為 Y 的 DATA。
  - 面板中間的座標值是否接近(X_MAX/2, Y_MAX/2) 。

- 座標方向

  - 確認左上、右上、左下、右下座標是否接近(0, 0) (X_MAX, 0) (0, Y_MAX) (X_MAX, Y_MAX) 。

- 多指觸控

  - FINGERS

    手指數目是否正確。任意多指按壓移除，最後留在 TP 上的手指數與 REGISTER 回報的手指數是否相同。

  - FINGER ID
    
    每個觸控點都有一個 finger ID，要仔細觀察 ID 與相對應的 X/Y 座標是否一致，例如順序是否一致(先按第一點為 ID = 0，後按的第二點為 ID = 1，當第一點 TOUCH-UP 時，第二點的是否仍維持 ID = 1)，移動的點是否一致(先按第一點為 ID = 0，後按的第二點為 ID = 1，第一點移動，第二點不動，是否只有 ID = 0 的座標有改變，ID = 1 的座標維持原位)。

Report 至 SDL
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- ``SDL_PollEvent()`` 所得之 X/Y 座標是否正確。
- 座標方向是否正確。
- SLIDE EVENT 的方向是否正確。
