.. _win32_uart_simulation:

Win32 Uart 模擬
=============================

功能描述
-------------------------

可透過此提供的 API，當用 Win32 模擬開發時，可以在 Win32 PC 端模擬對下控產品做 Uart 操作，即為可在 Win32 模擬開發的階段，就能開發 ITE 板端對下控產品之間的 Uart 溝通。

相關程式碼檔案
-------------------------

.. flat-table::
    :header-rows: 1

    * - 路徑
      - 描述
    * - ``sdk/driver/itp/itp_uart_api.c``
      - 主要 API 函式檔案，提供對 Uart 的操作。
 
相關 API 說明
-------------------------

ComInit
^^^^^^^^^^^^^^^^

.. c:function:: int ComInit(int ComNum, int Baudrate)

  Win32 上 Serial Port 的初始化。

**參數**

``ComNum``

  下控產品和 Win32 相連接的是第幾個 ComPort。ex: COM13，ComNum=13。

``Baudrate``

  Baudrate 即為 Uart 用的傳輸速率。

**回傳值**

0 為成功，-1 為失敗。
 
ComExit
^^^^^^^^^^^^^^^^

.. c:function:: void ComExit(int ComNum)

Win32 上關閉 Serial Port 的操作。

**參數**

``ComNum``

  下控產品和 Win32 相連接的是第幾個 ComPort。ex: COM13，ComNum=13。

 
UartReceive
^^^^^^^^^^^^^^^^

.. c:function:: int UartReceive(int ComNum, char *str, int len)

等於原先 Uart 中 ``read()``的功能一樣。

**參數**

``ComNum``

  在 Win32 上，ComNum 同上；在 openrtos 上，為 device id (ex: ``ITP_DEVICE_UART0`` )。

``str``

  欲讀取的字串。

``len``

  欲讀取的字串的長度。

**回傳值**

若有，則回傳 str 長度，否則為 0。
 
UartSend
^^^^^^^^^^^^^^^^

.. c:function:: int UartSend(int ComNum, char *str, int len)

等於原先 Uart 中 ``write()`` 的功能一樣。

**參數**

``ComNum``

  在 Win32 上，ComNum 同上；在 openrtos 上，為 device id (ex: ``ITP_DEVICE_UART0`` )。

``str``

  欲寫入的字串。

``len``

  欲寫入的字串的長度。

**回傳值**
  
寫入字串的長度。
 
注意事項
-------------------------

在 Win32 模擬開發與實際在 openrots 開發上，使用 Win32 Uart API 時，應注意：

- 在 Win32 上模擬開發流程

  1. 先呼叫 ``ComInit()`` 初始化。
  2. 在需要發送或接收的地方分別呼叫 ``UartSend()`` 或 ``UartReceive()``，``UartReceive()`` 函數回傳的長度需要確認 buffer 是否有資料。
  3. 不需要使用時，再呼叫 ``ComExit()``。

- 若轉移到在 openrots 上開發

  1. 依然可使用 Uart 原先 driver 底層的 ioctl (ex: ``read``、``write`` )。
  2. 若要繼續使用 Win32 Uart API，須將 ``ComInit()`` 及 ``ComExit()`` 用 Win32 的 compiler flag 隔開，而 ``UartReceive()`` 及 ``UartSend()`` 的 ComNum 參數要修改為Uart Device id (ex: ``ITP_DEVICE_UART1`` )。
