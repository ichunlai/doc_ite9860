﻿.. ITE9860 SDK Documentation master file, created by
   sphinx-quickstart on Wed Feb  5 12:06:50 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=================================
ITE SoC IT986x系列軟體開發指南
=================================

.. toctree::
  :maxdepth: 3
  :caption: 目錄

  quickstart.rst
  sdk_architecture.rst
  build_option.rst
  tool.rst
  driver.rst
  third_party.rst
  debug.rst
  upgrade.rst
  touch_panel_develop.rst
  win32_uart_simulation.rst
  faq.rst

.. only:: html

  .. toctree::

    appendix.rst

.. Indices and tables
.. ********************************

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
